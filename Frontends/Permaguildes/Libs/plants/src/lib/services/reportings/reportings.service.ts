import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NGXLogger } from 'ngx-logger';
import { catchError, mapTo } from 'rxjs/operators';
import { ApisHttpClientExtended } from 'apis-helpers';

@Injectable({
  providedIn: 'root'
})
export class ReportingsService {

  constructor(private http: ApisHttpClientExtended,
    private logger: NGXLogger,
    public dialog: MatDialog) {
  }

  sendReporting(name: string, email: string, message: string, codePlant: number): Observable<boolean> {
    return this.http.postToPlantsApi('/reportings', { name: name, email: email, message: message, codePlant: codePlant })
      .pipe(
        mapTo(true),
        catchError(error => {
          return of(false);
        }));
  }
}
