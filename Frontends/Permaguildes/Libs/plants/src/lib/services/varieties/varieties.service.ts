import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { GetterRelation1N } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider, ApisHttpClientExtended } from 'apis-helpers';
import { Plant } from '../../models/plant.model';
import { Variety } from '../../models/variety.model';
import { lastValueFrom, map, Observable, switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VarietiesService extends GetterRelation1N<Plant, number, Variety, number> {
  getRelationsFromEntity(data: Plant): Variety[] {
    return data.varieties;
  }
  setRelationsFromEntity(data: Plant, relations: Variety[]) {
    data.varieties = relations;
  }
  getReferenceKey(relation: Variety): number {
    return relation.codePlant;
  }

  constructor(protected http: ApisHttpClientExtended,
    logger: NGXLogger,
    config: ApisConfigurationProvider) { 
      super(http, logger, null, null, 
        config.getApiUrl(config.params.plantsApi, 'plants/{referenceKey}/varieties'), 
        null, Variety);
    }
}
