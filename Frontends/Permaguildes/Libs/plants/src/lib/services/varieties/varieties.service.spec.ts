import { ApisTest } from 'apis-helpers';
import { VarietiesService } from './varieties.service';

describe(VarietiesService.name, () => {
  const test = new ApisTest<VarietiesService>();

  test.initXMLHttpRequestTestingController();

  test.initService(VarietiesService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });
});
