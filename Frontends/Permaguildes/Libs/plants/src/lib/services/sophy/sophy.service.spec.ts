import { ApisTest } from 'apis-helpers';
import { SophyService } from './sophy.service';

describe(SophyService.name, () => {
  const test = new ApisTest<SophyService>();
  test.initService(SophyService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });
});
