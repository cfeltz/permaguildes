import { Injectable } from '@angular/core';
import { ApisHttpClientExtended } from 'apis-helpers';
import { Sophy } from '../../models/sophy.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

@Injectable({
  providedIn: 'root'
})
export class SophyService {

  constructor(protected http: ApisHttpClientExtended) { }

  public getSophy(code: number): Observable<Sophy> {
    const url = '/Sophy/' + code;
    return this.http.getFromPlantsApi(url).pipe(
      map((jsonItem: Object) => createApiMessageInstance(Sophy).loadFromJson(jsonItem)));
  }

  public getUrlFromCode(code: number) {
    return `http://sophy.tela-botanica.org/PSHTM/PS${code}.htm`;
  }

  public getUrlImageFromCode(code: number) {
    return `http://sophy.tela-botanica.org/PSHTM/images/P${code}.jpg`;
  }
}
