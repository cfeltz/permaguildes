import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import { LabelsService, LabelType } from './labels.service';
import familiesData from 'Libs/plants/src/lib/data/labels/families.json';
import insectsData from 'Libs/plants/src/lib/data/labels/insects.json';
import sexualCharacteristicsData from 'Libs/plants/src/lib/data/labels/sexualCharacteristics.json';
import stratumsData from 'Libs/plants/src/lib/data/labels/stratums.json';
import vegetationTypesData from 'Libs/plants/src/lib/data/labels/vegetationTypes.json';
import { ApisTest } from 'apis-helpers';

describe(LabelsService.name, () => {
  const test = new ApisTest<LabelsService>();
  test.initService(LabelsService, {
    beforeEach: () => {
      test.initConfig();
      test.initHttpTestingController();
    }
  });

  it('get family', waitForAsync(() => {
    test.component['_loadLabelsByType'](LabelType.Family, () => {
      const family = test.component.getFamilyLabel(10);
      expect(family).toEqual('Anacardiaceae');
    });
    test.http.expectOne(test.config.params.plantsApi + '/api/Common/Families').flush(familiesData);
  }));

  it('get insects', waitForAsync(() => {
    test.component['_loadLabelsByType'](LabelType.Insects, () => {
      const insect = test.component.getInsectLabel(10);
      expect(insect).toEqual('Phytoptes (acariens)');
    });
    test.http.expectOne(test.config.params.plantsApi + '/api/Common/Insects').flush(insectsData);

    test.component['_loadLabelsByType'](LabelType.Insects, () => {
      const insects = test.component.getInsectsLabels([2, 4, 6]);
      expect(insects).toEqual('Araignées rouges (acariens), Chenilles, Doryphores');
    });
  }));

  it('get sexual characteristic', waitForAsync(() => {
    test.component['_loadLabelsByType'](LabelType.SexualCharacteristic, () => {
      const sexualCharacteristic = test.component.getSexualCharacteristicLabel(3);
      expect(sexualCharacteristic).toEqual('Dioïques');
    });
    test.http.expectOne(test.config.params.plantsApi + '/api/Common/SexualCharacteristics').flush(sexualCharacteristicsData);
  }));

  it('get stratum', waitForAsync(() => {
    test.component['_loadLabelsByType'](LabelType.Stratum, () => {
      const stratum = test.component.getStratumLabel(7);
      expect(stratum).toEqual('Plantes grimpantes');
    });
    test.http.expectOne(test.config.params.plantsApi + '/api/Common/Stratums').flush(stratumsData);
  }));

  it('get vegetations types', waitForAsync(() => {
    test.component['_loadLabelsByType'](LabelType.VegetationTypes, () => {
      const stratum = test.component.getVegetationTypeLabel(5);
      expect(stratum).toEqual('Persistant');
    });
    test.http.expectOne(test.config.params.plantsApi + '/api/Common/VegetationTypes').flush(vegetationTypesData);

    test.component['_loadLabelsByType'](LabelType.VegetationTypes, () => {
      const stratum = test.component.getVegetationTypesLabels([1, 2, 3]);
      expect(stratum).toEqual('Annuelle, Biannuelle, Caduc');
    });
  }));
});
