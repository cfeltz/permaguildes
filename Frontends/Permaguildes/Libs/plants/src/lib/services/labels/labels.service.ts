import { Injectable, NgModule } from '@angular/core';
import { ApisHttpClientExtended } from 'apis-helpers';
import { String } from 'typescript-string-operations';

export enum LabelType {
  Family = <any>'Famille',
  Insects = <any>'Insectes',
  SexualCharacteristic = <any>'Caractéristique sexuelle',
  Stratum = <any>'Strate',
  VegetationTypes = <any>'Types de végétation',
  Auxiliaries = <any>'Auxiliaires',
  Repellents = <any>'Répulsifs'
}

@Injectable({
  providedIn: 'root'
})
export class LabelsService {
  static _labelsLoaded = false;
  static _families: {[key: number]: string} = {};
  static _insects: {[key: number]: string} = {};
  static _auxiliaries: {[key: number]: string} = {};
  static _repellents: {[key: number]: string} = {};
  static _sexualCharacteristics: {[key: number]: string} = {};
  static _stratums: {[key: number]: string} = {};
  static _vegetationTypes: {[key: number]: string} = {};
  readonly _monthNames = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];

  constructor(protected http: ApisHttpClientExtended) { }
  private _loadLabelsByType(type: LabelType | any, callback?: () => void): void {
    let ref: {[key: number]: string};
    let url = '/Common/{0}';
    //#region Set ref table and url
    switch (type) {
      case LabelType.Family:
          ref = LabelsService._families;
          url = String.Format(url, 'Families');
          break;
      case LabelType.Insects:
          ref = LabelsService._insects;
          url = String.Format(url, 'Insects');
          break;
      case LabelType.SexualCharacteristic:
          ref = LabelsService._sexualCharacteristics;
          url = String.Format(url, 'SexualCharacteristics');
          break;
      case LabelType.Stratum:
          ref = LabelsService._stratums;
          url = String.Format(url, 'Stratums');
          break;
      case LabelType.VegetationTypes:
          ref = LabelsService._vegetationTypes;
          url = String.Format(url, 'VegetationTypes');
          break;
      default:
          console.error('Unknown label: ' + type);
    }
    //#endregion

    if (Object.keys(ref).length === 0) {
      const requestResult = this.http.getFromPlantsApi<any[]>(url);

      requestResult.subscribe(
        result => {
          result.forEach((item) => {
            ref[item.code] = item.libelle;
            if (ref === LabelsService._insects) {
              if (item.auxiliary) {
                LabelsService._auxiliaries[item.code] = item.libelle;
              } else {
                LabelsService._repellents[item.code] = item.libelle;
              }
            }
          });
          console.log(type.toString() + ' loaded');
          if (callback) {
            callback();
          }
        },
        error => console.error('Failed to load ' + type.toString() + ' labels')
      );
    } else {
      if (callback) {
        callback();
      }
    }
  }

  public loadLabels(): void {
    if (!LabelsService._labelsLoaded) {
      this._loadLabelsByType(LabelType.Family);
      this._loadLabelsByType(LabelType.Insects);
      this._loadLabelsByType(LabelType.SexualCharacteristic);
      this._loadLabelsByType(LabelType.Stratum);
      this._loadLabelsByType(LabelType.VegetationTypes);
      LabelsService._labelsLoaded = true;
    }
  }

  public getFamilies(): {[key: number]: string} {
    return LabelsService._families;
  }
  public getInsects(): {[key: number]: string} {
    return LabelsService._insects;
  }
  public getAuxiliaries(): {[key: number]: string} {
    return LabelsService._auxiliaries;
  }
  public getRepellents(): {[key: number]: string} {
    return LabelsService._repellents;
  }
  public getSexualCharacteristics(): {[key: number]: string} {
    return LabelsService._sexualCharacteristics;
  }
  public getStratums(): {[key: number]: string} {
    return LabelsService._stratums;
  }
  public getVegetationTypes(): {[key: number]: string} {
    return LabelsService._vegetationTypes;
  }

  public getFamilyLabel(code: number): string {
    return LabelsService._families[code];
  }
  public getInsectLabel(code: number): string {
    return LabelsService._insects[code];
  }
  public getInsectsLabels(codes: number[]): string {
    let returnValue = '';
    let separator = '';
    if (codes != null) {
      codes.forEach(code => {
        returnValue += separator + LabelsService._insects[code];
        separator = ', ';
      });
    }
    return returnValue;
  }
  public getSexualCharacteristicLabel(code: number): string {
    return LabelsService._sexualCharacteristics[code];
  }
  public getStratumLabel(code: number): string {
    return LabelsService._stratums[code];
  }
  public getVegetationTypeLabel(code: number): string {
    return LabelsService._vegetationTypes[code];
  }
  public getVegetationTypesLabels(codes: number[]): string {
    let returnValue = '';
    let separator = '';
    if (codes != null) {
      codes.forEach(code => {
        returnValue += separator + LabelsService._vegetationTypes[code];
        separator = ', ';
      });
    }
    return returnValue;
  }

  public getMonthLabel(code: number): string {
    return this._monthNames[code - 1];
  
  }
}
