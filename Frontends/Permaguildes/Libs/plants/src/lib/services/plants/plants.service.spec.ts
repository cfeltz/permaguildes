import { PlantsService } from './plants.service';
import { waitForAsync } from '@angular/core/testing';

import plantsData from 'Libs/plants/src/lib/data/plants2.json';
import amandierData from 'Libs/plants/src/lib/data/amandier.json';
import { ApisTest } from 'apis-helpers';
import { Plant } from '../../models/plant.model';

describe(PlantsService.name, () => {
  const test = new ApisTest<PlantsService>();
  test.initService(PlantsService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });

  it('load plants', waitForAsync(() => {
    const plants$ = test.component.getPlants();
    const subscribe = plants$.subscribe((plants: Plant[]) => {
      expect(plants.length).toBeGreaterThan(0);
      subscribe.unsubscribe();
    });
    test.http.expectOne(req => true).flush(plantsData);
  }));

  it('load specific plants', waitForAsync(() => {
    const codes: number[] = [10, 12, 13, 14, 15, 16, 17, 18, 19, 30];
    const plants$ = test.component.getPlants(codes);
    const subscribe = plants$.subscribe((plants: Plant[]) => {
      expect(plants.length).toEqual(10);
      expect(plants.filter(v => codes.indexOf(v.code) >= 0).length).toEqual(10);
      subscribe.unsubscribe();
    });
    test.http.expectOne(req => true).flush(plantsData);
  }));

  it('load plant', waitForAsync(() => {
    const amandier$ = test.component.getPlant(9);
    const subscribe = amandier$.subscribe((amandier: Plant) => {
      expect(amandier.code).toEqual(9);
      subscribe.unsubscribe();
    });
    test.http.expectOne(req => true).flush(amandierData);
  }));

  it('load plant from local list', waitForAsync(() => {
    PlantsService.plants = undefined;
    let amandier$ = test.component.getPlantFromLocalList(9);

    // List not loaded
    let subscribe = amandier$.subscribe((amandier: Plant) => {
      expect(amandier.code).toEqual(9);
      expect(amandier instanceof Plant).toBeTruthy();
      subscribe.unsubscribe();

      // Update local data
      amandier.description = 'Test';

      // List already loaded
      amandier$ = test.component.getPlantFromLocalList(9);
      subscribe = amandier$.subscribe((amandier2: Plant) => {
        expect(amandier.code).toEqual(9);
        expect(amandier.description).toEqual('Test');
        expect(amandier instanceof Plant).toBeTruthy();
        subscribe.unsubscribe();
      });
    });

    // Load list
    test.http.expectOne(req => true).flush(plantsData);
  }));

  it('update plant', waitForAsync(() => {
    const amandier$ = test.component.getPlant(9);
    const subscribe = amandier$.subscribe(async (amandier: Plant) => {
      expect(amandier.code).toEqual(9);
      subscribe.unsubscribe();

      amandier.description = 'Test';
      test.component.updatePlant(amandier).then((amandierUpdated) => {
        expect(amandierUpdated.description).toEqual('Test');
      });

      // Expect server POST request
      test.http.expectOne(req => req.method === 'POST', 'Post request to update plant').flush(amandier);
    });
    test.http.expectOne(req => true).flush(amandierData);
  }));
});
