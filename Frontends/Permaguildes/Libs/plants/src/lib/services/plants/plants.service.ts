import { Injectable } from '@angular/core';
import { createApiMessageInstance, createSubjectWithResult, Getter, HubsManagmentService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider, ApisHttpClientExtended } from 'apis-helpers';
import { NGXLogger } from 'ngx-logger';
import { lastValueFrom, Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Companion } from '../../models/companion.model';
import { Plant } from '../../models/plant.model';
import { Rootstock } from '../../models/rootstock.model';
import { Variety } from '../../models/variety.model';
import { RootstocksService } from '../rootstocks/rootstocks.service';
import { VarietiesService } from '../varieties/varieties.service';

@Injectable({
  providedIn: 'root'
})
export class PlantsService extends Getter<Plant, number> {
  static plants: Plant[];
  static defaultIconsSrc: string[];
  // static defaultIconsSrc$: Observable<any>[] = [];

  constructor(protected http: ApisHttpClientExtended,
    logger: NGXLogger,
    public config: ApisConfigurationProvider,
    public varietiesService: VarietiesService, 
    public rootstocksService: RootstocksService, 
    hubsManagment: HubsManagmentService) { 
      super(http, logger, hubsManagment, 
        config.getApiUrl(config.params.plantsApi, 'plants'), 
        config.getApiHubUrl(config.params.plantsApi), 
        Plant, 'Plant');
      PlantsService.defaultIconsSrc = [];
      PlantsService.defaultIconsSrc[1] = 'T1.png';
      PlantsService.defaultIconsSrc[2] = 'T1.png';
      PlantsService.defaultIconsSrc[3] = 'T1.png';
      PlantsService.defaultIconsSrc[4] = 'G1.png';
      PlantsService.defaultIconsSrc[5] = 'G1.png';
      PlantsService.defaultIconsSrc[6] = 'G1.png';
      PlantsService.defaultIconsSrc[7] = 'C1.png';
    
    }

  public getIconSrc(iconSrc: string, codeStratum: number): string {
    if (iconSrc) {
      return iconSrc;
    } else {
      return PlantsService.defaultIconsSrc[codeStratum];
    }
  }

  public getPlants(codes?: number[]): Observable<Plant[]> {
    const obsResult = this.http.getFromPlantsApi('/Plants/GetPlants');

    if (codes) {
      return obsResult.pipe(
        map((jsonArray: Object[]) => jsonArray.filter(jsonItem => codes.indexOf(jsonItem['code']) >= 0)),
        map((jsonArray: Object[]) => {
          const returnValue = jsonArray.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));
          PlantsService.plants = returnValue;
          return returnValue;
        })
      );
    } else {
      return obsResult.pipe(
        map((jsonArray: Object[]) => {
          const returnValue = jsonArray.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));
          PlantsService.plants = returnValue;
          return returnValue;
        })
      );
    }
  }

  public getPlantsNames(): Observable<Object> {
    const result = this.http.getFromPlantsApi('/Plants/GetPlantsNames');
    return result.pipe(
      map((jsonArray: Object[]) => jsonArray.map(jsonItem => {
        return { 'key': jsonItem['key'], 'value': jsonItem['value'] };
      }))
    );
  }

  public getPlant(code: number): Observable<Plant> {
    const url = '/Plants/LoadPlant?codePlant=' + code;
    const obsResult$ = this.http.getFromPlantsApi(url).pipe(
      map((jsonItem: Object) => {
        const plant = createApiMessageInstance(Plant).loadFromJson(jsonItem);

        this.loadCompanions(code);
        this.loadVarieties(code);
        this.loadRootstocks(code);

        return plant;
      }),
      createSubjectWithResult<Plant>((result) => {
        this.setSingleDataInRealtimeCache('getPlant-' + code, result);
      }),
      catchError(error => {
        console.error('Failed to load plant, use plant from list');
        return this.getPlantFromLocalList$(code);
      })
    );

    return obsResult$;
  }

  private getUrl(path: string) {
    return this.http.getUrl(this.config.params.plantsApi, path);
  }

  public loadCompanions(codePlant: number) {
    const urlCompanions = this.getUrl('/Plants/LoadPlantCompanions?codePlant=' + codePlant);
    this.http.getMultipleData(Companion, urlCompanions).pipe(
      tap((companions: Companion[]) => {
        this.getDataForUpdate(codePlant, (data) => {
          data.companions = companions;
        });
        this.refreshData();
      })
    ).subscribe();
  }

  public loadVarieties(codePlant: number) {
    this.varietiesService.getMultipleDataByReferenceKey(codePlant).pipe(
      tap((varieties: Variety[]) => {
        this.getDataForUpdate(codePlant, (data) => {
          data.varieties = varieties;
        });
        this.refreshData();
      })
    ).subscribe();
  }

  public loadRootstocks(codePlant: number) {
    this.rootstocksService.getMultipleDataByReferenceKey(codePlant).pipe(
      tap((rootstocks: Rootstock[]) => {
        this.getDataForUpdate(codePlant, (data) => {
          data.rootstocks = rootstocks;
        });
        this.refreshData();
      })
    ).subscribe();
  }

  public getPlantFromLocalList$(code: number): Observable<Plant> {
    if (!PlantsService.plants) {
      return this.getPlants().pipe(
        map((jsonArray: Plant[]) => jsonArray.find(v => v.code === code))
      );
    } else {
      const plant = this.getPlantFromLocalList(code);
      const returnValue = of<Plant>(plant);
      return returnValue;
    }
  }

  public getPlantFromLocalList(code: number): Plant {
    if (PlantsService.plants) {
      const plant = PlantsService.plants.find(v => v.code === code);
      return plant;
    }
  }

  public async createPlant(plant: Plant): Promise<Plant> {
    const url = '/Plants/CreatePlant';
    return lastValueFrom(this.http.postToPlantsApi<Plant>(url, plant));
  }

  public async updatePlant(plant: Plant): Promise<Plant> {
    const url = '/Plants/UpdatePlant';
    return lastValueFrom(this.http.postToPlantsApi<Plant>(url, plant));
  }
}
