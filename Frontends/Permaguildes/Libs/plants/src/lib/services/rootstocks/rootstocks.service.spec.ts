import { ApisTest } from 'apis-helpers';
import { RootstocksService } from './rootstocks.service';

describe(RootstocksService.name, () => {
  const test = new ApisTest<RootstocksService>();

  test.initXMLHttpRequestTestingController();

  test.initService(RootstocksService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });
});
