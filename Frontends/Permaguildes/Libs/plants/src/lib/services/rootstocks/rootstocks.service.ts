import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { GetterRelation1N } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider, ApisHttpClientExtended } from 'apis-helpers';
import { Rootstock } from '../../models/rootstock.model';
import { Plant } from '../../models/plant.model';

@Injectable({
  providedIn: 'root'
})
export class RootstocksService extends GetterRelation1N<Plant, number, Rootstock, number> {

  getRelationsFromEntity(data: Plant): Rootstock[] {
    return data.rootstocks;
  }
  setRelationsFromEntity(data: Plant, relations: Rootstock[]) {
    data.rootstocks = relations;
  }
  getReferenceKey(relation: Rootstock): number {
    return relation.codePlant;
  }

  constructor(protected http: ApisHttpClientExtended,
    logger: NGXLogger,
    config: ApisConfigurationProvider) { 
      super(http, logger, null, null, 
        config.getApiUrl(config.params.plantsApi, 'plants/{referenceKey}/rootstocks'), 
        null, Rootstock);
    }
}
