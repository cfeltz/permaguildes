import { Entity } from "@cfeltz/angular-helpers";

export class Companion extends Entity {
  public plant;

  constructor(
    public codePlant: number,
    public fidelity: number,
    public frequency: number,
    public pdcum: number,
    public gap: number,
    public name: string,
    public latinNames: string,
    public codeSophy: number,
    public sophyName: string,
    public comment: string,
    public typeMessage: string) {
      super();
    }

    public get isDiscriminantPlant(): boolean {
      return (this.fidelity != undefined || this.frequency != undefined || this.pdcum != undefined);
    }

    public get isSimilarPlant(): boolean {
      return (this.gap != undefined);
    }
}
