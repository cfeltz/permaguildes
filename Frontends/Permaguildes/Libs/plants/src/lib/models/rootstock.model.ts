import { EntityWithKey } from "@cfeltz/angular-helpers";

export class Rootstock extends EntityWithKey<number> {
  constructor(
    public code?: number,
    public codePlant?: number,
    public name?: string,
    public minHeight?: number,
    public maxHeight?: number,
    public minSpan?: number,
    public maxSpan?: number,
    public description?: string,
    public typeMessage?: string) {
    super();
  }

  public GetKey(): number {
    return this.code;
  }

  public GetTextValue(): string {
    return this.name;
  }

  public GetShortName(): string {
    const idx = this.name.indexOf(' (');
    if (idx >= 0) {
      return this.name.substr(0, idx); 
    } else {
      return this.name;
    }
  }
}
