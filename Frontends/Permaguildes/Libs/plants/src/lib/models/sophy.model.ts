import { Entity } from "@cfeltz/angular-helpers";

export class Sophy extends Entity {

  constructor(
    public code: number,
    public sophyName: string,
    public obs: number,
    public loc: number,
    public dis: number,
    public qdr: number,
    public coc: number,
    public url: string,
    public typeMessage: string
  ) {
    super();
  }
}
