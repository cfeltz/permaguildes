import { EventEmitter } from '@angular/core';
import { createApiMessageInstance, EntityWithKey } from '@cfeltz/angular-helpers';
import { SearchProperties } from '../enums/searchProperties.enum';
import { Companion } from './companion.model';
import { Rootstock } from './rootstock.model';
import { Variety } from './variety.model';

export class Plant extends EntityWithKey<number> {
  sizeUnits: string;
  sizeUnitsChange = new EventEmitter<string>();

  public static getSizeUnitsByStrate(codeStratum: number): string {
    if ([1, 2, 3, 7].indexOf(codeStratum) >= 0) {
      return 'm';
    } else {
      return 'cm';
    }
  }

  
  public GetKey(): number {
    return this.code;
  }

  constructor(public code: number,
    public name?: string,
    public codeFamily?: number,
    public latinNames?: string,
    private _codeStratum?: number,
    public codesVegetationTypes?: number[],
    public codeSex?: number,
    public water?: number,
    public growth?: number,
    public edibilityRating?: number,
    public medicinalRating?: number,
    public minUsdahardiness: number = 1,
    public maxUsdahardiness: number = 1,
    public minHeight?: number,
    public maxHeight?: number,
    public minSpan?: number,
    public maxSpan?: number,
    public floweringPeriods?: number[],
    public harvestPeriods?: number[],
    public exposureShadow?: boolean,
    public exposureMiddle?: boolean,
    public exposureSun?: boolean,
    public fertility?: boolean,
    public nitrate?: boolean,
    public othersMinerals?: boolean,
    public windbreaker?: boolean,
    public melliferous?: boolean,
    public aromaticPlant?: boolean,
    public auxiliariesAttractive?: boolean,
    public pestRepellents?: boolean,
    public melliferousRating?: number,
    public othersMineralsRating?: number,
    public codesAuxiliariesInsects?: number[],
    public codesRepellentsInsects?: number[],
    public description?: string,
    public externalLink?: string,
    public infosToKnow?: string,
    public pfafUrl?: string,
    public pfafKnownHazards?: string,
    public pfafDescription?: string,
    public remarks?: string,
    public varieties?: Variety[],
    public rootstocks?: Rootstock[],
    public iconCode?: number,
    public iconSrc?: string,
    public codeSophy?: number,
    public companions?: Companion[],
    public dtUpdate?: Date,
    public dtSuppression?: Date,
    public lighten?: boolean,
    public typeMessage?: string) {
    super();

    if (!this.floweringPeriods) {
      this.floweringPeriods = [];
    }
    if (!this.harvestPeriods) {
      this.harvestPeriods = [];
    }

    if (lighten) {
      this.pfafKnownHazards = 'Veuillez vous connecter au réseau pour charger les données';
      this.pfafDescription = 'Veuillez vous connecter au réseau pour charger les données';
      this.remarks = 'Veuillez vous connecter au réseau pour charger les données';
      this.infosToKnow = 'Veuillez vous connecter au réseau pour charger les données';
    }
  }

  public loadFromJson(json: object): this {
    super.loadFromJson(json);
    this._codeStratum = json['codeStratum'];
    this.setSizeUnits(false);
        
    if (json['companions']) {
      this.companions = [];
      json['companions'].forEach(companion => {
        this.companions.push(createApiMessageInstance(Companion).loadFromJson(companion));
      });
    }

    if (json['varieties']) {
      this.varieties = [];
      json['varieties'].forEach(r => {
        this.varieties.push(createApiMessageInstance(Variety).loadFromJson(r));
      });
    }

    if (json['rootstocks']) {
      this.rootstocks = [];
      json['rootstocks'].forEach(r => {
        this.rootstocks.push(createApiMessageInstance(Rootstock).loadFromJson(r));
      });
    }

    return this;
  }

  public get codeStratum(): number {
      return this._codeStratum;
  }
  public set codeStratum(value: number) {
      this._codeStratum = value;
      this.setSizeUnits();
  }

  private setSizeUnits(update = true) {
    this.sizeUnits = Plant.getSizeUnitsByStrate(this._codeStratum);
    if (this.sizeUnits == 'm') {
      if (update) {
        this.minHeight = 1;
        this.maxHeight = 1;
        this.minSpan = 1;
        this.maxSpan = 1;
        this.sizeUnitsChange.emit(this.sizeUnits);
      }
    } else {
      if (update) {
        this.minHeight = 5;
        this.maxHeight = 5;
        this.minSpan = 5;
        this.maxSpan = 5;
        this.sizeUnitsChange.emit(this.sizeUnits);
      }
    }
  }


  public checkProperty(property: SearchProperties): boolean {
    let plantMatchThisProperty = false;
    switch (property) {
        case SearchProperties.Tree: {
            plantMatchThisProperty = (this.codeStratum == 1 || this.codeStratum == 2);
            break;
          }
        case SearchProperties.Shrub: {
          plantMatchThisProperty = (this.codeStratum == 3);
            break;
          }
        case SearchProperties.Herb: {
          plantMatchThisProperty = (this.codeStratum == 4 || this.codeStratum == 5);
            break;
          }
        case SearchProperties.Climber: {
            plantMatchThisProperty = (this.codeStratum == 7);
            break;
          }
        case SearchProperties.Nitrate: {
            plantMatchThisProperty = this.nitrate;
            break;
          }
        case SearchProperties.Mineral: {
            plantMatchThisProperty = this.othersMinerals;
            break;
          }
        case SearchProperties.ShadowOnly: {
            plantMatchThisProperty = this.exposureShadow && ! this.exposureMiddle && ! this.exposureSun;
            break;
          }
        case SearchProperties.ShadowSupport: {
            plantMatchThisProperty = this.exposureShadow;
            break;
          }
        case SearchProperties.HalfShadow: {
            plantMatchThisProperty = this.exposureMiddle;
            break;
          }
        case SearchProperties.SunSupport: {
            plantMatchThisProperty = this.exposureSun;
            break;
          }
        case SearchProperties.SunNeed: {
            plantMatchThisProperty = this.exposureSun && ! this.exposureMiddle && ! this.exposureShadow;
            break;
          }
        case SearchProperties.Caduc: {
            plantMatchThisProperty = (this.codesVegetationTypes ? this.codesVegetationTypes.includes(3) : false);
            break;
          }
        case SearchProperties.SemiEvergreen: {
            plantMatchThisProperty = (this.codesVegetationTypes ? this.codesVegetationTypes.includes(4) : false);
            break;
          }
        case SearchProperties.Evergreen: {
            plantMatchThisProperty = (this.codesVegetationTypes ? this.codesVegetationTypes.includes(5) : false);
            break;
          }
        case SearchProperties.DryPlant: {
            plantMatchThisProperty = (this.water === 1);
            break;
          }
        case SearchProperties.NeedWater: {
            plantMatchThisProperty = (this.water === 3);
            break;
          }
        case SearchProperties.WaterPlant: {
            plantMatchThisProperty = false; // TODO: Water plant attribut does not exist
            break;
          }
        case SearchProperties.SlowGrowth: {
            plantMatchThisProperty = (this.growth === 1);
            break;
          }
        case SearchProperties.FastGrowth: {
            plantMatchThisProperty = (this.growth === 3);
            break;
          }
        case SearchProperties.Windbreak: {
            plantMatchThisProperty = this.windbreaker;
            break;
          }
        case SearchProperties.AttractingWildWife: {
            plantMatchThisProperty = this.auxiliariesAttractive;
            break;
          }
        case SearchProperties.InsectRepellent: {
            plantMatchThisProperty = this.pestRepellents;
            break;
          }
        case SearchProperties.GoodEdible: {
            plantMatchThisProperty = this.edibilityRating >= 3;
            break;
          }
        case SearchProperties.VeryGoodEdible: {
            plantMatchThisProperty = this.edibilityRating >= 4;
            break;
          }
        case SearchProperties.GoodMedicinal: {
            plantMatchThisProperty = this.medicinalRating >= 3;
            break;
          }
        case SearchProperties.VeryGoodMedicinal: {
            plantMatchThisProperty = this.medicinalRating >= 4;
            break;
          }
        default: {
            plantMatchThisProperty = false;
            break;
        }
    }
    return plantMatchThisProperty;
  }

}
