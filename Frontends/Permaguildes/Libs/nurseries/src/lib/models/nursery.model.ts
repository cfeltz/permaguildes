import { EntityWithKey } from "@cfeltz/angular-helpers";

export class Nursery extends EntityWithKey<number> {
  public GetKey(): number {
    return this.code;
  }
  
  public GetTextValue(): string {
    return this.name;
  }

  constructor(
    public code?: number,
	  public name?: string,
    public address?: string,
    public zipCode?: string,
    public city?: string,
	  public email?: string,
    public url?: string,
    public tel1?: string,
    public tel2?: string,
    public description?: string,
    public typeMessage?: string,
    public errorMessage?: string) {
      super();
  }

  public cloneForServer(): Nursery {
    return new Nursery(
      this.code,
      this.name,
      this.address,
      this.zipCode,
      this.city,
      this.email,
      this.url,
      this.tel1,
      this.tel2,
      this.description
    );
  }
}
