
import { waitForAsync } from '@angular/core/testing';
import { ApisTest } from 'apis-helpers';
import { Nursery } from '../../models/nursery.model';
import { NurseriesService } from './nurseries.service';

describe(NurseriesService.name, () => {
  const test = new ApisTest<NurseriesService>();

  test.initXMLHttpRequestTestingController();

  test.initService(NurseriesService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });

  it('get project data', waitForAsync(() => {
    const data$ = test.component.getAllData(true);
    const subscribe = data$.subscribe((data: Nursery[]) => {
      expect(data.length).toBeGreaterThan(0);
      subscribe.unsubscribe();
    });
    test.http.expectOne(req => true).flush([{ 'code': 1, 'name': 'test' }]);
  }));
});
