import { Injectable } from '@angular/core';
import { lastValueFrom, map, Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { Crud, HubsManagmentService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider, ApisHttpClientExtended } from 'apis-helpers';
import { Nursery } from '../../models/nursery.model';

@Injectable({
  providedIn: 'root'
})
export class NurseriesService extends Crud<Nursery, number> {
  constructor(protected http: ApisHttpClientExtended,
    logger: NGXLogger,
    config: ApisConfigurationProvider,
    hubsManagment: HubsManagmentService) { 
      super(http, logger, hubsManagment, 
        config.getApiUrl(config.params.nurseriesApi, 'nurseries'), 
        config.getApiHubUrl(config.params.nurseriesApi), 
        Nursery, 'Nursery');
  }

  public getNurseries(): Observable<Nursery[]> {
    return this.getAllData();
  }

  public getNursery(code: number): Observable<Nursery> {
    return this.getSingleDataByKey(code, true);
  }

  public async createNursery(nursery: Nursery): Promise<Nursery> {
    return lastValueFrom(this.createSingleData(nursery));
  }

  public async updateNursery(nursery: Nursery): Promise<Nursery> {
    return lastValueFrom(this.updateSingleData(nursery.cloneForServer()));
  }
}
