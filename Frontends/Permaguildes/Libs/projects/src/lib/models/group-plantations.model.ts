import { EntityWithKey } from "@cfeltz/angular-helpers";
import { Plantation } from "./plantation.model";

export class GroupPlantations extends EntityWithKey<number> {
  public GetKey(): number {
    return this.code;
  }

  public cloneForServer(): GroupPlantations {
    return new GroupPlantations(
        this.code,
        this.name,
        this.codeProject,
        this.description
    );
  }

  public plantations: Plantation[];

  constructor(
    public code?: number,
    public name?: string,
    public codeProject?: number,
    public description?: string,
    public typeMessage?: string,
    public errorMessage?: string) {
      super();
    }
}
