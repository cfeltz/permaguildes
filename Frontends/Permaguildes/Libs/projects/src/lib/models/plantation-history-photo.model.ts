import { EntityWithKey } from "@cfeltz/angular-helpers";

export class PlantationHistoryPhoto extends EntityWithKey<number> {
  public GetKey(): number {
    return this.code;
  }

  constructor(
    public code?: number,
	  public codeHistory?: number,
    public filename?: string,
    public url?: string,
    public typeMessage?: string,
    public errorMessage?: string) {
     super(); 
    }
}
