import { EntityWithKey } from "@cfeltz/angular-helpers";
import { Nursery } from "nurseries";
import { Plant, Rootstock, Variety } from "plants";
import { PlantationHistory } from "projects";

export class Plantation extends EntityWithKey<number> {
  public mapImage: any;
  public mapSpanCircle: any;
  public mapTrunkCircle: any;
  public mapLabel: any;
  public mapFullLabel: any;
  public plant: Plant;
  public varieties: Variety;
  public rootstock: Rootstock;
  public nursery: Nursery;

  public GetKey(): number {
    return this.code;
  }

  public getShortName() {
    let shortName = this.shortName ?? this.name;
    if (shortName.length == 0) {
      shortName = this.name;
    }
    const idxOpenParenthesis = shortName.indexOf('(');
    const idxCloseParenthesis = shortName.indexOf(')');
    if (idxOpenParenthesis > 0 && idxCloseParenthesis > 0) {
      shortName = shortName.substring(0, idxOpenParenthesis);
    }
    const idxSeparator = shortName.indexOf('/');
    if (idxSeparator > 0) {
      shortName = shortName.substring(0, idxSeparator);
    }
    return shortName;
  }

  constructor(
    public code?: number,
	  public name?: string,
    public shortName?: string,
    public codePlant?: number,
    public codeVariety?: number,
    public codeRootstock?: number,
    public codeGroup?: number,
    public codeProject?: number,
    public codeStratum?: number,
    public visible?: boolean,
    public latitude?: number,
    public longitude?: number,
    public height?: number,
    public span?: number,
    public iconSrc?: string,
    public codeNursery?: number,
    public plantingYear?: Date,
    public locationDescription?: string,
    public comment?: string,
    public history?: PlantationHistory[],
    public typeMessage?: string,
    public errorMessage?: string) {
     super(); 
    }

  public cloneForServer(): Plantation {
    return new Plantation(
      this.code,
      this.name,
      this.shortName,
      this.codePlant,
      this.codeVariety,
      this.codeRootstock,
      this.codeGroup,
      this.codeProject,
      this.codeStratum,
      this.visible,
      this.latitude,
      this.longitude,
      this.height,
      this.span,
      this.iconSrc,
      this.codeNursery, 
      this.plantingYear,
      this.locationDescription,
      this.comment
    );
  }

  public copy(): Plantation {
    const p = this.cloneForServer();
    p.code = 0;
    p.plant = this.plant;
    return p;
  }

  public static fromPlant(plant: Plant, latitude: number, longitude: number): Plantation {
    let height = 5;
    let span = 4;
    if (plant.minHeight && plant.maxHeight) {
      height = Math.ceil((plant.maxHeight + plant.minHeight) / 2);
    }
    if (plant.minSpan && plant.maxSpan) {
      span = Math.ceil((plant.maxSpan + plant.minSpan) / 2);
    } else if (plant.minHeight && plant.maxHeight) {
      span = height;
    }
    return new Plantation(
        0, plant.name, undefined, plant.code, undefined, undefined,
        undefined, undefined, plant.codeStratum,
        true, latitude, longitude,
        height, span,
        plant.iconSrc,
        undefined, undefined, undefined
    );
  }
}
