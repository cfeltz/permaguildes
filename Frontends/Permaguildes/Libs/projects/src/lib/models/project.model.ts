import { Plantation } from "./plantation.model";
import { GroupPlantations } from "./group-plantations.model";
import { createApiMessageInstance, EntityWithKey } from "@cfeltz/angular-helpers";
import { Shape } from "./shape.model";

export class Project extends EntityWithKey<number> {
  static DEFAULT_LATITUDE = 47;
  static DEFAULT_LONGITUDE = 3;
  static DEFAULT_ZOOM = 6;

  public havePlantationInGroups: boolean = false;

  public GetKey(): number {
    return this.code;
  }

  static newProject(name: string, description: string): Project {
    const project = new Project(0, 
      name, 
      this.DEFAULT_LATITUDE, 
      this.DEFAULT_LONGITUDE, 
      this.DEFAULT_ZOOM, 
      false,
      description);
    return project;
  }

  constructor(
    public code?: number,
	  public name?: string,
    public latitude?: number,
    public longitude?: number,
    public zoom?: number,
    public positionLocked?: boolean,
	  public description?: string,
    public freezePlantations?: boolean,
    public autoDrawPlantations?: boolean,
    public drawPlantations?: boolean,
    public showPlantationNames?: boolean,
    public showPlantationVarieties?: boolean,
    public typeMessage?: string,
    public errorMessage?: string,
    public groups?: GroupPlantations[],
	  public plantations?: Plantation[],
    public shapes?: Shape[]) {
      super();
  }

  public cloneForServer(): Project {
    return new Project(
      this.code,
      this.name,
      this.latitude,
      this.longitude,
      this.zoom,
      this.positionLocked,
      this.description,
      this.freezePlantations,
      this.autoDrawPlantations,
      this.drawPlantations,
      this.showPlantationNames,
      this.showPlantationVarieties
    );
  }

  public loadFromJson(json: object): this {
    super.loadFromJson(json);
    
    if (json['groups']) {
      this.groups = [];
      json['groups'].forEach(r => {
        this.groups.push(createApiMessageInstance(GroupPlantations).loadFromJson(r));
      });
    }

    if (json['plantations']) {
      this.plantations = [];
      json['plantations'].forEach(r => {
        this.plantations.push(createApiMessageInstance(Plantation).loadFromJson(r));
      });
    }

    if (json['shapes']) {
      this.shapes = [];
      json['shapes'].forEach(r => {
        this.shapes.push(createApiMessageInstance(Shape).loadFromJson(r));
      });
    }

    this.fillPlantationsInGroups();

    return this;
  }

  private fillPlantationsInGroups() {
    // Add plantations array on groups
    if (this.groups !== undefined && this.groups !== null) {
      this.groups?.forEach(group => {
        group.plantations = [];
      });
      this.plantations?.forEach(plantation => {
        if (plantation.codeGroup != undefined) {
          // Get group and add plantation
          const group = this.groups.find(g => g.code == plantation.codeGroup);
          group.plantations.push(plantation);
        }
      });
      this.havePlantationInGroups = true;
    }
  }

  public getPlantationByCode(code: number): Plantation {
    const idx = this.plantations.findIndex(p => p.code == code);
    return this.plantations[idx];
  }

  public getShapeByCode(code: number): Shape {
    const idx = this.shapes.findIndex(p => p.code == code);
    return this.shapes[idx];
  }
}
