import { EntityWithKey } from "@cfeltz/angular-helpers";

export class Health extends EntityWithKey<number> {
  public GetKey(): number {
    return this.code;
  }

  public GetTextValue(): string {
    return this.text;
  }

  constructor(
    public code?: number,
	  public text?: string,
    public typeMessage?: string,
    public errorMessage?: string) {
     super(); 
    }
}
