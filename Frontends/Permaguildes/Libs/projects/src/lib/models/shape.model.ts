import { EntityWithKey } from "@cfeltz/angular-helpers";

export class Shape extends EntityWithKey<number> {
  public GetKey(): number {
    return this.code;
  }

  constructor(
    public code?: number,
	  public codeProject?: number,
    public geometryType?: string,
    public coordinates?: string,
    public color?: string,
    public fillColor?: string,
    public fill?: boolean,
    public opacity?: number,
    public fillOpacity?: number,
    public stroke?: boolean,
    public weight?: number,
    public weightIsInFeet?: boolean,
    public radius?: number,
    public typeMessage?: string,
    public errorMessage?: string) {
     super(); 
    }

  public cloneForServer(): Shape {
    return new Shape(
      this.code,
      this.codeProject,
      this.geometryType,
      this.coordinates,
      this.color,
      this.fillColor,
      this.fill,
      this.opacity,
      this.fillOpacity,
      this.stroke,
      this.weight,
      this.weightIsInFeet,
      this.radius
    );
  }
}
