import { createApiMessageInstance, EntityWithKey } from "@cfeltz/angular-helpers";
import { PlantationHistoryPhoto } from "./plantation-history-photo.model";

export class PlantationHistory extends EntityWithKey<number> {
  public GetKey(): number {
    return this.code;
  }

  constructor(
    public code?: number,
	  public codePlantation?: number,
    public dtHistory?: Date,
    public health?: number,
    public height?: number,
    public span?: number,
    public trunkCircumference?: number,
    public comment?: string,
    public photos?: PlantationHistoryPhoto[],
    public typeMessage?: string,
    public errorMessage?: string) {
     super(); 
    }

  public cloneForServer(): PlantationHistory {
    return new PlantationHistory(
      this.code,
      this.codePlantation,
      this.dtHistory,
      this.health,
      this.height,
      this.span,
      this.trunkCircumference,
      this.comment
    );
  }

  public loadFromJson(json: object): this {
    super.loadFromJson(json);
    
    if (json['photos']) {
      this.photos = [];
      json['photos'].forEach(r => {
        this.photos.push(createApiMessageInstance(PlantationHistoryPhoto).loadFromJson(r));
      });
    }

    return this;
  }
}
