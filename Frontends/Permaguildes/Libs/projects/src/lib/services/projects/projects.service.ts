import { Injectable } from '@angular/core';
import { lastValueFrom, Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { Crud, HubsManagmentService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider, ApisHttpClientExtended } from 'apis-helpers';
import { Project } from '../../models/project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService extends Crud<Project, number> {
  constructor(protected http: ApisHttpClientExtended,
    logger: NGXLogger,
    config: ApisConfigurationProvider,
    hubsManagment: HubsManagmentService) { 
      super(http, logger, hubsManagment, 
        config.getApiUrl(config.params.projectsApi, 'projects'), 
        config.getApiHubUrl(config.params.projectsApi), 
        Project, 'Project');
  }

  public getProjects(): Observable<Project[]> {
    return this.getAllData(true);
  }

  public getProject(code: number): Observable<Project> {
    return this.getSingleDataByKey(code, true);
  }

  public async createProject(project: Project): Promise<Project> {
    return lastValueFrom(this.createSingleData(project));
  }

  public async updateProject(project: Project): Promise<Project> {
    return lastValueFrom(this.updateSingleData(project.cloneForServer()));
  }
}
