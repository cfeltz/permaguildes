
import { waitForAsync } from '@angular/core/testing';
import { ApisTest } from 'apis-helpers';
import { Project } from '../../models/project.model';
import { ProjectsService } from './projects.service';

describe(ProjectsService.name, () => {
  const test = new ApisTest<ProjectsService>();

  test.initXMLHttpRequestTestingController();

  test.initService(ProjectsService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });

  it('get project data', waitForAsync(() => {
    const data$ = test.component.getAllData(true);
    const subscribe = data$.subscribe((data: Project[]) => {
      expect(data.length).toBeGreaterThan(0);
      subscribe.unsubscribe();
    });
    test.http.expectOne(req => true).flush([{ 'code': 1, 'name': 'test' }]);
  }));
});
