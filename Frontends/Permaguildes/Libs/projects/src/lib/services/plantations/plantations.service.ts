import { Injectable } from '@angular/core';
import { Plantation, Project } from '../../models';
import { NGXLogger } from 'ngx-logger';
import { CrudRelation1N, HubsManagmentService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider, ApisHttpClientExtended } from 'apis-helpers';
import { ProjectsService } from '../projects/projects.service';
import { lastValueFrom, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlantationsService extends CrudRelation1N<Project, number, Plantation, number> {

  getRelationsFromEntity(data: Project): Plantation[] {
    return data.plantations;
  }
  setRelationsFromEntity(data: Project, relations: Plantation[]) {
    data.plantations = relations;
  }
  getReferenceKey(relation: Plantation): number {
    return relation.codeProject;
  }

  constructor(protected http: ApisHttpClientExtended,
    public logger: NGXLogger,
    public config: ApisConfigurationProvider,
    public projectsService: ProjectsService,
    public hubsManagment: HubsManagmentService) { 
    super(http, logger, projectsService, hubsManagment, 
      config.getApiUrl(config.params.projectsApi, 'projects/{referenceKey}/plantations'), 
      config.getApiHubUrl(config.params.projectsApi), 
      Plantation, 'Plantation');
  }

  public async addPlantationInProject(codeProject: number, plantation: Plantation): Promise<Plantation> {
    plantation.codeProject = codeProject;
    return lastValueFrom(this.createSingleData(plantation));
  }

  public removePlantationFromProject(codeProject: number, codePlantation: number) {
    lastValueFrom(this.deleteSingleData(codeProject, codePlantation));
  }

  public removePlantationsFromProject(codeProject: number, codesPlantation: number[]) {
    codesPlantation.forEach(codePlantation => {
      this.removePlantationFromProject(codeProject, codePlantation);
    });
  }

  //#region Override internal refresh to update plantations list from groups
  protected override refreshRelationInternal(project: Project, relation: Plantation) {
    //#region Remove plantation from old group if needed
    let groupUpdated = false;
    let plantation = this.getRelationFromEntity(project, relation.GetKey());
    if (plantation) {
      if (plantation.codeGroup != relation.codeGroup) {
        groupUpdated = true;
        const oldGroup = project.groups?.find(g => g.code == plantation.codeGroup);
        if (oldGroup) {
          const idx = oldGroup.plantations.findIndex(p => p.GetKey() === relation.code);
          if (idx >= 0) {
            oldGroup.plantations.splice(idx, 1);
          }
        }
      }
    }
    //#endregion    

    super.refreshRelationInternal(project, relation);
    
    //#region Add plantation in new group if needed
    if (groupUpdated) {
      plantation = this.getRelationFromEntity(project, relation.GetKey());
      const newGroup = project.groups?.find(g => g.code == relation.codeGroup);
      newGroup?.plantations.push(plantation);
    }
    //#endregion  
  }

  // Internal refresh of relation (delete)
  protected override refreshDeletedRelationInternal(project: Project, deletedRelation: Plantation) {
    //#region Remove plantation from old group if needed
    let plantation = this.getRelationFromEntity(project, deletedRelation.GetKey());
    if (plantation) {
      if (plantation.codeGroup != deletedRelation.codeGroup) {
        const oldGroup = project.groups?.find(g => g.code == plantation.codeGroup);
        if (oldGroup) {
          const idx = oldGroup.plantations.findIndex(p => p.GetKey() === deletedRelation.code);
          if (idx >= 0) {
            oldGroup.plantations.splice(idx, 1);
          }
        }
      }
    }
    //#endregion  
    super.refreshDeletedRelationInternal(project, deletedRelation);
  }
  //#endregion


  public async updatePlantationPosition(project: Project, codePlantation: number, latitude: number, longitude: number): Promise<Plantation> {
    const url = `/Projects/${project.code}/plantations/${codePlantation}/updatePosition`;
    return lastValueFrom(this.http.postToProjectsApi<Plantation>(url, { latitude: latitude,  longitude: longitude }).pipe(
      tap(p => {
        const plantation = project.plantations.find(p => p.code == codePlantation);
        plantation.latitude = latitude;
        plantation.longitude = longitude;
      })
    ));
  }

  public async updatePlantationGroup(project: Project, codePlantation: number, codeGroup: number): Promise<Plantation> {
    const url = `/Projects/${project.code}/plantations/${codePlantation}/updateGroup/${codeGroup??0}`;
    return lastValueFrom(this.http.postToProjectsApi<Plantation>(url).pipe(
      tap(p => {
        const plantation = project.plantations.find(p => p.code == codePlantation);
        const oldGroup = project.groups.find(p => p.code == plantation.codeGroup);
        const newGroup = project.groups.find(p => p.code == codeGroup);
        plantation.codeGroup = codeGroup;

        // Remove plantation node from current group node
        oldGroup?.plantations.splice(oldGroup.plantations.indexOf(plantation), 1);

        // Add plantation node in new group node
        newGroup?.plantations.push(plantation);

      })
    ));
  }

  public async updatePlantation(codeProject: number, plantation: Plantation): Promise<Plantation> {
    const url = `/Projects/${codeProject}/plantations/${plantation.code}`;
    return lastValueFrom(this.http.postToProjectsApi<Plantation>(url, plantation.cloneForServer()));
  }
}
