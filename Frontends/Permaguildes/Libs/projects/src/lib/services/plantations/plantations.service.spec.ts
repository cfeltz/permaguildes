import { ApisTest } from 'apis-helpers';
import { PlantationsService } from './plantations.service';

describe(PlantationsService.name, () => {
  const test = new ApisTest<PlantationsService>();

  test.initXMLHttpRequestTestingController();

  test.initService(PlantationsService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });
});
