import { ApisTest } from 'apis-helpers';
import { PlantationHistoryService } from './plantation-history.service';

describe(PlantationHistoryService.name, () => {
  const test = new ApisTest<PlantationHistoryService>();

  test.initXMLHttpRequestTestingController();

  test.initService(PlantationHistoryService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });
});
