import { Injectable } from '@angular/core';
import { Health, Plantation, PlantationHistory, PlantationHistoryPhoto } from '../../models';
import { NGXLogger } from 'ngx-logger';
import { CrudRelation1N, HubsManagmentService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider, ApisHttpClientExtended } from 'apis-helpers';
import { lastValueFrom, map, Observable } from 'rxjs';
import { PlantationsService } from '../plantations/plantations.service';

@Injectable({
  providedIn: 'root'
})
export class PlantationHistoryService extends CrudRelation1N<Plantation, number, PlantationHistory, number> {
  private healthList$: Observable<Health[]>;

  getRelationsFromEntity(data: Plantation): PlantationHistory[] {
    return data.history;
  }
  setRelationsFromEntity(data: Plantation, relations: PlantationHistory[]) {
    data.history = relations;
  }
  getReferenceKey(relation: PlantationHistory): number {
    return relation.codePlantation;
  }

  constructor(protected http: ApisHttpClientExtended,
    public logger: NGXLogger,
    public config: ApisConfigurationProvider,
    public plantationsService: PlantationsService,
    public hubsManagment: HubsManagmentService) { 
    super(http, logger, plantationsService, hubsManagment, 
      config.getApiUrl(config.params.projectsApi, 'plantations/{referenceKey}/history'), 
      config.getApiHubUrl(config.params.projectsApi), 
      PlantationHistory, 'PlantationHistoryDTO');
  }

  public getHealthList(): Observable<Health[]> {
    if (!this.healthList$) {
      const url = this.config.getApiUrl(this.config.params.projectsApi, 'common/plantation-history-health');
      const obsResult$ = this.getMultipleDataByUrlTyped(url, Health);
      this.healthList$ = obsResult$;
    }
    return this.healthList$;
  }

  public getHealth(health: number): Observable<Health> {
    this.getHealthList();
    return this.healthList$.pipe(
      map((all: Health[]) => all.find(h => h.GetKey() == health))
    );
  }

  public async getAllHistoryOfPlantation(codePlantation: number): Promise<PlantationHistory[]> {
    return lastValueFrom(this.getMultipleDataByReferenceKey(codePlantation));
  }

  public async addHistory(codePlantation: number, history: PlantationHistory): Promise<PlantationHistory> {
    history.codePlantation = codePlantation;
    return lastValueFrom(this.createSingleData(history));
  }

  public async updateHistory(codePlantation: number, history: PlantationHistory): Promise<PlantationHistory> {
    history.codePlantation = codePlantation;
    return lastValueFrom(this.updateSingleData(history));
  }

  public removeHistory(codePlantation: number, codeHistory: number) {
    lastValueFrom(this.deleteSingleData(codePlantation, codeHistory));
  }

  public uploadPhotos(codePlantation: number, codeHistory: number, files: File[]): Observable<any> {
    const url = this.getUrlFromControllerWithReference(codePlantation, codeHistory.toString()) + '/AddPhotos';
    return this.http.postMultiPartsWithFiles(url, 'files', files);
  }

  public removePhoto(codePlantation: number, codeHistory: number, codePhoto: number) {
    const url = this.getUrlFromControllerWithReference(codePlantation, codeHistory.toString()) + '/photos/' + codePhoto;
    lastValueFrom(this.http.delete(url));
  }
}
