import { ApisTest } from 'apis-helpers';
import { ShapesService } from './shapes.service';

describe(ShapesService.name, () => {
  const test = new ApisTest<ShapesService>();

  test.initXMLHttpRequestTestingController();

  test.initService(ShapesService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });
});
