import { Injectable } from '@angular/core';
import { Shape, Project } from '../../models';
import { NGXLogger } from 'ngx-logger';
import { CrudRelation1N, HubsManagmentService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider, ApisHttpClientExtended } from 'apis-helpers';
import { ProjectsService } from '../projects/projects.service';
import { lastValueFrom, Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShapesService extends CrudRelation1N<Project, number, Shape, number> {

  getRelationsFromEntity(data: Project): Shape[] {
    return data.shapes;
  }
  setRelationsFromEntity(data: Project, shapes: Shape[]) {
    data.shapes = shapes;
  }
  getReferenceKey(relation: Shape): number {
    return relation.codeProject;
  }

  constructor(protected http: ApisHttpClientExtended,
    public logger: NGXLogger,
    public config: ApisConfigurationProvider,
    public projectsService: ProjectsService,
    public hubsManagment: HubsManagmentService) { 
    super(http, logger, projectsService, hubsManagment, 
      config.getApiUrl(config.params.projectsApi, 'projects/{referenceKey}/shapes'), 
      config.getApiHubUrl(config.params.projectsApi), 
      Shape, 'Shape');
  }

  public getShapesFromProject(code: number): Observable<Shape[]> {
    return this.getMultipleDataByReferenceKey(code);
  }

  public async addShapeInProject(codeProject: number, shape: Shape): Promise<Shape> {
    shape.codeProject = codeProject;
    return lastValueFrom(this.createSingleData(shape));
  }

  public removeShapeFromProject(codeProject: number, codeShape: number) {
    lastValueFrom(this.deleteSingleData(codeProject, codeShape));
  }

  public async updateShape(codeProject: number, shape: Shape): Promise<Shape> {
    const url = `/Projects/${codeProject}/shapes/${shape.code}`;
    return lastValueFrom(this.http.postToProjectsApi<Shape>(url, shape.cloneForServer()));
  }
}
