import { Injectable } from '@angular/core';
import { GroupPlantations, Plantation, Project } from '../../models';
import { NGXLogger } from 'ngx-logger';
import { CrudRelation1N, HubsManagmentService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider, ApisHttpClientExtended } from 'apis-helpers';
import { ProjectsService } from '../projects/projects.service';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GroupsService extends CrudRelation1N<Project, number, GroupPlantations, number> {

  getRelationsFromEntity(data: Project): GroupPlantations[] {
    return data.groups;
  }
  setRelationsFromEntity(data: Project, relations: GroupPlantations[]) {
    data.groups = relations;
  }
  getReferenceKey(relation: GroupPlantations): number {
    return relation.codeProject;
  }

  constructor(protected http: ApisHttpClientExtended,
    logger: NGXLogger,
    config: ApisConfigurationProvider,
    projectsService: ProjectsService,
    hubsManagment: HubsManagmentService) { 
      super(http, logger, projectsService, hubsManagment, 
        config.getApiUrl(config.params.projectsApi, 'projects/{referenceKey}/groups'), 
        config.getApiHubUrl(config.params.projectsApi), 
        GroupPlantations, 'GroupPlantations');
    }

    public async addGroupInProject(codeProject: number, groupName: string): Promise<GroupPlantations> {
      const group = new GroupPlantations(0, groupName, codeProject);
      return lastValueFrom(this.createSingleData(group));
    }
  
    public removeGroupFromProject(codeProject: number, codeGroup: number) {
      lastValueFrom(this.deleteSingleData(codeProject, codeGroup));
    }

    public async updateGroup(group: GroupPlantations): Promise<GroupPlantations> {
      return lastValueFrom(this.updateSingleData(group.cloneForServer()));
    }
  

}
