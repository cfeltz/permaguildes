import { ApisTest } from 'apis-helpers';
import { GroupsService } from './groups.service';

describe(GroupsService.name, () => {
  const test = new ApisTest<GroupsService>();

  test.initXMLHttpRequestTestingController();

  test.initService(GroupsService, {
    beforeEach: () => {
      test.initHttpTestingController();
    }
  });
});
