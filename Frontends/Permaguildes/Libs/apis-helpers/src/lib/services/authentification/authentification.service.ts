import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApisHttpClientExtended } from '../apis-http-client-extended/apis-http-client-extended.service';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  constructor(protected http: ApisHttpClientExtended) { }

  public jwtIsConfiguredOnServer(): Observable<boolean> {
    return this.http.getFromAuthApi('/authenticate/IsJwtConfigured');
  }

  public is4IsConfiguredOnServer(): Observable<boolean> {
    return this.http.getFromAuthApi('/authenticate/IsIdentityServerConfigured');
  }
}
