import { ApisTest } from '../../modules';
import { AuthService } from './auth.service';

describe(AuthService.name, () => {
  const test = new ApisTest<AuthService>();
  test.initService(AuthService, {});
});
