import { ApisHttpClientExtended } from './apis-http-client-extended.service';
import { ApisTest } from '../../modules/apis-testing/apis-test';

describe(ApisHttpClientExtended.name, () => {
  const test = new ApisTest<ApisHttpClientExtended>();
  test.initService(ApisHttpClientExtended, {});
});
