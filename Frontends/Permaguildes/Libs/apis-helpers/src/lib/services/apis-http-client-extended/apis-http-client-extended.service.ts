import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApisConfigurationProvider } from '../../config';
import { NGXLogger } from 'ngx-logger';
import { HttpClientExtended } from '@cfeltz/angular-helpers';

@Injectable({
  providedIn: 'root',
})
export class ApisHttpClientExtended extends HttpClientExtended {
  constructor(protected http: HttpClient, 
    protected config: ApisConfigurationProvider, 
    protected logger: NGXLogger) {
    super(http, config, logger);
  }

  public getUrl(api: string, path: string) {
    const suffix = this.config.getApiSuffix(api);
    return api + suffix + path;
  }

  public get<T>(url: string): Observable<T> {
    const obsResult = this.http.get<T>(url);
    return obsResult;
  }

  public getFromPlantsApi<T>(path: string): Observable<T> {
    return this.get<T>(this.getUrl(this.config.params.plantsApi, path));
  }

  public getFromAuthApi<T>(path: string): Observable<T> {
    return this.get<T>(this.getUrl(this.config.params.authJwtApi, path));
  }
  public getFromProjectsApi<T>(path: string): Observable<T> {
    return this.get<T>(this.getUrl(this.config.params.projectsApi, path));
  }


  private postJsonReplacer(key: string, value: any): any {
    if (value !== null && value !== undefined && !(value instanceof Array) && typeof(value) === 'object') {
      const newValue = {};
      for (const p in value) {
        if (value[p] instanceof EventEmitter) {
          this.logger.debug('Post serialization: remove attribute ' + p);
        } else {
          if (p.startsWith('_')) {
            newValue[p.substring(1)] = value[p];
          } else {
            newValue[p] = value[p];
          }
        }
      }
      return newValue;
    } else {
      return value;
    }
  }

  public post<T>(url: string, data?: any): Observable<T> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    const dataSerialized = JSON.stringify(data, (k, v) => this.postJsonReplacer(k, v));
    const obsResult = this.http.post<T>(url, dataSerialized, {headers: headers});
    return obsResult;
  }

  public postToPlantsApi<T>(path: string, data?: any): Observable<T> {
    return this.post<T>(this.getUrl(this.config.params.plantsApi, path), data);
  }

  public postToAuthApi<T>(path: string, data?: any): Observable<T> {
    return this.post<T>(this.getUrl(this.config.params.authJwtApi, path), data);
  }

  public postToProjectsApi<T>(path: string, data?: any): Observable<T> {
    return this.post<T>(this.getUrl(this.config.params.projectsApi, path), data);
  }

  public deleteToProjectsApi<T>(path: string, data?: any): Observable<T> {
    return this.delete(this.getUrl(this.config.params.projectsApi, path));
  }

  public delete(url: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.delete(url, {headers: headers});
  }
}
