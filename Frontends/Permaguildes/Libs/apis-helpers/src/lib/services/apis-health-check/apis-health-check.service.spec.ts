import { ApisTest } from '../../modules/apis-testing/apis-test';
import { ApisHealthCheckService } from './apis-health-check.service';

describe(ApisHealthCheckService.name, () => {
  const test = new ApisTest<ApisHealthCheckService>();
  test.initService(ApisHealthCheckService, {});
});
