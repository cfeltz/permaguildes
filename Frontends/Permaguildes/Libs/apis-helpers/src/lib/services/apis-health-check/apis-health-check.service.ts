import { Injectable } from '@angular/core';
import { HealthCheckService, HealthCheck } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider } from '../../config';
import { ApisHttpClientExtended } from '../apis-http-client-extended/apis-http-client-extended.service';

@Injectable({
  providedIn: 'root'
})
export class ApisHealthCheckService extends HealthCheckService {

  constructor(
    protected http: ApisHttpClientExtended, protected config: ApisConfigurationProvider) {
    super(http);
  }

  public healthChecks(): Promise<HealthCheck> {
    const urls = [
      this.config.params.authJwtApi,
      this.config.params.plantsApi,
      this.config.params.logsApi,
      this.config.params.projectsApi
    ];

    return super.healthChecks(urls);
  }

}
