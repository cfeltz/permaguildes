import { Injectable } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { ApisConfigurationProvider } from '../../config/apis-config-provider';
import { MetaTag } from '../../models/metatag.model';

@Injectable({
  providedIn: 'root'
})
export class MetatagService {
  private urlMeta = 'og:url';
  private titleMeta = 'og:title';
  private descriptionMeta = 'og:description';
  private imageMeta = 'og:image';
  private secureImageMeta = 'og:image:secure_url';
  private twitterTitleMeta = 'twitter:text:title';
  private twitterImageMeta = 'twitter:image';

  constructor(private titleService: Title, private metaService: Meta, private config: ApisConfigurationProvider) { }

  public setDefault() {
    this.setTitle(this.config.params.applicationName + ', ' + this.config.params.applicationSubtitle);
    this.setSocialMediaTags(
      this.config.params.baseUrl,
      this.config.params.applicationName,
      this.config.params.applicationSubtitle,
      this.config.params.baseUrl + '/assets/images/og-image.jpg');
  }

  public setTitle(title: string): void {
    this.titleService.setTitle(title);
  }

  public setSocialMediaTags(url: string, title: string, description: string, imageUrl: string): void {
    const tags = [
      new MetaTag(this.urlMeta, url, true),
      new MetaTag(this.titleMeta, title, true),
      new MetaTag(this.descriptionMeta, description, true),
      new MetaTag(this.imageMeta, imageUrl, true),
      new MetaTag(this.secureImageMeta, imageUrl, true),
      new MetaTag(this.twitterTitleMeta, title, false),
      new MetaTag(this.twitterImageMeta, imageUrl, false)
    ];
    this.setTags(tags);
  }

  private setTags(tags: MetaTag[]): void {
    tags.forEach(siteTag => {
      const tag = siteTag.isOpenGraph ? this.metaService.getTag('property=\'${siteTag.name}\'')
         : this.metaService.getTag('name=\'${siteTag.name}\'');
      if (siteTag.isOpenGraph) {
        this.metaService.updateTag({ property: siteTag.name, content: siteTag.value });
      } else {
        this.metaService.updateTag({ name: siteTag.name, content: siteTag.value });
      }
    });
  }
}
