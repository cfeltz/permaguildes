import { Injectable } from '@angular/core';
import { HubConnection } from '@microsoft/signalr';
import { ApiMessage, HttpClientExtended, HubsManagmentService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider } from '../../config/apis-config-provider';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApisHubsManagmentService extends HubsManagmentService {

  constructor(http: HttpClientExtended, logger: NGXLogger, protected config: ApisConfigurationProvider) { 
    super(http, logger);
  }

  public connectHubFromApi(api: string, path: string): HubConnection {
    const suffix = this.config.getHubSuffix(api);
    return super.connectHub(api + suffix + path);
  }

  public getFromControllerAndHub<T extends ApiMessage>(c: new () => T,
                                hub: HubConnection,
                                hubMethodName: string,
                                controllerApi: string, controllerPath: string, controllerOptions?: any): Observable<T> {
    const suffix = this.config.getHubSuffix(controllerApi);
    return super.getFromControllerAndHub(c, hub, hubMethodName, controllerApi + suffix + controllerPath);
  }



  public connectHubFromPlantsApi(path: string): HubConnection {
    return this.connectHubFromApi(this.config.params.plantsApi, path);
  }
}
