import { ApisHubsManagmentService } from './apis-hubs-managment.service';
import { ApisTest } from '../../modules/apis-testing/apis-test';

describe(ApisHubsManagmentService.name, () => {
  const test = new ApisTest<ApisHubsManagmentService>();
  test.initService(ApisHubsManagmentService, {});
});
