import { Injectable } from '@angular/core';
import { ConfigurationProvider, Configuration } from '@cfeltz/angular-helpers';
import { ApisConfig } from './apis-config.model';

@Injectable({ providedIn: 'root' })
export abstract class ApisConfigurationProvider extends ConfigurationProvider {
  abstract get params(): ApisConfig;

  public apiUseGateway(api: string): boolean {
    // return api.startsWith(this.params.gatewayApi);
    return false;
  }

  public getApiSuffix(api: string): string {
    return this.apiUseGateway(api) ? '' : '/api';
  }

  public getApiUrl(api: string, path?: string, subPath?: string, queryParams?: string | any): string {
    let url = api + this.getApiSuffix(api) + '/';
    if (path) {
      url += path;
    }
    if (subPath) {
      url += '/' + subPath;
    }
    if (queryParams) {
      if (typeof(queryParams) === 'string') {
        url += '?' + queryParams;
      } else {
        const params = new URLSearchParams();
        for (const key in queryParams) {
          if (queryParams.hasOwnProperty(key)) {
            params.set(key, queryParams[key]);
          }
        }
        url += '?' + params.toString();
      }
    }
    return url;
  }

  public getHubSuffix(api: string): string {
    return this.apiUseGateway(api) ? 'Hub' : '';
  }

  public getApiHubUrl(api: string, path?: string): string {
    let url = api + this.getHubSuffix(api) + '/';
    if (path) {
      url += path;
    }
    return url;
  }
}

@Injectable({ providedIn: 'root' })
export class DefaultApisConfiguration extends ApisConfigurationProvider {
  get params(): ApisConfig {
    // return default config
    return {
      production: false,
      baseUrl: 'http://127.0.0.1:5201',
      applicationName: 'PermaGuildes',
      applicationSubtitle: 'Aide à la création d\'une forêt comestible',
      authJwtApi: 'http://127.0.0.1:45000',
      authJwtApiOnGateway: false,
      authIS4Api: '',
      authIS4ClientId: '',
      authIS4Scope: '',
      authIS4SilentRefreshTimeout: 0,
      authIS4TimeoutFactor: 0,
      authIS4AllowedUrls: '',
      plantsApi: 'http://127.0.0.1:45000',
      logsApi: 'http://127.0.0.1:45001',
      projectsApi: 'http://127.0.0.1:45002',
      nurseriesApi: 'http://127.0.0.1:45003'
    };
  }
}

export class ApisConfiguration extends Configuration {
}
