import { Config } from '@cfeltz/angular-helpers';
import { ApisConfigurationInterface } from '../interfaces';

export class ApisConfig extends Config implements ApisConfigurationInterface {
  plantsApi: string;
  logsApi: string;
  projectsApi: string;
  nurseriesApi: string;
}
