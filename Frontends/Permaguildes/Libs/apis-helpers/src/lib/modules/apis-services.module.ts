import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApisHttpClientExtended } from '../services/apis-http-client-extended/apis-http-client-extended.service';
import { ApisHubsManagmentService } from '../services/apis-hubs-managment/apis-hubs-managment.service';
import { ApisConfiguration, ApisConfigurationProvider, DefaultApisConfiguration } from '../config';
import { AuthConfig, OAuthModuleConfig } from 'angular-oauth2-oidc';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtTokenInterceptor, AuthJwtService, AuthJwtModule, ConfigurationProvider } from '@cfeltz/angular-helpers';
import { AuthIS4Service, resolveAuthIS4Config, AuthIS4Module } from '@cfeltz/angular-helpers';
import { NgxCaptureModule } from 'ngx-capture';

@NgModule({
  imports: [
    CommonModule,
    NgxCaptureModule
  ],
  exports: [
    AuthJwtModule,
    AuthIS4Module
  ]
})
export class ApisServicesModule {

  constructor(@Optional() @SkipSelf() parentModule: ApisServicesModule) {
    if (parentModule) {
      throw new Error('ApisServicesModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(apisConfiguration: ApisConfiguration = {}): ModuleWithProviders<ApisServicesModule> {
    const apisConfigProvider = apisConfiguration.config || {
      provide: ApisConfigurationProvider, useClass: DefaultApisConfiguration
    };
    const configProfider = {
      provide: ConfigurationProvider, useClass: apisConfigProvider['useClass']
    };
    return {
      ngModule: ApisServicesModule,
      providers: [
        ApisHttpClientExtended,
        ApisHubsManagmentService,
        AuthJwtService,
        apisConfigProvider,
        configProfider,
        { provide: HTTP_INTERCEPTORS, useClass: JwtTokenInterceptor, multi: true }
      ]
    };
  }
}
