import { NgModule } from '@angular/core';
import { DirectivesModule } from '@cfeltz/angular-helpers';

@NgModule({
  imports: [
    DirectivesModule
  ],
  exports: [
    DirectivesModule
  ]
})
export class ApisDirectivesModule {
}
