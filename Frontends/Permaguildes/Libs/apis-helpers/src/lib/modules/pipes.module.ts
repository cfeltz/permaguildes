import { NgModule} from '@angular/core';
import { FilterPipe } from '../pipes/filter.pipe';
import { OrderByPipe } from '../pipes/orderby.pipe';

@NgModule({
  imports: [
  ],
  declarations: [
    FilterPipe,
    OrderByPipe
  ],
  exports: [
    FilterPipe,
    OrderByPipe
  ]
})
export class AppPipesModule {}
