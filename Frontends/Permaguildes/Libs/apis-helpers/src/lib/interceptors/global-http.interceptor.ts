import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
// import HttpStatusCode from '@cfeltz/angular-helpers';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class GlobalHttpInterceptor implements HttpInterceptor {

  constructor(private toastr: ToastrService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        switch (error.status) {
          // case HttpStatusCode.PRECONDITION_FAILED: {
          case 412: {
            const jsonErrorResponse = error.error;
            const message = jsonErrorResponse?.messages[0];
            this.toastr.error(message);
            break;
          }
          // case HttpStatusCode.INTERNAL_SERVER_ERROR: {
          case 500: {
            this.toastr.error('Erreur interne du serveur');
            break;
          }
        }
        return throwError(error);
      })
    );
  }
}
