import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'orderby'
})
export class OrderByPipe  implements PipeTransform {
  transform(array: any, field: string, order = 'asc'): any[] {
    if (!Array.isArray(array)) {
      return;
    }
    let orderMult = 1;
    if (order === 'desc') {
      orderMult = -1;
    }
    array.sort((a: any, b: any) => {
      if (a[field] < b[field]) {
        return -1 * orderMult;
      } else if (a[field] > b[field]) {
        return 1 * orderMult;
      } else {
        return 0;
      }
    });
    return array;
  }
}