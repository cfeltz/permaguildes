import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'filter',
    pure: false
})
export class FilterPipe implements PipeTransform {
    transform(items: any[], fieldOrFunction: (string | ((item: any, param: any) => boolean)), valueOrParam: any): any[] {
    if (!items) {
      return [];
    }
    if (!fieldOrFunction) {
      return items;
    }
    if (typeof fieldOrFunction === 'function') {
      // fieldOrFunction is a function
      return items.filter(singleItem => {
        return fieldOrFunction(singleItem, valueOrParam);
      });
    } else {
      // fieldOrFunction is a field
      return items.filter(singleItem => {
        const fieldNotNull = (singleItem != null && singleItem[fieldOrFunction] != null &&  singleItem[fieldOrFunction] != undefined);
        if (valueOrParam === null) {
          // Keep element if field value is undefined
          return !fieldNotNull;
        } else {
          // Keep element if field value is defined or egal to value
          return (fieldNotNull && (valueOrParam == undefined || singleItem[fieldOrFunction].indexOf(valueOrParam) >= 0));
        }
      });
    }
  }
}