import { ApiMessage } from '@cfeltz/angular-helpers';

export class TextApiMessage extends ApiMessage {
    constructor(public message?: string, public typeMessage?: string, public errorMessage?: boolean) {
      super();
    }

    public loadFromJson(json: object): this {
      this.message = json['message'];
      this.typeMessage = json['typeMessage'];
      this.errorMessage = json['errorMessage'];
      return this;
    }
}
