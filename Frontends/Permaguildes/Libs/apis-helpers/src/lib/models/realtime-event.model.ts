import { ApiMessage } from '@cfeltz/angular-helpers';

export enum RealtimeEventType {
  Added = 0,
  Updated = 1,
  Removed = 2
}


export class RealtimeEvent extends ApiMessage {
    constructor(public eventType?: RealtimeEventType, public object?: ApiMessage) {
      super();
    }

    public loadFromJson(json: object): this {
      this.eventType = json['eventType'];
      this.object = json['object'];
      return this;
    }
}
