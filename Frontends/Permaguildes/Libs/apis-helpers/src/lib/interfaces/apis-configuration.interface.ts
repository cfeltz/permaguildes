import { ConfigurationInterface } from '@cfeltz/angular-helpers';

export interface ApisConfigurationInterface extends ConfigurationInterface {
  plantsApi: string;
  logsApi: string;
  projectsApi: string;
  nurseriesApi: string;
}
