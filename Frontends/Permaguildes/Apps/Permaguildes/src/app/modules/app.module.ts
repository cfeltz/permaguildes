import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, APP_INITIALIZER, Injectable, LOCALE_ID } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule, GlobalHttpInterceptor } from 'apis-helpers';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { PagesModule } from './pages.module';
import { AppDirectivesModule } from './directives.module';
import { AppComponent } from '../app.component';
import { AppComponentsModule } from './components.module';
import { AppDialogsModule } from './dialogs.module';
import { INGXLoggerConfig, LoggerModule, NgxLoggerLevel, TOKEN_LOGGER_CONFIG } from 'ngx-logger';
import { Config } from 'Apps/Permaguildes/src/configs/config';
import { ApisConfig, ApisConfigurationProvider, ApisServicesModule, ServicesModule } from 'apis-helpers';
import { UncatchedErrorHandler } from '@cfeltz/angular-helpers';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

/**
 * Use to update config of library after loaded in APP_INITIALIZER
 */
@Injectable({ providedIn: 'root' })
export class ConfigFromApp extends ApisConfigurationProvider {
  constructor(private configStore: Config) {
    super();
    console.log('Load application configuration');
  }

  get params(): ApisConfig {
    return this.configStore.params;
  }
}

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppMaterialModule,
    ScrollingModule,
    ServicesModule,
    BrowserModule.withServerTransition({ appId: 'PermaGuildes' }),
    BrowserTransferStateModule,
    ServiceWorkerModule.register('ngsw-worker.js'),
    PagesModule,
    AppComponentsModule,
    AppDialogsModule,
    AppDirectivesModule,
    HttpClientModule,
    HttpClientModule,
    LoggerModule.forRoot(undefined, { 
      configProvider: {
        provide: TOKEN_LOGGER_CONFIG, 
        useFactory: resolveNgxLoggerConfig
      }
    }),
    ToastrModule.forRoot({
      positionClass: 'toast-top-right'
    }),
    ApisServicesModule.forRoot({
      config: {
        provide: ApisConfigurationProvider,
        useClass: ConfigFromApp
      }
    })
  ],
  declarations: [
    AppComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'},
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: Config) => () => configService.loadConfig(),
      deps: [Config], multi: true
    },
    { provide: HTTP_INTERCEPTORS, useClass: GlobalHttpInterceptor, multi: true },
    { provide: ErrorHandler, useClass: UncatchedErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

export function resolveNgxLoggerConfig(): INGXLoggerConfig  {
  const config = {
    serverLoggingUrl: (Config.params ? Config.getApiUrl(Config.params.logsApi, 'logs') : undefined),
    level: NgxLoggerLevel.DEBUG,
    serverLogLevel: NgxLoggerLevel.INFO
  };
  return config;
}
