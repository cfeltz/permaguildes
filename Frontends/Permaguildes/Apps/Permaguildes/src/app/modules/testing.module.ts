import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule, Type } from '@angular/core';
import { AppMaterialModule } from 'apis-helpers';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { ToastrModule } from 'ngx-toastr';
import { Config } from '../../configs/config';
import { ApisTest } from 'apis-helpers';
import { TestComponentOptions } from '@cfeltz/angular-helpers';

@NgModule({
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: Config) => () => configService.loadConfig(),
      deps: [Config], multi: true
    }
  ],
  imports: [
    LoggerTestingModule,
    AppMaterialModule,
    ToastrModule.forRoot()
  ],
  exports: [
    AppMaterialModule,
    LoggerTestingModule,
    ToastrModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class PlantsTestingModule { }

export class PermaguildesTest<T> extends ApisTest<T> {
  public initConfig() {
    super.initConfig();
  }

  public initComponent(type: Type<T>, options?: TestComponentOptions) {
    if (!options) {
      options = {};
    }

    options.imports = options.imports ?
      options.imports.concat([
        PlantsTestingModule
      ]) : [PlantsTestingModule];

    super.initComponent(type, options);
  }

  public initService(type: Type<T>, options?: TestComponentOptions) {
    if (!options) {
      options = {};
    }

    options.imports = options.imports ?
      options.imports.concat([
        PlantsTestingModule
      ]) : [PlantsTestingModule];

    super.initService(type, options);
  }
}
