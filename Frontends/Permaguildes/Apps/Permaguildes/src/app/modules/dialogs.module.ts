import { NgModule} from '@angular/core';
import { AppMaterialModule } from 'apis-helpers';

import { AppComponentsModule } from './components.module';
import { ProjectCreationDialogComponent } from '../dialogs/project-creation-dialog/project-creation-dialog.component';
import { ReportingDialogComponent } from '../dialogs/reporting-dialog/reporting-dialog.component';
import { PlantDetailDialogComponent } from '../dialogs/plant-detail-dialog/plant-detail-dialog.component';
import { GroupDetailDialogComponent } from '../dialogs/group-detail-dialog/group-detail-dialog.component';
import { PlantationDetailDialogComponent } from '../dialogs/plantation-detail-dialog/plantation-detail-dialog.component';
import { NurseryDetailDialogComponent } from '../dialogs/nursery-detail-dialog/nursery-detail-dialog.component';
import { PlantationHistoryDialogComponent } from '../dialogs/plantation-history-dialog/plantation-history-dialog.component';
import { MessageBoxDialogComponent } from '../dialogs/messagebox-dialog/messagebox-dialog.component';

@NgModule({
  imports: [
    AppMaterialModule,
    AppComponentsModule
  ],
  declarations: [
    ProjectCreationDialogComponent,
    ReportingDialogComponent,
    MessageBoxDialogComponent,
    PlantDetailDialogComponent,
    GroupDetailDialogComponent,
    PlantationDetailDialogComponent,
    PlantationHistoryDialogComponent,
    NurseryDetailDialogComponent
  ],
  entryComponents: [
    ProjectCreationDialogComponent,
    ReportingDialogComponent,
    MessageBoxDialogComponent,
    PlantDetailDialogComponent,
    GroupDetailDialogComponent,
    PlantationDetailDialogComponent,
    PlantationHistoryDialogComponent,
    NurseryDetailDialogComponent
  ]
})
export class AppDialogsModule {}
