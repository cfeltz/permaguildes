import { NgModule} from '@angular/core';
import { ClickOutsideDirective } from '../directives/click-outside/click-outside.directive';
import { DragDirective } from '../directives/drag-and-drop/drag.directive';
import { DropDirective } from '../directives/drag-and-drop/drop.directive';
import { CodeSophyDirective } from '../validators/codesophy.directive';

@NgModule({
  declarations: [
    ClickOutsideDirective,
    DragDirective,
    DropDirective,
    CodeSophyDirective
  ],
  exports: [
    ClickOutsideDirective,
    DragDirective,
    DropDirective,
    CodeSophyDirective
  ]
})
export class AppDirectivesModule {}
