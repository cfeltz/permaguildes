import { NgModule} from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppMaterialModule, AppPipesModule } from 'apis-helpers';

import { DataModule } from './data.module';
import { RouterModule, Routes } from '@angular/router';

import { AuthJwtModule, ComponentsModule } from '@cfeltz/angular-helpers';
import { AuthentificationComponent } from '../components/authentification/authentification.component';
import { AppDirectivesModule } from './directives.module';
import { ProjectMapComponent } from '../components/projects/project-map/project-map.component';
import { SearchComponent } from '../components/plants/search/search.component';
import { PlantsExtendedListComponent } from '../components/plants/plants-extended-list/plants-extended-list.component';
import { PropertiesIconsComponent } from '../components/plants/icons/properties-icons/properties-icons.component';
import { WaterIconComponent } from '../components/plants/icons/water-icon/water-icon.component';
import { GrowthPropertyComponent } from '../components/plants/properties/growth-property/growth-property.component';
import { EdibilityIconComponent } from '../components/plants/icons/edibility-icon/edibility-icon.component';
import { MedicinalIconComponent } from '../components/plants/icons/medicinal-icon/medicinal-icon.component';
import { UsdahardinessPropertyComponent } from '../components/plants/properties/usdahardiness-property/usdahardiness-property.component';
import { AuxiliariesPropertyComponent } from '../components/plants/properties/auxiliaries-property/auxiliaries-property.component';
import { RepellentsPropertyComponent } from '../components/plants/properties/repellents-property/repellents-property.component';
import { HeightPropertyComponent } from '../components/plants/properties/height-property/height-property.component';
import { FloweringPropertyComponent } from '../components/plants/properties/flowering-property/flowering-property.component';
import { HarvestPropertyComponent } from '../components/plants/properties/harvest-property/harvest-property.component';
import { LinksPropertyComponent } from '../components/plants/properties/links-property/links-property.component';
import { SpanPropertyComponent } from '../components/plants/properties/span-property/span-property.component';
import { LabeledPropertyComponent } from '../components/plants/properties/labeled-property/labeled-property.component';
import { ExposurePropertyComponent } from '../components/plants/properties/exposure-property/exposure-property.component';
import { CompanionsComponent } from '../components/plants/companions/companions.component';
import { PlantsFinderComponent } from '../components/plants/plants-finder/plants-finder.component';
import { PlantDetailComponent } from '../components/plants/plant-detail/plant-detail.component';
import { PlantationsListComponent } from '../components/projects/plantations-list/plantations-list.component';
import { SettingsComponent } from '../components/projects/settings/settings.component';
import { AppDropdownlistFieldComponent } from '../components/dropdownlist-field/dropdownlist-field.component';
import { AppAutocompletelistFieldComponent } from '../components/autocompletelist-field/autocompletelist-field.component';


@NgModule({
  imports: [
    AppMaterialModule,
    FlexLayoutModule,
    DataModule,
    RouterModule,
    AuthJwtModule,    
    AppPipesModule,
    AppDirectivesModule,
    ComponentsModule
  ],
  declarations: [
    // Components
    SearchComponent,
    PlantsExtendedListComponent,
    PropertiesIconsComponent,
    WaterIconComponent,
    GrowthPropertyComponent,
    EdibilityIconComponent,
    MedicinalIconComponent,
    UsdahardinessPropertyComponent,
    AuxiliariesPropertyComponent,
    RepellentsPropertyComponent,
    HeightPropertyComponent,
    FloweringPropertyComponent,
    HarvestPropertyComponent,
    LinksPropertyComponent,
    SpanPropertyComponent,
    LabeledPropertyComponent,
    ExposurePropertyComponent,
    CompanionsComponent,
    AuthentificationComponent,
    ProjectMapComponent,
    PlantsFinderComponent,
    PlantDetailComponent,
    PlantationsListComponent,
    SettingsComponent,
    AppDropdownlistFieldComponent,
    AppAutocompletelistFieldComponent
  ],
  exports: [
    // Components
    SearchComponent,
    PlantsExtendedListComponent,
    PropertiesIconsComponent,
    WaterIconComponent,
    GrowthPropertyComponent,
    EdibilityIconComponent,
    MedicinalIconComponent,
    UsdahardinessPropertyComponent,
    AuxiliariesPropertyComponent,
    RepellentsPropertyComponent,
    HeightPropertyComponent,
    FloweringPropertyComponent,
    HarvestPropertyComponent,
    LinksPropertyComponent,
    SpanPropertyComponent,
    LabeledPropertyComponent,
    ExposurePropertyComponent,
    CompanionsComponent,
    AuthentificationComponent,
    ProjectMapComponent,
    PlantsFinderComponent,
    PlantDetailComponent,
    PlantationsListComponent,
    SettingsComponent,
    AppDropdownlistFieldComponent,
    AppAutocompletelistFieldComponent,
    FlexLayoutModule,
    AppDirectivesModule, 
    AuthJwtModule,
    ComponentsModule,
    AppPipesModule
  ]
})
export class AppComponentsModule {}
