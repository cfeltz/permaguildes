import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizedPageComponent } from '../pages/authorized-page/authorized-page.component';
import { UnauthorizedPageComponent } from '../pages/unauthorized-page/unauthorized-page.component';
import { HeartCheckPageComponent } from '../pages/heart-check-page/heart-check-page.component';
import { AuthJwtGuard } from '@cfeltz/angular-helpers';
import { ProjectPageComponent } from '../pages/project-page/project-page.component';
import { PlantsFinderPageComponent } from '../pages/plants-finder-page/plants-finder-page.component';
import { PlantDetailPageComponent } from '../pages/plant-detail-page/plant-detail-page.component';
import { HomePageComponent } from '../pages/home-page/home-page.component';

const routes: Routes = [
  { path: '',
    redirectTo: '/nav/home',
    pathMatch: 'full'
  },
  { path: 'health', component: HomePageComponent },
  { path: 'nav/health', component: HeartCheckPageComponent },
  { path: 'nav/home', component: HomePageComponent },
  { path: 'nav/authorized-page', component: AuthorizedPageComponent },
  { path: 'nav/unauthorized-page', component: UnauthorizedPageComponent, canActivate: [AuthJwtGuard], canLoad: [AuthJwtGuard] },
  { path: 'nav/plants-finder', component: PlantsFinderPageComponent },
  { path: 'nav/plant/:code', component: PlantDetailPageComponent },
  { path: 'nav/project/:code', component: ProjectPageComponent }
  // ,{ path: '**', component: PageNotFoundComponent } // TODO
];

@NgModule({
  imports: [
    // RouterModule.forRoot(routes)
    RouterModule.forRoot(
      routes,
      {
        enableTracing: true,
        onSameUrlNavigation: 'reload',
        relativeLinkResolution: 'legacy'
      }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
