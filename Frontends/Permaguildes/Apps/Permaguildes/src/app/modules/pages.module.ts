import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppMaterialModule } from 'apis-helpers';
import { AppComponentsModule } from './components.module';
import { HomePageComponent } from '../pages/home-page/home-page.component';
import { UnauthorizedPageComponent } from '../pages/unauthorized-page/unauthorized-page.component';
import { HeartCheckPageComponent } from '../pages/heart-check-page/heart-check-page.component';
import { AuthorizedPageComponent } from '../pages/authorized-page/authorized-page.component';
import { RouterModule } from '@angular/router';
import { AuthJwtModule } from '@cfeltz/angular-helpers';
import { ProjectPageComponent } from '../pages/project-page/project-page.component';
import { PlantsFinderPageComponent } from '../pages/plants-finder-page/plants-finder-page.component';
import { PlantDetailPageComponent } from '../pages/plant-detail-page/plant-detail-page.component';

@NgModule({
  imports: [
    AppComponentsModule,
    AppMaterialModule,
    CommonModule,
    RouterModule,
    AuthJwtModule
  ],
  declarations: [
    HomePageComponent,
    ProjectPageComponent,
    PlantsFinderPageComponent,
    PlantDetailPageComponent,
    AuthorizedPageComponent,
    UnauthorizedPageComponent,
    HeartCheckPageComponent
  ]
})
export class PagesModule {}
