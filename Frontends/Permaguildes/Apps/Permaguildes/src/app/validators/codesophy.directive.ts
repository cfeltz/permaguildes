import { Directive, forwardRef } from '@angular/core';
import { AbstractControl, AsyncValidator, ValidationErrors, NG_ASYNC_VALIDATORS } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { SophyService } from 'plants';

@Directive({
  selector: '[app-codesophy]',
  providers: [
    { provide: NG_ASYNC_VALIDATORS, useExisting: forwardRef(() => CodeSophyDirective), multi: true },
  ]
})
export class CodeSophyDirective implements AsyncValidator {
  constructor(private _sophyService: SophyService) {
  }

  validate(c: AbstractControl): Observable<ValidationErrors | null>  {
    if (c.value) {
      return this._sophyService.getSophy(c.value).pipe(
        map(sophy => {
          return sophy ? null : {message: 'Code sophy inexistant'};
        })
      );
    }
    return of(null);
  }
}
