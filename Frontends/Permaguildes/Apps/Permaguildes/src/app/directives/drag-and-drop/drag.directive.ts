import { DOCUMENT } from '@angular/common';
import { OnInit, OnDestroy, Directive, Inject, ElementRef, EventEmitter, Output, Input } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DragAndDropEvent, DragAndDropService } from '../../services/drag-and-drop/drag-and-drop.service';
@Directive({
  selector: "[appDrag]", 
})
export class DragDirective implements OnInit, OnDestroy {

  private element: HTMLElement;
  private preview: HTMLElement;
  @Input() dragAlign: string;
  @Input() dragData: any;
  @Input() dragEnable: boolean = true;

  @Output() dragStarted = new EventEmitter<DragAndDropEvent>();
  @Output() dragStopped = new EventEmitter<DragAndDropEvent>();
  @Output() dragInProgress = new EventEmitter<DragAndDropEvent>();

  private subscriptions: Subscription[] = [];
  
  constructor(
      private elementRef: ElementRef,
      private dragAndDropService: DragAndDropService, 
      @Inject(DOCUMENT) private document: any
  ) {}
  
  ngOnInit(): void {
    if (this.dragEnable) {
      this.element = this.elementRef.nativeElement as HTMLElement;
      this.initDrag();
    }
  }

  initDrag(): void {
    this.element.style.cursor = 'grab';
    const dragStart$ = fromEvent<MouseEvent>(this.element, "mousedown");
    const dragEnd$ = fromEvent<MouseEvent>(this.document, "mouseup");
    const drag$ = fromEvent<MouseEvent>(this.document, "mousemove").pipe(
        takeUntil(dragEnd$)
    );

    let initialX: number, initialY: number, currentX = 0, currentY = 0;
    let dragSub: Subscription;

    // Drag start subscription
    const dragStartSub = dragStart$.subscribe((mousedownEvent: MouseEvent) => {
      this.preview = this.element.querySelector("app-drag-preview");
      // Create preview if doesn't exist
      if (!this.preview) { 
        this.preview = document.createElement('app-drag-preview');
        this.preview.appendChild(<HTMLElement>this.element.cloneNode(true));
        this.element.appendChild(this.preview);
      }

      this.element.style.cursor = 'grabbing';
      this.element.classList.add('app-dragging');
      currentX = mousedownEvent.clientX - this.preview.offsetLeft;
      currentY = mousedownEvent.clientY - this.preview.offsetTop;
      initialX = mousedownEvent.clientX - currentX;
      initialY = mousedownEvent.clientY - currentY;


      if (this.dragAlign) {
        switch (this.dragAlign) {
          case 'bottom': {
            initialX = initialX + this.preview.clientWidth / 2;
            initialY = initialY + this.preview.clientHeight;
            break;
          }
          case 'top': {
            initialX = initialX + this.preview.clientWidth / 2;
            break;
          }
          case 'left': {
            initialY = initialY + this.preview.clientHeight / 2;
            break;
          }
          case 'right': {
            initialX = initialX + this.preview.clientWidth;
            initialY = initialY + this.preview.clientHeight / 2;
            break;
          }
          case 'center': {
            initialX = initialX + this.preview.clientWidth / 2;
            initialY = initialY + this.preview.clientHeight / 2;
            break;
          }
        }
      }

      // Start drag subscription
      this.dragStarted.emit(new DragAndDropEvent(this.element, this.preview, initialX, initialY, this.dragData, mousedownEvent));
      dragSub = drag$.subscribe((mousemoveEvent: MouseEvent) => {
        mousemoveEvent.preventDefault();

        currentX = mousemoveEvent.clientX - initialX;
        currentY = mousemoveEvent.clientY - initialY;

        this.preview.style.transform =
            "translate3d(" + currentX + "px, " + currentY + "px, 0)";
        
        this.dragInProgress.emit(new DragAndDropEvent(this.element, this.preview, initialX, initialY, this.dragData, mousemoveEvent));
      });

      // Drag end subscription
      const dragEndSub = dragEnd$.subscribe((mouseupEvent: MouseEvent) => {
        initialX = currentX;
        initialY = currentY;
        this.element.style.cursor = 'grab';
        this.element.classList.remove('app-dragging');
        if (dragSub) {
          dragSub.unsubscribe();
        }
        const ev = new DragAndDropEvent(this.element, this.preview, initialX, initialY, this.dragData, mouseupEvent)
        this.dragStopped.emit(ev);
        this.dragAndDropService.drop(ev);
        dragEndSub.unsubscribe();
      });

      this.subscriptions.push.apply(this.subscriptions, [ dragStartSub, dragSub, dragEndSub ]);
    });
  }
  
  ngOnDestroy(): void {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }
}