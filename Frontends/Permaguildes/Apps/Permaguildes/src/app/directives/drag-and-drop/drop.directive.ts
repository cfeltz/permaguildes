import { OnInit, OnDestroy, Directive, Output, EventEmitter, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { DragAndDropEvent, DragAndDropService } from '../../services/drag-and-drop/drag-and-drop.service';

@Directive({
  selector: "[appDrop]", 
})
export class DropDirective implements OnDestroy {
  private dropSubscription: Subscription;
  private element: HTMLElement;
  @Output() drop = new EventEmitter<any>();
  
  constructor(private elementRef: ElementRef,
    private dragAndDropService: DragAndDropService) {
    this.element = this.elementRef.nativeElement as HTMLElement;
    this.dropSubscription = dragAndDropService.getDropSubject().subscribe((ev: DragAndDropEvent) => {
      const bound = this.element.getBoundingClientRect();
      if (bound.left <= ev.mouseEvent.clientX && ev.mouseEvent.clientX <= bound.right
        && bound.top <= ev.mouseEvent.clientY && ev.mouseEvent.clientY <= bound.bottom) {
        this.drop.emit(ev);
      }
    });
  }

  ngOnDestroy(): void {
    this.dropSubscription.unsubscribe();
  }
}