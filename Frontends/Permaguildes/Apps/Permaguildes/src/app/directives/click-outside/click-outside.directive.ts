import { Directive, ElementRef, EventEmitter, HostListener, Output, Input } from '@angular/core';

@Directive({
    selector: '[appClickOutside]'
})
export class ClickOutsideDirective {
    @Input() clickOutsideEnable = true;
    @Output() clickOutside: EventEmitter<Event> = new EventEmitter<Event>();

    constructor(private elementRef: ElementRef) { 
    }

    @HostListener('document:mousedown', ['$event'])
    onDocumentClick(event: Event) {
        if (!this.clickOutsideEnable || !event.target) {
          return;
        }
        if (!this.isClickInElement(event)) {
            this.clickOutside.emit(event);
        }
    }

    private isClickInElement(event: Event): boolean {
        return this.elementRef.nativeElement.contains(event.target);
    }
}
