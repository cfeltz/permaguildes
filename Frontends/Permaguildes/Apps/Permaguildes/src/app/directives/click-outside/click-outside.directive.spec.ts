import { ClickOutsideDirective } from './click-outside.directive';
import { Component, DebugElement, EventEmitter, Output } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestDirective } from '@cfeltz/angular-helpers';

@Component({
  selector: 'app-test-click-outside',
  template: `
    <div id="modal-gallery-wrapper"
         appClickOutside [clickOutsideEnable]="enableCloseOutside"
         (clickOutside)="onClickOutside($event)">
      <div class="ng-overlay"></div>
      <div id="flex-min-height-ie-fix">
        <div id="modal-gallery-container">
          <img id="current-image-test" class="inside current-image-next" src="assets/images/home.jpg"/>
          <div id="hidden-div-test" class="div-class hidden"></div>
        </div>
      </div>
    </div>
  `
})
class TestClickOutsideComponent {
  @Output() clicked: EventEmitter<boolean> = new EventEmitter<boolean>();

  onClickOutside(event: boolean) {
    this.clicked.emit(event);
  }
}



describe(ClickOutsideDirective.name, () => {
  const test = new TestDirective<ClickOutsideDirective, TestClickOutsideComponent>();
  test.initDirective(ClickOutsideDirective, TestClickOutsideComponent, {});

  describe('---click outside---', () => {
    beforeEach(() => test.fixture.detectChanges());

    it(`should check for appClickOutside`, () => {
      const clickOutsideDirective: DebugElement = test.fixture.debugElement.query(By.directive(ClickOutsideDirective));
      expect(clickOutsideDirective.name).toBe('div');
      expect(clickOutsideDirective.attributes['id']).toBe('modal-gallery-wrapper');
      expect(clickOutsideDirective.attributes['appClickOutside']).toBe('');
    });

    it(`should close the modal gallery after a click`, () => {
      test.directive.clickOutsideEnable = true;
      test.fixture.detectChanges();
      const overlay: DebugElement = test.fixture.debugElement.query(By.css('div#modal-gallery-wrapper'));
      test.component.clicked.subscribe((res: boolean) => {
        expect(res).toBe(true);
      });
      // in this scenario I'm using the native click
      overlay.nativeElement.click();
    });

    it(`shouldn't close the modal gallery after a click, because close-outside directive is disabled`, () => {
      test.directive.clickOutsideEnable = false;
      test.fixture.detectChanges();
      const overlay: DebugElement = test.fixture.debugElement.query(By.css('div#modal-gallery-wrapper'));
      test.component.clicked.subscribe(() => {
        fail('if clickOutsideEnable = false there will be no clicked events');
      });
      overlay.nativeElement.click();
    });

    it(`shouldn't close the modal gallery after a click over the current-image, because it isn't 'inside'`, () => {
      test.directive.clickOutsideEnable = true;
      test.fixture.detectChanges();
      const image: DebugElement = test.fixture.debugElement.query(By.css('img#current-image-test'));
      test.component.clicked.subscribe((res: boolean) => {
        fail(`if it's 'not inside' (variable isInside = false) there will be no clicked events`);
      });
      image.nativeElement.click();
    });

    it(`should close the modal gallery after a click over an 'hidden' div`, () => {
      test.directive.clickOutsideEnable = true;
      test.fixture.detectChanges();
      const image: DebugElement = test.fixture.debugElement.query(By.css('div#hidden-div-test'));
      test.component.clicked.subscribe((res: boolean) => {
        expect(res).toBe(true);
      });
      image.nativeElement.click();
    });
  });
});
