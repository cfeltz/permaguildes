import { Component, OnInit, ViewChild } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { LabelsService } from 'plants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'PermaGuildes, outil d\'aide à la création d\'une forêt comestible ';
  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  constructor(private _labelsService: LabelsService) {}

  ngOnInit() {
    console.log('Init component AppComponent');
    this._labelsService.loadLabels();
  }
}
