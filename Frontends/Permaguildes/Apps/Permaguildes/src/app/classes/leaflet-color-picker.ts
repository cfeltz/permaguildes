import * as L from 'leaflet';

L.LeafletColorPicker = L.Control.extend({
  onAdd: function (map) {
    const _this = this;
    const container = L.DomUtil.create('a', 'leaflet-draw-draw-color');
    container.id = 'colorPicker';
    container.href = '#';
    container.title = 'Pick color';
    container.innerHTML = '<span class="sr-only">Pick color</span>' + 
    '<input type="color" id="color" value="' + this.options.defaultShapeColor + '" name="leaflet-colorchoice" ' + 
    'style="width:100%;height:100%;border:0;background-color:#fff">' ;
    document.getElementsByClassName('leaflet-draw-toolbar')[0].append(container);

    const input = document.getElementsByName('leaflet-colorchoice')[0];
    input.onchange = function(e: any) {
      _this.updateColor(e.currentTarget.value);
    };

    const dummyContainer = L.DomUtil.create('span');
    dummyContainer.style.display = 'none';
    return dummyContainer;
  },

  updateColor: function(color) {
    this.updatePathColor(this.options.drawControlOptions.polyline, color);
    this.updatePathColor(this.options.drawControlOptions.polygon, color);
    this.updatePathColor(this.options.drawControlOptions.rectangle, color);
    this.updatePathColor(this.options.drawControlOptions.circle, color);
    
  },

  updatePathColor: function(path, color) {
    if (path) {
      if (path.shapeOptions) {
        path.shapeOptions.color = color;
      }
    }
  }
 });

export class CFeltzLeafletExtension {
  constructor() {

  }

  public getColorPicker(defaultShapeColor: string, drawControlOptions: any) {
    return new L.LeafletColorPicker({
      defaultShapeColor: defaultShapeColor,
      drawControlOptions: drawControlOptions
    }); 
  }
}
