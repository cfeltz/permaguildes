import { PlantProperties } from './plantProperties.enum';

export function PlantPropertiesAware(constructor: Function) {
    constructor.prototype.PlantProperties = PlantProperties;
}
