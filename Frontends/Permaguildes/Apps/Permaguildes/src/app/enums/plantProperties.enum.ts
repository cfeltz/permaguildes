export enum PlantProperties {
  ExposureSun = 'Plante de plein soleil',
  ExposureMiddle = 'Plante de mi-ombre',
  ExposureShadow = 'Plante d\'ombre',
  LowWater = 'Plante nécessitant peu d\'eau',
  DryWater = 'Plante nécessitant beaucoup d\'eau',
  Nitrate = 'Plante fixatrice d\'azote',
  OthersMinerals = 'Plante fixatrice de minéraux',
  Windbreaker = 'Brise-vent',
  Auxiliaries = 'Attire les auxiliaires',
  Repellents = 'Répulsif à insectes',
  Herbaceous = 'Herbacée',
  GroundCover = 'Couvre-sol',
  GoodEdibility = 'Bon comestible',
  GoodMedicinal = 'Bonne médicinale'
}
