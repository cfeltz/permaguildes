import { LocationStrategy } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthJwtService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider } from 'apis-helpers';
import { NGXLogger } from 'ngx-logger';
import { NurseriesService, Nursery } from 'nurseries';
import { Plant, PlantsService, RootstocksService, VarietiesService } from 'plants';
import { Plantation, PlantationHistoryService } from 'projects';
import { fromEvent, Observable, of, Subscription } from 'rxjs';
import { PermaguildesGenericDialogsService } from '../../services/permaguildes-generic-dialogs/permaguildes-generic-dialogs.service';

export class PlantDetailDialogModel {
  constructor(public codePlant: number, public plantation?: Plantation) {}
}

@Component({
  selector: 'app-plant-detail-dialog',
  templateUrl: './plant-detail-dialog.component.html',
  styleUrls: ['./plant-detail-dialog.component.scss'], 
})
export class PlantDetailDialogComponent implements OnInit, OnDestroy {
  plant$: Observable<Plant>;
  updateMode = false;
  public projectsApiBaseUrl;
  public sizeUnits: string;
  public nursery$: Observable<Nursery>;
  private closeDialogOnNavigationSub: Subscription;
  
  constructor(public dialogRef: MatDialogRef<PlantDetailDialogComponent>, 
    private logger: NGXLogger,
    private config: ApisConfigurationProvider,
    private genericDialogsService : PermaguildesGenericDialogsService,
    private plantsService: PlantsService,
    public varietiesService: VarietiesService, 
    public rootstocksService: RootstocksService, 
    private plantationHistoryService: PlantationHistoryService, 
    public nurseriesService: NurseriesService, 
    private authService: AuthJwtService,
    private location: LocationStrategy,
    @Inject(MAT_DIALOG_DATA) public data: PlantDetailDialogModel) {
      this.projectsApiBaseUrl = config.params.projectsApi;
  }

  ngOnInit() {
    this.closeDialogOnNavigationSub = this.genericDialogsService.closeDialogOnNavigation(this.dialogRef);
    const code = this.data.codePlant;
    if (code > 0) {
      this.plant$ = this.plantsService.getPlant(code);
      if (this.data.plantation) {
        this.sizeUnits = Plant.getSizeUnitsByStrate(this.data.plantation.codeStratum);
        if (this.data.plantation.codeNursery) {
          this.nursery$ = this.nurseriesService.getSingleDataByKeyFromAll(this.data.plantation.codeNursery);
        }
        if (!this.data.plantation.history) {
          this.plantationHistoryService.getAllHistoryOfPlantation(this.data.plantation.code).then(history => {
            this.data.plantation.history = history;
          });
        }
      }
    } else {
      if (this.authService.hasToken()) {
        this.logger.info('Open new plant dialog');
        this.plant$ = of(new Plant(0));
        this.updateMode = true;
      } else {
        this.authService.openLoginDialog().subscribe(result => {
          if (this.authService.hasToken()) {
            this.logger.info('Open new plant dialog');
            this.plant$ = of(new Plant(0));
            this.updateMode = true;
          }
        });
      }    
    }
  }
  
  ngOnDestroy(): void {
    this.closeDialogOnNavigationSub?.unsubscribe();
  }
}

