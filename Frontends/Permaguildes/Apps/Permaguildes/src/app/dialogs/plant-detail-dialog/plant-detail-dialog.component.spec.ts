import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthJwtService } from '@cfeltz/angular-helpers';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { PlantsService } from 'plants';
import { PlantDetailDialogComponent } from './plant-detail-dialog.component';

describe(PlantDetailDialogComponent.name, () => {
  const test = new PermaguildesTest<PlantDetailDialogComponent>();
  test.initComponent(PlantDetailDialogComponent, {
    providers: [ 
      PlantsService, AuthJwtService,
      { provide: MatDialogRef, useValue: [] },
      { provide: MAT_DIALOG_DATA, useValue: [ { codePlant: 1 }] }
    ]
  });
});
