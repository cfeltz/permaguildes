import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { NurseriesService } from 'nurseries';
import { NurseryDetailDialogComponent } from './nursery-detail-dialog.component';

describe(NurseryDetailDialogComponent.name, () => {
  const test = new PermaguildesTest<NurseryDetailDialogComponent>();
  test.initComponent(NurseryDetailDialogComponent, {
    providers: [ 
      NurseriesService,
      { provide: MatDialogRef, useValue: [] },
      { provide: MAT_DIALOG_DATA, useValue: { } }
    ]
  });
});
