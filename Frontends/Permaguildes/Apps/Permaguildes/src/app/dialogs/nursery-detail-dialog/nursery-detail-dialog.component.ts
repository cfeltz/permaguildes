import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NurseriesService, Nursery } from 'nurseries';
import { lastValueFrom, Subscription } from 'rxjs';
import { PermaguildesGenericDialogsService } from '../../services/permaguildes-generic-dialogs/permaguildes-generic-dialogs.service';

export class NurseryDetailDialogModel {
  constructor(public nursery: Nursery) {}
}

@Component({
  selector: 'app-nursery-detail-dialog',
  templateUrl: './nursery-detail-dialog.component.html',
  styleUrls: ['./nursery-detail-dialog.component.scss'],
})
export class NurseryDetailDialogComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;
  private closeDialogOnNavigationSub: Subscription;

  constructor(
    public dialog: MatDialog,
    private genericDialogsService : PermaguildesGenericDialogsService,
    private dialogRef: MatDialogRef<NurseryDetailDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: NurseryDetailDialogModel, 
    private formBuilder: FormBuilder, 
    private nurseriesService: NurseriesService) {
      if (!this.data) {
        this.data = new NurseryDetailDialogModel(new Nursery());
      }
  }

  ngOnInit() {
    this.closeDialogOnNavigationSub = this.genericDialogsService.closeDialogOnNavigation(this.dialogRef);
    this.formGroup = this.formBuilder.group({
      name: [this.data.nursery?.name, Validators.required],
      address: [this.data.nursery?.address],
      zipCode: [this.data.nursery?.zipCode],
      city: [this.data.nursery?.city],
      email: [this.data.nursery?.email],
      url: [this.data.nursery?.url],
      tel1: [this.data.nursery?.tel1],
      tel2: [this.data.nursery?.tel2],
      description: [this.data.nursery?.description]
    });
  }
  
  ngOnDestroy(): void {
    this.closeDialogOnNavigationSub?.unsubscribe();
  }

  get f() { return this.formGroup.value; }
  public async save() {
    let nursery = this.data.nursery;
    nursery.name = this.f.name;
    nursery.address = this.f.address;
    nursery.zipCode = this.f.zipCode;
    nursery.city = this.f.city;
    nursery.email = this.f.email;
    nursery.url = this.f.url;
    nursery.tel1 = this.f.tel1;
    nursery.tel2 = this.f.tel2;
    nursery.description = this.f.description;
    if (nursery.GetKey()) {
      nursery = await this.nurseriesService.updateNursery(nursery);
    } else {
      nursery = await this.nurseriesService.createNursery(nursery);
    }
    this.dialogRef.close(nursery);
  }
}

