import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlantsService } from 'plants';
import { Plantation, PlantationsService } from 'projects';
import { PermaguildesTest } from '../../modules/testing.module';
import { PlantationHistoryDialogComponent } from './plantation-history-dialog.component';

describe(PlantationHistoryDialogComponent.name, () => {
  const test = new PermaguildesTest<PlantationHistoryDialogComponent>();
  test.initComponent(PlantationHistoryDialogComponent, {
    providers: [ 
      { provide: MatDialogRef, useValue: [] },
      { provide: MAT_DIALOG_DATA, useValue: [ { codePlantation: 1, codeStratum: 1 }] }
    ]
  });
});
