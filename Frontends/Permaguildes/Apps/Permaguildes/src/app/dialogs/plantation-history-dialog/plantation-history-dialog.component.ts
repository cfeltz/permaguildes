import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IAuthJwtService, RealtimeEntitiesEvent, ImagesService } from '@cfeltz/angular-helpers';
import { ApisConfigurationProvider } from 'apis-helpers';
import { Plant } from 'plants';
import { Health, Plantation, PlantationHistory, PlantationHistoryPhoto, PlantationHistoryService } from 'projects';
import { Observable, tap } from 'rxjs';
import { PermaguildesGenericDialogsService } from '../../services/permaguildes-generic-dialogs/permaguildes-generic-dialogs.service';

export class PlantationHistoryDialogModel {
  constructor(public plantation: Plantation) {}
}

@Component({
  selector: 'app-plantation-history-dialog',
  templateUrl: './plantation-history-dialog.component.html',
  styleUrls: ['./plantation-history-dialog.component.scss'],
})
export class PlantationHistoryDialogComponent implements OnInit {
  public projectsApiBaseUrl;
  codeHistoryToEdit = 0;
  spinnerVisibility = false;
  formGroup: FormGroup;
  public isJwtLoggedIn$: Observable<boolean>;
  private selectedHistory: PlantationHistory;

  public healthList$: Observable<Health[]>;
  public history: PlantationHistory[] = [];
  public displayedColumns = [ 'dtHistory', 'health', 'height', 'span', 'trunkCircumference', 'comment', 'actions'];
  public dataSource: MatTableDataSource<PlantationHistory>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('dialogContent') private dialogContent: ElementRef;

  constructor(
    public dialog: MatDialog,
    public config: ApisConfigurationProvider,
    private imagesService: ImagesService, 
    private dialogsService: PermaguildesGenericDialogsService,
    private formBuilder: FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: PlantationHistoryDialogModel,
    @Inject('IAuthJwtService') private authJwtService: IAuthJwtService, 
    public plantationHistoryService: PlantationHistoryService) {
      this.isJwtLoggedIn$ = authJwtService.isLoggedIn();
      this.dataSource = new MatTableDataSource<PlantationHistory>();
      this.projectsApiBaseUrl = config.params.projectsApi;
  }

  ngOnInit() {
    this.healthList$ = this.plantationHistoryService.getHealthList();
    this.formGroup = this.formBuilder.group({
      dtHistory: [undefined, Validators.required],
      health: [undefined, Validators.required],
      height: [],
      span: [],
      trunkCircumference: [],
      comment: []
    });
    this.subscribeToRealtimeEvents();
    this.initDatasource();
  }

  //#region Realtime events
  private subscribeToRealtimeEvents() {
    this.plantationHistoryService.realtimeEvent.subscribe((realtimeEvent: RealtimeEntitiesEvent<PlantationHistory, number>) => {
      if (realtimeEvent) {
        switch (realtimeEvent.eventType) {
          case "Added": 
          case "Updated": 
          case "Removed": {
            this.dataSource.data = [];
            this.dataSource.data = this.data.plantation.history;
            if (realtimeEvent.eventType == "Added") {
              setTimeout(() => {
                this.dialogContent.nativeElement.scrollTop = this.dialogContent.nativeElement.scrollHeight;
              }, 500);
            }
            break;
          }
        }
      }
    });
  }
  //#endregion

  public async initDatasource() {
    this.dataSource.data = [];
    if (!this.data.plantation.history) {
      this.data.plantation.history = await this.plantationHistoryService.getAllHistoryOfPlantation(this.data.plantation.code);
    }
    this.dataSource.data = this.data.plantation.history;
    this.dataSource.paginator = this.paginator;
  }

  get f() { return this.formGroup.value; }
  public async addHistory() {
    let history = new PlantationHistory(0, this.data.plantation.code, this.f.dtHistory, this.f.health, 
                                            Math.round(this.f.height * 100), 
                                            Math.round(this.f.span * 100), 
                                            Math.round(this.f.trunkCircumference), this.f.comment);
    history = await this.plantationHistoryService.addHistory(this.data.plantation.code, history);
  }

  public async updateHistory() {
    let history = new PlantationHistory(this.codeHistoryToEdit, this.data.plantation.code, this.f.dtHistory, this.f.health, 
                                            Math.round(this.f.height * 100), 
                                            Math.round(this.f.span * 100), 
                                            Math.round(this.f.trunkCircumference), this.f.comment);
    history = await this.plantationHistoryService.updateHistory(this.data.plantation.code, history);
    this.codeHistoryToEdit = 0;
  }

  public goToEditMode(history: PlantationHistory) {
    this.codeHistoryToEdit = history.code;
    this.formGroup.setValue({
      dtHistory: history.dtHistory,
      health: history.health,
      height: history.height / 100,
      span: history.span / 100,
      trunkCircumference: history.trunkCircumference,
      comment: history.comment
    });
  }

  public removeHistory(history: PlantationHistory) {
    if (history.photos?.length > 0) {
      const sub = this.dialogsService.openConfirmDialog(`Confirmation`, `Etes-vous sûr de vouloir supprimer cet historique avec les photos qu'il contient`)
      .pipe(
        tap(response => {
          if (response) {
            this.plantationHistoryService.removeHistory(history.codePlantation, history.code);
          }
          sub.unsubscribe();
        })
      ).subscribe();
    } else {
      this.plantationHistoryService.removeHistory(history.codePlantation, history.code);
    }
  }

  public removePhoto(photo: PlantationHistoryPhoto) {
      this.plantationHistoryService.removePhoto(this.data.plantation.code, photo.codeHistory, photo.code);
  }

  public openPhoto(codeHistory: number) {
    alert(`Pour le moment il n'est pas encore possible d'ajouter des photos`);
  }

  public addPhotos(history: PlantationHistory) {
    this.selectedHistory = history;
  }

  public handleFileInput(files: FileList) {
    this.spinnerVisibility = true;
    this.imagesService.resizeImageFiles(files).then(files => {
      if (files.length > 0) {
        this.sendFiles(files);
      }
    });
  }

  private sendFiles(uploadFiles: File[]) {
    this.plantationHistoryService.uploadPhotos(this.selectedHistory.codePlantation, this.selectedHistory.code, uploadFiles).subscribe(
      (result) => this.spinnerVisibility = false,
      (error) => this.spinnerVisibility = false
    );
  }
}

