import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GroupPlantations, GroupsService, Project } from 'projects';
import { Subscription } from 'rxjs';
import { PermaguildesGenericDialogsService } from '../../services/permaguildes-generic-dialogs/permaguildes-generic-dialogs.service';

export class GroupDetailDialogModel {
  constructor(public project: Project, public group: GroupPlantations) {}
}

@Component({
  selector: 'app-group-detail-dialog',
  templateUrl: './group-detail-dialog.component.html',
  styleUrls: ['./group-detail-dialog.component.scss'],
})
export class GroupDetailDialogComponent implements OnInit, OnDestroy {
  groupForm: FormGroup;
  private closeDialogOnNavigationSub: Subscription;

  constructor(private dialogRef: MatDialogRef<GroupDetailDialogComponent>, 
    private genericDialogsService : PermaguildesGenericDialogsService,
    @Inject(MAT_DIALOG_DATA) public data: GroupDetailDialogModel, 
    private formBuilder: FormBuilder, 
    private groupsService: GroupsService) {
  }

  ngOnInit() {
    this.closeDialogOnNavigationSub = this.genericDialogsService.closeDialogOnNavigation(this.dialogRef);
    this.groupForm = this.formBuilder.group({
      name: [this.data.group.name],
      description: [this.data.group.description]
    });
  }
  
  ngOnDestroy(): void {
    this.closeDialogOnNavigationSub?.unsubscribe();
  }

  get f() { return this.groupForm.controls; }

  async save() {
    this.data.group.name = this.f.name.value;
    this.data.group.description = this.f.description.value;
    await this.groupsService.updateGroup(this.data.group);
    this.dialogRef.close(this.data.group);
  }
}

