import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { GroupPlantations, GroupsService } from 'projects';
import { GroupDetailDialogComponent } from './group-detail-dialog.component';

describe(GroupDetailDialogComponent.name, () => {
  const test = new PermaguildesTest<GroupDetailDialogComponent>();
  test.initComponent(GroupDetailDialogComponent, {
    providers: [ 
      GroupsService,
      { provide: MatDialogRef, useValue: [] },
      { provide: MAT_DIALOG_DATA, useValue: { codeProject: 1, group: new GroupPlantations() } }
    ]
  });
});
