import { ProjectCreationDialogComponent } from './project-creation-dialog.component';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';

describe(ProjectCreationDialogComponent.name, () => {
  const test = new PermaguildesTest<ProjectCreationDialogComponent>();
  test.initComponent(ProjectCreationDialogComponent, {
    providers: [ MatDialogModule, { provide: MatDialogRef, useValue: [] }, ],
  });
});
