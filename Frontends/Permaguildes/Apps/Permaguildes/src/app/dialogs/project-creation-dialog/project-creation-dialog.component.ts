import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Project, ProjectsService } from 'projects';
import { Subscription } from 'rxjs';
import { PermaguildesGenericDialogsService } from '../../services/permaguildes-generic-dialogs/permaguildes-generic-dialogs.service';

@Component({
  selector: 'app-project-creation-dialog',
  templateUrl: './project-creation-dialog.component.html',
  styleUrls: ['./project-creation-dialog.component.scss']
})
export class ProjectCreationDialogComponent implements OnInit, OnDestroy {
  formGroup = new FormGroup({
    name: new FormControl(),
    description: new FormControl()
  });
  private closeDialogOnNavigationSub: Subscription;

  constructor(private dialogRef: MatDialogRef<ProjectCreationDialogComponent>, 
    private genericDialogsService : PermaguildesGenericDialogsService,
    private router: Router, 
    private projectsService: ProjectsService) {
  }

  ngOnInit() {
    this.closeDialogOnNavigationSub = this.genericDialogsService.closeDialogOnNavigation(this.dialogRef);
  }
  
  ngOnDestroy(): void {
    this.closeDialogOnNavigationSub?.unsubscribe();
  }

  public async newProject(): Promise<void> {
    let project = Project.newProject(this.formGroup.value.name, this.formGroup.value.description);
    project = await this.projectsService.createProject(project);
    this.router.navigate([`/nav/project/${project.code}`]);
    this.dialogRef.close();
  }

}
