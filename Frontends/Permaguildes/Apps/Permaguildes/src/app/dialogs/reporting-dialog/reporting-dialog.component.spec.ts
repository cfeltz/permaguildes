import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { ReportingsService } from 'plants';
import { ReportingDialogComponent } from './reporting-dialog.component';

describe(ReportingDialogComponent.name, () => {
  const test = new PermaguildesTest<ReportingDialogComponent>();
  test.initComponent(ReportingDialogComponent, {
    providers: [ 
      ReportingsService,
      { provide: MatDialogRef, useValue: [] },
      { provide: MAT_DIALOG_DATA, useValue: [ { codePlant: 1 }] }
    ]
  });
});
