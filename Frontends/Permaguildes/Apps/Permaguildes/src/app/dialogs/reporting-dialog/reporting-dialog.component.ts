import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ReportingsService } from 'plants';
import { Subscription } from 'rxjs';
import { PermaguildesGenericDialogsService } from '../../services/permaguildes-generic-dialogs/permaguildes-generic-dialogs.service';

export class ReportingDialogModel {
  constructor(public codePlant: number) {}
}

@Component({
  selector: 'app-reporting-dialog',
  templateUrl: './reporting-dialog.component.html',
  styleUrls: ['./reporting-dialog.component.scss'],  
})
export class ReportingDialogComponent implements OnInit, OnDestroy {
  reportingForm: FormGroup;
  private closeDialogOnNavigationSub: Subscription;

  constructor(private dialogRef: MatDialogRef<ReportingDialogComponent>, 
    private genericDialogsService : PermaguildesGenericDialogsService,
    @Inject(MAT_DIALOG_DATA) public data: ReportingDialogModel, 
    private formBuilder: FormBuilder, 
    private reportingsService: ReportingsService,    
    private toastr: ToastrService) {
  }

  ngOnInit() {
    this.closeDialogOnNavigationSub = this.genericDialogsService.closeDialogOnNavigation(this.dialogRef);
    this.reportingForm = this.formBuilder.group({
      name: [''],
      email: new FormControl('', [ Validators.email ]), 
      message: ['']
    });
  }
  
  ngOnDestroy(): void {
    this.closeDialogOnNavigationSub?.unsubscribe();
  }
  
  get f() { return this.reportingForm.controls; }

  get emailIsInvalid() {
    return this.reportingForm.get('email').errors?.email
  }

  sendReporting() {
    this.reportingsService.sendReporting(this.f.name.value, this.f.email.value, this.f.message.value, this.data.codePlant)
    .subscribe(success => {
      if (success) {        
        this.toastr.info('Signalement envoyé');
        this.dialogRef.close();
      }
    });
  }
}

