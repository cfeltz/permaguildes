import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlantsService } from 'plants';
import { Plantation, PlantationsService } from 'projects';
import { PermaguildesTest } from '../../modules/testing.module';
import { PlantationDetailDialogComponent } from './plantation-detail-dialog.component';

describe(PlantationDetailDialogComponent.name, () => {
  const test = new PermaguildesTest<PlantationDetailDialogComponent>();
  test.initComponent(PlantationDetailDialogComponent, {
    providers: [ 
      PlantationsService, PlantsService,
      { provide: MatDialogRef, useValue: [] },
      { provide: MAT_DIALOG_DATA, useValue: [ { codeProject: 1, plantation: new Plantation() }] }
    ]
  });
});
