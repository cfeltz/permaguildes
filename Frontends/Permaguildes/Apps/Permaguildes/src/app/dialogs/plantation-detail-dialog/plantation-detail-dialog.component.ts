import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Plant, Rootstock, RootstocksService, VarietiesService, Variety } from 'plants';
import { Plantation, PlantationsService } from 'projects';
import { NurseriesService, Nursery } from 'nurseries';
import { lastValueFrom, Observable, Subscription } from 'rxjs';
import { NurseryDetailDialogComponent } from '../nursery-detail-dialog/nursery-detail-dialog.component';
import { MatOptionSelectionChange } from '@angular/material/core';
import { ToastrService } from 'ngx-toastr';
import { PermaguildesSubDialogsService } from '../../services/permaguildes-subdialogs/permaguildes-subdialogs.service';
import { PermaguildesGenericDialogsService } from '../../services/permaguildes-generic-dialogs/permaguildes-generic-dialogs.service';

export class PlantationDetailDialogModel {
  constructor(public codeProject: number, public plantation: Plantation) {}
}

@Component({
  selector: 'app-plantation-detail-dialog',
  templateUrl: './plantation-detail-dialog.component.html',
  styleUrls: ['./plantation-detail-dialog.component.scss'],
})
export class PlantationDetailDialogComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;
  private closeDialogOnNavigationSub: Subscription;
  sizeUnits: string;
  rootstocks$: Observable<Rootstock[]>;
  varieties$: Observable<Variety[]>;
  nurseries$: Observable<Nursery[]>;

  constructor(
    private dialogsService: PermaguildesSubDialogsService,
    private genericDialogsService : PermaguildesGenericDialogsService,
    public dialog: MatDialog,
    private dialogRef: MatDialogRef<PlantationDetailDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: PlantationDetailDialogModel, 
    private formBuilder: FormBuilder, 
    private nurseriesService: NurseriesService, 
    private plantationsService: PlantationsService, 
    private varietiesService: VarietiesService, 
    private rootstocksService: RootstocksService, 
    private toastr: ToastrService) {
  }

  ngOnInit() {
    this.closeDialogOnNavigationSub = this.genericDialogsService.closeDialogOnNavigation(this.dialogRef);
    this.rootstocks$ = this.rootstocksService.getMultipleDataByReferenceKey(this.data.plantation?.codePlant);
    this.varieties$ = this.varietiesService.getMultipleDataByReferenceKey(this.data.plantation?.codePlant);
    this.nurseries$ = this.nurseriesService.getAllData(true);
    this.sizeUnits = Plant.getSizeUnitsByStrate(this.data.plantation?.codeStratum);
    this.formGroup = this.formBuilder.group({
      name: [this.data.plantation?.name, Validators.required],
      shortName: [this.data.plantation?.shortName],
      codeRootstock: [this.data.plantation?.codeRootstock],
      codeVariety: [this.data.plantation?.codeVariety],
      height: [this.data.plantation?.height, Validators.required],
      span: [this.data.plantation?.span, Validators.required],
      codeNursery: [this.data.plantation?.codeNursery],
      plantingYear: [this.data.plantation?.plantingYear],
      locationDescription: [this.data.plantation?.locationDescription],
      comment: [this.data.plantation?.comment]      
    });
  }
  
  ngOnDestroy(): void {
    this.closeDialogOnNavigationSub?.unsubscribe();
  }

  get f() { return this.formGroup.value; }
  public async save() {
    this.data.plantation.name = this.f.name;
    this.data.plantation.shortName = this.f.shortName;
    this.data.plantation.codeRootstock = (this.f.codeRootstock === 0 ? null : this.f.codeRootstock);
    this.data.plantation.codeVariety = (this.f.codeVariety === 0 ? null : this.f.codeVariety);
    this.data.plantation.height =  Math.round(this.f.height);
    this.data.plantation.span = Math.round(this.f.span);
    this.data.plantation.codeNursery = (this.f.codeNursery === 0 ? null : this.f.codeNursery);
    this.data.plantation.plantingYear = this.f.plantingYear;
    this.data.plantation.locationDescription = this.f.locationDescription;
    this.data.plantation.comment = this.f.comment;
    await this.plantationsService.updatePlantation(this.data.codeProject, this.data.plantation);
    this.dialogRef.close(this.data.plantation);
  }

  public openHistory() {
    this.dialogsService.openPlantationHistoryDialog(this.data.plantation);
  }

  private showToastMessage(message: string) {
    if (message) {
      this.toastr.info(message, undefined, {
        positionClass: 'toast-bottom-center',
        toastClass: 'ngx-toastr toast-info toast-info-black toast-large'
      });
    }
  }

  public async onVarietyChange(ev: MatOptionSelectionChange) {
    if (ev.source.value && ev.source.value !== "0") {
      const variety = await lastValueFrom(this.varietiesService.getSingleDataByKey(this.data.plantation.codePlant, ev.source.value));
      this.showToastMessage(variety?.description);
    }
  }

  public async onRootstockChange(ev: MatOptionSelectionChange) {
    if (ev.source.value && ev.source.value !== "0") {
      const rootstock = await lastValueFrom(this.rootstocksService.getSingleDataByKey(this.data.plantation.codePlant, ev.source.value));
      if (rootstock) {
        this.showToastMessage(rootstock.description);
        const newHeight = Math.ceil((rootstock.maxHeight + rootstock.minHeight) / 2);
        const newSpan = Math.ceil((rootstock.maxSpan + rootstock.minSpan) / 2);
        if (newHeight) {
          this.formGroup.controls.height.setValue(newHeight);
        }
        if (newSpan) {
          this.formGroup.controls.span.setValue(newSpan);
        }
      }
    }
  }

  public onAddNursery() {
    const sub = this.dialogsService.openNurseryDetailDialog().subscribe(n => {
      this.formGroup.controls.codeNursery.setValue(n.code);
      sub.unsubscribe();
    });
  }
}

