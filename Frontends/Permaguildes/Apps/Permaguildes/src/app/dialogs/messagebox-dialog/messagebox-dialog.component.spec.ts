import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PermaguildesTest } from '../../modules/testing.module';

import { MessageBoxDialogComponent } from './messagebox-dialog.component';

describe(MessageBoxDialogComponent.name, () => {
  const test = new PermaguildesTest<MessageBoxDialogComponent>();
  test.initDialog(MessageBoxDialogComponent);
});

