import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';

/**
 * Class to represent confirm dialog model.
 *
 */
export class MessageBoxDialogModel {
  constructor(public title: string, public message: string, public allowAccept: boolean = true) {}
}

@Component({
  selector: 'app-messagebox-dialog',
  templateUrl: './messagebox-dialog.component.html',
  styleUrls: ['./messagebox-dialog.component.scss'],
})
export class MessageBoxDialogComponent {
  title: string;
  message: string;
  allowAccept: boolean;

  constructor(
    public dialogRef: MatDialogRef<MessageBoxDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MessageBoxDialogModel
  ) {
    // Update view with given values
    this.title = data.title;
    this.message = data.message;
    this.allowAccept = data.allowAccept ? data.allowAccept : true;
  }

  onConfirm(): void {
    // Close the dialog, return true
    this.dialogRef.close(true);
  }

  onDismiss(): void {
    // Close the dialog, return false
    this.dialogRef.close(false);
  }
}

