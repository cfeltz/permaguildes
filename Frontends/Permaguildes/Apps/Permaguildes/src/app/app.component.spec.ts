import { AppComponent } from './app.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';

describe(AppComponent.name, () => {
  const test = new PermaguildesTest<AppComponent>();
  test.initComponent(AppComponent, {
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  });

  it('should have a title', () => {
    expect(test.component.title).toBeDefined();
  });
});
