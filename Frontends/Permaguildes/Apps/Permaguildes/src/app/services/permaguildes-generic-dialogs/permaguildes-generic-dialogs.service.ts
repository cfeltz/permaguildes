import { ElementRef, Injectable } from '@angular/core';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { NGXLogger } from 'ngx-logger';
import { MessageBoxDialogComponent, MessageBoxDialogModel } from '../../dialogs/messagebox-dialog/messagebox-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class PermaguildesGenericDialogsService {

  constructor(private logger: NGXLogger, 
    public dialog: MatDialog) {
  }

  isOnDialog(elRef: ElementRef) {
    return (elRef.nativeElement.closest("mat-dialog-container") != null);
  }

  openConfirmDialog(title: string, message: string): Observable<any> {
    // Open window 
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'dialog';
    dialogConfig.data = new MessageBoxDialogModel(title, message, true);
    const dialogRef = this.dialog.open(MessageBoxDialogComponent, dialogConfig);
    return dialogRef.afterClosed();
  }

  openMessageDialog(title: string, message: string): Observable<any> {
    // Open window 
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'dialog';
    dialogConfig.data = new MessageBoxDialogModel(title, message, false);
    const dialogRef = this.dialog.open(MessageBoxDialogComponent, dialogConfig);
    return dialogRef.afterClosed();
  }

  closeDialogOnNavigation<T>(dialogRef: MatDialogRef<T>): Subscription {
    window.history.pushState(null, null, window.location.href);
    const sub = fromEvent(window, 'popstate').subscribe((e) => {
      sub.unsubscribe();
      e.preventDefault();
      // window.history.go(1);
      dialogRef.close();
    });
    return sub;
  }
}
