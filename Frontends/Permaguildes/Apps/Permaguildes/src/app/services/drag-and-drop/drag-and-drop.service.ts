import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Subject } from 'rxjs';

export class DragAndDropEvent {
  constructor(public element: HTMLElement, 
              public preview: HTMLElement, 
              public clientX: number,
              public clientY: number, 
              public dragData: any, 
              public mouseEvent: MouseEvent) {
    
  }
}

@Injectable({
  providedIn: 'root'
})
export class DragAndDropService {
  private dropSubject = new Subject<DragAndDropEvent>();

  constructor(private logger: NGXLogger) {
  }

  public drop(ev: DragAndDropEvent) {
    this.dropSubject.next(ev);
  }

  public getDropSubject() {
    return this.dropSubject;
  }
}
