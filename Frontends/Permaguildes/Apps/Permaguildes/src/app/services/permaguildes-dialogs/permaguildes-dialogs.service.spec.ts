import { PermaguildesTest } from "../../modules/testing.module";
import { PermaguildesDialogsService } from "./permaguildes-dialogs.service";

describe(PermaguildesDialogsService.name, () => {
  const test = new PermaguildesTest<PermaguildesDialogsService>();
  test.initService(PermaguildesDialogsService, {});
});
