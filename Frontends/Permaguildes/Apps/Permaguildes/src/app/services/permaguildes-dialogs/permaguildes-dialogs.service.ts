import { ElementRef, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NGXLogger } from 'ngx-logger';
import { ReportingDialogComponent, ReportingDialogModel } from 'Apps/Permaguildes/src/app/dialogs/reporting-dialog/reporting-dialog.component';
import { PlantDetailDialogComponent, PlantDetailDialogModel } from '../../dialogs/plant-detail-dialog/plant-detail-dialog.component';
import { GroupPlantations, Plantation, Project } from 'projects';
import { GroupDetailDialogComponent, GroupDetailDialogModel } from '../../dialogs/group-detail-dialog/group-detail-dialog.component';
import { PlantationDetailDialogComponent, PlantationDetailDialogModel } from '../../dialogs/plantation-detail-dialog/plantation-detail-dialog.component';
import { AuthJwtGuard } from '@cfeltz/angular-helpers';
import { ProjectCreationDialogComponent } from '../../dialogs/project-creation-dialog/project-creation-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class PermaguildesDialogsService {

  constructor(private logger: NGXLogger, 
    private authJwtGuard: AuthJwtGuard,
    public dialog: MatDialog) {
  }

  isOnDialog(elRef: ElementRef) {
    return (elRef.nativeElement.closest("mat-dialog-container") != null);
  }
 
  openReportingDialog(codePlant: number): Observable<any> {
    // Open window 
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'dialog';
    dialogConfig.data = new ReportingDialogModel(codePlant);
    const dialogRef = this.dialog.open(ReportingDialogComponent, dialogConfig);
    return dialogRef.afterClosed();
  }

  openPlantDetailDialog(codePlant: number, plantation?: Plantation): Observable<any> {
    // Open window 
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'dialog';
    dialogConfig.data = new PlantDetailDialogModel(codePlant, plantation);
    const dialogRef = this.dialog.open(PlantDetailDialogComponent, dialogConfig);
    return dialogRef.afterClosed();
  }

  openGroupDetailDialog(project: Project, group: GroupPlantations): Observable<any> {
    // Open window 
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'dialog';
    dialogConfig.data = new GroupDetailDialogModel(project, group);
    const dialogRef = this.dialog.open(GroupDetailDialogComponent, dialogConfig);
    return dialogRef.afterClosed();
  }

  openPlantationDetailDialog(codeProject: number, plantation: Plantation): Observable<any> {
    // Open window 
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = ['dialog', 'medium-dialog'];
    dialogConfig.data = new PlantationDetailDialogModel(codeProject, plantation);
    const dialogRef = this.dialog.open(PlantationDetailDialogComponent, dialogConfig);
    return dialogRef.afterClosed();
  }

  openProjectCreationDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = ['dialog', 'medium-dialog'];
    this.authJwtGuard.canExecute(() => this.dialog.open(ProjectCreationDialogComponent, dialogConfig));
  }
}
