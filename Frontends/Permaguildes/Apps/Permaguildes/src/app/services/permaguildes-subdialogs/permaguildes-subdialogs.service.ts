import { ElementRef, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NGXLogger } from 'ngx-logger';
import { PlantationHistoryDialogComponent, PlantationHistoryDialogModel } from '../../dialogs/plantation-history-dialog/plantation-history-dialog.component';
import { Plantation } from 'projects';
import { NurseryDetailDialogComponent } from '../../dialogs/nursery-detail-dialog/nursery-detail-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class PermaguildesSubDialogsService {

  constructor(private logger: NGXLogger, 
    public dialog: MatDialog) {
  }

  isOnDialog(elRef: ElementRef) {
    return (elRef.nativeElement.closest("mat-dialog-container") != null);
  }

  openNurseryDetailDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = ['dialog', 'medium-dialog'];
    const dialogRef = this.dialog.open(NurseryDetailDialogComponent, dialogConfig);
    return dialogRef.afterClosed();
  }

  openPlantationHistoryDialog(plantation: Plantation): Observable<any> {
    // Open window 
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;    
    dialogConfig.panelClass = ['dialog', 'large-dialog'];
    dialogConfig.data = new PlantationHistoryDialogModel(plantation);
    const dialogRef = this.dialog.open(PlantationHistoryDialogComponent, dialogConfig);
    return dialogRef.afterClosed();
  }
}
