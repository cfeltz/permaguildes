import { PermaguildesTest } from "../../modules/testing.module";
import { PermaguildesSubDialogsService } from "./permaguildes-subdialogs.service";

describe(PermaguildesSubDialogsService.name, () => {
  const test = new PermaguildesTest<PermaguildesSubDialogsService>();
  test.initService(PermaguildesSubDialogsService, {});
});
