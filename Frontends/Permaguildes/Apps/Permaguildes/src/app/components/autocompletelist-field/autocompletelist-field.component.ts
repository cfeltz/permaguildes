import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { ControlContainer, FormGroup } from "@angular/forms";
import { MatAutocomplete, MatAutocompleteTrigger } from "@angular/material/autocomplete";
import { MatOptionSelectionChange } from "@angular/material/core";
import { MatSelectChange } from "@angular/material/select";
import { EntityWithKey } from "@cfeltz/angular-helpers";
import { debounceTime, filter, map, Observable, startWith, Subscription, switchMap, tap } from "rxjs";


@Component({
  selector: "app-autocompletelist-field",
  templateUrl: "./autocompletelist-field.component.html",
  styleUrls: ["./autocompletelist-field.component.scss"],
})
export class AppAutocompletelistFieldComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;
  @Input() label: string;
  @Input() controlName: string;
  @Input() list$: Observable<EntityWithKey<number | string>[]>;
  list: EntityWithKey<number | string>[];
  filteredList: EntityWithKey<number | string>[];
  @Input() emptyText: string;
  @Input() newItemText: string;
  @Output() selectionChange = new EventEmitter<MatOptionSelectionChange>();
  @Output() addItem = new EventEmitter();
  private sub1: Subscription;
  private sub2: Subscription;

  constructor(private controlContainer: ControlContainer) {}
 
  get control() { 
    return this.formGroup.get(this.controlName);
  }

  ngOnInit() {
    this.formGroup = <FormGroup>this.controlContainer.control;
    this.sub1 = this.list$.subscribe(l => {
      this.list = l;
      if (this.control.value) {
        this.filteredList = l.filter(item => item.GetKey() === this.control.value);
      } else {
        this.filteredList = l;
      }
      this.control.updateValueAndValidity();
    });
    this.sub2 = this.control.valueChanges.pipe(
      debounceTime(500),
      tap(() => {
        this.filteredList = [];
      }),
      switchMap(value => this.list$.pipe(
        map(list => {
          if (value === '0' || value === undefined || value === null) {
            return list;
          } else {
            const filterValue = value.toString().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            const result = list.filter(item => 
              item.GetKey() === value || item.GetTextValue().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").includes(filterValue));
            return result;
          }
        })
      ))
    )
    .subscribe(data => { 
      this.filteredList = data; 
    });
  }

  ngOnDestroy(): void {
    this.sub1?.unsubscribe();
    this.sub2?.unsubscribe();
  }

  displayFn(item: string): string {
    if (item === '0' || item === '-1' || item == null  || item == undefined) {
      return "";
    } else if (this.list !== undefined) {
      return this.list.find(i => i.GetKey().toString() == item)?.GetTextValue();
    } else {
      return "";
    }
  }

  onSelectionChange(e: MatOptionSelectionChange) {
    if (e.source.selected) {
      if (e.source.value == -1) { // No replace by ===
        this.addItem.emit();
      } else {
        this.selectionChange.emit(e);
      }
    }
  }
}
