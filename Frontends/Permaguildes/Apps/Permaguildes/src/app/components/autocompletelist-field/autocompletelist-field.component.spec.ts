import { AppAutocompletelistFieldComponent } from './autocompletelist-field.component';
import { ControlContainer, FormControl, FormGroup, FormGroupDirective } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { Component } from '@angular/core';
import { EntityWithKey, Test } from '@cfeltz/angular-helpers';

@Component({
  selector: 'app-test-autocompletelist-field-wrapper',
  template: '<app-autocompletelist-field label="title" controlNameFrom="ctrlNameFrom" controlNameTo="ctrlNameTo" [list$]="list$"></app-autocompletelist-field>'
})
class TestAutocompletelistFieldWrapperComponent {
  list$: Observable<EntityWithKey<number | string>[]> = of();
}

describe(AppAutocompletelistFieldComponent.name, () => {
  const fg: FormGroup = new FormGroup({
    'test': new FormControl('')
  });
  
  const fgd: FormGroupDirective = new FormGroupDirective([], []);
  fgd.form = fg;

  const test = new Test<TestAutocompletelistFieldWrapperComponent>();
  test.initComponent(TestAutocompletelistFieldWrapperComponent, {
    providers: [
      { provide: ControlContainer, useValue: fgd }
    ]
  });
});
