import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ControlContainer, FormGroup } from "@angular/forms";
import { MatSelectChange } from "@angular/material/select";
import { EntityWithKey } from "@cfeltz/angular-helpers";
import { Observable } from "rxjs";


@Component({
  selector: "app-dropdownlist-field",
  templateUrl: "./dropdownlist-field.component.html",
  styleUrls: ["./dropdownlist-field.component.scss"],
})
export class AppDropdownlistFieldComponent implements OnInit {
  formGroup: FormGroup;
  @Input() label: string;
  @Input() controlName: string;
  @Input() list$: Observable<EntityWithKey<number | string>[]>;
  @Input() emptyText: string;
  @Input() newItemText: string;
  @Output() selectionChange = new EventEmitter<MatSelectChange>();
  @Output() addItem = new EventEmitter();

  constructor(private controlContainer: ControlContainer) {}
  
  get control() { 
    return this.formGroup.get(this.controlName);
  }

  ngOnInit() {
    this.formGroup = <FormGroup>this.controlContainer.control;
  }

  onSelectionChange(e: MatSelectChange) {
    if (e.value == -1) { // No replace by ===
      this.addItem.emit();
    } else {
      this.selectionChange.emit(e);
    }
  }
}
