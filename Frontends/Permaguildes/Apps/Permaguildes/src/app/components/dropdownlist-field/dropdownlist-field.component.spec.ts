import { AppDropdownlistFieldComponent } from './dropdownlist-field.component';
import { ControlContainer, FormControl, FormGroup, FormGroupDirective } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { Component } from '@angular/core';
import { EntityWithKey, Test } from '@cfeltz/angular-helpers';

@Component({
  selector: 'app-test-dropdownlist-field-wrapper',
  template: '<app-dropdownlist-field label="title" controlNameFrom="ctrlNameFrom" controlNameTo="ctrlNameTo" [list$]="list$"></app-dropdownlist-field>'
})
class TestDropdownlistFieldWrapperComponent {
  list$: Observable<EntityWithKey<number | string>[]> = of();
}

describe(AppDropdownlistFieldComponent.name, () => {
  const fg: FormGroup = new FormGroup({
    'test': new FormControl('')
  });
  
  const fgd: FormGroupDirective = new FormGroupDirective([], []);
  fgd.form = fg;

  const test = new Test<TestDropdownlistFieldWrapperComponent>();
  test.initComponent(TestDropdownlistFieldWrapperComponent, {
    providers: [
      { provide: ControlContainer, useValue: fgd }
    ]
  });
});
