import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PermaguildesTest } from '../../../modules/testing.module';
import { SettingsComponent } from './settings.component';

describe(SettingsComponent.name, () => {
  const test = new PermaguildesTest<SettingsComponent>();
  test.initComponent(SettingsComponent, {
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  });
});
