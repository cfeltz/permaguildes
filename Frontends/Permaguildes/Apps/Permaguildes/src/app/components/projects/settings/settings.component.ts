import { AfterViewInit, Component, Inject, Input, OnDestroy } from "@angular/core";
import { MatCheckboxChange } from "@angular/material/checkbox";
import { IAuthJwtService } from "@cfeltz/angular-helpers";
import { NGXLogger } from "ngx-logger";
import { Project, ProjectsService } from "projects";
import { Observable, Subscription } from "rxjs";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})

export class SettingsComponent implements AfterViewInit, OnDestroy {
  @Input() project: Project;
  public isLogged: boolean = false;
  private isLoggedInSub1: Subscription;
  private isJwtLoggedIn$: Observable<boolean>;

  constructor(private logger: NGXLogger, 
    private projectsService: ProjectsService,
    @Inject('IAuthJwtService') private authJwtService: IAuthJwtService) { 
      this.isJwtLoggedIn$ = authJwtService.isLoggedIn();
  }

  ngAfterViewInit(): void {
    const _this = this;
    this.isLoggedInSub1 = this.isJwtLoggedIn$.subscribe({
      next(isLogged) {
        _this.isLogged = isLogged;
      }
    });
  }

  ngOnDestroy(): void {
    this.isLoggedInSub1?.unsubscribe();
  }

  public saveSettings(id: number, ev: MatCheckboxChange) {
    if (id === 2 && !this.project.showPlantationNames) {
      this.project.showPlantationVarieties = false;
    }
    if (id === 3 && this.project.showPlantationVarieties) {
      this.project.showPlantationNames = true;
    }
    if (this.isLogged) {      
      this.projectsService.updateProject(this.project);
    }
  }

  public savePosition() {
    this.project.positionLocked = true;
    this.projectsService.updateProject(this.project);
  }
}
