import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PlantsService } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { PlantationsListComponent } from './plantations-list.component';
import { PlantationsService, ProjectsService } from 'projects';
import { PermaguildesDialogsService } from 'Apps/Permaguildes/src/app/services/permaguildes-dialogs/permaguildes-dialogs.service';

describe(PlantationsListComponent.name, () => {
  const test = new PermaguildesTest<PlantationsListComponent>();
  test.initComponent(PlantationsListComponent, {
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    providers: [ ProjectsService, PlantationsService, PlantsService, PermaguildesDialogsService ]
  });
});
