import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Inject, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { PlantsService, VarietiesService } from 'plants';
import { GroupPlantations, Plantation, Project, PlantationsService, GroupsService } from 'projects';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { BehaviorSubject, lastValueFrom, Observable, Subscription, tap } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { PermaguildesDialogsService } from 'Apps/Permaguildes/src/app/services/permaguildes-dialogs/permaguildes-dialogs.service';
import { DragAndDropEvent } from '../../../services/drag-and-drop/drag-and-drop.service';
import { IAuthJwtService, RealtimeEntitiesEvent } from '@cfeltz/angular-helpers';
import { SelectionModel } from '@angular/cdk/collections';

export class PlantationsTreeNode {
  codeGroup: number;
  codePlantation?: number;
  name: string;
  plantations?: PlantationsTreeNode[];
  isAddGroupNode: boolean = false;
}


@Component({
  selector: 'app-plantations-list',
  templateUrl: './plantations-list.component.html',
  styleUrls: ['./plantations-list.component.scss']
})

export class PlantationsListComponent implements OnInit, OnDestroy {
  readonly defaultLimit = 25;
  readonly defaultLoading = 25;
  public limit = this.defaultLimit;
  private treeData: PlantationsTreeNode[];
  public treeControl = new NestedTreeControl<PlantationsTreeNode>(node => node.plantations);
  public dataSource = new MatTreeNestedDataSource<PlantationsTreeNode>();
  private dataChange = new BehaviorSubject<PlantationsTreeNode[]>([]);
  private selection = new SelectionModel<PlantationsTreeNode>(true);

  private isLogged: boolean = false;
  private isLoggedInSub1: Subscription;
  private isJwtLoggedIn$: Observable<boolean>;

  @Output() plantationSelect = new EventEmitter<Plantation>();
  @Output() plantationUnselect = new EventEmitter<Plantation>();
  @Output() plantationVisibilityChange = new EventEmitter<Plantation>();

  @Input() maxWidth: number;
  @Input() dragEnable: boolean = false;
  @Input() project: Project;
  @ViewChild('newGroupValue', { static: false }) newGroupValue: ElementRef;

  constructor(private plantationsService: PlantationsService, 
    private groupsService: GroupsService, 
    private plantsService: PlantsService, 
    private varietiesService: VarietiesService, 
    private dialogsService: PermaguildesDialogsService,
    private changeDetector: ChangeDetectorRef,
    @Inject('IAuthJwtService') private authJwtService: IAuthJwtService, 
    private toastr: ToastrService) { 
      const _this = this;
      this.isJwtLoggedIn$ = authJwtService.isLoggedIn();
      this.isLoggedInSub1 = this.isJwtLoggedIn$.subscribe({
        next(isLogged) {
          _this.isLogged = isLogged;
        }
      });
  }

  ngOnInit(): void {
    this.dataChange.subscribe(data => {
      this.dataSource.data = data;
    });
    this.treeData = this.buildTree();
    this.dataChange.next(this.treeData);

    this.subscribeToRealtimeEvents();
  }

  ngOnDestroy(): void {
    this.isLoggedInSub1?.unsubscribe();
  }

  //#region Groups and plantations search
  public getPlantationFromNode(plantationNode: PlantationsTreeNode) {
    return this.project.plantations.find(g => g.code == plantationNode.codePlantation);
  }

  private getGroupNode(codeGroup: number): PlantationsTreeNode {
    return this.treeData.find(g => g.codeGroup == codeGroup && !g.isAddGroupNode);
  }

  private getPlantationNode(codePlantation: number, groupNode?: PlantationsTreeNode): PlantationsTreeNode {
    let plantationNode = undefined;
    this.treeData.forEach(g => {
      if (groupNode === undefined || g === groupNode) {
        g.plantations?.forEach(p => {
          if (p.codePlantation === codePlantation) {
            plantationNode = p;
            return;
          }
        });
        if (plantationNode) {
          return;
        }
      }
    });

    return plantationNode;
  }
  //#endregion

  //#region Realtime events
  private subscribeToRealtimeEvents() {
    this.plantationsService.realtimeEvent.subscribe((realtimeEvent: RealtimeEntitiesEvent<Plantation, number>) => {
      if (realtimeEvent) {
        const plantation = realtimeEvent.obj;
        switch (realtimeEvent.eventType) {
          case "Added": {
            const groupNode = this.getGroupNode(plantation.codeGroup);
            groupNode.plantations.push(this.buildPlantationNode(plantation));
            this.dataChange.next([]);
            this.dataChange.next(this.treeData);
            break;
          }
          case "Updated": {
            const plantationNode = this.getPlantationNode(plantation.GetKey());
            this.refreshPlantationName(plantationNode, plantation);
            if (plantationNode.codeGroup !== plantation.codeGroup) {
              // Remove plantation node from current group node
              const oldGroupNode = this.getGroupNode(plantationNode.codeGroup);
              oldGroupNode.plantations.splice(oldGroupNode.plantations.indexOf(plantationNode), 1);

              // Add plantation node in new group node
              const newGroupNode = this.getGroupNode(plantation.codeGroup);
              newGroupNode.plantations.push(plantationNode);
            
              // Expand node if closed
              if (!this.treeControl.isExpanded(newGroupNode)) {
                this.treeControl.expand(newGroupNode)
              }

              // Refresh
              plantationNode.codeGroup = plantation.codeGroup;
              this.dataChange.next([]);  
              this.dataChange.next(this.treeData);
            }
            break;
          }
          case "Removed": {
            // Remove plantation node from current group node
            const groupNode = this.getGroupNode(plantation.codeGroup);
            const plantationNode = this.getPlantationNode(plantation.GetKey(), groupNode);
            groupNode.plantations.splice(groupNode.plantations.indexOf(plantationNode), 1);
            this.dataChange.next([]);
            this.dataChange.next(this.treeData);
            this.unselectPlantationNode(plantationNode);
            break;
          }  
        }
      }
    });
  }
  //#endregion

  //#region Build tree
  private buildTree(): PlantationsTreeNode[] {
    const nodes: PlantationsTreeNode[] = [];
    if (this.project) {
      this.project.groups.forEach(g => {
        const node = this.buildGroupNode(g);
        nodes.push(node);
      });

      // Add ungrouped plantations
      const nodeMore = new PlantationsTreeNode();
      nodeMore.codeGroup = undefined;
      nodeMore.name = 'Groupe par détaut';
      const ungroupedPlantations = this.project.plantations.filter(p => p.codeGroup == undefined);
      nodeMore.plantations = this.buildPlantationsNodes(ungroupedPlantations);
      nodes.push(nodeMore);

      // Add new grouped button
      const nodeAdd = new PlantationsTreeNode();
      nodeAdd.codeGroup = undefined;
      nodeAdd.isAddGroupNode = true;
      nodeAdd.name = 'Ajouter un group';
      nodes.push(nodeAdd);
    }
    
    return nodes;
  }

  private buildGroupNode(group: GroupPlantations): PlantationsTreeNode {
    const node = new PlantationsTreeNode();
    node.codeGroup = group.code;
    node.name = group.name;
    node.plantations = this.buildPlantationsNodes(group.plantations);
    return node;
  }

  private buildPlantationsNodes(plantations: Plantation[]): PlantationsTreeNode[] {
    const nodes: PlantationsTreeNode[] = [];
    plantations?.sort((a, b) => a.name.localeCompare(b.name)).forEach(p => {
      nodes.push(this.buildPlantationNode(p));
    });
    return nodes;
  }

  private buildPlantationNode(plantation: Plantation): PlantationsTreeNode {
    const node = new PlantationsTreeNode();
    node.codeGroup = plantation.codeGroup;
    node.codePlantation = plantation.code;
    this.refreshPlantationName(node, plantation);
    return node;
  }

  private refreshPlantationName(node: PlantationsTreeNode, plantation: Plantation) {
    node.name = plantation.name;
    if (plantation.codeVariety > 0) {
      let varietyName = this.varietiesService.getSingleDataByKeyFromCache(plantation.codePlant, plantation.codeVariety)?.GetShortName();
      if (varietyName) {
        if (varietyName.startsWith(node.name + ' ')) {
          varietyName = varietyName.substring(node.name.length + 1);
        }
        node.name += ` (${varietyName})`;
      } else if (plantation.codeVariety > 0) {
        lastValueFrom(this.varietiesService.getSingleDataByKey(plantation.codePlant, plantation.codeVariety, true).pipe(
          tap(variery => {
            this.refreshPlantationName(node, plantation);
          })
        ));
      }
    }
  }

  isExpandableNode = (_: number, node: PlantationsTreeNode) => (node.plantations != undefined);
  isAddGroupNode = (_: number, node: PlantationsTreeNode) => (node.isAddGroupNode == true);
  //#endregion
  
  //#region Selection
  private selectPlantationNode(node: PlantationsTreeNode) {
    this.selection.select(node);
    this.changeDetector.detectChanges();
  }

  private unselectPlantationNode(node: PlantationsTreeNode) {
    this.selection.deselect(node);
    this.changeDetector.detectChanges();
  }

  private unselectAllPlantationNodes() {
    this.selection.clear();  
    this.changeDetector.detectChanges();
  }
  //#endregion

  //#region Actions
  public onClickPlantation(e: PointerEvent , plantationNode: PlantationsTreeNode) {
    if (!e.ctrlKey) {
      // Unselect all
      this.selection.selected.forEach(n => {
        this.plantationUnselect.emit(this.getPlantationFromNode(n));
      });
      this.unselectAllPlantationNodes();
    }
    this.selectPlantationNode(plantationNode);
    const plantation = this.getPlantationFromNode(plantationNode);
    this.plantationSelect.emit(plantation);
  }

  public getIconUrl(plantation: Plantation) {
    return 'assets/images/trees/' + this.plantsService.getIconSrc(plantation.iconSrc, plantation.codeStratum);
  }

  public async addGroup(groupName: string) {
    const newGroup = await this.groupsService.addGroupInProject(this.project.code, groupName);
    // Add this group in treeview
    const node = this.buildGroupNode(newGroup);
    this.treeData.splice(this.treeData.length - 2, 0, node);
    this.dataChange.next(this.treeData);
    this.newGroupValue.nativeElement.value = "";
  }

  public removeGroup(groupNode: PlantationsTreeNode) {
    if (groupNode.plantations.length > 0) {
      this.toastr.error("Impossible de supprimer un groupe dans lequel des plantations sont  présentes");
    } else {
      this.groupsService.removeGroupFromProject(this.project.code, groupNode.codeGroup);
      this.treeData.splice(this.treeData.indexOf(groupNode), 1);
      this.dataChange.next(this.treeData);
    }
  }

  public updateGroup(groupNode: PlantationsTreeNode) {
    const group = this.project.groups.find(g => g.code == groupNode.codeGroup);
    const sub = this.dialogsService.openGroupDetailDialog(this.project, group).subscribe(newGroup => {
      if (newGroup) {
        groupNode.name = newGroup.name;
        this.dataChange.next(this.treeData);
      }
      sub.unsubscribe();
    });
  }

  public removePlantation(plantationNode: PlantationsTreeNode) {
    this.plantationsService.removePlantationFromProject(this.project.code, plantationNode.codePlantation);
  }

  public updatePlantation(plantationNode: PlantationsTreeNode) {
    const plantation = this.getPlantationFromNode(plantationNode);
    this.dialogsService.openPlantationDetailDialog(this.project.code, plantation);
  }

  public onDrop(ev: DragAndDropEvent, codeGroup: number) {
    if (ev.dragData.constructor.name === 'Array') {
      ev.dragData.forEach(node => {
        if (node.constructor.name === PlantationsTreeNode.name && node.codePlantation) {
          this.onDropPlantationNode(node, codeGroup);
        }
      });
    }
  }

  private onDropPlantationNode(plantationNode: PlantationsTreeNode, codeGroup: number) {
    if (plantationNode.codeGroup != codeGroup) {
      this.plantationsService.updatePlantationGroup(this.project, plantationNode.codePlantation, codeGroup);
    }
  }

  public openDetail(plantationNode: PlantationsTreeNode) {
    const plantation = this.getPlantationFromNode(plantationNode);
    this.dialogsService.openPlantDetailDialog(plantation.codePlant, plantation);
  }

  public changeVisibility(plantationNode: PlantationsTreeNode) {
    const plantation = this.getPlantationFromNode(plantationNode);
    plantation.visible = !plantation.visible;
    if (this.isLogged) {
      this.plantationsService.updatePlantation(this.project.code, plantation);
    }
    if (!plantation.visible) {
      this.unselectPlantationNode(plantationNode);
      this.plantationUnselect.emit(plantation);
    }
    this.plantationVisibilityChange.emit(plantation);
  }
  //#endregion

  //#region External actions
  public selectPlantation(plantation: Plantation) {
    if (plantation) {
      const node = this.getPlantationNode(plantation.code);
      if (!this.selection.isSelected(node)) {
        this.selectPlantationNode(node);
        const groupNode = this.getGroupNode(plantation.codeGroup);
        this.treeControl.expand(groupNode);
        setTimeout(() => {
          const element = document.querySelector('app-plantations-list mat-tree mat-nested-tree-node > div > mat-tree-node > div.isPlantationNode.selected');
          if (element) {
            element.scrollIntoView();
          }
        });
      }
    } else {
      this.unselectAllPlantationNodes();
    }
  }

  public unselectPlantation(plantation: Plantation) {
    const node = this.getPlantationNode(plantation.code);
    if (this.selection.isSelected(node)) {
      this.unselectPlantationNode(node);
    }
  }
  //#endregion
}
