import { Component, AfterViewInit, Input, Output, EventEmitter, Inject, OnDestroy, ChangeDetectorRef, ApplicationRef } from '@angular/core';
import * as L from 'leaflet';
import * as Log from "loglevel";
import {Services, LExtended} from 'geoportal-extensions-leaflet';
import { Project, Plantation, PlantationsService, ProjectsService, ShapesService, Shape } from 'projects';
import { DragAndDropEvent } from '../../../services/drag-and-drop/drag-and-drop.service';
import { PlantsService, Plant, VarietiesService, RootstocksService } from 'plants';
import { NGXLogger } from 'ngx-logger';
import { IAuthJwtService, RealtimeEntitiesEvent } from '@cfeltz/angular-helpers';
import { SelectionModel } from '@angular/cdk/collections';
import { lastValueFrom, map, Observable, Subscription } from 'rxjs';
import { CFeltzLeafletExtension } from '../../../classes/leaflet-color-picker';
import { ToastrService } from 'ngx-toastr';
import { PermaguildesDialogsService } from '../../../services';

@Component({
  selector: 'app-project-map',
  templateUrl: './project-map.component.html',
  styleUrls: ['./project-map.component.scss'],
})
export class ProjectMapComponent implements AfterViewInit, OnDestroy {
  private map;
  private geoportailKey = 'essentiels';
  private readonly ratioLat = 0.00001; // = 1m
  private readonly ratioLng = 0.000025; // = 1m
  @Input() project: Project;
  @Output() plantationSelect = new EventEmitter<Plantation>();
  @Output() plantationUnselect = new EventEmitter<Plantation>();
  private defaultLatitude: number;
  private defaultLongitude: number;
  private defaultZoom: number;
  private selectingPlantation: Plantation;
  private selection = new SelectionModel<Plantation>(true);
  private copiedPlantations: Plantation[];
  private lastLatLngMouseUp: any;
  private lastLatLngMouseUpCtrlCBackup: any;
  private shapesLayers: any;
  private defaultShapeColor = '#3388ff';
  private searchControl: any;
  private drawControl: any;
  private colorPickerControl: any;
  private drawControlOptions = {
    polyline: { shapeOptions: { color: this.defaultShapeColor } },
    polygon: { shapeOptions: { color: this.defaultShapeColor } },
    rectangle: { shapeOptions: { color: this.defaultShapeColor } },
    circle: { shapeOptions: { color: this.defaultShapeColor } },
    marker: false,
    circlemarker: false
  };

  public isLogged: boolean = false;
  private isLoggedInSub1: Subscription;
  private isJwtLoggedIn$: Observable<boolean>;

  //#region Initialization
  constructor(private logger: NGXLogger, 
    private projectsService: ProjectsService, 
    private plantationsService: PlantationsService, 
    private plantsService: PlantsService,
    private varietiesService: VarietiesService, 
    private rootstocksService: RootstocksService, 
    private shapesService: ShapesService,
    private toastr: ToastrService, 
    private dialogsService: PermaguildesDialogsService,
    private changeDetectorRef: ChangeDetectorRef,
    @Inject('IAuthJwtService') private authJwtService: IAuthJwtService) { 
      this.isJwtLoggedIn$ = authJwtService.isLoggedIn();
  }

  ngAfterViewInit(): void {
    if (!this.project) {
      this.defaultLatitude = Project.DEFAULT_LATITUDE;
      this.defaultLongitude = Project.DEFAULT_LONGITUDE;
      this.defaultZoom = Project.DEFAULT_ZOOM;
    } else {
      this.defaultLatitude = this.project.latitude;
      this.defaultLongitude = this.project.longitude;
      this.defaultZoom = this.project.zoom;
    }

    this.initMapWithGeoportail();
    // this.initMapWithOpenStreetMap();

    this.loadVarietiesOfPlantations();
  }

  ngOnDestroy(): void {
    this.isLoggedInSub1?.unsubscribe();
  }

  private async loadVarietiesOfPlantations() {
    if (this.project.plantations && this.project.plantations.length > 0) {
      const plantationsWithVarieties = this.project.plantations.filter(p => p.codeVariety > 0);
      const plantationsWithRootstocks = this.project.plantations.filter(p => p.codeRootstock > 0);
      const codePlantsForVarieties = plantationsWithVarieties.map(p => p.codePlant);
      const codeVarieties = plantationsWithVarieties.map(p => p.codeVariety);
      const codePlantsForRootstocks = plantationsWithRootstocks.map(p => p.codePlant);
      const codeRootstocks = plantationsWithRootstocks.map(p => p.codeRootstock);
      if (codeVarieties.length > 0 || codeRootstocks.length > 0) {
        await Promise.all([
          lastValueFrom(this.varietiesService.getMultipleDataByKeys(codePlantsForVarieties, codeVarieties, true)), 
          lastValueFrom(this.rootstocksService.getMultipleDataByKeys(codePlantsForRootstocks, codeRootstocks, true))
        ]);

        this.project.plantations?.map(plantation => {
          if (plantation.codeVariety || plantation.codeRootstock) {
            this.createMapLabels(plantation, false, true);
          }
        });
      }
    }
  }

  private async loadPlantOfPlantation(plantation: Plantation) {
    const plant = await lastValueFrom(this.plantsService.getSingleDataByKey(plantation.codePlant));
    plantation.plant = plant;
    if (plantation.codeVariety || plantation.codeRootstock) {
      this.createMapLabels(plantation);
    }
  }


  private initMapWithOpenStreetMap(): void {
    this.map = L.map('map', {
      center: L.latLng(this.defaultLatitude, this.defaultLongitude),
      zoom: this.defaultZoom,
      maxZoom: 25
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 25,
      maxNativeZoom: 19,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);

    this.initShapesLayers();
  }

  private initMapWithGeoportail(): void {
    Services.getConfig({
        apiKey : this.geoportailKey,
        timeOut : 20000,
        onSuccess : () => {
          this.createGeoportailMap();
        }
    });
  }

  private addLayers(): void {
    // See https://geoservices.ign.fr/ressources_documentaires/Espace_documentaire/Bouquets_de_ressources_Geoservices_IGN.pdf
    var layerOrtho = LExtended.geoportalLayer.WMTS({ layer : "ORTHOIMAGERY.ORTHOPHOTOS" }, { maxZoom: 25, maxNativeZoom: 19 });
    var layerCadastral = LExtended.geoportalLayer.WMTS({ layer : "CADASTRALPARCELS.PARCELLAIRE_EXPRESS" }, { maxZoom: 25, maxNativeZoom: 19 });
    var layerPlan = LExtended.geoportalLayer.WMTS({ layer : "GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2" }, { maxZoom: 25, maxNativeZoom: 19 });
    // var layerAlti = LExtended.geoportalLayer.WMTS({ layer : "ELEVATIONGRIDCOVERAGE.HIGHRES" }, { maxZoom: 25, maxNativeZoom: 19 });

    var layerSwitcher = LExtended.geoportalControl.LayerSwitcher({
      collapsed : this.isMobile(),
      layers : [
        { layer : layerPlan, 
          config: {
            visibility : this.project.zoom < 17
          }
        }, 
        { 
          layer : layerOrtho, 
          config: { 
            visibility : this.project.zoom >= 17
          } 
        }, 
        // { layer : layerAlti }, 
        {
          layer : layerCadastral,
          config : {
            title : "Cadastre",
            description : "Parcelles cadastrales",
            visibility : true
          }
        }
      ]
    });
    this.map.addControl(layerSwitcher);
  }

  //#region Shapes
  private initShapesLayers() {
    const _this = this;
    this.loadShapes();    
    
    this.drawControl = new L.Control.Draw({
      draw: this.drawControlOptions,
      edit: {
        featureGroup: this.shapesLayers
      }
    });
    this.map.on(L.Draw.Event.CREATED, function (e) {
      const layer = e.layer;    
      const shape = _this.layerToShape(layer);
      _this.shapesService.addShapeInProject(shape.codeProject, shape);
    });
    this.map.on(L.Draw.Event.EDITED, function (e) {
      e.layers.eachLayer(layer => {
        const shape = _this.layerToShape(layer);
        _this.shapesService.updateShape(_this.project.code, shape);
      });
    });
    this.map.on(L.Draw.Event.DELETED, function (e) {
      e.layers.eachLayer(layer => {
        const shape = _this.layerToShape(layer);
        _this.shapesService.removeShapeFromProject(_this.project.code, shape.code);
      });
    });
  }

  private layerToShape(layer: any) {
    //#region Clean after update
    if (layer.options.original) {
      layer.options = layer.options.original;
    }
    if (layer.options.onEachFeature) {
      delete layer.options.onEachFeature;
    }
    if (layer.options.pointToLayer) {
      delete layer.options.pointToLayer;
    }
    //#endregion

    const geoJson = layer.toGeoJSON();
    const properties = layer.options;
    if (properties.radius) {
      properties.radius = layer.getRadius();
    }
    return new Shape(properties.code, this.project.code, 
      geoJson.geometry.type, 
      JSON.stringify(geoJson.geometry.coordinates),
      properties.color, 
      properties.fillColor, 
      properties.fill, 
      properties.opacity, 
      properties.fillOpacity, 
      properties.stroke, 
      properties.weight, 
      false, 
      properties.radius);
  }

  private loadShapes() {
    const _this = this;
    this.shapesLayers = new L.FeatureGroup();
    lastValueFrom(this.shapesService.getShapesFromProject(this.project.code).pipe(
      map(shapes => {
        this.project.shapes.forEach(shape => {
          this.addLayerFromShape(shape);
        });
        this.map.addLayer(this.shapesLayers);
        this.shapesLayers.bringToBack();
      })
    ));
    this.map.on("zoomend", function () {
      _this.shapesLayers?.eachLayer(function(layer) {
        _this.updateWeightFromFeet(layer);
      });
    });
  }

  private addLayerFromShape(shape: Shape): any {
    const properties: any = {
      code: shape.code,
      clickable: false,
      lineCap: 'butt',
      color: shape.color, 
      fillColor: shape.fillColor, 
      fill: shape.fill, 
      opacity: shape.opacity, 
      fillOpacity: shape.fillOpacity, 
      stroke: shape.stroke, 
      weight: shape.weight, 
      weightIsInFeet: shape.weightIsInFeet, 
      radius: shape.radius
    };
    if (shape.weightIsInFeet) {
      properties.weightInFeet = shape.weight;
    }
    properties.pointToLayer = (feature, latlng) => {
      if (feature.properties.radius) {
        return new L.Circle(latlng, feature.properties.radius, feature.properties);
      } else {
        return new L.Marker(latlng);
      }
    }
    properties.onEachFeature = (feature, layer) => {
      this.shapesLayers.addLayer(layer);
    }
    const layer = L.geoJson({
      type: shape.geometryType, 
      coordinates: JSON.parse(shape.coordinates), 
      properties: properties
    }, properties);

    this.updateWeightFromFeet(layer);

    return layer;
  }

  private updateWeightFromFeet(layer: any)
  {
    if (layer.options.weightIsInFeet) {
      // let weight = 2 * (this.project.zoom - 16) * layer.options.weightInFeet / 3;
      let weight = 1;
      let gap = this.project.zoom - 16;
      if (gap > 0) {
        let temp = 1;
        for (let i = 2; i <= gap; i++) {
          temp = 2 * temp;
        }
        weight = temp * layer.options.weightInFeet / 3;
      }
      layer.setStyle({
        weight: weight
      });
    }
  }
  //#endregion

  private addExtensions(): void {
    const _this = this;
    var mp = LExtended.geoportalControl.MousePosition();
    this.map.addControl(mp);
    this.isLoggedInSub1 = this.isJwtLoggedIn$.subscribe({
      next(isLogged) {
        _this.isLogged = isLogged;
        if (isLogged) {
          if (!_this.drawControl) {
            _this.initShapesLayers();
          }
          _this.map.addControl(_this.drawControl);
          if (!_this.searchControl) {
            _this.searchControl = LExtended.geoportalControl.SearchEngine();
          }
          _this.map.addControl(_this.searchControl);
          if (!_this.colorPickerControl) {
            _this.colorPickerControl = new CFeltzLeafletExtension().getColorPicker(_this.defaultShapeColor, _this.drawControlOptions);
          }
          _this.map.addControl(_this.colorPickerControl);
        } else {
          if (!_this.drawControl) {
            _this.loadShapes();
          }
          if (_this.drawControl) {
            _this.map.removeControl(_this.drawControl);
          }
          if (_this.searchControl) {
            _this.map.removeControl(_this.searchControl);
          }
          if (_this.colorPickerControl) {
            _this.map.removeControl(_this.colorPickerControl);
          }
        }
      },
      error(msg) {
        _this.logger.error(msg);
      }
    });
    var measureProfil = LExtended.geoportalControl.ElevationPath();
    this.map.addControl(measureProfil);
  }
  //#endregion

  //#region Init map
  private createGeoportailMap() {
    const _this = this;

    // Création de la map
    this.map  = L.map('map', {
      zoom : this.defaultZoom,
      maxZoom: 25,
      center: L.latLng(this.defaultLatitude, this.defaultLongitude),
      wheelDebounceTime: 400,
      doubleClickZoom: true
    });

    //#region Mouse events
    this.map.on("mousedown", function (e) {
      _this.selectingPlantation = undefined;
    });

    this.map.on("mouseup", function (e) {
      if (!e.originalEvent.ctrlKey) {
        // Unselect all
        _this.selection.selected.forEach(p => _this.unselectPlantation(p));
      }
      _this.selectPlantation(_this.selectingPlantation);

      if (_this.selectingPlantation === undefined && _this.isMobile()) {
        _this.map.removeEventListener('touchstart');
        _this.map.dragging.enable();
      }
      _this.lastLatLngMouseUp = e.latlng;
    });

    this.map.on("moveend", function () {
      _this.project.latitude = _this.map.getCenter().lat;
      _this.project.longitude = _this.map.getCenter().lng;
      if (_this.isLogged && !_this.project.positionLocked) {
        _this.projectsService.updateProject(_this.project);
      }
    });
    
    this.map.on("zoomend", function () {
      _this.project.zoom = _this.map.getZoom();
      if (_this.isLogged && !_this.project.positionLocked) {
        _this.projectsService.updateProject(_this.project);
      }
      _this.changeDetectorRef.detectChanges();
    });
    //#endregion

    //#region Keyboard events
    this.map.on("keyup", function (event) {
      if(event.originalEvent.key === 'Delete') {
        const codes = _this.selection.selected.map(p => p.code);
        _this.plantationsService.removePlantationsFromProject(_this.project.code, codes);
      }
      if(event.originalEvent.ctrlKey) {
        if (event.originalEvent.code === 'KeyC') {
          _this.copiedPlantations = _this.selection.selected;
          _this.lastLatLngMouseUpCtrlCBackup = _this.lastLatLngMouseUp;
          _this.logger.debug(`Copy ${_this.copiedPlantations.length} plantations`);
        }
        if (event.originalEvent.code === 'KeyV' && _this.copiedPlantations?.length > 0) {
          _this.logger.debug(`Paste ${_this.copiedPlantations.length} plantations`);
          _this.copiedPlantations.forEach(p => {
            const copy = p.copy();
            copy.latitude = p.latitude + (_this.lastLatLngMouseUp.lat - _this.lastLatLngMouseUpCtrlCBackup.lat);
            copy.longitude = p.longitude + (_this.lastLatLngMouseUp.lng - _this.lastLatLngMouseUpCtrlCBackup.lng);
            _this.plantationsService.addPlantationInProject(_this.project.code, copy);
          });
        }
      }
    });
    //#endregion
    
    this.addLayers();
    this.addExtensions();
    this.drawPlantations();

    this.subscribeToRealtimeEvents();

    // Desactivate Geoportail layer logger
    Log.getLogger('layer-event').disableAll();
    Log.getLogger('layerswitcher').disableAll();

    this.logger.info('Map created');
  }
  //#endregion

  //#region Drawing
  private drawPlantations(): void {
    this.project.plantations.forEach(plantation => {
      this.drawPlantation(plantation);
    });
  }

  private generateBounds(latitude: number, longitude: number, height: number, span: number) {
    const corner1 = L.latLng(latitude + height * this.ratioLat, longitude - span / 2 * this.ratioLng);
    const corner2 = L.latLng(latitude, longitude + span / 2 * this.ratioLng);
    const bounds = L.latLngBounds(corner1, corner2);
    return bounds;
  }

  private drawPlantation(plantation: Plantation) {
    this.createMapImages(plantation);
    this.createMapLabels(plantation);  
  }

  private createMapImages(plantation: Plantation) {
    let iconSrc = plantation.iconSrc;
    if (!iconSrc) {
      iconSrc = this.plantsService.getPlantFromLocalList(plantation.codePlant)?.iconSrc;
    }
    const iconFilename = this.plantsService.getIconSrc(iconSrc, plantation.codeStratum);
    const imageUrl = `assets/images/trees/${iconFilename}`;
    const sizeUnits = Plant.getSizeUnitsByStrate(plantation.codeStratum);
    const unitsRatio = (sizeUnits == 'cm' ? 0.01 : 1);
    const heightInMeters = plantation.height * unitsRatio;
    const spanInMeters = plantation.span * unitsRatio;
    const imageBounds = this.generateBounds(plantation.latitude, plantation.longitude, heightInMeters, spanInMeters);

    if (plantation.mapSpanCircle) {
      this.map.removeLayer(plantation.mapSpanCircle);
    }
    if (plantation.mapTrunkCircle) {
      this.map.removeLayer(plantation.mapTrunkCircle);
    }
    if (plantation.mapImage) {
      this.map.removeLayer(plantation.mapImage);
    }

    plantation.mapSpanCircle = L.circle([plantation.latitude, plantation.longitude], {
      interactive: false,
      color: '#007700', 
      opacity: 0.1,
      radius: plantation.span * unitsRatio
    }).addTo(this.map);
    let truncRadius = plantation.span * unitsRatio / 5;
    if (truncRadius > 0.8) {
      truncRadius = 0.8;
    }
    plantation.mapTrunkCircle = L.circle([plantation.latitude, plantation.longitude], {
      color: '#C8781E', 
      opacity: 0.8,
      fillOpacity: 0.8,
      radius: truncRadius
    }).addTo(this.map);
    plantation.mapImage = L.imageOverlay(imageUrl, imageBounds, { interactive: true }).addTo(this.map);
    plantation.mapImage.setZIndex(1000);

    plantation.mapSpanCircle.plantation = plantation;
    plantation.mapTrunkCircle.plantation = plantation;
    plantation.mapImage.plantation = plantation;
    plantation.mapImage.getElement().classList.add('mapImage')
    plantation.mapSpanCircle.getElement().classList.add('mapCircle')
    plantation.mapTrunkCircle.getElement().classList.add('mapCircle')

    if (spanInMeters >= 2) {
      plantation.mapImage.getElement().classList.add('largePlant')
    } else {
      plantation.mapImage.getElement().classList.add('smallPlant')
    }

    if (this.selection.isSelected(plantation)) {
      plantation.mapImage.getElement().classList.add('selected')
      plantation.mapSpanCircle.getElement().classList.add('selected')
      plantation.mapTrunkCircle.getElement().classList.add('selected')
    }
    if (!plantation.visible) {
      plantation.mapImage.getElement().classList.add('hidden')
      plantation.mapSpanCircle.getElement().classList.add('hidden')
      plantation.mapTrunkCircle.getElement().classList.add('hidden')
    }

    this.setMouseHandler(plantation.mapImage);
    this.setMouseHandler(plantation.mapTrunkCircle);
  }

  private createMapLabels(plantation: Plantation, createLabel: boolean = true, createFullLabel: boolean = true) {
    if (this.map) {
      const sizeUnits = Plant.getSizeUnitsByStrate(plantation.codeStratum);
      const unitsRatio = (sizeUnits == 'cm' ? 0.01 : 1);
      const spanInMeters = plantation.span * unitsRatio;

      if (plantation.mapLabel && createLabel) {
        this.map.removeLayer(plantation.mapLabel);
      }
      if (plantation.mapFullLabel && createFullLabel) {
        this.map.removeLayer(plantation.mapFullLabel);
      }

      if (createLabel) {
        const mapLabelDiv = L.divIcon({
          html: `<span>${plantation.getShortName()}</span>`,
          className: 'mapLabel',
          iconSize: "auto"
        });
        plantation.mapLabel = L.marker(L.latLng(plantation.latitude, plantation.longitude), {
          icon: mapLabelDiv,
          alt:  plantation.name
        }).addTo(this.map);

        if (spanInMeters >= 2) {
          plantation.mapLabel.getElement().classList.add('largePlant')
        } else {
          plantation.mapLabel.getElement().classList.add('smallPlant')
        }

        if (this.selection.isSelected(plantation)) {
          plantation.mapLabel.getElement().classList.add('selected')
        }
        if (!plantation.visible) {
          plantation.mapLabel.getElement().classList.add('hidden')
        }
      }

      if (createFullLabel) {
        const mapFullLabelDiv = L.divIcon({
          html: `<div>${plantation.getShortName()}</div><div>${this.getDetailledLabel(plantation, true)}</div>`,
          className: 'mapFullLabel',
          iconSize: "auto"
        });
        plantation.mapFullLabel = L.marker(L.latLng(plantation.latitude, plantation.longitude), {
          icon: mapFullLabelDiv,
          alt:  plantation.name
        }).addTo(this.map);

        if (spanInMeters >= 2) {
          plantation.mapFullLabel.getElement().classList.add('largePlant')
        } else {
          plantation.mapFullLabel.getElement().classList.add('smallPlant')
        }

        if (this.selection.isSelected(plantation)) {
          plantation.mapFullLabel.getElement().classList.add('selected')
        }
        if (!plantation.visible) {
          plantation.mapFullLabel.getElement().classList.add('hidden')
        }
      }
    }
  }

  private getDetailledLabel(plantation: Plantation, short: boolean = false): string {
    let returnValue: string = '';

    let varietyName = this.varietiesService.getSingleDataByKeyFromCache(plantation.codePlant, plantation.codeVariety)?.GetShortName();
    let rootstockName = this.rootstocksService.getSingleDataByKeyFromCache(plantation.codePlant, plantation.codeRootstock)?.GetShortName();

    if (!varietyName) {
      const idxOpenParenthesis = plantation.name.indexOf('(');
      const idxCloseParenthesis = plantation.name.indexOf(')');
      if (idxOpenParenthesis > 0 && idxCloseParenthesis > 0) {
        varietyName = plantation.name.substring(idxOpenParenthesis, idxCloseParenthesis + 1);
      }
    }

    if (rootstockName) {
      // Keep just 'Franc' instead of 'Franc de...'
      if (rootstockName.endsWith(` d'` + plantation.name.toLowerCase())) {
        rootstockName = rootstockName.substring(0, rootstockName.length - plantation.name.length - 3)
      } else if (rootstockName.endsWith(` de ` + plantation.name.toLowerCase())) {
        rootstockName = rootstockName.substring(0, rootstockName.length - plantation.name.length - 4)
      } else if (rootstockName.startsWith(`Franc de `)) {
        rootstockName = rootstockName.substring(`Franc de `.length);
        rootstockName = rootstockName[0].toUpperCase() + rootstockName.substring(1);
      }
    }

    if (varietyName && rootstockName) {
      returnValue = `${varietyName} (${rootstockName})`;
    } else if (varietyName) {
      returnValue = varietyName;
    } else if (rootstockName) {
      returnValue = rootstockName;
    }
    if (short) {
      if (returnValue.startsWith(plantation.name + ' ')) {
        returnValue = returnValue.substring(plantation.name.length + 1);
      }
    }
    return returnValue;
  }

  private isMobile() {
    return window.matchMedia('(pointer: coarse)').matches;
  }

  private showToastMessageFromPlantation(plantation: Plantation) {
    const title = plantation.name;
    let message = `Cliquez pour plus d'infos`;
    const getDetailledLabel = this.getDetailledLabel(plantation);
    if (getDetailledLabel != '') {
      message = getDetailledLabel + `...`;
    }

    this.toastr.clear();
    this.toastr.info(message, title, {
      positionClass: 'toast-bottom-center',
      toastClass: 'ngx-toastr toast-info toast-info-black',
      onActivateTick: true
    }).onTap.subscribe(() => {
      this.dialogsService.openPlantDetailDialog(plantation.codePlant, plantation);
    });
  }

  private setMouseHandler(layer: any) {
    const _this = this;
    const plantation = layer.plantation;
    let moving = false;
    let selection: Plantation[];
    let lastLatLng = undefined;

    const touchend = (e) => {
      if (_this.isMobile()) {
        window.removeEventListener('touchmove', mouseOrTouchMove);
        window.removeEventListener('touchend', touchend);
      }
    };

    const touchstart = (e) => {
      window.addEventListener('touchmove', mouseOrTouchMove);
      window.addEventListener('touchend', touchend);
    };

    const mousedown = (e) => {
      lastLatLng = e.latlng;
      if (_this.getSelectedPlantations().indexOf(plantation) == -1) {
        _this.showToastMessageFromPlantation(plantation);
      }
      _this.selectPlantation(plantation);
      if (_this.isLogged && !_this.project.freezePlantations) {
        moving = false;
        _this.map.dragging.disable();
        selection = (e.originalEvent.ctrlKey ? _this.selection.selected : [plantation]);

        if (_this.isMobile()) {
          _this.map.removeEventListener('touchstart');
          _this.map.on('touchstart', touchstart);
        } else {
          _this.map.on('mousemove', mouseOrTouchMove);
        }
      }
    };

    const mouseOrTouchMove = (e) => {
      let latlng = e.latlng;
      if (!latlng) {
        // touchmove (from mobile)
        const containerPoint = _this.map.mouseEventToContainerPoint(e.touches[0]);
        const layerPoint = _this.map.containerPointToLayerPoint(containerPoint);
        latlng = _this.map.layerPointToLatLng(layerPoint);
      }
      moving = true;
      selection.forEach(p => {
        p.latitude += latlng.lat - lastLatLng.lat;
        p.longitude += latlng.lng - lastLatLng.lng;
        _this.refreshPlantationPosition(p);
      });
      lastLatLng = latlng;
    };

    const mouseup = (e) => {
      if (_this.isLogged) {
        if (moving) {
          selection.forEach(p => {
            _this.plantationsService.updatePlantationPosition(_this.project, p.code, p.latitude, p.longitude);
          });
        }

        if (!_this.isMobile()) {
          _this.map.removeEventListener('mousemove');
          _this.map.dragging.enable();
        }

      }
      _this.selectingPlantation = plantation;
    }

    layer.on({
      mousedown: mousedown, 
      mouseup: mouseup
    }); 
  }
  //#endregion

  //#region Realtime events
  private refreshPlantationPosition(plantation: Plantation) {
    const sizeUnits = Plant.getSizeUnitsByStrate(plantation.codeStratum);
    const unitsRatio = (sizeUnits == 'cm' ? 0.01 : 1);
    const imageBounds = this.generateBounds(plantation.latitude, plantation.longitude, plantation.height *  unitsRatio, plantation.span *  unitsRatio);
    plantation.mapImage.setBounds(imageBounds);
    plantation.mapSpanCircle.setLatLng(L.latLng(plantation.latitude, plantation.longitude));
    plantation.mapTrunkCircle.setLatLng(L.latLng(plantation.latitude, plantation.longitude));
    plantation.mapLabel.setLatLng(L.latLng(plantation.latitude, plantation.longitude));
    plantation.mapFullLabel.setLatLng(L.latLng(plantation.latitude, plantation.longitude));
  }

  private subscribeToRealtimeEvents() {
    //#region Plantations
    this.plantationsService.beforeRealtimeEvent.subscribe((realtimeEvent: RealtimeEntitiesEvent<Plantation, number>) => {
      if (realtimeEvent) {
        switch (realtimeEvent.eventType) {
          case "Removed":
            const plantation = this.project.getPlantationByCode(realtimeEvent.obj.GetKey());
            this.unselectPlantation(plantation);
            if (this.selectingPlantation = plantation) {
              this.selectingPlantation = undefined;
            }
            this.map.removeLayer(plantation.mapImage);
            this.map.removeLayer(plantation.mapSpanCircle);
            this.map.removeLayer(plantation.mapTrunkCircle);
            this.map.removeLayer(plantation.mapLabel);
            this.map.removeLayer(plantation.mapFullLabel);
            break;
        }
      }
    });

    this.plantationsService.realtimeEvent.subscribe((realtimeEvent: RealtimeEntitiesEvent<Plantation, number>) => {
      if (realtimeEvent) {
        const plantation = this.project.getPlantationByCode(realtimeEvent.obj.GetKey());
        switch (realtimeEvent.eventType) {
          case "Added":
            this.loadPlantOfPlantation(plantation);
            this.drawPlantation(plantation);
            break;
          case "Updated":
            this.refreshPlantationPosition(plantation);
            this.createMapImages(plantation);
            this.createMapLabels(plantation);

            const p: Promise<any>[] = [];
            if (plantation.codeVariety > 0 && this.varietiesService.getSingleDataByKeyFromCache(plantation.codePlant, plantation.codeVariety) == undefined) {
              p.push(lastValueFrom(this.varietiesService.getSingleDataByKey(plantation.codePlant, plantation.codeVariety, true)));
            }
            if (plantation.codeRootstock > 0 && this.rootstocksService.getSingleDataByKeyFromCache(plantation.codePlant, plantation.codeRootstock) == undefined) {
              p.push(lastValueFrom(this.rootstocksService.getSingleDataByKey(plantation.codePlant, plantation.codeRootstock, true)));
            }
            
            if (p.length > 0) {
              Promise.all(p).then(() => {
                this.createMapLabels(plantation, false, true);
              });
            }
            break;
        }
      }
    });
    //#endregion Plantations

    //#region Shapes
    this.shapesService.beforeRealtimeEvent.subscribe((realtimeEvent: RealtimeEntitiesEvent<Shape, number>) => {
      if (realtimeEvent) {
        switch (realtimeEvent.eventType) {
          case "Removed":
            // Remove from another user
            const shape = this.project.getShapeByCode(realtimeEvent.obj.GetKey());
            if (shape) {
              this.shapesLayers.eachLayer(layer => {
                if (layer.feature.properties.code == shape.code) {
                  this.shapesLayers.removeLayer(layer);
                }
              });
            }
            break;
        }
      }
    });

    this.shapesService.realtimeEvent.subscribe((realtimeEvent: RealtimeEntitiesEvent<Shape, number>) => {
      if (realtimeEvent) {
        const shape = this.project.getShapeByCode(realtimeEvent.obj.GetKey());
        switch (realtimeEvent.eventType) {
          case "Added":
            this.addLayerFromShape(shape);
            break;
          case "Updated":
            // Update from another user
            this.shapesLayers.eachLayer(layer => {
              if (layer.feature.properties.code == shape.code) {
                this.shapesLayers.removeLayer(layer);
                this.addLayerFromShape(shape);
              }
            });
            break;
        }
      }
      //#endregion
    });
  }
  //#endregion

  //#region External actions
  public onDrop(ev: DragAndDropEvent) {
    if (ev.dragData.constructor.name == Plant.name) {
      let latLng = this.map.mouseEventToLatLng(ev.mouseEvent);
      this.addPlant(ev.dragData, latLng.lat, latLng.lng);
    }
  }

  public addPlant(plant: Plant, latitude: number, longitude: number) {
    const plantation = Plantation.fromPlant(plant, latitude, longitude);
    plantation.codeProject = this.project.code;
    this.plantationsService.addPlantationInProject(this.project.code, plantation);
  }

  public getSelectedPlantations(): Plantation[] {
    return this.selection.selected;
  }

  public selectPlantation(plantation: Plantation) {
    if (plantation) {
      if (!this.selection.isSelected(plantation)) {
        this.selection.select(plantation);
        plantation.mapImage.getElement().classList.add('selected')
        plantation.mapSpanCircle.getElement().classList.add('selected')
        plantation.mapTrunkCircle.getElement().classList.add('selected')
        plantation.mapLabel.getElement().classList.add('selected')
        plantation.mapFullLabel.getElement().classList.add('selected')
        this.plantationSelect.emit(plantation);
      }
    } else {
      this.unselectAllPlantations();
    }
  }

  public unselectPlantation(plantation: Plantation) {
    if (this.selection.isSelected(plantation)) {
      this.selection.deselect(plantation);
      plantation.mapImage.getElement().classList.remove('selected')
      plantation.mapSpanCircle.getElement().classList.remove('selected')
      plantation.mapTrunkCircle.getElement().classList.remove('selected')
      plantation.mapLabel.getElement().classList.remove('selected')
      plantation.mapFullLabel.getElement().classList.remove('selected')
      this.plantationUnselect.emit(plantation);
    }
  }

  public selectAllPlantations() {
    this.selection.selected.forEach(p => this.selectPlantation(p));
  }

  public unselectAllPlantations() {
    this.selection.selected.forEach(p => this.unselectPlantation(p));
  }

  public updateVisibility(plantation: Plantation) {
    if (plantation.visible) {
      plantation.mapImage.getElement().classList.remove('hidden');
      plantation.mapSpanCircle.getElement().classList.remove('hidden');
      plantation.mapTrunkCircle.getElement().classList.remove('hidden');
      plantation.mapLabel.getElement().classList.remove('hidden');
      plantation.mapFullLabel.getElement().classList.remove('hidden');
    } else {
      plantation.mapImage.getElement().classList.add('hidden');
      plantation.mapSpanCircle.getElement().classList.add('hidden');
      plantation.mapTrunkCircle.getElement().classList.add('hidden');
      plantation.mapLabel.getElement().classList.add('hidden');
      plantation.mapFullLabel.getElement().classList.add('hidden');
    }
  }
  //#endregion
}