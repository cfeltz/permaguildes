import { ProjectMapComponent } from './project-map.component';
import { PermaguildesTest } from '../../../modules/testing.module';
import { PlantationsService, ProjectsService, ShapesService } from 'projects';
import { PlantsService } from 'plants';

describe(ProjectMapComponent.name, () => {
  const test = new PermaguildesTest<ProjectMapComponent>();
  test.initComponent(ProjectMapComponent, {
    providers: [ ProjectsService, PlantationsService, PlantsService, ShapesService ]
  });
});
