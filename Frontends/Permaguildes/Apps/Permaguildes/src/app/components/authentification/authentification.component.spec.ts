import { AuthentificationComponent } from './authentification.component';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler';

describe(AuthentificationComponent.name, () => {
  const test = new PermaguildesTest<AuthentificationComponent>();
  test.initComponent(AuthentificationComponent);
});
