import { Component, Inject } from '@angular/core';
import { IAuthJwtService } from '@cfeltz/angular-helpers';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.scss']
})
export class AuthentificationComponent {
  public isJwtLoggedIn$: Observable<boolean>;

  public isLoggedIn$(): Observable<boolean> {
    return this.isJwtLoggedIn$;
  }

  constructor(@Inject('IAuthJwtService') private authJwtService: IAuthJwtService) {
      this.isJwtLoggedIn$ = authJwtService.isLoggedIn();
  }

  public logout() {
    this.authJwtService.logout().subscribe();
  }

}
