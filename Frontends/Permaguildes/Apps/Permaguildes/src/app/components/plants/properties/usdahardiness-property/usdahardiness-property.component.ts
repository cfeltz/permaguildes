import { Options } from '@angular-slider/ngx-slider';
import { Component, OnInit, Input } from '@angular/core';
import { Plant } from 'plants';

@Component({
  selector: 'app-usdahardiness-property',
  templateUrl: './usdahardiness-property.component.html',
  styleUrls: ['./usdahardiness-property.component.scss']
})

export class UsdahardinessPropertyComponent implements OnInit {
  @Input() plant: Plant;
  @Input() updateMode: boolean;

  options: Options = {
    showTicksValues: true,
    stepsArray: [
      { value: 1, legend: '-51°C' },
      { value: 2, legend: '-45°C' },
      { value: 3, legend: '-40°C' },
      { value: 4, legend: '-35°C' },
      { value: 5, legend: '-29°C' },
      { value: 6, legend: '-23°C' },
      { value: 7, legend: '-17°C' },
      { value: 8, legend: '-12°C' },
      { value: 9, legend: '-7°C' },
      { value: 10, legend: '-1°C' },
      { value: 11, legend: '+4°C' }
    ]
  };
  isVisible: boolean;

  constructor() { }

  ngOnInit() {
    if (this.plant) {
      this.isVisible = (this.plant.minUsdahardiness !== undefined && this.plant.minUsdahardiness !== null);
    }
  }
  
  onChangeVisibility() {
    if (this.isVisible) {
      if (!this.plant.minUsdahardiness)
        this.plant.minUsdahardiness = 1;
      if (!this.plant.maxUsdahardiness)
        this.plant.maxUsdahardiness = 11;
    } else if (!this.isVisible) {
      this.plant.minUsdahardiness = null;
      this.plant.maxUsdahardiness = null;
    }
  }
}
