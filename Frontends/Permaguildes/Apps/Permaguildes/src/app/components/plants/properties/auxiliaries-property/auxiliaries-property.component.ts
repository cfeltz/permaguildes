import { Component, Input } from '@angular/core';
import { LabelType, Plant } from 'plants';

@Component({
  selector: 'app-auxiliaries-property',
  templateUrl: './auxiliaries-property.component.html',
  styleUrls: ['./auxiliaries-property.component.scss']
})

export class AuxiliariesPropertyComponent {
  @Input() plant: Plant;
  @Input() updateMode: boolean;
  labelTypes = LabelType;

  constructor() { }
}
