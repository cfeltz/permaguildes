import { RouterModule } from '@angular/router';
import { PlantsExtendedListComponent } from './plants-extended-list.component';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { PropertiesIconsComponent } from '../icons/properties-icons/properties-icons.component';


describe(PlantsExtendedListComponent.name, () => {
  const test = new PermaguildesTest<PlantsExtendedListComponent>();
  test.initComponent(PlantsExtendedListComponent, {
    declarations: [ PropertiesIconsComponent ],
    imports: [ RouterModule ]
  });
});
