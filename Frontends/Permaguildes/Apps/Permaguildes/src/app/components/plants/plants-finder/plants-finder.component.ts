import { Component, Input, ViewChild } from '@angular/core';
import { PlantsExtendedListComponent } from '../plants-extended-list/plants-extended-list.component';

@Component({
  selector: 'app-plants-finder',
  templateUrl: './plants-finder.component.html',
  styleUrls: ['./plants-finder.component.scss']
})
export class PlantsFinderComponent {
  @Input() maxWidth = 100;
  @Input() dragEnable: boolean = false;
  @Input() withDialogs: boolean = true;
  @ViewChild('list', { static: true }) list: PlantsExtendedListComponent;

  constructor() {}

  filterChangeResult(ev) {
    this.list.setPlants(ev.result);
  }
}
