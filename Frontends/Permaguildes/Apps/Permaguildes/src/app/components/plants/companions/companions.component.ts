import { Component, Input } from '@angular/core';

import { FormControl } from '@angular/forms';
import { Companion, Plant, SophyService } from 'plants';

@Component({
  selector: 'app-companions',
  templateUrl: './companions.component.html',
  styleUrls: ['./companions.component.scss'],
})
export class CompanionsComponent {
  public codeSophyCtrl = new FormControl();

  @Input() plant: Plant;
  @Input() updateMode: boolean;
  showAllDiscriminants = false;
  showAllSimilars = false;

  constructor(public sophyService: SophyService) { }

  discriminantsPlantsFilter(companion: Companion, showAllDiscriminants: boolean) {
    return companion.fidelity !== null 
        && companion.fidelity !== undefined 
        && (showAllDiscriminants || companion.codePlant);
  }

  similarsPlantsFilter(companion: Companion, showAllSimilars: boolean) {
    return companion.gap !== null 
        && companion.gap !== undefined 
        && (companion.fidelity == null || companion.fidelity == undefined)
        && (showAllSimilars || companion.codePlant);
  }
}
