import { PlantsFinderComponent } from './plants-finder.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PlantsService, LabelsService } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';

describe(PlantsFinderComponent.name, () => {
  const test = new PermaguildesTest<PlantsFinderComponent>();
  test.initComponent(PlantsFinderComponent, {
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    providers: [ PlantsService, LabelsService ]
  });
});
