import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { UsdahardinessPropertyComponent } from './usdahardiness-property.component';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(UsdahardinessPropertyComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<UsdahardinessPropertyComponent>();
  test.initComponent(UsdahardinessPropertyComponent, {});

  it('should create with no usda hardness attribute', () => {
    const plant = plantsList.find(v => v.name === 'Kiway');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
  });

  it('should create with usda hardness attributes', () => {
    const plant = plantsList.find(v => v.name === 'Caroubier');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
  });
});
