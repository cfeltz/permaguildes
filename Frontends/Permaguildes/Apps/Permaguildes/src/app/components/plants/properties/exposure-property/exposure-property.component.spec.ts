import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { LabelsService } from 'plants';
import { ExposurePropertyComponent } from './exposure-property.component';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(ExposurePropertyComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<ExposurePropertyComponent>();
  test.initComponent(ExposurePropertyComponent, {
    providers: [
      LabelsService
    ]
  });

  it('should create without exposure attribute', () => {
    const plant = plantsList.find(v => v.name === 'Kiway');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
  });

  it('should create with exposure attribute', () => {
    const plant = plantsList.find(v => v.name === 'Caroubier');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
  });

});
