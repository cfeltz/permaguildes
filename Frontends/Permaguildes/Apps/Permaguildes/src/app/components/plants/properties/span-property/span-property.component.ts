import { Options } from '@angular-slider/ngx-slider';
import { Component, OnInit, Input } from '@angular/core';
import { Plant } from 'plants';

@Component({
  selector: 'app-span-property',
  templateUrl: './span-property.component.html',
  styleUrls: ['./span-property.component.scss']
})

export class SpanPropertyComponent implements OnInit {
  @Input() plant: Plant;
  @Input() updateMode: boolean;
  options: Options;
  isVisible: boolean;

  constructor() { }

  private buildSliderOptions() {
    if (this.plant.sizeUnits === 'm') {
      this.options = {
        showTicks: true,
        stepsArray: [
          { value: 1, legend: '1m' },
          { value: 2, legend: '2m' },
          { value: 3, legend: '3m' },
          { value: 4, legend: '4m' },
          { value: 5, legend: '5m' },
          { value: 6, legend: '6m' },
          { value: 7, legend: '7m' },
          { value: 8, legend: '8m' },
          { value: 9, legend: '9m' },
          { value: 10, legend: '10m' },
          { value: 15, legend: '15m' },
          { value: 20, legend: '20m' },
          { value: 50, legend: '50m' },
          { value: 100, legend: '100m' }
        ]
      };
    } else {
      this.options = {
        showTicks: true,
        stepsArray: [
          { value: 5, legend: '5cm' },
          { value: 10, legend: '10cm' },
          { value: 20, legend: '20cm' },
          { value: 30, legend: '30cm' },
          { value: 40, legend: '40cm' },
          { value: 50, legend: '50cm' },
          { value: 60, legend: '60cm' },
          { value: 70, legend: '70cm' },
          { value: 80, legend: '80cm' },
          { value: 90, legend: '90cm' },
          { value: 100, legend: '1m' },
          { value: 120, legend: '1,2m' },
          { value: 150, legend: '1,5m' },
          { value: 180, legend: '1,8m' },
          { value: 200, legend: '2m' },
          { value: 250, legend: '2,5m' },
          { value: 500, legend: '5m' }
        ]
      };
    }
  }

  ngOnInit() {
    if (this.plant) {
      this.isVisible = (this.plant.minSpan !== undefined && this.plant.minSpan !== null);
      this.plant.sizeUnitsChange.subscribe(value => {
        this.buildSliderOptions();
      });
      this.buildSliderOptions();
    }
  }
  
  onChangeVisibility() {
    if (this.isVisible) {
      if (!this.plant.minSpan)
        this.plant.minSpan = 1;
      if (!this.plant.maxSpan)
        this.plant.maxSpan = 11;
    } else if (!this.isVisible) {
      this.plant.minSpan = null;
      this.plant.maxSpan = null;
    }
  }
}
