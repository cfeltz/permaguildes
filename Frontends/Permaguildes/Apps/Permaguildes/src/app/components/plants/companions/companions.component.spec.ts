import { CompanionsComponent } from './companions.component';
import amandierData from 'Libs/plants/src/lib/data/amandier.json';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { CodeSophyDirective } from 'Apps/Permaguildes/src/app/validators/codesophy.directive';
import { FilterPipe, OrderByPipe } from 'apis-helpers';
import { Plant, SophyService } from 'plants';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(CompanionsComponent.name, () => {
  const amandier = createApiMessageInstance(Plant).loadFromJson(amandierData)

  const test = new PermaguildesTest<CompanionsComponent>();
  test.initComponent(CompanionsComponent, {
    beforeEach: () => {
      test.component.plant = amandier;
    },
    declarations: [ OrderByPipe, FilterPipe, CodeSophyDirective ],    
    providers: [ SophyService ]
  });

  it('stable should create', () => {
    expect(test.component).toBeTruthy();

    test.fixture.whenStable().then(() => {
      test.fixture.detectChanges();
      expect(test.component).toBeTruthy();
    });
  });

  it('show companions with fidelity', async() => {
    expect(test.component).toBeTruthy();

    test.fixture.whenStable().then(() => {
      test.fixture.detectChanges();
      expect(test.component).toBeTruthy();
      expect(test.fixture.nativeElement.querySelectorAll('mat-list-item').length).toBeGreaterThan(10);
    });
  });
});
