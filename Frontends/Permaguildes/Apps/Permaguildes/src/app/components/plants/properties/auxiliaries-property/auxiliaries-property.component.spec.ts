import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { AuxiliariesPropertyComponent } from './auxiliaries-property.component';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(AuxiliariesPropertyComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<AuxiliariesPropertyComponent>();
  test.initComponent(AuxiliariesPropertyComponent, {});
});
