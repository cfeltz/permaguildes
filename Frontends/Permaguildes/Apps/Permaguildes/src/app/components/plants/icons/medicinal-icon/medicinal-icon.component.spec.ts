import { MedicinalIconComponent } from './medicinal-icon.component';
import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(MedicinalIconComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<MedicinalIconComponent>();
  test.initComponent(MedicinalIconComponent, {});

  it('should create with no medicinal attributes', () => {
    const plant = plantsList.find(v => v.name === 'Kiway');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.textContent).toEqual('');
  });

  it('should create with medicinal attributes', () => {
    const plant = plantsList.find(v => v.name === 'Caroubier');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.querySelectorAll('mat-icon.on').length).toEqual(2);
  });
});
