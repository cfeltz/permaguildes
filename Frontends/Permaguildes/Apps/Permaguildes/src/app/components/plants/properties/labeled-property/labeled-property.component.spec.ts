import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { LabeledPropertyComponent } from './labeled-property.component';
import { LabelsService } from 'plants';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(LabeledPropertyComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<LabeledPropertyComponent>();
  test.initComponent(LabeledPropertyComponent, {
    providers: [
      LabelsService
    ]
  });

  it('should create with stratum attribute', () => {
    const plant = plantsList.find(v => v.name === 'Caroubier');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
  });

});
