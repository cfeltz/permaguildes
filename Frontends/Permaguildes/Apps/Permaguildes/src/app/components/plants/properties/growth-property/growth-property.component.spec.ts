import { GrowthPropertyComponent } from './growth-property.component';
import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(GrowthPropertyComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<GrowthPropertyComponent>();
  test.initComponent(GrowthPropertyComponent, {});

  it('should create without growth attribute', () => {
    const plant = plantsList.find(v => v.name === 'Kiway');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.textContent).toEqual('');
  });

  it('should create with slow growth attribute', () => {
    const plant = plantsList.find(v => v.name === 'Caroubier');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.textContent).toEqual('Lente');
  });

  it('should create with medium growth attribute', () => {
    const plant = plantsList.find(v => v.name === 'Arbre de judée');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.textContent).toEqual('Moyenne');
  });

  it('should create with fast growth attribute', () => {
    const plant = plantsList.find(v => v.name === 'Arbre à kiwi');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.textContent).toEqual('Rapide');
  });
});
