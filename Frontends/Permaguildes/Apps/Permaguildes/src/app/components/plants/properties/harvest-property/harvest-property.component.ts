import { Options } from '@angular-slider/ngx-slider';
import { Component, OnInit, Input } from '@angular/core';
import { Plant } from 'plants';
import { LabelsService } from 'plants';

@Component({
  selector: 'app-harvest-property',
  templateUrl: './harvest-property.component.html',
  styleUrls: ['./harvest-property.component.scss']
})

export class HarvestPropertyComponent implements OnInit {
  @Input() plant: Plant;
  @Input() updateMode: boolean;

  options: Options = {
    showTicks: true,
    stepsArray: [
      { value: 1, legend: this.labelsService.getMonthLabel(1) },
      { value: 2, legend: this.labelsService.getMonthLabel(2) },
      { value: 3, legend: this.labelsService.getMonthLabel(3) },
      { value: 4, legend: this.labelsService.getMonthLabel(4) },
      { value: 5, legend: this.labelsService.getMonthLabel(5) },
      { value: 6, legend: this.labelsService.getMonthLabel(6) },
      { value: 7, legend: this.labelsService.getMonthLabel(7) },
      { value: 8, legend: this.labelsService.getMonthLabel(8) },
      { value: 9, legend: this.labelsService.getMonthLabel(9) },
      { value: 10, legend: this.labelsService.getMonthLabel(10) },
      { value: 11, legend: this.labelsService.getMonthLabel(11) },
      { value: 12, legend: this.labelsService.getMonthLabel(12) }
    ]
  };
  period1Visible: boolean;
  period2Visible: boolean;

  constructor(public labelsService: LabelsService) { }

  ngOnInit() {
    if (this.plant) {
      this.period1Visible = this.plant.harvestPeriods.length > 0;
      this.period2Visible = this.plant.harvestPeriods.length > 2;
    }
  }

  getMonthName(code: number): string {
    return this.labelsService.getMonthLabel(code);
  }

  onChangePeriodVisibility(period: number) {
    if (period == 1) {
      if (this.period1Visible && this.plant.harvestPeriods.length == 0) {
        this.plant.harvestPeriods.push(1);
        this.plant.harvestPeriods.push(12);
      } else if (!this.period1Visible && this.plant.harvestPeriods.length > 0) {
        this.period2Visible = false;
        this.plant.harvestPeriods = [];
      }
    } else if (period == 2) {
      if (this.period2Visible && this.plant.harvestPeriods.length == 0) {
        this.period1Visible = true;
        this.plant.harvestPeriods.push(1);
        this.plant.harvestPeriods.push(12);
        this.plant.harvestPeriods.push(1);
        this.plant.harvestPeriods.push(12);
      } else if (this.period2Visible && this.plant.harvestPeriods.length == 2) {
        this.plant.harvestPeriods.push(1);
        this.plant.harvestPeriods.push(12);
      } else if (!this.period2Visible && this.plant.harvestPeriods.length > 2) {
        this.plant.harvestPeriods.pop();
        this.plant.harvestPeriods.pop();
      }
    }
  }
}
