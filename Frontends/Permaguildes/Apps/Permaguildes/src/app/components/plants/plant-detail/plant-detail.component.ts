import { Component, ViewChild, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { PlantsService, LabelsService, SophyService, LabelType, Plant } from 'plants';
import { AuthJwtService } from '@cfeltz/angular-helpers';
import { MatAccordion, MatExpansionPanel } from '@angular/material/expansion';
import { NGXLogger } from 'ngx-logger';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { lastValueFrom, Observable, Subscription } from 'rxjs';
import { PermaguildesDialogsService } from '../../../services/permaguildes-dialogs/permaguildes-dialogs.service';

@Component({
  selector: 'app-plant-detail',
  templateUrl: './plant-detail.component.html',
  styleUrls: ['./plant-detail.component.scss'],
})
export class PlantDetailComponent {
  @ViewChild('accordion') accordion: MatAccordion;
  @Input() plant$: Observable<Plant>;
  @Input() updateMode = false;
  @Input() contentTitle: string = undefined;
  @Input() closeButton = false;
  @Output() closed = new EventEmitter();
  savingInProgress = false;
  labelTypes = LabelType;

  navigationSubscription: Subscription;

  constructor(
    private logger: NGXLogger,
    private dialogsService: PermaguildesDialogsService,
    private labelsService: LabelsService,
    private plantsService: PlantsService,
    private sophyService: SophyService,
    private authService: AuthJwtService,
    private location: Location,
    private toastr: ToastrService,
    private elRef: ElementRef
  ) { 
  }

  public edit(codePlant: number) {
    const _this = this;
    let editAction: any;
    if (this.dialogsService.isOnDialog(this.elRef)) {
      editAction = function() {
        const url = `/nav/plant/${codePlant}`;
        window.open(url, "_blank");
        // _this.router.navigateByUrl(url);
      };
    } else {
      editAction = function() {
        _this.updateMode = true;
      };
    }

    if (this.authService.hasToken()) {
      editAction();
    } else {
      this.authService.openLoginDialog().subscribe(result => {
        if (this.authService.hasToken()) {
          editAction();
        }
      });
    }

  }

  private async validate(plant: Plant): Promise<boolean> {
    if (plant.codeSophy) {
      let sophy = await lastValueFrom(this.sophyService.getSophy(plant.codeSophy));
      return (sophy != undefined);
    } else {
      return true;
    }

  }

  public async save(plant: Plant) {
    const valid = await this.validate(plant);
    if (valid) {
      this.savingInProgress = true;
      if (plant.code === 0) {
        this.plantsService.createPlant(plant).then((updatedPlant) => {
          plant = updatedPlant;
          this.location.replaceState(`/nav/plant/${plant.code}`);
          this.savingInProgress = false;
          this.toastr.info('Plante créée');
        }).catch((error) => {
          this.savingInProgress = false;
        });
      } else {
        this.plantsService.updatePlant(plant).then((updatedPlant) => {
          plant = updatedPlant;
          this.savingInProgress = false;
          this.toastr.info('Plante sauvegardée');
        }).catch((error) => {
          this.savingInProgress = false;
        });
      }
    } else {
      this.toastr.error('Sauvegarde impossible, veuillez corriger les erreurs');
    }
  }
}
