import { EdibilityIconComponent } from './edibility-icon.component';
import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { Plant } from 'plants';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(EdibilityIconComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<EdibilityIconComponent>();
  test.initComponent(EdibilityIconComponent, {});

  it('should create with no edibility attributes', () => {
    const plant = plantsList.find(v => v.name === 'Kiway');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.textContent).toEqual('');
  });

  it('should create with edibility attributes', () => {
    const plant = plantsList.find(v => v.name === 'Arbre de judée');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.querySelectorAll('mat-icon.on').length).toEqual(4);
  });
});
