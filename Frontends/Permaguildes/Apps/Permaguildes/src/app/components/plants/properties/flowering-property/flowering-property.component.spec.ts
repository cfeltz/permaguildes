import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { LabelsService } from 'plants';
import { FloweringPropertyComponent } from './flowering-property.component';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(FloweringPropertyComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<FloweringPropertyComponent>();
  test.initComponent(FloweringPropertyComponent, {
    providers: [
      LabelsService
    ]
  });

  it('should create with no flowering attribute', () => {
    const plant = plantsList.find(v => v.name === 'Kiway');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
  });

  it('should create with flowering attributes', () => {
    const plant = plantsList.find(v => v.name === 'Arbre à kiwi');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
  });
});
