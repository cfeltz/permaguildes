import { Component, OnInit, Input } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { PlantPropertiesAware } from 'Apps/Permaguildes/src/app/enums/plantProperties.decorator';
import { Plant } from 'plants';

@Component({
  selector: 'app-exposure-property',
  templateUrl: './exposure-property.component.html',
  styleUrls: ['./exposure-property.component.scss']
})
@PlantPropertiesAware
export class ExposurePropertyComponent implements OnInit {
  @Input() plant: Plant;
  @Input() updateMode: boolean;
  codes: string[];

  constructor() {
  }

  getCodes(): string[] {
    const returnValue: string[] = [];
    if (this.plant) {
      if (this.plant.exposureSun) {
        returnValue.push('1');
      }
      if (this.plant.exposureMiddle) {
        returnValue.push('2');
      }
      if (this.plant.exposureShadow) {
        returnValue.push('3');
      }
    }
    return returnValue;
  }

  updateSelection(e: MatSelectChange) {
    if (this.plant) {
      this.plant.exposureSun = (e.value.indexOf('1') > -1);
      this.plant.exposureMiddle = (e.value.indexOf('2') > -1);
      this.plant.exposureShadow = (e.value.indexOf('3') > -1);
    }
  }

  ngOnInit() {
    this.codes = this.getCodes();
  }
}
