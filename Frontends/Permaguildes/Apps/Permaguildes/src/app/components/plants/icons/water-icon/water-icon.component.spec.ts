import { WaterIconComponent } from './water-icon.component';
import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(WaterIconComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<WaterIconComponent>();
  test.initComponent(WaterIconComponent, {});

  it('should create without water attribute', () => {
    const plant = plantsList.find(v => v.name === 'Kiway');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.textContent).toEqual('');
  });

  it('should create with slow water attribute', () => {
    const plant = plantsList.find(v => v.name === 'Caroubier');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.querySelectorAll('mat-icon.on').length).toEqual(1);
  });

  it('should create with medium water attribute', () => {
    const plant = plantsList.find(v => v.name === 'Arbre de judée');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.querySelectorAll('mat-icon.on').length).toEqual(2);
  });

  it('should create with fast water attribute', () => {
    const plant = plantsList.find(v => v.name === 'Arbre à kiwi');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
    expect(test.fixture.nativeElement.querySelectorAll('mat-icon.on').length).toEqual(3);
  });
});
