import { Component, Input } from '@angular/core';
import { Plant } from 'plants';

@Component({
  selector: 'app-water-icon',
  templateUrl: './water-icon.component.html',
  styleUrls: ['./water-icon.component.scss']
})

export class WaterIconComponent {
  @Input() plant: Plant;
  @Input() updateMode: boolean;

  constructor() { }

  getTooltipText(water): string {
    switch (water) {
      case 1:
        return 'Supporte bien la sécheresse';
      case 2:
        return 'Eviter que le sol ne s\'asèche';
      case 3:
        return 'Besoin de beaucoup d\'eau';
      default:
        return undefined;
    }
  }

  onIconClick(water) {
    if (this.updateMode) {
      if (this.plant.water == water) {
        this.plant.water = 0;
      } else {
        this.plant.water = water;
      }
    }
  }
}
