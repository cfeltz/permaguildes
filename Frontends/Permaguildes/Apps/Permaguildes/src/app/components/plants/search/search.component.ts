import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Plant, SearchProperties } from 'plants';
import { StringHelper } from '@cfeltz/angular-helpers';
import { PlantsService, LabelsService } from 'plants';
import { MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material/autocomplete';

//#region Interfaces
export interface SearchResult {
  name: string;
  detail: string;
  plant: Plant;
  property: SearchProperties;
}
//#endregion

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})

export class SearchComponent implements OnInit, OnDestroy {
  @ViewChild('searchValue', { static: true }) searchValue: ElementRef;
  @ViewChild(MatAutocompleteTrigger, { static: true }) autocomplete: MatAutocompleteTrigger;

  public stateCtrl = new FormControl();
  public _filteredResultsObservable$: Observable<SearchResult[]>;
  private _filteredResults: SearchResult[] = [];
  //#region plants
  private _plants: Plant[] = [];
  private _filteredPlants: Plant[] = [];
  public _propertiesAttributes: SearchProperties[] = [];
  private _refreshPlantsList = true;
  //#endregion
  private readonly storageValueKey = 'plant-finder-search-value';

  //#region events
  @Output() filterChangeResult = new EventEmitter();
  //#endregion

  constructor(private _plantsService: PlantsService, private _testService: LabelsService) { }

  //#region Format
  private _plantToSearchResult(plant: Plant): SearchResult {
    const returnValue: SearchResult = { name: plant.name, detail: plant.latinNames, plant: plant, property: undefined };
    return returnValue;
  }

  private _searchPropertyToSearchResult(property: SearchProperties): SearchResult {
    const returnValue: SearchResult = { name: property.toString(), detail: '', plant: undefined, property: property };
    return returnValue;
  }
  //#endregion

  //#region Properties attributes
  private _addProperty(property: SearchProperties) {
    this._propertiesAttributes.push(property);
  }

  private _removeProperty(property: SearchProperties) {
    this._propertiesAttributes = this._propertiesAttributes.filter(function(value) {
      return value !== property;
    });
  }

  onRemoveAttribute(property: SearchProperties) {
    this._removeProperty(property);
    this._refreshPlantsList = true;
    this.stateCtrl.updateValueAndValidity({ onlySelf: false, emitEvent: true });
  }
  //#endregion

  //#region Set map result and return plants result
  private _setMapResult(value: string): SearchResult[] {
    // Get storage search value
    const storedValue = localStorage.getItem(this.storageValueKey);
    if(storedValue) {
      localStorage.removeItem(this.storageValueKey);
      console.log('SearchComponent get value ' + storedValue);
      this.stateCtrl.setValue(storedValue);
      value = storedValue;
    }
    
    return this._searchResults(value);
  }

  public removeStorageValue() {
    // Remove storage search value
    localStorage.removeItem(this.storageValueKey);
  }

  private _searchResults(filterValue: string): SearchResult[] {
    this._filteredResults = [];

    if (filterValue === undefined || filterValue === null || filterValue.length === 0) {
      // Return all properties by default (expept _propertiesAttributes)
      this._filteredResults = Object.values(SearchProperties).filter((property) => {
        return this._propertiesAttributes.length === 0 || this._propertiesAttributes.indexOf(property) === -1;
      }).map(
        property => this._searchPropertyToSearchResult(property));
    } else {
      this._plants.forEach(plant => {
        // Check if plant have properties in _propertiesAttributes
        let respectPropertiesAttributes = true;
        if (this._propertiesAttributes.length > 0) {
          this._propertiesAttributes.forEach(property => {
            if (!plant.checkProperty(property)) {
              respectPropertiesAttributes = false;
              return false;
            }
          });
        }

        if (respectPropertiesAttributes &&
            (StringHelper.findTextIntoString(filterValue, plant.name)
              || StringHelper.findTextIntoString(filterValue, plant.latinNames))) {
          // Return this plant on searchResult format
          this._filteredResults.push(this._plantToSearchResult(plant));
        }
      });

      // Get on search properties
      if (filterValue.length > 1) {
        Object.values(SearchProperties).forEach(property => {
          if (this._propertyValidatedByFilter(property, filterValue)) {
            // Filter match this property
            if (this._filteredResults.filter(r => r.name === property.toString()).length === 0) {
              // Add only one time each property
              this._filteredResults.push(this._searchPropertyToSearchResult(property));
            }
          }
        });
      }

      //#region Sort result
      const sortResultForPriorizeFilter = function(r1: SearchResult, r2: SearchResult) {
        let returnValue;
        const name1 = StringHelper.formatToCompareString(r1.name);
        const name2 = StringHelper.formatToCompareString(r2.name);
        if (name1.startsWith(filterValue) && !name2.startsWith(filterValue)) {
          returnValue = -1;
        } else if (!name1.startsWith(filterValue) && name2.startsWith(filterValue)) {
          returnValue = 1;
        } else {
          returnValue = (name1 < name2) ? -1 : (name1 > name2) ? 1 : 0;
        }
        return returnValue;
      };
      //#endregion
      this._filteredResults = this._filteredResults.sort(sortResultForPriorizeFilter);
    }

    if (this._refreshPlantsList) {
      this._refreshPlantsList = false;
      // Initialize plants list
      if (filterValue === undefined || filterValue === null || filterValue.length === 0) {
        this._plantsResults(undefined);
      } else {
        this._plantsResults(this._filteredResults);
      }
    }
    return this._filteredResults;
  }

  private _plantsResults(searchResults: SearchResult[]): Plant[] {
    this._filteredPlants = [];

    if (searchResults && searchResults.length > 0) {
      // Get all plants from search results
      searchResults.forEach(searchResult => {
        if (searchResult.plant) {
          this._filteredPlants.push(searchResult.plant);
        }
      });
    } else {
      if (this._propertiesAttributes.length === 0) {
        this._filteredPlants = this._plants;
      } else {
        // Get all plants with attributes
        this._plants.forEach(plant => {
          let respectPropertiesAttributes = true;
          // Check all attributes
          this._propertiesAttributes.forEach(property => {
            if (!plant.checkProperty(property)) {
              respectPropertiesAttributes = false;
              return false;
            }
          });
          if (respectPropertiesAttributes) {
            this._filteredPlants.push(plant);
          }
        });
      }
    }
    this.filterChangeResult.emit({ result: this._filteredPlants });
    return this._filteredPlants;
  }

  //#region Filter by search property
  private _propertyValidatedByFilter(property: SearchProperties, value: string): boolean {
    const regexp = /[- \']/; // Separators: [ ], ['], [-]
    const wordsValue = value.toString().split(regexp);

    let match = true;
    wordsValue.forEach(wordOfValue => {
      const wordsOfProperty = property.toString().split(regexp).filter(m => m.length >= wordOfValue.length);
      let matchWordOfValue = false;
      wordsOfProperty.forEach(wordOfProperty => {
        matchWordOfValue = matchWordOfValue || StringHelper.stringBeginByText(wordOfProperty, wordOfValue);
      });
      match = match && matchWordOfValue;
    });
    return match;
  }
  //#endregion
  //#endregion

  //#region Select result
  onSelectResult(ev: MatAutocompleteSelectedEvent) {
    const selectedResult = this._filteredResults.filter(result => result.name === ev.option.value);
    // If property selected, add it on attributes and search all plants with this attribute
    if (selectedResult.length === 1) {
      this._refreshPlantsList = true;
      if (selectedResult[0].property) {
        this._addProperty(selectedResult[0].property);
        this.stateCtrl.reset();
      } else {
        this.stateCtrl.updateValueAndValidity({ onlySelf: false, emitEvent: true });
      }
      this.searchValue.nativeElement.blur();
    }
  }

  // On Enter key press
  onValidateResult(ev) {
    this.autocomplete.closePanel();
    this._refreshPlantsList = true;
    this.stateCtrl.updateValueAndValidity({ onlySelf: false, emitEvent: true });
  }
  //#endregion

  ngOnInit() {
    console.log('Init component SearchComponent');
    this._plantsService.getPlants().subscribe(
      result => {
        console.log('Plants loaded in search component');
        this._plants = result;

        // Map search result on stateCtrl values
        this._filteredResultsObservable$ = this.stateCtrl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._setMapResult(value))
            );
      },
      error => console.error('Failed to load plants')
    );
  }

  ngOnDestroy(): void {
    const value = this.searchValue.nativeElement.value;
    console.log('SearchComponent save value ' + value);
    localStorage.setItem(this.storageValueKey, value);
  }

}
