import { Component, Input } from '@angular/core';
import { StringHelper } from '@cfeltz/angular-helpers';
import { Plant } from 'plants';

@Component({
  selector: 'app-links-property',
  templateUrl: './links-property.component.html',
  styleUrls: ['./links-property.component.scss']
})
export class LinksPropertyComponent {
  @Input() plant: Plant;
  @Input() updateMode: boolean;

  constructor() { }

  isValidUrl(url: string) {
    if (StringHelper.isNullOrEmpty(url)) {
      return true;
    } else {
      const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.%]+$/;
      const regex = new RegExp(urlRegex);
      return regex.test(url);
    }
  }

  isPfafUrl(url: string) {
    if (StringHelper.isNullOrEmpty(url)) {
      return true;
    } else {
      const urlRegex = /^(?:http(s)?:\/\/)?(?:www.)?(pfaf.org\/user\/Plant.aspx\?LatinName=){1}/;
      const regex = new RegExp(urlRegex);
      return regex.test(url);
    }
  }
}
