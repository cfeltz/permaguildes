import { Component, Input, OnInit } from '@angular/core';
import { Plant } from 'plants';
import { PlantsService, LabelsService, SophyService } from 'plants';
import { Observable } from 'rxjs';
import { PermaguildesDialogsService } from '../../../services/permaguildes-dialogs/permaguildes-dialogs.service';

@Component({
  selector: 'app-plants-extended-list',
  templateUrl: './plants-extended-list.component.html',
  styleUrls: ['./plants-extended-list.component.scss'],
})

export class PlantsExtendedListComponent implements OnInit {
  readonly defaultLimit = 25;
  readonly defaultLoading = 25;

  @Input() maxWidth: number;
  @Input() dragEnable: boolean = false;
  @Input() withDialogs: boolean = false;

  public limit = this.defaultLimit;
  public plants: Plant[];
  public plantsNames$: Observable<Object>;
  setPlants(plants: Plant[]) {
    this.limit = this.defaultLimit;
    this.plants = plants;
  }

  constructor(
    public labelsService: LabelsService, 
    public sophyService: SophyService, 
    private plantsService: PlantsService, 
    private dialogsService: PermaguildesDialogsService) { }

  ngOnInit() {
    this.plantsNames$ = this.plantsService.getPlantsNames();
  }

  onScroll(e) {
    if (this.plants && this.plants.length > this.limit) {
      if (e.currentTarget.scrollHeight <= e.currentTarget.scrollTop + e.currentTarget.offsetHeight) {
        this.limit += this.defaultLoading;
      }
    }
  }

  public openDetail(codePlant: number) {
    this.dialogsService.openPlantDetailDialog(codePlant);
  }

  public getIconUrl(plant: Plant) {
    return 'assets/images/trees/' + this.plantsService.getIconSrc(plant.iconSrc, plant.codeStratum);
  }
}
