import { PropertiesIconsComponent } from './properties-icons.component';
import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(PropertiesIconsComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));
  const plant = plantsList.find(v => v.name === 'Arbre de judée');

  const test = new PermaguildesTest<PropertiesIconsComponent>();
  test.initComponent(PropertiesIconsComponent, {
    beforeEach: () => {
      test.component.plant = plant;
    }
  });
});
