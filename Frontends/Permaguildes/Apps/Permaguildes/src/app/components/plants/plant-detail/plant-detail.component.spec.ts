import { WaterIconComponent } from '../icons/water-icon/water-icon.component';
import { GrowthPropertyComponent } from '../properties/growth-property/growth-property.component';
import { EdibilityIconComponent } from '../icons/edibility-icon/edibility-icon.component';
import { MedicinalIconComponent } from '../icons/medicinal-icon/medicinal-icon.component';

import { ActivatedRoute } from '@angular/router';
import { PlantsService, LabelsService, SophyService } from 'plants';

import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { UsdahardinessPropertyComponent } from '../properties/usdahardiness-property/usdahardiness-property.component';
import { HeightPropertyComponent } from '../properties/height-property/height-property.component';
import { LabeledPropertyComponent } from '../properties/labeled-property/labeled-property.component';
import { LinksPropertyComponent } from '../properties/links-property/links-property.component';
import { ExposurePropertyComponent } from '../properties/exposure-property/exposure-property.component';
import { CompanionsComponent } from '../companions/companions.component';
import { PermaguildesDialogsService } from '../../../services/permaguildes-dialogs/permaguildes-dialogs.service';
import { PlantDetailComponent } from './plant-detail.component';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(PlantDetailComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));
  const plant = plantsList.find(v => v.code === 1);

  const test = new PermaguildesTest<PlantDetailComponent>();
  test.initComponent(PlantDetailComponent, {
    declarations: [
      WaterIconComponent,
      GrowthPropertyComponent,
      EdibilityIconComponent,
      UsdahardinessPropertyComponent,
      HeightPropertyComponent,
      MedicinalIconComponent,
      LabeledPropertyComponent,
      ExposurePropertyComponent,
      LinksPropertyComponent,
      CompanionsComponent
    ],
    providers: [
      LabelsService,
      PlantsService,
      SophyService,
      PermaguildesDialogsService,
      {
        provide: ActivatedRoute,
        useValue: {
          snapshot: {
            paramMap: {
              get: (key: string) => 1
            },
          },
        },
      }
    ]
  });
});
