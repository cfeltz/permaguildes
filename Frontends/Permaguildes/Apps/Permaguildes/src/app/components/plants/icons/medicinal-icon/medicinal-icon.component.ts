import { Component, Input } from '@angular/core';
import { Plant } from 'plants';

@Component({
  selector: 'app-medicinal-icon',
  templateUrl: './medicinal-icon.component.html',
  styleUrls: ['./medicinal-icon.component.scss']
})

export class MedicinalIconComponent {
  @Input() plant: Plant;
  @Input() updateMode: boolean;

  constructor() { }

  getTooltipText(medicinalRating): string {
    switch (medicinalRating) {
      case 1:
        return 'Plante ayant de faibles vertues médicicales';
      case 2:
        return 'Plante ayant quelques vertues médicicales';
      case 3:
        return 'Plante ayant de bonnes vertues médicicales';
      case 4:
        return 'Plante ayant de très bonnes vertues médicicales';
      case 5:
        return 'Plante ayant d\'excellentes vertues médicicales';
      default:
        return undefined;
    }
  }

  onIconClick(medicinalRating) {
    if (this.updateMode) {
      if (this.plant.medicinalRating == medicinalRating) {
        this.plant.medicinalRating = 0;
      } else {
        this.plant.medicinalRating = medicinalRating;
      }
    }
  }
}
