import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { HarvestPropertyComponent } from './harvest-property.component';
import { LabelsService } from 'plants';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(HarvestPropertyComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));

  const test = new PermaguildesTest<HarvestPropertyComponent>();
  test.initComponent(HarvestPropertyComponent, {
    providers: [
      LabelsService
    ]
  });

  it('should create with no harvest attribute', () => {
    const plant = plantsList.find(v => v.name === 'Kiway');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
  });

  it('should create with harvest attributes', () => {
    const plant = plantsList.find(v => v.name === 'Arbre à kiwi');
    test.component.plant = plant;
    test.fixture.detectChanges();
    expect(test.component).toBeTruthy();
  });
});
