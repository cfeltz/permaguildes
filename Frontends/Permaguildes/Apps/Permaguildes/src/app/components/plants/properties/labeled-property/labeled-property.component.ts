import { getLocaleNumberSymbol, KeyValue } from '@angular/common';
import { Component, OnInit, Input } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { Plant } from 'plants';
import { LabelsService, LabelType } from 'plants';

@Component({
  selector: 'app-labeled-property',
  templateUrl: './labeled-property.component.html',
  styleUrls: ['./labeled-property.component.scss'],
})

export class LabeledPropertyComponent implements OnInit {
  @Input() plant: Plant;
  @Input() updateMode: boolean;
  @Input() labelType: LabelType;
  items: {[key: number]: string};
  title: string;
  label: string;
  value: string;
  isMultiple: boolean;
  codes: string | string[];
  orderby: any;
  valueAscOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
    return a.value.localeCompare(b.value);
  }

  constructor(public labelsService: LabelsService) {
  }

  getTitle(): string {
    return this.labelType?.toString();
  }

  getLabel(item): string {
    switch (this.labelType) {
      case LabelType.Stratum: {
        return item.key + ' - ' + item.value;
      }
      default: {
        return item.value;
      }
    }
  }

  getValue() {
    switch (this.labelType) {
      case LabelType.Family: {
        this.orderby = this.valueAscOrder;
        return this.labelsService.getFamilyLabel(this.plant.codeFamily);
      }
      case LabelType.Stratum: {
        return this.labelsService.getStratumLabel(this.plant.codeStratum);
      }
      case LabelType.SexualCharacteristic: {
        return this.labelsService.getSexualCharacteristicLabel(this.plant.codeSex);
      }
      case LabelType.VegetationTypes: {
        return this.labelsService.getVegetationTypesLabels(this.plant.codesVegetationTypes);
      }
      case LabelType.Insects: {
        return this.labelsService.getInsectsLabels(this.plant.codesAuxiliariesInsects.concat(this.plant.codesRepellentsInsects));
      }
      case LabelType.Auxiliaries: {
        return this.labelsService.getInsectsLabels(this.plant.codesAuxiliariesInsects);
      }
      case LabelType.Repellents: {
        return this.labelsService.getInsectsLabels(this.plant.codesRepellentsInsects);
      }
      default: {
        return undefined;
      }
    }
  }

  getIsMultiple(): boolean {
    switch (this.labelType) {
      case LabelType.Family:
      case LabelType.Stratum:
      case LabelType.SexualCharacteristic: {
        return false;
      }
      case LabelType.Insects:
      case LabelType.Auxiliaries:
      case LabelType.Repellents:
      case LabelType.VegetationTypes: {
        return true;
      }
    }
  }

  getCodes(): string | string[] {
    switch (this.labelType) {
      case LabelType.Family: {
        return this.plant.codeFamily?.toString();
      }
      case LabelType.Stratum: {
        return this.plant.codeStratum?.toString();
      }
      case LabelType.SexualCharacteristic: {
        return this.plant.codeSex?.toString();
      }
      case LabelType.VegetationTypes: {
        return this.plant.codesVegetationTypes?.map(code => code.toString());
      }
      case LabelType.Insects: {
        const combined = this.plant.codesVegetationTypes?.concat(this.plant.codesRepellentsInsects);
        return combined.map(code => code.toString());
      }
      case LabelType.Auxiliaries: {
        return this.plant.codesAuxiliariesInsects?.map(code => code.toString());
      }
      case LabelType.Repellents: {
        return this.plant.codesRepellentsInsects?.map(code => code.toString());
      }
      default: {
        return undefined;
      }
    }
  }

  getItems(): {[key: number]: string} {
    switch (this.labelType) {
      case LabelType.Family: {
        return this.labelsService.getFamilies();
      }
      case LabelType.Stratum: {
        return this.labelsService.getStratums();
      }
      case LabelType.SexualCharacteristic: {
        return this.labelsService.getSexualCharacteristics();
      }
      case LabelType.VegetationTypes: {
        return this.labelsService.getVegetationTypes();
      }
      case LabelType.Insects: {
        return this.labelsService.getInsects();
      }
      case LabelType.Auxiliaries: {
        return this.labelsService.getAuxiliaries();
      }
      case LabelType.Repellents: {
        return this.labelsService.getRepellents();
      }
    }
  }

  updateSelection(e: MatSelectChange) {
    switch (this.labelType) {
      case LabelType.Family: {
        this.plant.codeFamily = +e.value;
        break;
      }
      case LabelType.Stratum: {
        this.plant.codeStratum = +e.value;
        break;
      }
      case LabelType.SexualCharacteristic: {
        this.plant.codeSex = +e.value;
        break;
      }
      case LabelType.VegetationTypes: {
        const codes: string[] = e.value;
        this.plant.codesVegetationTypes = codes.map(v => +v);
        break;
      }
      case LabelType.Auxiliaries: {
        const codes: string[] = e.value;
        this.plant.codesAuxiliariesInsects = codes.map(v => +v);
        break;
      }
      case LabelType.Repellents: {
        const codes: string[] = e.value;
        this.plant.codesRepellentsInsects = codes.map(v => +v);
        break;
      }
    }
  }


  ngOnInit() {
    this.items = this.getItems();
    this.title = this.getTitle();
    this.value = this.getValue();
    this.isMultiple = this.getIsMultiple();
    this.codes = this.getCodes();
  }
}
