import { Component, Input } from '@angular/core';
import { PlantPropertiesAware } from 'Apps/Permaguildes/src/app/enums/plantProperties.decorator';
import { Plant } from 'plants';

@Component({
  selector: 'app-properties-icons',
  templateUrl: './properties-icons.component.html',
  styleUrls: ['./properties-icons.component.scss']
})
@PlantPropertiesAware
export class PropertiesIconsComponent {
  @Input() plant: Plant;

  constructor() { }
}
