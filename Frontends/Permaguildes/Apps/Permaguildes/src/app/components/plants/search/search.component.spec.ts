import { TestBed } from '@angular/core/testing';
import { SearchComponent } from './search.component';
import { HttpTestingController} from '@angular/common/http/testing';
import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { PlantsService, SearchProperties } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';

class InternalCall {
  //#region expect _propertyToCompare
  static searchPropertyWithResult(component: SearchComponent, value: string, property: SearchProperties) {
    expect(component['_propertyValidatedByFilter'](property, value))
    .toBeTruthy('\'' + value + '\' return no result (' + property.toString() + ')');
  }

  static searchPropertyWithoutResult(component: SearchComponent, value: string, property: SearchProperties) {
    expect(component['_propertyValidatedByFilter'](property, value))
    .toBeFalsy('\'' + value + '\' return result');
  }
  //#endregion

  //#region count filter result
  static countResults(component: SearchComponent, filterValue: string, countExpected: any, failureContext?: string) {
    const result = component['_searchResults'](filterValue);
    expect(result.length).toBe(countExpected, 'count filter result with '
      + filterValue + (failureContext ? ' - ' + failureContext : ''));
  }
  //#endregion

  //#region count external results
  static async countExternalResults(component: SearchComponent, filterValue: string, countExpected: any, failureContext?: string) {
    let subscription: { unsubscribe: () => void; };
    const promise = new Promise((resolve, reject) => {
      subscription = component.filterChangeResult.subscribe(state => {
        expect(state.result.length).toBe(countExpected, 'count external results with '
          + filterValue + (failureContext ? ' - ' + failureContext : ''));
        resolve(state);
      }, reject);
    });

    if (filterValue.length > 0) {
      const searchResults = component['_searchResults'](filterValue);
      const externalResults = component['_plantsResults'](searchResults);
    } else {
      const searchResults = component['_searchResults'](filterValue);
      const externalResults = component['_plantsResults'](undefined);
    }
    await promise.then(() => {
      subscription.unsubscribe();
    });
  }
  //#endregion

  //#region properties attributes
  static addProperty(component: SearchComponent, property: SearchProperties) {
    component['_addProperty'](property);
  }

  static removeProperty(component: SearchComponent, property: SearchProperties) {
    component['_removeProperty'](property);
  }
  //#endregion
}

describe(SearchComponent.name, () => {
  const test = new PermaguildesTest<SearchComponent>();
  test.initComponent(SearchComponent, {
    providers: [ PlantsService ]
  });

  it('filter singles properties', () => {
    InternalCall.searchPropertyWithResult(test.component, 'arbre', SearchProperties.Tree);
    InternalCall.searchPropertyWithResult(test.component, 'Arbre', SearchProperties.Tree);
    InternalCall.searchPropertyWithResult(test.component, 'Arbuste', SearchProperties.Shrub);
    InternalCall.searchPropertyWithResult(test.component, 'herb', SearchProperties.Herb);
    InternalCall.searchPropertyWithResult(test.component, 'grimpant', SearchProperties.Climber);
    InternalCall.searchPropertyWithResult(test.component, 'fixa', SearchProperties.Nitrate);
    InternalCall.searchPropertyWithResult(test.component, 'azote', SearchProperties.Nitrate);
    InternalCall.searchPropertyWithResult(test.component, 'mineraux', SearchProperties.Mineral);
    InternalCall.searchPropertyWithResult(test.component, 'ombre', SearchProperties.ShadowOnly);
    InternalCall.searchPropertyWithResult(test.component, 'ombre', SearchProperties.ShadowSupport);
    InternalCall.searchPropertyWithResult(test.component, 'ombre', SearchProperties.HalfShadow);
    InternalCall.searchPropertyWithResult(test.component, 'soleil', SearchProperties.SunSupport);
    InternalCall.searchPropertyWithResult(test.component, 'plein soleil', SearchProperties.SunSupport);
    InternalCall.searchPropertyWithoutResult(test.component, 'soleil uniq', SearchProperties.SunSupport);
    InternalCall.searchPropertyWithResult(test.component, 'soleil uniq', SearchProperties.SunNeed);
    InternalCall.searchPropertyWithResult(test.component, 'Caduc', SearchProperties.Caduc);
    InternalCall.searchPropertyWithResult(test.component, 'Semi persistant', SearchProperties.SemiEvergreen);
    InternalCall.searchPropertyWithResult(test.component, 'Persistant', SearchProperties.Evergreen);
    InternalCall.searchPropertyWithResult(test.component, 'peu d\'eau', SearchProperties.DryPlant);
    InternalCall.searchPropertyWithResult(test.component, 'eau', SearchProperties.NeedWater);
    InternalCall.searchPropertyWithResult(test.component, 'beaucoup', SearchProperties.NeedWater);
    InternalCall.searchPropertyWithResult(test.component, 'aqua', SearchProperties.WaterPlant);
    InternalCall.searchPropertyWithResult(test.component, 'Croissance', SearchProperties.SlowGrowth);
    InternalCall.searchPropertyWithResult(test.component, 'rapide', SearchProperties.FastGrowth);
    InternalCall.searchPropertyWithResult(test.component, 'vent', SearchProperties.Windbreak);
    InternalCall.searchPropertyWithResult(test.component, 'faune', SearchProperties.AttractingWildWife);
    InternalCall.searchPropertyWithResult(test.component, 'insect', SearchProperties.InsectRepellent);
    InternalCall.searchPropertyWithResult(test.component, 'comestible', SearchProperties.GoodEdible);
    InternalCall.searchPropertyWithResult(test.component, 'très comestible', SearchProperties.VeryGoodEdible);
    InternalCall.searchPropertyWithResult(test.component, 'med', SearchProperties.GoodMedicinal);
    InternalCall.searchPropertyWithResult(test.component, 'méd', SearchProperties.VeryGoodMedicinal);
  });

  it('count filters results on properties', () => {
    const http = TestBed.inject(HttpTestingController);
    http.expectOne(req => true).flush(plantsData);
    InternalCall.countResults(test.component, 'fix', 2);
    InternalCall.countResults(test.component, 'fixateur', 2);
    InternalCall.countResults(test.component, 'ombre', 3);
    InternalCall.countResults(test.component, 'soleil', 2);
    InternalCall.countResults(test.component, 'persistant', 2);
    InternalCall.countResults(test.component, 'peu eau', 1);
    InternalCall.countResults(test.component, 'vent', 1);
    InternalCall.countResults(test.component, 'comestible', 2);
    InternalCall.countResults(test.component, 'Croissance rapide', 1);
    InternalCall.countResults(test.component, 'arbre', 3);
    InternalCall.countResults(test.component, 'grimpant', 1);
  });

  it('count filters results on plants', () => {
    const http = TestBed.inject(HttpTestingController);
    http.expectOne(req => true).flush(plantsData);
    InternalCall.countResults(test.component, '', Object.values(SearchProperties).length);
    InternalCall.countResults(test.component, 'kiw', 2);
    InternalCall.countResults(test.component, 'carou', 1);
  });

  it('count output plants result', async () => {
    const http = TestBed.inject(HttpTestingController);
    http.expectOne(req => true).flush(plantsData);
    await InternalCall.countExternalResults(test.component, '', plantsData.length);
    await InternalCall.countExternalResults(test.component, 'kiw', 2);
    await InternalCall.countExternalResults(test.component, 'fix', 0);
  });

  it('count results with properties attributes', async () => {
    const http = TestBed.inject(HttpTestingController);
    http.expectOne(req => true).flush(plantsData);
    InternalCall.countResults(test.component, 'a', 4);
    await InternalCall.countExternalResults(test.component, 'a', 4);
    InternalCall.countResults(test.component, 'ar', 5);
    await InternalCall.countExternalResults(test.component, 'ar', 3);

    InternalCall.addProperty(test.component, SearchProperties.Nitrate);
    InternalCall.countResults(test.component, '', Object.values(SearchProperties).length - 1);
    InternalCall.countResults(test.component, 'a', 2, 'with nitrate attribute');
    await InternalCall.countExternalResults(test.component, 'a', 2, 'with nitrate attribute');
    InternalCall.countResults(test.component, 'ar', 4, 'with nitrate attribute');
    await InternalCall.countExternalResults(test.component, 'ar', 2, 'with nitrate attribute');
    InternalCall.countResults(test.component, 'car', 1, 'with nitrate attribute');
    await InternalCall.countExternalResults(test.component, 'car', 1, 'with nitrate attribute');

    InternalCall.addProperty(test.component, SearchProperties.AttractingWildWife);
    InternalCall.countResults(test.component, 'ar', 3, 'with attracting wildWife attribute');
    await InternalCall.countExternalResults(test.component, 'ar', 1, 'with attracting wildWife attribute');

    InternalCall.removeProperty(test.component, SearchProperties.Nitrate);
    InternalCall.countResults(test.component, 'ar', 3, 'with nitrate attribute');
    await InternalCall.countExternalResults(test.component, 'ar', 1, 'with nitrate attribute');
  });
});
