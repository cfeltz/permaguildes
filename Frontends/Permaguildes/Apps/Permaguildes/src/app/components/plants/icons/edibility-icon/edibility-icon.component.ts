import { Component, Input } from '@angular/core';
import { Plant } from 'plants';

@Component({
  selector: 'app-edibility-icon',
  templateUrl: './edibility-icon.component.html',
  styleUrls: ['./edibility-icon.component.scss']
})

export class EdibilityIconComponent {
  @Input() plant: Plant;
  @Input() updateMode: boolean;

  constructor() { }

  getTooltipText(edibilityRating): string {
    switch (edibilityRating) {
      case 1:
        return 'Bon comestible mais sans intérêt';
      case 2:
        return 'Bon comestible mais sans grand intérêt';
      case 3:
        return 'Bon comestible';
      case 4:
        return 'Très bon comestible';
      case 5:
        return 'Excellent comestible';
      default:
        return undefined;
    }
  }

  onIconClick(edibilityRating) {
    if (this.updateMode) {
      if (this.plant.edibilityRating == edibilityRating) {
        this.plant.edibilityRating = 0;
      } else {
        this.plant.edibilityRating = edibilityRating;
      }
    }
  }
}
