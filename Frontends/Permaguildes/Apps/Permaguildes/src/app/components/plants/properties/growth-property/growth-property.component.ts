import { Options } from '@angular-slider/ngx-slider';
import { Component, OnInit, Input } from '@angular/core';
import { Plant } from 'plants';

@Component({
  selector: 'app-growth-property',
  templateUrl: './growth-property.component.html',
  styleUrls: ['./growth-property.component.scss']
})

export class GrowthPropertyComponent implements OnInit {
  @Input() plant: Plant;
  @Input() updateMode: boolean;
  options: Options;

  constructor() { }

  ngOnInit() {
    this.options = {
      showTicks: true,
      stepsArray: [
        { value: 1, legend: 'Lente' },
        { value: 2, legend: 'Moyenne' },
        { value: 3, legend: 'Rapide' }
      ]
    };
  }
}
