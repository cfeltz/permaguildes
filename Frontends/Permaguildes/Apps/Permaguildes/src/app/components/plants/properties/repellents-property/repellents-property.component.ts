import { Component, Input } from '@angular/core';
import { Plant, LabelType } from 'plants';

@Component({
  selector: 'app-repellents-property',
  templateUrl: './repellents-property.component.html',
  styleUrls: ['./repellents-property.component.scss']
})

export class RepellentsPropertyComponent {
  @Input() plant: Plant;
  @Input() updateMode: boolean;
  labelTypes = LabelType;

  constructor() { }
}
