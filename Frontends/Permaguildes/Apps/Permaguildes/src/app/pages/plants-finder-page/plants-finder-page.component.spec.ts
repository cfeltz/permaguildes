import { PlantsFinderPageComponent } from './plants-finder-page.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PlantsService, LabelsService } from 'plants';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';

describe(PlantsFinderPageComponent.name, () => {
  const test = new PermaguildesTest<PlantsFinderPageComponent>();
  test.initComponent(PlantsFinderPageComponent, {
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    providers: [ PlantsService, LabelsService ]
  });
});
