import { Component, OnInit } from '@angular/core';
import { MetatagService } from 'apis-helpers';

@Component({
  selector: 'app-plants-finder-page',
  templateUrl: './plants-finder-page.component.html',
  styleUrls: ['./plants-finder-page.component.scss']
})
export class PlantsFinderPageComponent implements OnInit {
  constructor(private _metatagService: MetatagService) {}

  ngOnInit() {
    this._metatagService.setDefault();
  }
}
