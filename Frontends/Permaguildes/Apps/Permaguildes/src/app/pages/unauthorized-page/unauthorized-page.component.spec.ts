import { RouterTestingModule } from '@angular/router/testing';
import { UnauthorizedPageComponent } from './unauthorized-page.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';

describe(UnauthorizedPageComponent.name, () => {
  const test = new PermaguildesTest<UnauthorizedPageComponent>();
  test.initComponent(UnauthorizedPageComponent, {
    providers: [ MatDialogModule ],
    imports: [ RouterTestingModule.withRoutes([]) ]
  });
});
