import { Component } from '@angular/core';
import { AuthJwtService } from '@cfeltz/angular-helpers';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unauthorized-page',
  templateUrl: './unauthorized-page.component.html',
  styleUrls: ['./unauthorized-page.component.scss']
})
export class UnauthorizedPageComponent {

  constructor(private router: Router, private authService: AuthJwtService) { }

  public logout() {
    this.authService.logout()
    .subscribe(success => {
      if (success) {
        this.router.navigate(['/nav/home']);
      }
    });
  }
}
