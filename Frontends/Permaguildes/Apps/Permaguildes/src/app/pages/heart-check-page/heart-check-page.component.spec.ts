import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';

import { HeartCheckPageComponent } from './heart-check-page.component';
describe(HeartCheckPageComponent.name, () => {
  const test = new PermaguildesTest<HeartCheckPageComponent>();
  test.initComponent(HeartCheckPageComponent, {});
});
