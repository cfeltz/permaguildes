import { Component, OnInit } from '@angular/core';
import { HealthCheck } from '@cfeltz/angular-helpers';
import { ApisHealthCheckService } from 'apis-helpers';


@Component({
  selector: 'app-heart-check-page',
  templateUrl: './heart-check-page.component.html',
  styleUrls: ['./heart-check-page.component.css']
})
export class HeartCheckPageComponent implements OnInit {
  public checkResult: HealthCheck[];
  displayedColumns: string[] = ['name', 'status', 'duration' ];

  constructor(private healthcheckService: ApisHealthCheckService) { }

  ngOnInit(): void {
    this.healthcheckService.healthChecks().then(check => {
      this.checkResult = [];
      for (const key in check.entries) {
        if (check.entries[key]) {
          this.checkResult.push(check.entries[key]);
        }
      }
    });
  }

}
