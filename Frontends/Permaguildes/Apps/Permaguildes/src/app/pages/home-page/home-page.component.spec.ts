import { HomePageComponent } from './home-page.component';
import { MatDialogModule } from '@angular/material/dialog';
import { OverlayModule } from '@angular/cdk/overlay';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler';
import { AuthentificationComponent } from 'Apps/Permaguildes/src/app/components/authentification/authentification.component';

describe(HomePageComponent.name, () => {
  const test = new PermaguildesTest<HomePageComponent>();
  test.initComponent(HomePageComponent, {
    providers: [ MatDialogModule ],
    imports: [ OverlayModule ],
    declarations: [ AuthentificationComponent ]
  });
});
