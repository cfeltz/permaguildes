import { Component, OnInit, ViewChild, TemplateRef, ElementRef, ViewContainerRef, HostListener, Inject, OnDestroy } from '@angular/core';
import { Overlay, OverlayRef, OverlayConfig } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MetatagService } from 'apis-helpers';
import { ProjectCreationDialogComponent } from 'Apps/Permaguildes/src/app/dialogs/project-creation-dialog/project-creation-dialog.component';
import { Project, ProjectsService } from 'projects';
import { firstValueFrom, lastValueFrom, Observable, Subscription } from 'rxjs';
import { AuthJwtGuard } from '@cfeltz/angular-helpers';
import { PermaguildesDialogsService } from '../../services/permaguildes-dialogs/permaguildes-dialogs.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit, OnDestroy {
  private overlayRef: OverlayRef;
  private projectsDetails: TemplatePortal;
  private backdropClickSub: Subscription;
  
  public projects: Project[];
  public appVersion: string = require( '../../../../package.json').version;

  @ViewChild('projectList', { static: true }) template: TemplateRef<any>;
  @ViewChild('projectListButton', { static: true }) projectListButton: ElementRef;

  constructor(
    private overlay: Overlay,
    private vcr: ViewContainerRef,
    public dialog: MatDialog,
    private dialogsService: PermaguildesDialogsService,
    private projectsService: ProjectsService,
    private metatagService: MetatagService) {
  }

  ngOnInit() {
    this.metatagService.setDefault();
    lastValueFrom(this.projectsService.getProjects()).then(r => this.projects = r);
  }
  
  ngOnDestroy(): void {
    this.overlayRef?.dispose();
    this.projectsDetails = undefined;
    this.overlayRef = undefined;
    this.backdropClickSub?.unsubscribe();  }

  onProjectListClick() {
    // Open overlay to access to the projects list
    const config = new OverlayConfig();
    config.hasBackdrop = true;
    config.positionStrategy = this.overlay.position().flexibleConnectedTo(this.projectListButton.nativeElement)
      .withPositions([{originX: 'start', originY: 'bottom', overlayX: 'start', overlayY: 'top'}]);
    this.overlayRef = this.overlay.create(config);

    this.projectsDetails =  new TemplatePortal(this.template, this.vcr);
    this.backdropClickSub = this.overlayRef.backdropClick().subscribe(() => {
      this.overlayRef.dispose();
      this.projectsDetails = undefined;
      this.overlayRef = undefined;
    });
    this.overlayRef.attach(this.projectsDetails);
  }

  onClickOutside() {

  }

  onNewProjectClick() {
    this.dialogsService.openProjectCreationDialog();
  }
  
}
