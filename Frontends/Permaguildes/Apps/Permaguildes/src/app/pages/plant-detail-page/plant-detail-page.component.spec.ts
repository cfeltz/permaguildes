import { WaterIconComponent } from '../../components/plants/icons/water-icon/water-icon.component';
import { GrowthPropertyComponent } from '../../components/plants/properties/growth-property/growth-property.component';
import { EdibilityIconComponent } from '../../components/plants/icons/edibility-icon/edibility-icon.component';
import { MedicinalIconComponent } from '../../components/plants/icons/medicinal-icon/medicinal-icon.component';
import { PlantDetailPageComponent } from './plant-detail-page.component';

import { ActivatedRoute } from '@angular/router';
import { PlantsService, LabelsService, SophyService } from 'plants';

import plantsData from 'Libs/plants/src/lib/data/plants.json';
import { Plant } from 'plants';
import { CompanionsComponent } from '../../components/plants/companions/companions.component';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';
import { UsdahardinessPropertyComponent } from '../../components/plants/properties/usdahardiness-property/usdahardiness-property.component';
import { HeightPropertyComponent } from '../../components/plants/properties/height-property/height-property.component';
import { LabeledPropertyComponent } from '../../components/plants/properties/labeled-property/labeled-property.component';
import { LinksPropertyComponent } from '../../components/plants/properties/links-property/links-property.component';
import { ExposurePropertyComponent } from '../../components/plants/properties/exposure-property/exposure-property.component';
import { PermaguildesDialogsService } from '../../services/permaguildes-dialogs/permaguildes-dialogs.service';
import { createApiMessageInstance } from '@cfeltz/angular-helpers';

describe(PlantDetailPageComponent.name, () => {
  const plantsList = plantsData.map(jsonItem => createApiMessageInstance(Plant).loadFromJson(jsonItem));
  const plant = plantsList.find(v => v.code === 1);

  const test = new PermaguildesTest<PlantDetailPageComponent>();
  test.initComponent(PlantDetailPageComponent, {
    declarations: [
      WaterIconComponent,
      GrowthPropertyComponent,
      EdibilityIconComponent,
      UsdahardinessPropertyComponent,
      HeightPropertyComponent,
      MedicinalIconComponent,
      LabeledPropertyComponent,
      ExposurePropertyComponent,
      LinksPropertyComponent,
      CompanionsComponent
    ],
    providers: [
      LabelsService,
      PlantsService,
      SophyService,
      PermaguildesDialogsService,
      {
        provide: ActivatedRoute,
        useValue: {
          snapshot: {
            paramMap: {
              get: (key: string) => 1
            },
          },
        },
      }
    ]
    // beforeEach: () => {
    //   test.initHttpTestingController();
    // },
    // shouldCreateExtension: () => {
    //   test.http.expectOne(req => true).flush(plant);
    //   test.fixture.whenStable().then(() => {
    //     test.fixture.detectChanges();
    //     expect(test.component).toBeTruthy();
    //   });
    // }
  });
});
