import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { PlantsService, LabelsService, SophyService, LabelType } from 'plants';
import { Plant } from 'plants';
import { MetatagService } from 'apis-helpers';
import { AuthJwtService } from '@cfeltz/angular-helpers';
import { MatAccordion, MatExpansionPanel } from '@angular/material/expansion';
import { NGXLogger } from 'ngx-logger';
import { Observable, of, Subscription } from 'rxjs';
import { PermaguildesDialogsService } from '../../services/permaguildes-dialogs/permaguildes-dialogs.service';

@Component({
  selector: 'app-plant-detail-page',
  templateUrl: './plant-detail-page.component.html',
  styleUrls: ['./plant-detail-page.component.scss'],
})
export class PlantDetailPageComponent implements OnInit, OnDestroy {
  @ViewChild('descriptionPanel') descriptionPanel: MatExpansionPanel;
  @ViewChild('accordion') accordion: MatAccordion;
  plant$: Observable<Plant>;
  updateMode = false;
  savingInProgress = false;
  labelTypes = LabelType;

  navigationSubscription: Subscription;

  constructor(
    private logger: NGXLogger,
    private router: Router,
    private route: ActivatedRoute,
    private plantsService: PlantsService,
    private authService: AuthJwtService,
    private metatagService: MetatagService
  ) { }

  fixSameComponentIssueInit() {
    if (!this.navigationSubscription || this.navigationSubscription.closed) {
      this.navigationSubscription = this.router.events.subscribe(ev => {
        if (ev instanceof NavigationEnd) {
          if (this.router.url.startsWith('/nav/plant/')) {
            // Reload page
            window.location.reload();
          }
        }
      });
    }
  }

  fixSameComponentIssueDestroy() {
    this.navigationSubscription?.unsubscribe();
  }

  ngOnInit() {
    this.fixSameComponentIssueInit();
    const code = +this.route.snapshot.paramMap.get('code');
    this.metatagService.setDefault();
    if (code > 0) {
      this.plant$ = this.plantsService.getPlant(code);
    } else {
      if (this.authService.hasToken()) {
        this.logger.info('Open new plant page');
        this.plant$ = of(new Plant(0));
        this.updateMode = true;
      } else {
        this.authService.openLoginDialog().subscribe(result => {
          if (this.authService.hasToken()) {
            this.logger.info('Open new plant page');
            this.plant$ = of(new Plant(0));
            this.updateMode = true;
          }
        });
      }    
    }
  }

  ngOnDestroy() {
    this.fixSameComponentIssueDestroy();
  }
}
