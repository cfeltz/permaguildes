import { ProjectPageComponent } from './project-page.component';
import { PermaguildesTest } from '../../modules/testing.module';
import { ProjectsService } from 'projects';

describe(ProjectPageComponent.name, () => {
  const test = new PermaguildesTest<ProjectPageComponent>();
  test.initComponent(ProjectPageComponent, {
    providers: [ ProjectsService ]
  });
});
