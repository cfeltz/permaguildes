import { Component, HostListener, Inject, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Plantation, Project, ProjectsService } from 'projects';
import { NGXLogger } from 'ngx-logger';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { PlantationsListComponent } from '../../components/projects/plantations-list/plantations-list.component';
import { ProjectMapComponent } from '../../components/projects/project-map/project-map.component';
import { IAuthJwtService } from '@cfeltz/angular-helpers';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.scss'],
})
export class ProjectPageComponent implements OnInit, OnDestroy {
  public view: number;
  public navOpened: boolean;
  public project$: Observable<Project>;
  public isLogged: boolean = false;
  private isLoggedInSub1: Subscription;
  private isJwtLoggedIn$: Observable<boolean>;
  private windowWidth: number;
  private windowHeight: number;
  @ViewChildren('projectMap') projectMap: QueryList<ProjectMapComponent>;
  @ViewChildren('plantationsList') plantationsListComponent: QueryList<PlantationsListComponent>;

  constructor(private logger: NGXLogger, 
    private route: ActivatedRoute,
    @Inject('IAuthJwtService') public authJwtService: IAuthJwtService, 
    private projectsService: ProjectsService) { 
      const _this = this;
      this.isJwtLoggedIn$ = authJwtService.isLoggedIn();
      this.isLoggedInSub1 = this.isJwtLoggedIn$.subscribe({
        next(isLogged) {
          _this.isLogged = isLogged;
        }
      });
  }

  async ngOnInit() {
    this.view = 1;
    this.windowWidth = window.innerWidth;
    this.windowHeight = window.innerHeight;
    const code = +this.route.snapshot.paramMap.get('code');
    this.project$ = this.projectsService.getProject(code);
  }
  
  ngOnDestroy(): void {
    this.isLoggedInSub1?.unsubscribe();
  }

  @HostListener('window:resize')
  onResize() {
    this.windowWidth = window.innerWidth;
    this.windowHeight = window.innerHeight;
  }

  public isVerticalScreen(): boolean {
    return this.windowWidth <= this.windowHeight;
  }

  public showPlantsFinder(): boolean {
    return this.isLogged;
  }

  public onMapPlantationSelect(plantation: Plantation) {
    this.plantationsListComponent?.first?.selectPlantation(plantation);
  }

  public onMapPlantationUnselect(plantation: Plantation) {
    this.plantationsListComponent?.first?.unselectPlantation(plantation);
  }

  public onListPlantationSelect(plantation: Plantation) {
    this.projectMap?.first?.selectPlantation(plantation);
  }

  public onListPlantationUnselect(plantation: Plantation) {
    this.projectMap?.first?.unselectPlantation(plantation);
  }

  public onListPlantationVisibilityChange(plantation: Plantation) {
    this.projectMap?.first?.updateVisibility(plantation);
  }

  public onNavOpened(e: any) {
    const selectedPlantations = this.projectMap?.first?.getSelectedPlantations();
    selectedPlantations.forEach(p => {
      this.plantationsListComponent?.first?.selectPlantation(p);
    });
  }

  public waitAndSelectPlantationOnList() {
    const _this = this;
    setTimeout(function() {
      _this.onNavOpened(null);
    }, 1000);
  }
  
}
