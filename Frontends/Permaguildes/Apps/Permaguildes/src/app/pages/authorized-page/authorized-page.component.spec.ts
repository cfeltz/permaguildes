import { RouterTestingModule } from '@angular/router/testing';
import { AuthorizedPageComponent } from './authorized-page.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PermaguildesTest } from 'Apps/Permaguildes/src/app/modules/testing.module';

describe(AuthorizedPageComponent.name, () => {
  const test = new PermaguildesTest<AuthorizedPageComponent>();
  test.initComponent(AuthorizedPageComponent, {
    providers: [ MatDialogModule ],
    imports: [ RouterTestingModule.withRoutes([]) ]
  });
});
