import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthJwtService } from '@cfeltz/angular-helpers';

@Component({
  selector: 'app-authorized-page',
  templateUrl: './authorized-page.component.html',
  styleUrls: ['./authorized-page.component.scss']
})
export class AuthorizedPageComponent {
  isLoggedIn$: Observable<boolean>;

  constructor(private router: Router, private authService: AuthJwtService) {
    this.isLoggedIn$ = authService.isLoggedIn();
  }

  public login() {
    this.authService.login('cfeltz', 'Christophe.feltz@gmail.com1').subscribe();
  }

  public logout() {
    this.authService.logout().subscribe();
  }
}
