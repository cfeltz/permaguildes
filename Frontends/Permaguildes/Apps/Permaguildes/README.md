# Development configuration
Install NodeJS https://nodejs.org/ (NodeJS v14.15.1 LTS)
Install Git https://git-scm.com/download/win
Install the following modules : 
npm install -g @angular/cli@11.2.8
npm install -g windows-build-tools (with an administrator account: use cmd)
npm install -g http-server
npm install

#nginx usage
http://nginx.org/en/docs/beginners_guide.html
Configure nginx.conf (etc/nginx/sites-enabled/default) with 
        location / {
            root   ../Development/plants/Frontends/Permaguildes/dist/PermaGuildes;
            index  index.html index.htm;
        }
				
		rewrite ^/nav/* /index.html;


# Migration to next Angular version
See https://update.angular.io/

# Development
Run 'npm install' to load modules
Run 'ng test' to check units tests
Run 'ng build' to build project (use the `--prod="true"` flag for a production build)
Run 'ng serve' to start server

Run `ng generate component pages/component-name --module modules/pages` to generate a new page
Run `ng generate component components/component-name --module modules/components` to generate a new component
    and add it to AppComponentsModule.exports
Run `ng generate service services/service-name/service-name` to generate a new service
Run `ng generate directive directives/directive-name --module directives` to generate a new directive 
You can also use `ng g pipe|service|class|guard|interface|enum|module`.

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/) 
    --> Not implemented

# Test Service Worker
run 'ng build --prod'
run 'http-server -p 4300 -c-1 dist/Permaguildes'

# References
https://angular.io/api : Angular API
https://wkoza.developpez.com/ : Apprendre à programmer avec le framework Angular (directives, databinding...)
https://github.com/angular/flex-layout/blob/master/README.md : Flex API reference
https://makina-corpus.com/blog/metier/2017/premiers-pas-avec-rxjs-dans-angular : Utilisation des observables
IGN : 
    https://geoservices.ign.fr/blog/2021/02/08/Acceder_gratuitement_offre_IGN.html
    https://geoservices.ign.fr/ressources_documentaires/Espace_documentaire/Bouquets_de_ressources_Geoservices_IGN.pdf
    https://github.com/IGNF/geoportal-third-party-integration/blob/master/simple-map-leaflet-angular