Angular configuration with nginx : 
Generate certificate : 
    letsencrypt certonly -a webroot --webroot-path=/var/www/Frontends/Plants -d www.permaguildes.fr
      --> /etc/letsencrypt/live/www.permaguildes.fr/fullchain.pem
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

nginx.conf
    server {
      listen 80;
      listen [::]:80;
      server_name  www.permaguildes.fr;
      
      return 301 https://$server_name$request_uri;
    }

    server {
      listen 443 ssl http2;
      listen [::]:443 ssl http2;
      server_name  www.permaguildes.fr;
      include snippets/ssl-www.permaguildes.fr.conf;
      include snippets/ssl-params.conf;
      
      root /var/www/Frontends/Plants;

      location / {
        index  index.html index.htm;
      }
          
      rewrite ^/nav/* /index.html;
    }

Source : https://www.supinfo.com/articles/single/3558-installer-certificat-ssl-nginx-avec-let-s-encrypt



.Net core configuration with nginx reverse proxy
Generate certificate : 
    mkdir -p /var/www/Backends/PlantsApi/.well-known
    add to nginx.conf
      location /.well-known {
        alias /var/www/Backends/PlantsApi/.well-known;
      }

    certbot certonly --webroot -w /var/www/Backends/PlantsApi/ -d plantsapi.permaguildes.fr
      --> /etc/letsencrypt/live/plantsapi.permaguildes.fr/fullchain.pem

nginx.conf
    server {
      listen 80;
      listen [::]:80;
      server_name  plantsapi.permaguildes.fr;

      rewrite ^ https://$host$request_uri? permanent;
    }

    server {
      listen 443 ssl http2;
      listen [::]:443 ssl http2;
      server_name  plantsapi.permaguildes.fr;
      include snippets/ssl-plantsapi.permaguildes.fr.conf;
      include snippets/ssl-params.conf;
      
      location /.well-known {
        alias /var/www/Backends/PlantsApi/.well-known;
      }

      location / {
        proxy_pass         http://localhost:5000;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection keep-alive;
        proxy_set_header   Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
      }
    }
  
Source with reverse proxy : https://w3.nonsenz.org/2017/06/lets-encrypt-nginx-https/