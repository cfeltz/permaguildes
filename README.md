Projet d'aide à la création d'une forêt comestible.

Projets : 
    Frontends
        Plants (http://localhost:4300/) : Site web affichant la liste des plantes
    Microservices
        PlantsApi (http://localhost:45000/api/**/*)
        PlantsApi.Tests
        PlantsApi.BusinessTests

Le projet est configuré pour fonctionner avec trois types de bases de données (SQL Server, MySql et Postgres).
La cible final est à priori MySql.

Serveur : 
    FELTZ-NGINX : 109.238.6.122
    Folders : /var/www/Backends/PlantsApi
              /var/www/Frontends/Plants


Troubleshoot guide : 
bind: An attempt was made to access a socket in a way forbidden by its access permissions
--> netsh interface ipv4 show excludedportrange protocol=tcp