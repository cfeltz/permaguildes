#!/bin/bash

platform=$1

if [ -z "$platform" ]
then 
    echo "Please specify platform arg (integration, uat, preprod, beta or prod)"
    exit 1
fi

sed s/{platform}/$platform/g ../../Common/Config/docker-compose.platform.bases.yml > Config/docker-compose.$platform.bases.yml

docker-compose -p Permaguildes -f ../../Common/Config/docker-compose.base.sql.mysql.yml \
                           -f ../../Common/Config/docker-compose.base.elk.yml \
                           -f ../../Common/Config/docker-compose.base.traefik.yml \
                           -f ../../Common/Config/docker-compose.base.portainer.yml \
                           -f Config/docker-compose.$platform.bases.yml \
                           -f Config/docker-compose.$platform.bases.extension.yml \
                           up -d 