DELIMITER $$
CREATE PROCEDURE dropAllTables()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE tableName VARCHAR(255);
	DECLARE str  VARCHAR(255);
	DECLARE curTable CURSOR FOR SELECT table_name FROM information_schema.tables WHERE table_schema = DATABASE();
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	OPEN curTable;

	SET FOREIGN_KEY_CHECKS = 0;
	getTable:  LOOP
		FETCH curTable INTO tableName;
		IF done THEN 
        LEAVE getTable;
	   END IF;
	   
		SET @sql := CONCAT('DROP TABLE IF EXISTS ', tableName);
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
	END LOOP;
	CLOSE curTable;
	
	SET FOREIGN_KEY_CHECKS = 1;
END$$

DELIMITER ;

CALL dropAllTables();
DROP PROCEDURE dropAllTables;