#!/bin/bash
source ../../Common/Scripts/include.sh  

tag=$1
dockerregistry=registry.gitlab.com/cfeltz/permaguildes

for project in "${projects[@]}"
do
    projectDetail=(${project//:/ })
    projectName=${projectDetail[0]}
    projectPreTag=${projectDetail[1]}

    docker pull $dockerregistry/$projectName:${projectPreTag}$tag
    docker tag $dockerregistry/$projectName:${projectPreTag}$tag permaguildes.$projectName:${projectPreTag}$tag
done
