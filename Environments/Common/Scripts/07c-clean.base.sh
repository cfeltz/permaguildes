#!/bin/bash

platform=$1

if [ -z "$platform" ]
then 
    echo "Please specify platform arg (integration, uat, preprod, beta or prod)"
    exit 1
fi

docker stop permaguildes.elasticsearch.$platform permaguildes.kibana.$platform permaguildes.heartbeat.$platform permaguildes.mysql.data.$platform 
docker rm permaguildes.elasticsearch.$platform permaguildes.kibana.$platform permaguildes.heartbeat.$platform permaguildes.mysql.data.$platform
docker ps -a