#!/bin/bash
source ../../Common/Scripts/include.sh  

environment=$1

if [ -z "$environment" ]
then 
    logError "Please specify environment arg (int, qat, demo, uat, prod, etc...)"
    exit 1
fi

for project in "${projects[@]}"
do
    projectDetail=(${project//:/ })
    projectName=${projectDetail[0]}
    projectPreTag=${projectDetail[1]}
    projectSuffix=$(echo $projectPreTag | sed 's/\.//' | sed 's/-//')

    logInfo "Stop permaguildes.$projectName${projectSuffix}.$environment"
    docker stop permaguildes.$projectName${projectSuffix}.$environment
done
