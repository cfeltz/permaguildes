#!/bin/bash
# For color usage, see https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
Color_Off='\033[0m'       # Text Reset
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Red='\033[0;31m'          # Red

logInfo() {
    echo -e "${Green}$1${Color_Off}"
}
logMainInfo() {
    echo -e "${Yellow}$1${Color_Off}"
}
logError() {
    echo -e "${Red}$1${Color_Off}"
}

projects=(www plantsapi logsapi projectsapi nurseriesapi)

RUNNING="$(basename $0)"

if [[ "$RUNNING" == "include.sh" ]]
then
  script "$@"
fi
