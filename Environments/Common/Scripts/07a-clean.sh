#!/bin/bash
source ../../Common/Scripts/include.sh  

environment=$1

if [ -z "$environment" ]
then 
    logError "Please specify environment arg (int, qat, demo, uat, prod, etc...)"
    exit 1
fi

for project in "${projects[@]}"
do
    projectDetail=(${project//:/ })
    projectName=${projectDetail[0]}
    projectPreTag=${projectDetail[1]}
    projectSuffix=$(echo $projectPreTag | sed 's/\.//' | sed 's/-//')

    logInfo "docker rm permaguildes.$projectName$projectSuffix.$environment -f"
    docker rm permaguildes.$projectName$projectSuffix.$environment -f
done
