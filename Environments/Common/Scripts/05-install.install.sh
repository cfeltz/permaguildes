#!/bin/bash
source ../../Common/Scripts/include.sh  

platform=$1
tagSource=$2
tagDestination=$3
environment=$tagDestination

if [ -z "$platform" ]
then 
    logError "Please specify platform arg (integration, uat, preprod, beta or prod)"
    exit 1
fi

if [ -z "$tagSource" ] || [ -z "$tagDestination" ]
then 
    logError "Please specify source and destination tags"
    exit 1
fi

for project in "${projects[@]}"
do
    projectDetail=(${project//:/ })
    projectName=${projectDetail[0]}
    projectPreTag=${projectDetail[1]}

    docker tag permaguildes.$projectName:${projectPreTag}$tagSource permaguildes.$projectName:${projectPreTag}$tagDestination
done

logInfo "Create Config/docker-compose.$environment.yml file"
sed s/{env}/$environment/g ../../Common/Config/docker-compose.env.yml > Config/docker-compose.$environment.yml

projectUrl="https://www.$environment.permaguildes.fr"
if [ "$platform" == "production" ] && [ "$environment" == "prod" ]
then
    logInfo "Remove prod in url"
    sed s/prod.permaguildes.fr/permaguildes.fr/g Config/docker-compose.$environment.yml > Config/docker-compose.$environment.tmp.yml
    rm Config/docker-compose.$environment.yml
    mv Config/docker-compose.$environment.tmp.yml Config/docker-compose.$environment.yml
    projectUrl="https://www.permaguildes.fr"
fi


# logInfo "Create Config/docker-compose.$platform.$environment.yml file"
# sed s/{env}/$environment/g Config/docker-compose.$platform.yml > Config/docker-compose.$platform.$environment.yml

# docker-compose -p Permaguildes -f Config/docker-compose.$environment.yml -f Config/docker-compose.$platform.$environment.yml up -d
docker-compose -p Permaguildes -f Config/docker-compose.$environment.yml up -d

logMainInfo "Open url $projectUrl"
echo "To debug, open Traefik url http://178.170.54.247:8080"
echo "or Portainer url http://178.170.54.247:9000"
