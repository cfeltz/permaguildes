ECHO OFF 
ECHO Remove logs files
rmdir ..\..\Backends\PlantsApi\Logs /Q /S 2> nul
rmdir ..\..\Backends\LogsApi\Logs /Q /S 2> nul
rmdir ..\..\Backends\ProjectsApi\Logs /Q /S 2> nul
rmdir ..\..\Backends\NurseriesApi\Logs /Q /S 2> nul
ECHO Done
ECHO .
