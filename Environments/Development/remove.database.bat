ECHO Remove databases
ECHO ON
docker exec -it permaguildes.mysql.data.dev mysql --user="PermaguildesUser" --password="Pass@word1" --execute="DROP DATABASE PlantsDev"
docker exec -it permaguildes.mysql.data.dev mysql --user="PermaguildesUser" --password="Pass@word1" --execute="DROP DATABASE ProjectsDev"
docker exec -it permaguildes.mysql.data.dev mysql --user="PermaguildesUser" --password="Pass@word1" --execute="DROP DATABASE NurseriesDev"
