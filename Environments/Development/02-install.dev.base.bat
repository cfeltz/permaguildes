docker-compose -p permaguildesdev -f ../Common/Config/docker-compose.base.sql.mysql.yml -f Config/docker-compose.base.sql.mysql.dev.yml up -d 
docker-compose -p permaguildesdev -f ../Common/Config/docker-compose.base.elk.yml -f Config/docker-compose.base.elk.dev.yml up -d 
docker-compose -p permaguildesdev -f ../Common/Config/docker-compose.base.portainer.yml -f Config/docker-compose.base.portainer.dev.yml up -d 
docker-compose -p permaguildesdev -f ../Common/Config/docker-compose.base.traefik.yml -f Config/docker-compose.base.traefik.dev.yml up -d 