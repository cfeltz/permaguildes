# To deploy on docker registry from dev registry
 - Open Azure CLI (download from https://aka.ms/installazurecliwindows) 
 - Execute scripts with command "bash xx-script.sh"
                  Script "03*-install.dev.registry.*.bat" pull images from Azure registry
                  Script "03-install.dev.local.compose.bat" build local images 


Remark : To use Traefik, please add this on C:\Windows\System32\drivers\etc\hosts
127.0.0.1 dev.permaguildes.fr
127.0.0.1 www.dev.permaguildes.fr
127.0.0.1 plantsapi.dev.permaguildes.fr
127.0.0.1 logsapi.dev.permaguildes.fr
127.0.0.1 projectsapi.dev.permaguildes.fr

And to see Traefik detail, open url http://dev.permaguildes.fr:8080