docker tag permaguildes.www:dev registry.gitlab.com/cfeltz/permaguildes/www:develop
docker tag permaguildes.plantsapi:dev registry.gitlab.com/cfeltz/permaguildes/plantsapi:develop
docker tag permaguildes.logsapi:dev registry.gitlab.com/cfeltz/permaguildes/logsapi:develop
docker tag permaguildes.projectsapi:dev registry.gitlab.com/cfeltz/permaguildes/projectsapi:develop

docker push registry.gitlab.com/cfeltz/permaguildes/www:develop
docker push registry.gitlab.com/cfeltz/permaguildes/plantsapi:develop
docker push registry.gitlab.com/cfeltz/permaguildes/logsapi:develop
docker push registry.gitlab.com/cfeltz/permaguildes/projectsapi:develop