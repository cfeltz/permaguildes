ECHO OFF 
ECHO Remove logs files
rmdir ..\..\Backends\PlantsApi\bin /Q /S 2> nul
rmdir ..\..\Backends\PlantsApi\obj /Q /S 2> nul
rmdir ..\..\Backends\PlantsApi\Logs /Q /S 2> nul
rmdir ..\..\Backends\PlantsApi.Tests\bin /Q /S 2> nul
rmdir ..\..\Backends\PlantsApi.Tests\obj /Q /S 2> nul
rmdir ..\..\Backends\PlantsApi.BusinessTests\bin /Q /S 2> nul
rmdir ..\..\Backends\PlantsApi.BusinessTests\obj /Q /S 2> nul

rmdir ..\..\Backends\LogsApi\bin /Q /S 2> nul
rmdir ..\..\Backends\LogsApi\obj /Q /S 2> nul
rmdir ..\..\Backends\LogsApi\Logs /Q /S 2> nul

rmdir ..\..\Backends\ProjectsApi\bin /Q /S 2> nul
rmdir ..\..\Backends\ProjectsApi\obj /Q /S 2> nul
rmdir ..\..\Backends\ProjectsApi\Logs /Q /S 2> nul
rmdir ..\..\Backends\ProjectsApi.BusinessTests\bin /Q /S 2> nul
rmdir ..\..\Backends\ProjectsApi.BusinessTests\obj /Q /S 2> nul

rmdir ..\..\Backends\NurseriesApi\bin /Q /S 2> nul
rmdir ..\..\Backends\NurseriesApi\obj /Q /S 2> nul
rmdir ..\..\Backends\NurseriesApi\Logs /Q /S 2> nul
rmdir ..\..\Backends\NurseriesApi.BusinessTests\bin /Q /S 2> nul
rmdir ..\..\Backends\NurseriesApi.BusinessTests\obj /Q /S 2> nul
ECHO .

rmdir %APPDATA%\Containers\PlantsApi /Q /S 2> nul
rmdir %APPDATA%\Containers\LogsApi /Q /S 2> nul
rmdir %APPDATA%\Containers\ProjectsApi /Q /S 2> nul
rmdir %APPDATA%\Containers\NurseriesApi /Q /S 2> nul
ECHO Done
ECHO .

