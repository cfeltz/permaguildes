docker pull registry.gitlab.com/cfeltz/permaguildes/www:develop
docker pull registry.gitlab.com/cfeltz/permaguildes/plantsapi:develop
docker pull registry.gitlab.com/cfeltz/permaguildes/logsapi:develop
docker pull registry.gitlab.com/cfeltz/permaguildes/projectsapi:develop

docker tag registry.gitlab.com/cfeltz/permaguildes/www:develop permaguildes.www:dev
docker tag registry.gitlab.com/cfeltz/permaguildes/plantsapi:develop permaguildes.plantsapi:dev
docker tag registry.gitlab.com/cfeltz/permaguildes/logsapi:develop permaguildes.logsapi:dev
docker tag registry.gitlab.com/cfeltz/permaguildes/projectsapi:develop permaguildes.projectsapi:dev