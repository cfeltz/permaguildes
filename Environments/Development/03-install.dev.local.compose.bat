cscript //NoLogo sed.vbs s/{env}/dev/ < ../Common/Config/docker-compose.env.yml > Config/docker-compose.dev.yml

docker-compose -p permaguildesdev -f Config/docker-compose.dev.yml -f Config/docker-compose.dev.local.yml -f Config/docker-compose.dev.local.build.yml up -d

echo "Open url http://www.dev.permaguildes.fr"