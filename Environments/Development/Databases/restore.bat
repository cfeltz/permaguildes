docker cp Backups\PlantsBackup.sql permaguildes.mysql.data.dev:/tmp
docker cp Backups\ProjectsBackup.sql permaguildes.mysql.data.dev:/tmp
docker cp Backups\NurseriesBackup.sql permaguildes.mysql.data.dev:/tmp

docker cp .\restore-indocker.sh permaguildes.mysql.data.dev:/tmp
docker exec -it permaguildes.mysql.data.dev /bin/bash -c "/tmp/restore-indocker.sh"