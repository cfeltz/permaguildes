#To prepare server
 - Se connecter en root (command su)
 - Install docker-compose : 
      curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
      chmod +x /usr/local/bin/docker-compose
      docker-compose --version

#Augmenter la taille du disque dur : https://fr-wiki.ikoula.com/fr
      rm /var/lib/cloud/sem/*
      Eteindre l'instance et modifier la taille du disque depuis cloudstack (https://cloudstack.ikoula.com/client/)
      restart
      Pour vérifier :   lsblk /dev/xvda 
                        parted -s /dev/xvda unit MB print

#Augmenter la taille du swapping du serveur : 
      free -h
      fallocate -l 5G /swapfile
      chmod 600 /swapfile
      mkswap /swapfile
      swapon /swapfile
      nano /etc/fstab
            /swapfile swap swap defaults 0 0
      swapon --show



#To install application on premise
 - cd /app
 - Execute scripts with command "bash linux-conversion.sh"
 - cd Production/ubuntu
 - Execute scripts with command "bash xx-script.sh"
 - Backup and restore database with commands "bash xx-backup.sh" and "bash xx-restore.sh" on Databases subfolder

Platform : 
 - 178.170.54.247 : root / ********
