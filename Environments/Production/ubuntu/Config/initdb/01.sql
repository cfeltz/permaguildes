CREATE DATABASE IF NOT EXISTS `Projectsbeta`;
GRANT ALL ON `Projectsbeta`.* TO 'PermaguildesUser'@'%';

CREATE DATABASE IF NOT EXISTS `Nurseriesbeta`;
GRANT ALL ON `Nurseriesbeta`.* TO 'PermaguildesUser'@'%';


CREATE DATABASE IF NOT EXISTS `Plantsprod`;
GRANT ALL ON `Plantsprod`.* TO 'PermaguildesUser'@'%';

CREATE DATABASE IF NOT EXISTS `Projectsprod`;
GRANT ALL ON `Projectsprod`.* TO 'PermaguildesUser'@'%';

CREATE DATABASE IF NOT EXISTS `Nurseriesprod`;
GRANT ALL ON `Nurseriesprod`.* TO 'PermaguildesUser'@'%';