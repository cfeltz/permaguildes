docker cp ./Backups/PlantsBetaBackup.sql permaguildes.mysql.data.production:/tmp
docker cp ./Backups/ProjectsBetaBackup.sql permaguildes.mysql.data.production:/tmp
docker cp ./Backups/NurseriesBetaBackup.sql permaguildes.mysql.data.production:/tmp

docker cp ./restore-beta-indocker.sh permaguildes.mysql.data.production:/tmp

docker exec -it permaguildes.mysql.data.production /bin/bash -c "chmod +x /tmp/restore-beta-indocker.sh"
docker exec -it permaguildes.mysql.data.production /bin/bash -c "/tmp/restore-beta-indocker.sh"