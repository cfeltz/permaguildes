rm -rf Backups/PlantsBetaBackup.old
rm -rf Backups/ProjectsBetaBackup.old
rm -rf Backups/NurseriesBetaBackup.old
cp Backups/PlantsBetaBackup.sql Backups/PlantsBetaBackup.old
cp Backups/ProjectsBetaBackup.sql Backups/ProjectsBetaBackup.old
cp Backups/NurseriesBetaBackup.sql Backups/NurseriesBetaBackup.old

docker cp ./backup-beta-indocker.sh permaguildes.mysql.data.production:/tmp
docker exec -it permaguildes.mysql.data.production /bin/bash -c "chmod +x /tmp/backup-beta-indocker.sh"
docker exec -it permaguildes.mysql.data.production /bin/bash -c "/tmp/backup-beta-indocker.sh"

docker cp permaguildes.mysql.data.production:/tmp/PlantsBetaBackup.sql ./Backups/PlantsBetaBackup.sql
docker cp permaguildes.mysql.data.production:/tmp/ProjectsBetaBackup.sql ./Backups/ProjectsBetaBackup.sql
docker cp permaguildes.mysql.data.production:/tmp/NurseriesBetaBackup.sql ./Backups/NurseriesBetaBackup.sql
