-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: Nurseriesprod
-- ------------------------------------------------------
-- Server version	5.7.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Nurseries`
--

DROP TABLE IF EXISTS `Nurseries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Nurseries` (
  `code` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zipCode` varchar(5) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `dtCreation` datetime(6) NOT NULL,
  `dtUpdate` datetime(6) NOT NULL,
  `tel1` varchar(25) DEFAULT NULL,
  `tel2` varchar(25) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Nurseries`
--

LOCK TABLES `Nurseries` WRITE;
/*!40000 ALTER TABLE `Nurseries` DISABLE KEYS */;
INSERT INTO `Nurseries` VALUES (1,'La Feuillade','Gaec Pépinière La Feuillade','30450','Génolhac',NULL,'2022-04-01 10:44:55.290048','2022-04-01 10:44:55.290048','04 66 61 15 92',NULL,'https://www.lafeuillade.com/',NULL),(2,'Permafruit','La coumeillo','11230','Tréziers','Contact par mail à privilégier : permafruit@gmail.com','2022-04-17 11:07:25.934638','2022-04-17 11:07:25.934638',NULL,NULL,'https://www.permafruit.fr/',NULL),(3,'Pépinière de la Brèze',NULL,'34700','Saint-Étienne-de-Gourgas',NULL,'2022-04-17 17:18:44.872983','2022-04-17 17:18:44.872983','06 56 76 46 07',NULL,NULL,NULL),(4,'Pépinière Jabouin',NULL,NULL,NULL,NULL,'2022-04-17 17:22:30.544247','2022-04-17 17:22:30.544247','06 12 93 08 54',NULL,'http://pepinieresjabouin-varietesanciennes.com/',NULL),(5,'Pépinière Watson, l\'arbre aux fruits','Hameau de Caillens','11140','Rodome','Contact mail : contact@larbreauxfruits.fr','2022-04-17 17:36:29.143770','2022-04-17 17:36:29.143770','06 87 93 38 88',NULL,'https://www.larbreauxfruits.fr/',NULL),(6,'Botanic','Parc d\'activité La Peyrière','34430','Saint-Jean-de-Védas',NULL,'2022-04-17 17:40:54.381306','2022-04-17 17:40:54.381306','04 67 68 61 90',NULL,NULL,NULL),(7,'Les Jardins de Peyreladas','Peyreladas','23480','Ars','Email : peyreladas@gmail.com','2022-04-17 17:44:15.150900','2022-04-17 17:44:15.150900','07 81 14 84 11',NULL,'https://www.jardins-de-peyreladas.com/',NULL),(8,'Arbres de provence','La curniére, route de Saint Cannat','13840','Rognes',NULL,'2022-04-17 18:00:48.039733','2022-04-17 18:00:48.039733','06 24 99 24 65',NULL,'http://arbres-de-provence.e-monsite.com/',NULL),(9,'Pépinière Grange','Chemin des terres rouges','30360','Vézénobres','Email : contact@pepinieregrange.fr','2022-04-17 18:27:48.491144','2022-04-17 18:27:48.491144','07 86 15 64 47',NULL,'https://www.pepinieregrange.fr/',NULL),(10,'Pépinière l\'Arc en Fleurs','156 chemin du Viala','34400','Saint-Christol','Email : contact@arcenfleurs.fr','2022-04-18 06:39:01.935394','2022-04-18 06:39:01.935394','06 65 17 78 26','06 72 94 49 23','https://arcenfleurs.fr/',NULL),(11,'Vessières','627 La Roseraie, KM 2 Saint-Féliu-d\'Avall','66170','Millas','Email : m.vessieres@hotmail.com','2022-04-18 07:08:58.135197','2022-04-18 07:08:58.135197','04 68 57 80 25',NULL,'http://www.rosiers-vessieres.fr/',NULL),(12,'Pépinière de la Bendola',NULL,'06540','Saorge','Email : pepinierebendola@posteo.net','2022-04-18 07:23:57.333004','2022-04-18 07:23:57.333004','07 83 32 10 85‬',NULL,'https://www.pepinieredelabendola.fr/',NULL),(13,'Pépinières Quissac','801 Rte de Saint-Come','30250','Souvignargues','Email : contact@jardin-ecologique.fr','2022-04-18 07:41:51.834174','2022-04-18 07:41:51.834174','06 28 19 14 55',NULL,'https://www.jardin-ecologique.fr/',NULL),(14,'Joala Pépinière','Le bourg, rue de l’église','24580','Fleurac','Email : joalapepiniere@gmail.com\nFacebook : https://www.facebook.com/Joala-P%C3%A9pini%C3%A8re-107741407599210/','2022-04-18 07:45:55.515199','2022-04-18 07:45:55.515199','07 83 46 86 53',NULL,'https://joala.fr/',NULL),(15,'VitiCabrol','18, Avenue de Bélarga','34230','Plaissan','Email : contact@viticabrol.fr','2022-04-18 08:16:13.945033','2022-04-18 08:16:13.945033','06 72 73 04 22',NULL,NULL,NULL),(16,'Les petits fruits de Daniel Duret','Le, Les Deffants','16360','Le Tâtre',NULL,'2022-04-28 06:28:47.698233','2022-04-28 06:28:47.698233','05 45 78 48 33',NULL,'https://www.petitsfruits.com/',NULL);
/*!40000 ALTER TABLE `Nurseries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `__EFMigrationsHistory`
--

DROP TABLE IF EXISTS `__EFMigrationsHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(150) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__EFMigrationsHistory`
--

LOCK TABLES `__EFMigrationsHistory` WRITE;
/*!40000 ALTER TABLE `__EFMigrationsHistory` DISABLE KEYS */;
INSERT INTO `__EFMigrationsHistory` VALUES ('20220321112939_Init','6.0.1'),('20220325080605_AddTelAndUrl','6.0.1'),('20220420085357_AddEmail','6.0.1');
/*!40000 ALTER TABLE `__EFMigrationsHistory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-05  7:00:44
