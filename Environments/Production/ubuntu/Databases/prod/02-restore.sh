docker cp ./Backups/PlantsProdBackup.sql permaguildes.mysql.data.production:/tmp
docker cp ./Backups/ProjectsProdBackup.sql permaguildes.mysql.data.production:/tmp
docker cp ./Backups/NurseriesProdBackup.sql permaguildes.mysql.data.production:/tmp

docker cp ./restore-prod-indocker.sh permaguildes.mysql.data.production:/tmp

docker exec -it permaguildes.mysql.data.production /bin/bash -c "chmod +x /tmp/restore-prod-indocker.sh"
docker exec -it permaguildes.mysql.data.production /bin/bash -c "/tmp/restore-prod-indocker.sh"