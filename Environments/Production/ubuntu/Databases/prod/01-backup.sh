rm -rf Backups/PlantsProdBackup.old
rm -rf Backups/ProjectsProdBackup.old
rm -rf Backups/NurseriesProdBackup.old
cp Backups/PlantsProdBackup.sql Backups/PlantsProdBackup.old
cp Backups/ProjectsProdBackup.sql Backups/ProjectsProdBackup.old
cp Backups/NurseriesProdBackup.sql Backups/NurseriesProdBackup.old

docker cp ./backup-prod-indocker.sh permaguildes.mysql.data.production:/tmp
docker exec -it permaguildes.mysql.data.production /bin/bash -c "chmod +x /tmp/backup-prod-indocker.sh"
docker exec -it permaguildes.mysql.data.production /bin/bash -c "/tmp/backup-prod-indocker.sh"

docker cp permaguildes.mysql.data.production:/tmp/PlantsProdBackup.sql ./Backups/PlantsProdBackup.sql
docker cp permaguildes.mysql.data.production:/tmp/ProjectsProdBackup.sql ./Backups/ProjectsProdBackup.sql
docker cp permaguildes.mysql.data.production:/tmp/NurseriesProdBackup.sql ./Backups/NurseriesProdBackup.sql
