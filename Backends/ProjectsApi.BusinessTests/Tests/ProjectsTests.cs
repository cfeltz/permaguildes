using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PermaGuildes.Backends.ProjectsApi.Models;
using Xunit;
using Xunit.Abstractions;

namespace PermaGuildes.Backends.ProjectsApi.BusinessTests.Tests
{
    public class ProjectsTests : LocalClassFixtureWithData
    {   
        public ProjectsTests(ProjectsApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        [Fact]
        public async Task GetProjectsAsync()
        {
            Project[] projects = await GetResult<Project[]>("/api/Projects");
        }

        #region Create project and get it
        [Fact]
        public async Task CreateProjectAndGetItAsync()
        {
            Project projectInDatabase = null;
            try 
            {
                string name = "Test project";
                var project = new Project(name, 43.565859f, 3.877085f, 18);
                project.description = "Project used by business tests";
                projectInDatabase = _dbContext.Projects.Where(a => a.name == name).FirstOrDefault();
                Assert.True(projectInDatabase == null);
                
                // Attempt to call request
                string url = "/api/projects";
                // Call request to create project and get result
                var result = await PostResult(url, project);
                Assert.Equal(HttpStatusCode.Created, result.StatusCode);

                projectInDatabase = _dbContext.Projects.Where(a => a.name == name).FirstOrDefault();
                Assert.True(projectInDatabase != null);
                Assert.True(projectInDatabase.name == name);

                // Get url location for get project
                HttpResponseHeaders headers = result.Headers;
                url = headers.Where(h => h.Key == "Location").Select(h => h.Value).FirstOrDefault().First();
                // Call request and get result
                var projectResult = await GetResult<Project>(url);

                Assert.True(projectResult != null);
                Assert.Equal(projectInDatabase.code, projectResult.code);
                Assert.Equal(project.name, projectResult.name);
   
            }
            finally 
            {
                if (projectInDatabase != null) 
                {
                    _dbContext.Projects.Remove(projectInDatabase);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }
        #endregion
    }
}