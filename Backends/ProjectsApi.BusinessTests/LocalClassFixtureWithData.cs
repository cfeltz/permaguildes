using Xunit.Abstractions;
using CFeltz.Shared.TestWebApiHelper.Tests;
using PermaGuildes.Backends.ProjectsApi.Database;
using Microsoft.EntityFrameworkCore;

namespace PermaGuildes.Backends.ProjectsApi.BusinessTests
{
    public abstract class LocalClassFixtureWithData : LocalClassFixture<Startup, ProjectsApiTestFixture>
    {
        protected ProjectsApiContext _dbContext;
        protected bool _skipTestsWithDatabaseImpact = false;

        protected LocalClassFixtureWithData(ProjectsApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
            _dbContext = fixture.GetApplicationContext<ProjectsApiContext>();
            if (!_dbContext.Database.IsSqlite() && !_dbContext.Database.IsInMemory()) 
            {
                var config = fixture.GetApplicationConfiguration();
                if (config["SkipTestsOnRealDatabase"] != null) 
                {
                    _skipTestsWithDatabaseImpact = bool.Parse(config["SkipTestsOnRealDatabase"]);
                }
            }
        }
    }
}