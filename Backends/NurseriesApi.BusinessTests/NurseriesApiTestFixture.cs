using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using CFeltz.Shared.TestWebApiHelper;
using CFeltz.Shared.WebApiHelper.ExtensionsBase;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper.Models;
using CFeltz.Shared.WebApiHelper.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;

namespace PermaGuildes.Backends.NurseriesApi.BusinessTests
{
    public class NurseriesApiTestFixture : TestFixture<Startup>
    {

        public NurseriesApiTestFixture() : base()
        {
        }

        protected override void ConfigureTestServices(IServiceCollection services, ApiConfiguration apiConfiguration = null) {
            base.ConfigureTestServices(services, apiConfiguration);
        }

    }
}