using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using System.Linq;
using PermaGuildes.Backends.NurseriesApi.Models;
using MassTransit.Testing;
using MassTransit;
using Microsoft.Extensions.Logging;
using PermaGuildes.Backends.NurseriesApi.Services;
using System.Threading;
using PermaGuildes.Backends.NurseriesApi.Database;
using PermaGuildes.Backends.NurseriesApi.Controllers;
using Microsoft.EntityFrameworkCore;
using CFeltz.Shared.TestWebApiHelper.Tests;

namespace PermaGuildes.Backends.NurseriesApi.BusinessTests.Tests
{
    public class NurseriesTests : LocalClassFixtureWithDatabaseCrudController
        <Startup, NurseriesApiTestFixture, NurseriesApiContext, NurseriesController, Nursery, Nursery>
    {   
        // override protected void Init()
        // {
        //     base.Init();
        //     this.fixture.AddTestServices<IHttpRequestService, TestHttpRequestService>();
        // }

        public NurseriesTests(NurseriesApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        #region Override abstract methods        
        protected override bool CompareAttributes(Nursery entity, Nursery dto) 
            => entity.name == dto.name && entity.description == dto.description;

        protected override bool CompareDtosAttributes(Nursery dto1, Nursery dto2) 
            => dto1.name == dto2.name && dto1.description == dto2.description;

        protected override Nursery CreateDtoForTest(string state) 
        {
            return new Nursery(state);
        }
        protected override Nursery GetEntityForTestFromRepository(string state) => this.GetDbSetOfEntities().Where(e => e.name == state).FirstOrDefault();

        protected override void UpdateDtoForTest(Nursery dto)
        {
            dto.description = "Nursery updated";
        }

        protected override DbSet<Nursery> GetDbSetOfEntities() => dbContext.Nurseries;
        #endregion


		#region Override CRUD tests
        [Fact]
        override public async Task CRUDTest_Create() => await base.CRUDTest_Create();

        [Fact]
        override public async Task CRUDTest_CreateMultiple() => await base.CRUDTest_CreateMultiple();

        [Fact]
        override public async Task CRUDTest_GetSingle() => await base.CRUDTest_GetSingle();
        
        [Fact]
        override public async Task CRUDTest_GetAll() => await base.CRUDTest_GetAll();

        [Fact]
        override public async Task CRUDTest_GetByList() => await base.CRUDTest_GetByList();

        [Fact]
        override public async Task CRUDTest_Update() => await base.CRUDTest_Update();

        [Fact]
        override public async Task CRUDTest_UpdateMultiple() => await base.CRUDTest_UpdateMultiple();

        [Fact]
        override public async Task CRUDTest_Remove() => await base.CRUDTest_Remove();

        [Fact]
        override public async Task CRUDTest_RemoveMultiple() => await base.CRUDTest_RemoveMultiple();
        #endregion
    }
}