using Xunit.Abstractions;
using CFeltz.Shared.TestWebApiHelper.Tests;
using PermaGuildes.Backends.NurseriesApi.Database;
using Microsoft.EntityFrameworkCore;

namespace PermaGuildes.Backends.NurseriesApi.BusinessTests
{
    public abstract class LocalClassFixtureWithData : LocalClassFixture<Startup, NurseriesApiTestFixture>
    {
        protected NurseriesApiContext _dbContext;
        protected bool _skipTestsWithDatabaseImpact = false;

        protected LocalClassFixtureWithData(NurseriesApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
            _dbContext = fixture.GetApplicationContext<NurseriesApiContext>();
            if (!_dbContext.Database.IsSqlite() && !_dbContext.Database.IsInMemory()) 
            {
                var config = fixture.GetApplicationConfiguration();
                if (config["SkipTestsOnRealDatabase"] != null) 
                {
                    _skipTestsWithDatabaseImpact = bool.Parse(config["SkipTestsOnRealDatabase"]);
                }
            }
        }
    }
}