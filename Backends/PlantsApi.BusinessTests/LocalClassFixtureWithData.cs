using Xunit.Abstractions;
using CFeltz.Shared.TestWebApiHelper.Tests;
using PermaGuildes.Backends.PlantsApi.Database;
using CFeltz.Shared.WebApiHelper.Models;
using Microsoft.EntityFrameworkCore;

namespace PermaGuildes.Backends.PlantsApi.BusinessTests
{
    public abstract class LocalClassFixtureWithData : LocalClassFixture<Startup, PlantsApiTestFixture>
    {
        protected PlantsApiContext _dbContext;
        protected bool _skipTestsWithDatabaseImpact = false;

        protected LocalClassFixtureWithData(PlantsApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
            _dbContext = fixture.GetApplicationContext<PlantsApiContext>();
            if (!_dbContext.Database.IsSqlite() && !_dbContext.Database.IsInMemory()) 
            {
                var config = fixture.GetApplicationConfiguration();
                if (config["SkipTestsOnRealDatabase"] != null) 
                {
                    _skipTestsWithDatabaseImpact = bool.Parse(config["SkipTestsOnRealDatabase"]);
                }
            }
        }

        protected JwtToken Login(string login = "cfeltz", string role = null, string password = "Christophe.feltz@gmail.com1")
        {
            var data = new { login = login, password = password };
            var task = PostResult<JwtToken>("/api/authenticate/login", data, true);
            task.Wait();
            JwtToken jwtToken = task.Result;
            this.AddJwtAuthorizationToken(jwtToken);
            return jwtToken;
        }
    }
}