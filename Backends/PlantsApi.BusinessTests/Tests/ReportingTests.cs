using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using Xunit;
using Xunit.Abstractions;

namespace PermaGuildes.Backends.PlantsApi.BusinessTests.Tests
{
    public class ReportingTests : LocalClassFixtureWithData
    {   
        public ReportingTests(PlantsApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        [Fact]
        public async Task ReportingsTestsAsync()
        {
            // Get all reporting
            var result = await GetResult<Reporting[]>("/api/Reportings");
            int count = result.Count();

            // Create new reporting
            var data = new { name = "Testeur", email = "email@test.com", message = "Message de reporting", codePlant = 1 };
            var reporting = await PostResult<Reporting>("/api/Reportings", data);
            Assert.True(reporting.code > 0);

            // Get all reporting
            result = await GetResult<Reporting[]>("/api/Reportings");
            Assert.Equal(count + 1, result.Count());
            var reporting2 = result.Where(r => r.code == reporting.code).FirstOrDefault();

            // Remove this reporting
            await DeleteResult($"/api/Reportings/{ reporting.code }");

            // Get all reporting
            result = await GetResult<Reporting[]>("/api/Reportings");
            Assert.Equal(count, result.Count());
        }
    }
}