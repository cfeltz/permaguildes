using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using Xunit;
using Xunit.Abstractions;

namespace PermaGuildes.Backends.PlantsApi.BusinessTests.Tests
{
    public class CommonDataTests : LocalClassFixtureWithData
    {   
        public CommonDataTests(PlantsApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        [Fact]
        public async Task GetVegetationTypesAsync()
        {
            var result = await GetResult<VegetationType[]>("/api/Common/VegetationTypes");
            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async Task GetSexualCharacteristicsAsync()
        {
            var result = await GetResult<SexualCharacteristic[]>("/api/Common/SexualCharacteristics");
            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async Task GetInsectsAsync()
        {
            var result = await GetResult<Insect[]>("/api/Common/Insects");
            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async Task GetStratumsAsync()
        {
            var result = await GetResult<PlantStratum[]>("/api/Common/Stratums");
            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async Task GetFamiliesAsync()
        {
            var result = await GetResult<PlantFamily[]>("/api/Common/Families");
            Assert.True(result.Count() > 0);
        }
    }
}