using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using Xunit;
using Xunit.Abstractions;

namespace PermaGuildes.Backends.PlantsApi.BusinessTests.Tests
{
    public class PlantsTests : LocalClassFixtureWithData
    {   
        public PlantsTests(PlantsApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
            string jwtServerBaseUrl = "https://plantsapi.permaguilde.fr";
            string jwtSecureKey = "c3fd8bac-fbe9-46c4-8cba-35a6ce37ad27";
            AddJwtAuthentification(jwtServerBaseUrl, jwtSecureKey);
        }

        [Fact]
        public async Task GetPlantsAsync()
        {
            Plant[] plants = await GetResult<Plant[]>("/api/Plants/GetPlants");
            Assert.True(plants.Count() > 0);
        }

        [Fact]
        public async Task GetPlantsNamesAsync()
        {
            KeyValuePair<int, string>[] plants = await GetResult<KeyValuePair<int, string>[]>("/api/Plants/GetPlantsNames");
            Assert.True(plants.Count() > 0);
        }

        [Fact]
        public async Task GetPlantsWithLimitAsync()
        {
            int limit = 10;
            Plant[] plants = await GetResult<Plant[]>("/api/Plants/GetPlants?limit=" + limit);
            Assert.Equal(plants.Count(), limit);
        }

        [Fact]
        public async Task LoadPlantAsync()
        {
            // Check result
            /*
              Get plant with all relations filled
              Found by the following request
                SELECT P.code, P.name, T.codeVegetationType, A.codeInsect AS codeAuxialiry, R.codeInsect AS codeRepellent FROM Plants AS P
                INNER JOIN PlantsTypes AS T on T.codePlant = P.code
                INNER JOIN PlantsAuxiliaries AS A ON A.codePlant = P.code
                INNER JOIN PlantsPests AS R ON R.codePlant = P.code
                WHERE NOT T.codePlant IS NULL AND NOT A.codePlant IS NULL AND NOT R.codePlant IS NULL
                ORDER BY P.code
            */

            dynamic plant = await GetResultDynamic("/api/Plants/LoadPlant?codePlant=2");

            Assert.True(plant.codesAuxiliariesInsects.Count > 0);
            Assert.True(plant.codesRepellentsInsects.Count > 0);
            Assert.True(plant.codesVegetationTypes.Count > 0);

            Assert.True(plant.codesAuxiliariesInsects[0] == 20);
            Assert.True(plant.codesRepellentsInsects[0] == 7 || plant.codesRepellentsInsects[1] == 7 || plant.codesRepellentsInsects[2] == 7 || plant.codesRepellentsInsects[3] == 7);
            Assert.True(plant.codesVegetationTypes[0] == 4);
            Assert.True(plant.codesVegetationTypes[1] == 5);

            dynamic plant2 = await GetResultDynamic("/api/Plants/LoadPlant?codePlant=9"); 
            var companions = await GetResultDynamicArray("/api/Plants/LoadPlantCompanions?codePlant=9"); 
            plant2.companions = companions;
            Assert.True(plant2.companions.Count > 0);

            dynamic brachipodium = null;
            dynamic prunusSpinosa = null;
            for (var i = 0; i < plant2.companions.Count; i++) 
            {
                var compagnon = plant2.companions[i];
                if (compagnon.sophyName == "BRACHYPODIUM RAMOSUM (L 1-6")
                {
                    brachipodium = compagnon;
                }
                else if (compagnon.sophyName == "PRUNUS SPINOSA L.       1-6")
                {
                    prunusSpinosa = compagnon;
                }
            }
            Assert.NotNull(brachipodium);
            Assert.Equal(62, (int)brachipodium.gap);
            Assert.Equal(45, (int)brachipodium.pdcum);
            Assert.Equal(21, (int)brachipodium.fidelity);
            Assert.Equal(1541, (int)brachipodium.frequency);

            Assert.NotNull(prunusSpinosa);
            Assert.True(prunusSpinosa.gap == null);
            Assert.Equal(16, (int)prunusSpinosa.pdcum);
            Assert.Equal(42, (int)prunusSpinosa.fidelity);
            Assert.Equal(5211, (int)prunusSpinosa.frequency);
            Assert.Equal("Prunellier", (string)prunusSpinosa.name);
            Assert.Equal("Prunus spinosa", (string)prunusSpinosa.latinNames);
        }
        
        [Fact]
        public async Task TestAssociationsAsync()
        {
            var data = new { codePlant1 = 9, codePlant2 = 10 };
            try 
            {
                Plant plant1 = await GetResult<Plant>("/api/Plants/LoadPlant?codePlant=" + data.codePlant1);
                Plant plant2 = await GetResult<Plant>("/api/Plants/LoadPlant?codePlant=" + data.codePlant2);

                Login();

                // Clean data : remove association if exist
                _dbContext.Database.ExecuteSqlInterpolated(
                    $"SELECT * FROM Associations WHERE codePlant1 = { data.codePlant1 } AND codePlant2 = { data.codePlant2 }");

                dynamic dynamicPlant1 = await GetResultDynamic("/api/Plants/LoadPlant?codePlant=" + data.codePlant1);
                dynamic dynamicPlant2 = await GetResultDynamic("/api/Plants/LoadPlant?codePlant=" + data.codePlant2);

                List<CompanionPlant> companions1 = dynamicPlant1.companions.ToObject<List<CompanionPlant>>();
                List<CompanionPlant> companions2 = dynamicPlant2.companions.ToObject<List<CompanionPlant>>();
                Assert.True(companions1.Where(c => c.sophyName  == null).Count() == 0);
                Assert.True(companions2.Where(c => c.sophyName  == null).Count() == 0);

                // Associate plants
                await PostResult("/api/Plants/AssociatePlants", data);

                dynamicPlant1 = await GetResultDynamic("/api/Plants/LoadPlant?codePlant=" + data.codePlant1);
                dynamicPlant2 = await GetResultDynamic("/api/Plants/LoadPlant?codePlant=" + data.codePlant2);

                companions1 = dynamicPlant1.companions.ToObject<List<CompanionPlant>>();
                companions2 = dynamicPlant2.companions.ToObject<List<CompanionPlant>>();
                Assert.True(companions1.Where(c => c.codePlant == plant2.code).Count() == 1);
                Assert.True(companions2.Where(c => c.codePlant == plant1.code).Count() == 1);

                // Remove association
                await PostResult("/api/Plants/RemovePlantsAssociation", data);
                // Remove again same data --> no error thrown
                await PostResult("/api/Plants/RemovePlantsAssociation", data);
            }
            finally
            {
                // Remove association
                await PostResult("/api/Plants/RemovePlantsAssociation", data);
                Logout();
            }
        }
    
        [Fact]
        public async Task UpdatePlantWithInvalidDataAsync()
        {
            try 
            {
                Login();

                var plantWithoutCode = new { name = "name" };
                var response = await PostResult("/api/Plants/UpdatePlant", plantWithoutCode, assertStatut: false); 
                Assert.Equal(HttpStatusCode.NoContent, response.StatusCode); // --> Plant need to be serialized
            }
            finally
            {
                Logout();
            }
        }

        [Fact]
        public async Task UpdatePlantAsync()
        {
            try 
            {
                var plant = await GetResult<Plant>("/api/Plants/LoadPlant?codePlant=1");
                var plantBackup = (Plant)plant.Clone();
                Login();

                // Check database values --> To restore : UPDATE Plants SET codeStratum = 2, minHeight = 5, maxHeight = 6 WHERE code = 1 ;
                Assert.Equal(2, plant.codeStratum);
                Assert.Equal(5, plant.minHeight);
                Assert.Equal(6, plant.maxHeight);
                
                plant.codeStratum = plant.codeStratum + 1;
                plant.minHeight = plant.minHeight + 100;
                plant.maxHeight = plant.maxHeight + 100;
                await PostResult("/api/Plants/UpdatePlant", plant);
                var newPlant = await GetResult<Plant>("/api/Plants/LoadPlant?codePlant=1");
                Assert.Equal(plant.codeStratum, newPlant.codeStratum);
                Assert.Equal(plant.minHeight, newPlant.minHeight);
                Assert.Equal(plant.maxHeight, newPlant.maxHeight);

                // Partial update             
                var data = new { code = 1, maxHeight = newPlant.maxHeight + 100 };
                await PostResult("/api/Plants/UpdatePlant", data);
                newPlant = await GetResult<Plant>("/api/Plants/LoadPlant?codePlant=1");
                Assert.Equal(plant.codeStratum, newPlant.codeStratum);
                Assert.Equal(plant.minHeight, newPlant.minHeight);
                Assert.Equal(plant.maxHeight + 100, newPlant.maxHeight);

                // Restore old values
                await PostResult("/api/Plants/UpdatePlant", plantBackup);
                newPlant = await GetResult<Plant>("/api/Plants/LoadPlant?codePlant=1");
                Assert.Equal(plantBackup.codeStratum, newPlant.codeStratum); // 2
                Assert.Equal(plantBackup.minHeight, newPlant.minHeight); // 5
                Assert.Equal(plantBackup.maxHeight, newPlant.maxHeight); // 6
            }
            finally
            {
                Logout();
            }
        }

        [Fact]
        public async Task UpdatePlantWithListsAsync()
        {
            try 
            {
                Newtonsoft.Json.Linq.JObject plant = await GetResultDynamic("/api/Plants/LoadPlant?codePlant=2");
                Login();

                // Update lists attributes
                var codesAuxiliariesInsects = _dbContext.Insects.Where(i => i.auxiliary).Select(i => i.code).Take(4).ToList();
                var codesRepellentsInsects = _dbContext.Insects.Where(i => !i.auxiliary).Select(i => i.code).Take(4).ToList();
                var codesVegetationTypes = _dbContext.VegetationTypes.Select(t => t.code).ToList();
                var data = new { 
                    code = 2, 
                    codesAuxiliariesInsects = codesAuxiliariesInsects, 
                    codesRepellentsInsects = codesRepellentsInsects, 
                    codesVegetationTypes = codesVegetationTypes 
                };
                await PostResult("/api/Plants/UpdatePlant", data);
                Newtonsoft.Json.Linq.JObject newPlant = await GetResultDynamic("/api/Plants/LoadPlant?codePlant=2");
                Assert.Equal(codesAuxiliariesInsects.Count(), newPlant.GetValue("codesAuxiliariesInsects").ToObject<List<int>>().Count());
                Assert.Equal(codesRepellentsInsects.Count(), newPlant.GetValue("codesRepellentsInsects").ToObject<List<int>>().Count());
                Assert.Equal(codesVegetationTypes.Count(), newPlant.GetValue("codesVegetationTypes").ToObject<List<int>>().Count());

                // Restore data
                await PostResult("/api/Plants/UpdatePlant", plant);
                newPlant = await GetResultDynamic("/api/Plants/LoadPlant?codePlant=2");
                Assert.Equal(plant.GetValue("codesAuxiliariesInsects").ToObject<List<int>>().Count(), 
                            newPlant.GetValue("codesAuxiliariesInsects").ToObject<List<int>>().Count());
                Assert.Equal(plant.GetValue("codesRepellentsInsects").ToObject<List<int>>().Count(), 
                            newPlant.GetValue("codesRepellentsInsects").ToObject<List<int>>().Count());
                Assert.Equal(plant.GetValue("codesVegetationTypes").ToObject<List<int>>().Count(), 
                            newPlant.GetValue("codesVegetationTypes").ToObject<List<int>>().Count());
            }
            finally
            {
                Logout();
            }

        }

    }
}