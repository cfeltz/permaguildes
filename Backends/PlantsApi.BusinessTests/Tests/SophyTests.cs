using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using Xunit;
using Xunit.Abstractions;

namespace PermaGuildes.Backends.PlantsApi.BusinessTests.Tests
{
    public class SophyTests : LocalClassFixtureWithData
    {   
        public SophyTests(PlantsApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        [Fact]
        public async Task GetSophyListFromTelaBotanicaAsync()
        {
            var result = await GetResult<Sophy[]>("/api/Sophy/GetFromTelaBotanica?withData=false");
            Assert.True(result.Count() > 100);
        }

        [Fact]
        public async Task LoadSophyListFromTelaBotanicaAndGetOnceAsync()
        {
            if (!_skipTestsWithDatabaseImpact)
            {
                try 
                {
                    Login();
                    await PostResult("/api/Sophy/ImportFromTelaBotanica?clean=true&limit=5", null);

                    var sophyList = _dbContext.Sophy.ToList();
                    Assert.True(sophyList.Count() == 5);

                    int code = sophyList.FirstOrDefault().code;
                    string sophyName = sophyList.FirstOrDefault().sophyName;
                    var sophy = await GetResult<Sophy>($"/api/Sophy/{ code }");
                    Assert.Equal(code, sophy.code);
                    Assert.Equal(sophyName, sophy.sophyName);
                }
                finally
                {
                    Logout();
                }
            }
        }

    }
}