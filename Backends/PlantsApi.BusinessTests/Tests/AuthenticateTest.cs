using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper.Models;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using Xunit;
using Xunit.Abstractions;

namespace PermaGuildes.Backends.PlantsApi.BusinessTests.Tests
{
    public class AuthenticateTest : LocalClassFixtureWithData
    {   
        public AuthenticateTest(PlantsApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        [Fact]
        public void LoginWithBadPassword()
        {
            var data = new { login = "test", password = "badPassword" };
            var response = PostResult<ApiMessage>("/api/authenticate/login", data, true, false);
            response.Wait();
            var result = response.Result;
            Assert.NotNull(result.errorMessage);
        }
        
        [Fact]
        public void LoginWithUnknownUser()
        {
            var data = new { login = "UnknownUser", password = "NA" };
            var response = PostResult<ApiMessage>("/api/authenticate/login", data, true, false);
            response.Wait();
            var result = response.Result;
            Assert.NotNull(result.errorMessage);
        }
                
        [Fact]
        public void LoginCall()
        {
            var result = Login();
            Assert.Null(result.errorMessage);
        }

        [Fact]
        public void UnauthorisedCall()
        {
            var response = GetResult("/api/Authenticate/TestAuthorization", null, false);            
            response.Wait();
            var result = response.Result;
            Assert.Equal(HttpStatusCode.Unauthorized, result.StatusCode);
        }

        [Fact]
        public void AuthorisedCall()
        {
            Login();
            var response = GetResult("/api/authenticate/TestAuthorization", null);            
            response.Wait();
            var result = response.Result;
            Logout();
        }
    }
}