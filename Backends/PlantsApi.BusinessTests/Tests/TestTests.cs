using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace PermaGuildes.Backends.PlantsApi.BusinessTests.Tests
{
    public class TestTests : LocalClassFixtureWithData
    {   
        public TestTests(PlantsApiTestFixture fixture, ITestOutputHelper output) : base(fixture, output)
        {
        }

        [Fact]
        public async Task GetExceptionAsync()
        {
            try 
            {
                var response = await GetResult("/api/Test/GetException", assertStatut: false);
            }
            catch(Exception ex)
            {
                var result = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(ex.Message);
                Assert.NotNull(result.GetValue("StackTraceString"));
            }
        }

        [Fact]
        public async Task CatchExceptionAsync()
        {
            await Assert.ThrowsAsync<Exception>(async () => await GetResult<string>("/api/Test/GetException"));
        }

    }
}