using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using CFeltz.Shared.WebApiHelper.ExtensionsBase;
using Microsoft.Extensions.Logging;
using AspNetCore.Http.Extensions;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper.Services;

namespace PermaGuildes.Backends.PlantsApi.BusinessTests
{
    /*
     * Mock Http request from TestServer (internal request from tested web api)
     */
    public class TestHttpRequestService : HttpRequestServiceBase
    {
        #region Initialization        
        public TestHttpRequestService(ILogger<TestHttpRequestService> logger)
             : base(logger, new HttpClient())
        {
            InitClient();
        }
        #endregion

        override public async Task<HttpResponseMessage> GetResult(string request, 
            Dictionary<string, string> queryParameter = null, bool assertStatut = false)
        {
            if (request.StartsWith("http://sophy.tela-botanica.org"))
            {
                return await base.GetResult(request, queryParameter, assertStatut);
            }
            else 
            {
                await Task.Run(() => { });
                return null;
            }
        }

        override public async Task<T> GetResult<T>(string request,
            Dictionary<string, string> queryParameter = null, bool assertStatut = true)
        {
            await Task.Run(() => { });
            if (typeof(T) == typeof(string)) {
                switch(request)
                {
                    default: 
                        return (T)(object)"";
                }
            } 
            else if (typeof(T) == typeof(ApiMessage)) 
            {
                switch(request)
                {
                    default: 
                        return (T)(object)new ApiMessage();
                }
            }
            return default(T);
        }

        override public async Task<T> PostMultipartFormDataResult<T>(string request, 
            MultipartFormDataContent content, bool assertStatut = false)
        {
            await Task.Run(() => { });
            if (typeof(T) == typeof(ApiMessage)) 
            {
                switch(request)
                {
                    default: 
                        return (T)(object)new ApiMessage();
                }
            }
            return default(T);
        }
    }
}