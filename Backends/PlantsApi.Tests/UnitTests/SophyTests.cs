
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Controllers;
using Xunit;
using Xunit.Abstractions;
using Microsoft.EntityFrameworkCore;
using CFeltz.Shared.TestWebApiHelper;
using PermaGuildes.Backends.PlantsApi.Services;
using PermaGuildes.Backends.PlantsApi.Models;
using System.Linq;

namespace PermaGuildes.Backends.PlantsApi.Tests.UnitTests
{
    /// <summary>
    ///  Vérification des données préremplies de la base de données
    /// </summary>
    public class SophyTests : UnitTestExtensionWithPlantsApiContext
    {
        public SophyTests() : base(withLoggers: true)
        {

        }

        [Fact]
        public async System.Threading.Tasks.Task GetSophyDataFromTelaBotanicaAsync()
        {
            SetDbContext();
            SetPlantsRepository();
            SetSophyRepository();
            SetSophyService();

            Sophy sophy = await _sophyService.GetSophyDataFromTelaBotanica(3);
            Assert.Equal("ACANTHUS MOLLIS L.      1-5", sophy.sophyName);
            Assert.Equal(36, sophy.obs);
            Assert.Equal(36, sophy.loc);
            Assert.Equal(28, sophy.dis);
            Assert.Equal(20, sophy.qdr);
            Assert.Equal(370, sophy.coc);

            Sophy sophyWithSpecialCar = await _sophyService.GetSophyDataFromTelaBotanica(1425);
            Assert.Equal("CIRSIUM MONTANUM (W. &  1-4", sophyWithSpecialCar.sophyName);
            Assert.Equal(32, sophyWithSpecialCar.obs);
            Assert.Equal(25, sophyWithSpecialCar.loc);
            Assert.Equal(21, sophyWithSpecialCar.dis);
            Assert.Equal(21, sophyWithSpecialCar.qdr);
            Assert.Equal(442, sophyWithSpecialCar.coc);
            Assert.Equal(30, sophyWithSpecialCar.similarsPlants.Count);
            Assert.Equal(23, sophyWithSpecialCar.discrimantsPlants.Count);

            var lastSimilarPlant = sophyWithSpecialCar.similarsPlants.ToList().LastOrDefault();
            Assert.Equal(60, lastSimilarPlant.gap);
            Assert.Equal("CAMPANULA RHOMBOIDALIS  2-4", lastSimilarPlant.sophyNameSimilarPlant);

            var lastDiscriminantPlant = sophyWithSpecialCar.discrimantsPlants.ToList().LastOrDefault();
            Assert.Equal(51, lastDiscriminantPlant.pdcum);
            Assert.Equal("ALNUS VIRIDIS (CHAIX) D 1-6", lastDiscriminantPlant.sophyNameDiscriminantPlant);
            Assert.Equal(33, lastDiscriminantPlant.fidelity);
            Assert.Equal(320, lastDiscriminantPlant.frequency);

            var discriminantPlantWithUndefinedPlant = sophy.discrimantsPlants
                .Where(d => d.sophyNameDiscriminantPlant == "PISTACIA LENTISCUS L.   3-6")
                .FirstOrDefault();
            Assert.NotNull(discriminantPlantWithUndefinedPlant);
            Assert.Null(discriminantPlantWithUndefinedPlant.pdcum);
            Assert.Null(discriminantPlantWithUndefinedPlant.fidelity);
            Assert.Null(discriminantPlantWithUndefinedPlant.frequency);
        }

    }
}
