using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Controllers;
using Xunit;
using System.Linq;
using PermaGuildes.Backends.PlantsApi.Models;
using CFeltz.Shared.WebApiHelper;

namespace PermaGuildes.Backends.PlantsApi.Tests.UnitTests
{
    /// <summary>
    ///  Tests relatifs aux propriétés d'une plante
    /// </summary>
    public class PlantsPropertiesTests : UnitTestExtensionWithPlantsApiContext
    {
        public PlantsPropertiesTests() : base(withLoggers: false)
        {

        }

        [Fact]
        public void UpdateBasicsProperties()
        {
            // Prepare test
            const string plantName = "Plante utilisée pour le test UpdateBasicsProperties";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(1).ToList();
            var families = _dbContext.PlantsFamilies.ToList();
            var stratums = _dbContext.PlantsStratums.ToList();
            var sex = _dbContext.SexualCharacteristics.ToList();

            plants[0].name = plantName;
            plants[0].latinNames = "Nouveau nom latin";
            plants[0].iconCode = 10;
            plants[0].water = 3;
            plants[0].growth = 3;
            plants[0].edibilityRating = 5;
            plants[0].medicinalRating = 5;
            plants[0].family = families[0];
            plants[0].stratum = stratums[0];
            plants[0].sex = sex[0];

            plants[0].minUsdahardiness = 6;
            plants[0].maxUsdahardiness = 12;
            plants[0].minHeight = 1;
            plants[0].maxHeight = 10;
            plants[0].minSpan = 1;
            plants[0].maxSpan = 8;
            
            plants[0].startFloweringPeriod1 = 1;
            plants[0].endFloweringPeriod1 = 4;
            plants[0].startFloweringPeriod2 = 8;
            plants[0].endFloweringPeriod2 = 12;
            plants[0].startHarvestPeriod1 = 2;
            plants[0].endHarvestPeriod1 = 5;
            plants[0].startHarvestPeriod2 = 9;
            plants[0].endHarvestPeriod2 = 12;
            
            plants[0].exposureShadow = true;
            plants[0].exposureMiddle = true;
            plants[0].exposureSun = true;
            
            plants[0].fertility = true;
            plants[0].windbreaker = true;
            plants[0].nitrate = true;
            plants[0].othersMinerals = true;       
            plants[0].othersMineralsRating = 5;
            plants[0].melliferous = true;
            plants[0].melliferousRating = 5;
            plants[0].aromaticPlant = true;
			
			plants[0].pfafKnownHazards = "Risques connus";
            plants[0].pfafDescription = "Description Pfaf";
            plants[0].pfafUrl = "https://pfaf.org/user/Default.aspx";
            plants[0].externalLink = "http://www.permaguildes.fr";
            plants[0].remarks = "Aucune remarque";
            plants[0].infosToKnow = "Info à savoir";

            _dbContext.SaveChanges();
            Plant plant = _dbContext.Plants.Where(p => p.name == plantName).FirstOrDefault();

            // Assert changes
            Assert.True(plant.name == plantName);
            Assert.True(plant.latinNames == "Nouveau nom latin");
            Assert.True(plant.iconCode == 10);
            Assert.True(plant.water == 3);
            Assert.True(plant.growth == 3);
            Assert.True(plant.edibilityRating == 5);
            Assert.True(plant.medicinalRating == 5);
            Assert.True(plant.family.code == families[0].code);
            Assert.True(plant.stratum.code == stratums[0].code);
            Assert.True(plant.sex.code == sex[0].code);
            Assert.True(plant.minUsdahardiness == 6);
            Assert.True(plant.maxUsdahardiness == 12);
            Assert.True(plant.minHeight == 1);
            Assert.True(plant.maxHeight == 10);
            Assert.True(plant.minSpan == 1);
            Assert.True(plant.maxSpan == 8);

            Assert.True(plant.startFloweringPeriod1 == 1);
            Assert.True(plant.endFloweringPeriod1 == 4);
            Assert.True(plant.startFloweringPeriod2 == 8);
            Assert.True(plant.endFloweringPeriod2 == 12);
            Assert.True(plant.startHarvestPeriod1 == 2);
            Assert.True(plant.endHarvestPeriod1 == 5);
            Assert.True(plant.startHarvestPeriod2 == 9);
            Assert.True(plant.endHarvestPeriod2 == 12);
            Assert.True(plant.maxSpan == 8);

            Assert.True(plant.exposureShadow == true);
            Assert.True(plant.exposureMiddle == true);
            Assert.True(plant.exposureSun == true);

            Assert.True(plant.fertility == true);
            Assert.True(plant.windbreaker == true);
            Assert.True(plant.nitrate == true);
            Assert.True(plant.othersMinerals == true);       
            Assert.True(plant.othersMineralsRating == 5);
            Assert.True(plant.melliferous == true);
            Assert.True(plant.melliferousRating == 5);
            Assert.True(plant.aromaticPlant == true);
            
            Assert.True(plant.pfafKnownHazards == "Risques connus");
            Assert.True(plant.pfafDescription == "Description Pfaf");
            Assert.True(plant.pfafUrl == "https://pfaf.org/user/Default.aspx");
            Assert.True(plant.externalLink == "http://www.permaguildes.fr");
            Assert.True(plant.remarks == "Aucune remarque");
            Assert.True(plant.infosToKnow == "Info à savoir");
        }

        [Fact]
        public void UpdateInsectsAssociations()
        {
            // Prepare test
            const string plantName = "Plante utilisée pour le test UpdateInsectsAssociations";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(1).ToList();
            var auxiliaries = _dbContext.Insects.Where(i => i.auxiliary == true).ToList();
            var pests = _dbContext.Insects.Where(i => i.auxiliary == false).ToList();

            plants[0].name = plantName;
			
            plants[0].AddAuxiliary(auxiliaries[0]);
            plants[0].AddAuxiliary(auxiliaries[0]); // Add duplicate item
            plants[0].AddPest(pests[0]);
            plants[0].AddPest(pests[1]);
            plants[0].AddPest(pests[1]); // Add duplicate item

            _dbContext.SaveChanges();
            Plant plant = _dbContext.Plants.Where(p => p.name == plantName).FirstOrDefault();

            // Assert changes
            Assert.True(plant.name == plantName);
            Assert.True(plant.auxiliaries.Count() == 1);
            Assert.True(plant.pests.Count() == 2);
            Assert.True(plant.auxiliariesAttractive == true);
            Assert.True(plant.pestRepellents == true);

            // Prepare next step
            plant.RemoveAuxiliary(auxiliaries[0]);
            plant.RemovePest(pests[0]);
            plant.RemovePest(pests[1]);
            _dbContext.SaveChanges();
            plant = _dbContext.Plants.Where(p => p.name == plantName).FirstOrDefault();

            // Assert changes
            Assert.True(plant.auxiliaries.Count() == 0);
            Assert.True(plant.pests.Count() == 0);
            Assert.True(plant.auxiliariesAttractive == true);
            Assert.True(plant.pestRepellents == true);
        }


        [Fact]
        public void ThrowsBadInsectAssociation()
        {
            // Prepare test
            const string plantName = "Plante utilisée pour le test CheckBadInsectAssociation";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(1).ToList();
            var auxiliaries = _dbContext.Insects.Where(i => i.auxiliary == true).ToList();
            var pests = _dbContext.Insects.Where(i => i.auxiliary == false).ToList();

            plants[0].name = plantName;
			
            var ex1 = Assert.Throws<LocalException>(() => plants[0].AddAuxiliary(pests[0]));
            var ex2 = Assert.Throws<LocalException>(() => plants[0].AddPest(auxiliaries[0]));
            
            // Assert changes
            Assert.Equal(ex1.id, (int)ExceptionsIdFromWebApiHelper.DataRulesViolation);
            Assert.Equal(ex2.id, (int)ExceptionsIdFromWebApiHelper.DataRulesViolation);
        }

        [Fact]
        public void GetPlantsAssociationWithInsect()
        {
            // Prepare test
            const string plantName = "Plante utilisée pour le test GetPlantsAssociationWithInsect";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(1).ToList();
            var auxiliaries = _dbContext.Insects.Where(i => i.auxiliary == true).ToList();
            var pests = _dbContext.Insects.Where(i => i.auxiliary == false).ToList();

            // Assert default data
            Assert.True(auxiliaries[0].plantsWithAuxiliary == null || auxiliaries[0].plantsWithAuxiliary.Count() == 0);
            Assert.True(auxiliaries[0].plantsWithPest == null || pests[0].plantsWithPest.Count() == 0);
            Assert.True(auxiliaries[0].plantsWithPest == null || pests[1].plantsWithPest.Count() == 0);

            // Prepare test
            plants[0].name = plantName;			
            plants[0].AddAuxiliary(auxiliaries[0]);
            plants[0].AddPest(pests[0]);
            plants[0].AddPest(pests[1]);

            _dbContext.SaveChanges();
            
            // Assert changes
            Assert.True(auxiliaries[0].plantsWithAuxiliary.Count() == 1);
            Assert.True(pests[0].plantsWithPest.Count() == 1);
            Assert.True(pests[1].plantsWithPest.Count() == 1);
        }

        [Fact]
        public void UpdateVegetationTypes()
        {
            // Prepare test
            const string plantName = "Plante utilisée pour le test UpdateVegetationTypes";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(1).ToList();
            var types = _dbContext.VegetationTypes.ToList();

            // Prepare test
            plants[0].name = plantName;			
            plants[0].AddVegetationType(types[0]);
            plants[0].AddVegetationType(types[1]);
            _dbContext.SaveChanges();
            Plant plant = _dbContext.Plants.Where(p => p.name == plantName).FirstOrDefault();
            
            // Assert changes
            Assert.True(plant.vegetationTypes.Count() == 2);

            // Next step : add duplicate type
            plant.AddVegetationType(types[0]);
            _dbContext.SaveChanges();
            Assert.True(plant.vegetationTypes.Count() == 2);

            // Next step : remove type
            plant.RemoveVegetationType(types[0]);
            _dbContext.SaveChanges();
            Assert.True(plant.vegetationTypes.Count() == 1);
        }
    }
}
