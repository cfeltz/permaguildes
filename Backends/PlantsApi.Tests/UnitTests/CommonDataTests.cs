using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Controllers;
using Xunit;
using Xunit.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace PermaGuildes.Backends.PlantsApi.Tests.UnitTests
{
    /// <summary>
    ///  Vérification des données préremplies de la base de données
    /// </summary>
    public class CommonDataTests : UnitTestExtensionWithPlantsApiContext
    {        
        public CommonDataTests() : base(withLoggers: true)
        {

        }

        private async Task TestCollectionRefGetterAsync<T>(Func<CommonController, Task<ActionResult<ICollection<T>>>> method, int? count = -1)
        {
            // Prepare test
            SetDbContext();
            SetCommonRepositories();
            SetCommonService();
            var controller = new CommonController(
                logger: CreateLogger<CommonController>(), 
                commonService: _commonService);

            // Action : call method to get the collection
            var response = await method(controller);
            ICollection<T> value = response.Value;
            _dbContext.Dispose();

            // Assert presence of items
            Assert.False(value == null);
            Assert.False(value.Count == 0);
            if (count != -1) {
                Assert.True(value.Count == count);
            }
        }        

        [Fact]
        public async System.Threading.Tasks.Task GetInsectsAsync()
        {
            await TestCollectionRefGetterAsync<Insect>((CommonController controller) =>
            {
                return controller.GetInsects();
            });
        }

        [Fact]

        public async System.Threading.Tasks.Task GetFamiliesAsync()
        {
            await TestCollectionRefGetterAsync<PlantFamily>((CommonController controller) =>
            {
                return controller.GetFamilies();
            });
        }

        [Fact]
        public async System.Threading.Tasks.Task GetStratumsAsync()
        {
            await TestCollectionRefGetterAsync<PlantStratum>((CommonController controller) =>
            {
                return controller.GetStratums();
            }, count: 7);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetSexualCharacteristicsAsync()
        {
            await TestCollectionRefGetterAsync<SexualCharacteristic>((CommonController controller) =>
            {
                return controller.GetSexualCharacteristics();
            }, count: 3);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetVegetationTypesAsync()
        {
            await TestCollectionRefGetterAsync<VegetationType>((CommonController controller) =>
            {
                return controller.GetVegetationTypes();
            }, count: 5);
        }
    }
}
