using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Controllers;
using Xunit;
using System.Linq;
using PermaGuildes.Backends.PlantsApi.Database;
using Microsoft.EntityFrameworkCore;
using CFeltz.Shared.WebApiHelper;

namespace PermaGuildes.Backends.PlantsApi.Tests.UnitTests
{
    /// <summary>
    ///  Vérification des fonctions génériques de la base de données
    /// </summary>
    public class DatabaseTests : UnitTestExtensionWithPlantsApiContext
    {
        public DatabaseTests() : base(withLoggers: false)
        {

        }


        [Fact]
        /// <summary>
        ///  Test des propriétés dtCreation / dtUpdate automatiquement mise à jour en base
        /// </summary>
        public void AutomaticPropertiesOfUpdatedEntity()
        {
            // Prepare test
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(5).ToList();
            plants[1].fertility = !plants[1].fertility;
            _dbContext.SaveChanges();

            Assert.True(plants[0].dtCreation > DateTime.MinValue);
            Assert.True(plants[0].dtCreation < DateTime.Now);
            Assert.True(plants[1].dtCreation > DateTime.MinValue);
            Assert.True(plants[1].dtCreation < DateTime.Now);
            Assert.True(plants[0].dtCreation == plants[0].dtUpdate);
            Assert.True(plants[1].dtUpdate > plants[1].dtCreation);
        }
    }
}
