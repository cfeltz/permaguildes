using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Controllers;
using Xunit;
using System.Linq;
using PermaGuildes.Backends.PlantsApi.Models;
using CFeltz.Shared.WebApiHelper;

namespace PermaGuildes.Backends.PlantsApi.Tests.UnitTests
{
    /// <summary>
    ///  Tests relatifs aux propriétés d'une plante
    /// </summary>
    public class AssociationsTests : UnitTestExtensionWithPlantsApiContext
    {
        public AssociationsTests() : base(withLoggers: false)
        {

        }

        [Fact]
        public void SinglePlantsAssociation()
        {
            // Prepare test
            const string plant1Name = "Première plante utilisée pour le test SinglePlantsAssociation";
            const string plant2Name = "Seconde plante utilisée pour le test SinglePlantsAssociation";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(2).ToList();

            plants[0].name = plant1Name;
            plants[1].name = plant2Name;
            plants[0].AddAssociationWithPlant(plants[1]);
            
            _dbContext.SaveChanges();
            Plant plant1 = _dbContext.Plants.Where(p => p.name == plant1Name).FirstOrDefault();
            Plant plant2 = _dbContext.Plants.Where(p => p.name == plant2Name).FirstOrDefault();
            
            // Assert changes
            Assert.True(plant1.name == plant1Name);
            Assert.True(plant2.name == plant2Name);

            Assert.True(plant1.associations.Count() == 1);
            Assert.True(plant2.associations.Count() == 1);

            // Next step : remove association
            plant1.RemoveAssociationWithPlant(plant2);
            _dbContext.SaveChanges();
            // Assert changes
            Assert.True(plant1.associations.Count() == 0);
            Assert.True(plant2.associations.Count() == 0);
        }

        [Fact]
        public void DoublePlantsAssociations()
        {
            // Prepare test
            const string plant1Name = "Première plante utilisée pour le test DoublePlantsAssociations";
            const string plant2Name = "Seconde plante utilisée pour le test DoublePlantsAssociations";
            const string plant3Name = "Troisième plante utilisée pour le test DoublePlantsAssociations";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(3).ToList();

            plants[0].name = plant1Name;
            plants[1].name = plant2Name;
            plants[2].name = plant3Name;
            plants[0].AddAssociationWithPlant(plants[1]);
            plants[0].AddAssociationWithPlant(plants[2]);
            
            _dbContext.SaveChanges();
            Plant plant1 = _dbContext.Plants.Where(p => p.name == plant1Name).FirstOrDefault();
            Plant plant2 = _dbContext.Plants.Where(p => p.name == plant2Name).FirstOrDefault();
            Plant plant3 = _dbContext.Plants.Where(p => p.name == plant3Name).FirstOrDefault();

            // Assert changes
            Assert.True(plant1.name == plant1Name);
            Assert.True(plant2.name == plant2Name);
            Assert.True(plant3.name == plant3Name);

            Assert.True(plant1.associations.Count() == 2);
            Assert.True(plant2.associations.Count() == 1);
            Assert.True(plant3.associations.Count() == 1);

            // Next step : remove association
            plant1.RemoveAssociationWithPlant(plant2);
            plant1.RemoveAssociationWithPlant(plant3);
            _dbContext.SaveChanges();
            // Assert changes
            Assert.True(plant1.associations.Count() == 0);
            Assert.True(plant2.associations.Count() == 0);
            Assert.True(plant3.associations.Count() == 0);
        }

        [Fact]
        public void MultiplePlantsAssociations()
        {
            // Prepare test
            const string plant1Name = "Première plante utilisée pour le test PlantsAssociations";
            const string plant2Name = "Seconde plante utilisée pour le test PlantsAssociations";
            const string plant3Name = "Troisième plante utilisée pour le test PlantsAssociations";
            const string plant4Name = "Quatrième plante utilisée pour le test PlantsAssociations";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(4).ToList();

            plants[0].name = plant1Name;
            plants[1].name = plant2Name;
            plants[2].name = plant3Name;
            plants[3].name = plant4Name;
            plants[0].AddAssociationWithPlant(plants[1]);
            plants[0].AddAssociationWithPlant(plants[2]);
            _dbContext.SaveChanges();
            plants[1].AddAssociationWithPlant(plants[0]);
            plants[3].AddAssociationWithPlant(plants[0]);
            plants[3].AddAssociationWithPlant(plants[1]);
            _dbContext.SaveChanges();
                        
            Plant plant1 = _dbContext.Plants.Where(p => p.name == plant1Name).FirstOrDefault();
            Plant plant2 = _dbContext.Plants.Where(p => p.name == plant2Name).FirstOrDefault();
            Plant plant3 = _dbContext.Plants.Where(p => p.name == plant3Name).FirstOrDefault();
            Plant plant4 = _dbContext.Plants.Where(p => p.name == plant4Name).FirstOrDefault();

            // Assert changes
            Assert.True(plant1.name == plant1Name);
            Assert.True(plant2.name == plant2Name);
            Assert.True(plant3.name == plant3Name);
            Assert.True(plant4.name == plant4Name);

            Assert.True(plant1.associations.Count() == 3);
            Assert.True(plant2.associations.Count() == 2);
            Assert.True(plant3.associations.Count() == 1);
            Assert.True(plant4.associations.Count() == 2);

            // Next step : remove association
            plant2.RemoveAssociationWithPlant(plant1);
            plant1.RemoveAssociationWithPlant(plant3);
            _dbContext.SaveChanges();
            plant4.RemoveAssociationWithPlant(plant2);
            plant4.RemoveAssociationWithPlant(plant1);
            _dbContext.SaveChanges();
            // Assert changes
            Assert.True(plant1.associations.Count() == 0);
            Assert.True(plant2.associations.Count() == 0);
            Assert.True(plant3.associations.Count() == 0);
            Assert.True(plant4.associations.Count() == 0);
        }


        [Fact]
        public void ThrowsAutoPlantAssociation()
        {
            // Prepare test
            const string plantName = "Première plante utilisée pour le test PlantsAssociations";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(1).ToList();

            plants[0].name = plantName;
            var ex = Assert.Throws<LocalException>(() => plants[0].AddAssociationWithPlant(plants[0]));
            _dbContext.SaveChanges();
                        
            // Assert changes
            Assert.Equal(ex.id, (int)ExceptionsIdFromWebApiHelper.DataRulesViolation);
        }

        [Fact]
        public void ThrowsUnknownPlantsAssociation()
        {
            // Prepare test
            const string plant1Name = "Première plante utilisée pour le test PlantsAssociations";
            const string plant2Name = "Seconde plante utilisée pour le test PlantsAssociations";
            SetDbContext();
            SeedPlantsDataset();
            var plants = _dbContext.Plants.Take(2).ToList();

            plants[0].name = plant1Name;
            plants[1].name = plant2Name;
            var ex = Assert.Throws<LocalException>(() => plants[0].RemoveAssociationWithPlant(plants[1]));
            _dbContext.SaveChanges();
                        
            // Assert changes
            Assert.Equal(ex.id, (int)ExceptionsIdFromWebApiHelper.DataNotFound);
        }
    }
}
