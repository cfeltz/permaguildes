using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.TestWebApiHelper;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.DatabaseRepositories;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Services;
using CFeltz.Shared.WebApiHelper.Services;

namespace PermaGuildes.Backends.PlantsApi.Tests
{
    public class UnitTestExtensionWithPlantsApiContext : UnitTestExtensionBase<PlantsApiContext, DbInitializer>
    {
        #region Repositories
        protected IInsectsRepository _insectsRepository;
        protected IVegetationTypeRepository _vegetationTypeRepository;
        protected IPlantFamiliesRepository _plantFamiliesRepository;
        protected IPlantStratumsRepository _plantStratumsRepository;
        protected ISexualCharacteristicsRepository _sexualCharacteristicsRepository;
        protected IPlantsRepository _plantsRepository;
        protected ISophyRepository _sophyRepository;
        #endregion

        #region Services
        protected CommonService _commonService;
        protected SophyService _sophyService;
        #endregion
        
        protected UnitTestExtensionWithPlantsApiContext(bool withLoggers) : base(withLoggers)
        {
        }

        protected void SeedPlantsDataset()
        {
            var families = _dbContext.PlantsFamilies.Take(5).ToList();
            var stratums = _dbContext.PlantsStratums.Take(5).ToList();
            Plant[] plants = new Plant[]
            {
                new Plant(name: "Plante de test", latinNames: "Premier nom latin", water: 1, growth: 1),
                new Plant(name: "Seconde plante de test", latinNames: "Second nom latin", water: 2, growth: 2),
                new Plant(name: "Troisième plante de test", latinNames: "Troisième nom latin", water: 3, growth: 3),
                new Plant(name: "Quatrième plante de test", latinNames: "Quatrième nom latin", water: 1, growth: 2), 
                new Plant(name: "Cinquième plante de test", latinNames: "Cinquième nom latin", water: 3, growth: 4)
            };
            foreach (Plant p in plants)
            {
                _dbContext.Plants.Add(p);
            }
            _dbContext.SaveChanges();
        }

        protected void SetCommonRepositories() {
            _insectsRepository = new InsectsRepository(_dbContext,
                    logger: CreateLogger<InsectsRepository>());
            _plantFamiliesRepository = new PlantFamiliesRepository(_dbContext,
                    logger: CreateLogger<PlantFamiliesRepository>());
            _plantStratumsRepository = new PlantStratumsRepository(_dbContext,
                    logger: CreateLogger<PlantStratumsRepository>());
            _vegetationTypeRepository = new VegetationTypeRepository(_dbContext,
                    logger: CreateLogger<VegetationTypeRepository>());
            _sexualCharacteristicsRepository = new SexualCharacteristicsRepository(_dbContext,
                    logger: CreateLogger<SexualCharacteristicsRepository>());
        }

        protected void SetSophyRepository() {
            _sophyRepository = new SophyRepository(_dbContext,
                    logger: CreateLogger<SophyRepository>());
        }

        protected void SetPlantsRepository() {
            _plantsRepository = new PlantsRepository(_dbContext,
                    logger: CreateLogger<PlantsRepository>());
        }

        protected void SetCommonService() {
            _commonService = new CommonService(logger: CreateLogger<CommonService>(), 
                insectsRepository : _insectsRepository, 
                vegetationTypeRepository : _vegetationTypeRepository, 
                plantFamiliesRepository : _plantFamiliesRepository, 
                plantStratumsRepository : _plantStratumsRepository, 
                sexualCharacteristicsRepository : _sexualCharacteristicsRepository);
        }

        protected IHttpRequestService GetHttpRequestService() {
            return new HttpRequestService(logger: CreateLogger<HttpRequestService>());
        }

        protected void SetSophyService(IHttpRequestService httpRequestService = null) {
            if (httpRequestService == null)
            {
                httpRequestService = GetHttpRequestService();
            }
            _sophyService = new SophyService(logger: CreateLogger<SophyService>(), 
                httpRequestService : httpRequestService, 
                sophyRepository : _sophyRepository, 
                plantsRepository : _plantsRepository);
        }

    }
}