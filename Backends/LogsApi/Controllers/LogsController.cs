using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using PermaGuildes.Backends.LogsApi.Models;
using CFeltz.Shared.WebApiHelper.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace PermaGuildes.Backends.LogsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogsController : ControllerExtensionBase
    {
        public LogsController(ILogger<LogsController> logger) : base(logger)
        {
        }

        [HttpOptions() , HttpPost()]
        public ActionResult CreateLog([FromBody] Newtonsoft.Json.Linq.JObject log)
        {
            using (_logger.BeginScope("Log from client")) {
                var formattedLog = new LogClient(log);
                _logger.Log(formattedLog.Level, formattedLog.ToString());
                return Ok();
            }
        }
    }
}