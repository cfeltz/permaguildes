using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using CFeltz.Shared.WebApiHelper;

namespace PermaGuildes.Backends.LogsApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webApi = new WebApi<Startup>();
            webApi.Main(args, "LogsApi");
        }
    }
}
