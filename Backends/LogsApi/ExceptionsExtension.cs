﻿using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Attributes;
using System;

namespace PermaGuildes.Backends.LogsApi
{
    public enum ExceptionsIdFromLogsApi {
        [DefaultMessage("Invalid format")]
        InvalidFormat = 20001
    }

    public class LogsApiException : LocalException
    {
        public LogsApiException(ExceptionsIdFromLogsApi id)
         : base((int)id, AttributesHelpers.GetStringAttributeOfType<DefaultMessage>(
             typeof(ExceptionsIdFromLogsApi), id.ToString()))
        {
        }

        public LogsApiException(ExceptionsIdFromLogsApi id, string message, params object[] args)
         : base((int)id, message, args)
        {
        }

        public LogsApiException(Exception innerException, ExceptionsIdFromLogsApi id, string message, params object[] args)
         : base((int)id, message, innerException, args)
        {
        }
    }
}
