#Development configuration
#Development
Run 'dotnet test' to check units tests
Run 'docker-compose -f "docker-compose.yml" up -d --build' to publish all component on dockers (vscode task "Compose All")
Run 'docker-compose -f "docker-compose.databasesonly.yml" up -d --build' to publish database server only (vscode task "Compose Databases only")


Open browser with url http://localhost:49000/api/**/*

#Architecture
LogsApi
	Database
LogsApi.Tests
	Units tests
LogsApi.BusinessTests
	Business tests

To convert LogsApi to .Net 2.1 : 
 - update Startup.cs signatures
 - update csproj
 - update web.config
 - uopdate launch.json

