using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Reflection;
using System;

namespace PermaGuildes.Backends.LogsApi
{
    public class Startup : CFeltz.Shared.WebApiHelper.Startup
    {
        public Startup(ILogger<Startup> logger, IConfiguration configuration, IWebHostEnvironment hostingEnvironment) : 
            base(logger, configuration, hostingEnvironment)
        {
        }
    }
}
