using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.ComponentModel;
using Microsoft.Extensions.Logging;

namespace PermaGuildes.Backends.LogsApi.Models
{
    public class LogClient
    {
        public LogClient(Newtonsoft.Json.Linq.JObject log)
        {
            int level = log.Value<int>("level");
            if (level >= 3) level--;
            this.Level = (LogLevel)level;
            this.FileName = log.Value<string>("fileName");
            this.LineNumber = log.Value<int>("lineNumber");
            this.Message = log.Value<string>("message");
            this.Timestamp = log.Value<DateTime>("timestamp");
        }
        public LogLevel Level { get; set; }
        public DateTime Timestamp { get; set; }
        public string FileName { get; set; }
        public int LineNumber { get; set; }
        public string Message { get; set; }

        public override string ToString(){
            return $"[{FileName}:{LineNumber}] {Message}";
        }
    }
}