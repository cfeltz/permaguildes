﻿using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Attributes;
using System;

namespace PermaGuildes.Backends.PlantsApi
{
    public enum ExceptionsIdFromPlantsApi {
        [DefaultMessage("Failed to import data")]
        FailedToImportData = 10001,
        [DefaultMessage("Need to force")]
        NeedToForce = 10002
    }

    public class PlantsApiException : LocalException
    {
        public PlantsApiException(ExceptionsIdFromPlantsApi id)
         : base((int)id, AttributesHelpers.GetStringAttributeOfType<DefaultMessage>(
             typeof(ExceptionsIdFromPlantsApi), id.ToString()))
        {
        }

        public PlantsApiException(ExceptionsIdFromPlantsApi id, string message, params object[] args)
         : base((int)id, message, args)
        {
        }

        public PlantsApiException(Exception innerException, ExceptionsIdFromPlantsApi id, string message, params object[] args)
         : base((int)id, message, innerException, args)
        {
        }
    }
}
