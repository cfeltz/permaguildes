﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class AddIndexOnSophyName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "sophyNameDiscriminantPlant",
                table: "SophyDiscriminatingPlants",
                type: "varchar(27) CHARACTER SET utf8mb4",
                maxLength: 27,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(255) CHARACTER SET utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "sophyName",
                table: "Sophy",
                type: "varchar(27) CHARACTER SET utf8mb4",
                maxLength: 27,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sophy_sophyName",
                table: "Sophy",
                column: "sophyName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Sophy_sophyName",
                table: "Sophy");

            migrationBuilder.AlterColumn<string>(
                name: "sophyNameDiscriminantPlant",
                table: "SophyDiscriminatingPlants",
                type: "varchar(255) CHARACTER SET utf8mb4",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(27) CHARACTER SET utf8mb4",
                oldMaxLength: 27);

            migrationBuilder.AlterColumn<string>(
                name: "sophyName",
                table: "Sophy",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(27) CHARACTER SET utf8mb4",
                oldMaxLength: 27,
                oldNullable: true);
        }
    }
}
