﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Insects",
                columns: table => new
                {
                    code = table.Column<int>(nullable: false),
                    libelle = table.Column<string>(nullable: true),
                    auxiliary = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Insects", x => x.code);
                });

            migrationBuilder.CreateTable(
                name: "PlantsFamilies",
                columns: table => new
                {
                    code = table.Column<int>(nullable: false),
                    libelle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantsFamilies", x => x.code);
                });

            migrationBuilder.CreateTable(
                name: "PlantsStratums",
                columns: table => new
                {
                    code = table.Column<int>(nullable: false),
                    libelle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantsStratums", x => x.code);
                });

            migrationBuilder.CreateTable(
                name: "SexualCharacteristics",
                columns: table => new
                {
                    code = table.Column<int>(nullable: false),
                    libelle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SexualCharacteristics", x => x.code);
                });

            migrationBuilder.CreateTable(
                name: "VegetationTypes",
                columns: table => new
                {
                    code = table.Column<int>(nullable: false),
                    libelle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VegetationTypes", x => x.code);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plants",
                columns: table => new
                {
                    code = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    dtCreation = table.Column<DateTime>(nullable: false),
                    dtUpdate = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    latinNames = table.Column<string>(nullable: true),
                    water = table.Column<int>(nullable: false),
                    growth = table.Column<int>(nullable: false),
                    codeFamily = table.Column<int>(nullable: false),
                    codeStratum = table.Column<int>(nullable: false),
                    iconCode = table.Column<int>(nullable: true),
                    edibilityRating = table.Column<int>(nullable: true),
                    medicinalRating = table.Column<int>(nullable: true),
                    codeSex = table.Column<int>(nullable: true),
                    minUsdahardiness = table.Column<int>(nullable: true),
                    maxUsdahardiness = table.Column<int>(nullable: true),
                    minHeight = table.Column<int>(nullable: true),
                    maxHeight = table.Column<int>(nullable: true),
                    minSpan = table.Column<int>(nullable: true),
                    maxSpan = table.Column<int>(nullable: true),
                    startFloweringPeriod1 = table.Column<int>(nullable: true),
                    endFloweringPeriod1 = table.Column<int>(nullable: true),
                    startFloweringPeriod2 = table.Column<int>(nullable: true),
                    endFloweringPeriod2 = table.Column<int>(nullable: true),
                    startHarvestPeriod1 = table.Column<int>(nullable: true),
                    endHarvestPeriod1 = table.Column<int>(nullable: true),
                    startHarvestPeriod2 = table.Column<int>(nullable: true),
                    endHarvestPeriod2 = table.Column<int>(nullable: true),
                    exposureShadow = table.Column<bool>(nullable: false),
                    exposureMiddle = table.Column<bool>(nullable: false),
                    exposureSun = table.Column<bool>(nullable: false),
                    fertility = table.Column<bool>(nullable: false),
                    windbreaker = table.Column<bool>(nullable: false),
                    nitrate = table.Column<bool>(nullable: false),
                    othersMinerals = table.Column<bool>(nullable: false),
                    othersMineralsRating = table.Column<int>(nullable: true),
                    melliferous = table.Column<bool>(nullable: false),
                    melliferousRating = table.Column<int>(nullable: true),
                    aromaticPlant = table.Column<bool>(nullable: false),
                    description = table.Column<string>(maxLength: 2048, nullable: true),
                    pfafKnownHazards = table.Column<string>(maxLength: 2048, nullable: true),
                    pfafDescription = table.Column<string>(maxLength: 2048, nullable: true),
                    pfafUrl = table.Column<string>(maxLength: 256, nullable: true),
                    sophyUrl = table.Column<string>(maxLength: 256, nullable: true),
                    externalLink = table.Column<string>(maxLength: 256, nullable: true),
                    remarks = table.Column<string>(maxLength: 2048, nullable: true),
                    infosToKnow = table.Column<string>(maxLength: 2048, nullable: true),
                    auxiliariesAttractive = table.Column<bool>(nullable: false),
                    pestRepellents = table.Column<bool>(nullable: false),
                    dtSuppression = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plants", x => x.code);
                    table.ForeignKey(
                        name: "FK_Plants_PlantsFamilies_codeFamily",
                        column: x => x.codeFamily,
                        principalTable: "PlantsFamilies",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plants_SexualCharacteristics_codeSex",
                        column: x => x.codeSex,
                        principalTable: "SexualCharacteristics",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Plants_PlantsStratums_codeStratum",
                        column: x => x.codeStratum,
                        principalTable: "PlantsStratums",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Associations",
                columns: table => new
                {
                    codePlant1 = table.Column<int>(nullable: false),
                    codePlant2 = table.Column<int>(nullable: false),
                    Plantcode = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Associations", x => new { x.codePlant1, x.codePlant2 });
                    table.ForeignKey(
                        name: "FK_Associations_Plants_Plantcode",
                        column: x => x.Plantcode,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Associations_Plants_codePlant1",
                        column: x => x.codePlant1,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Associations_Plants_codePlant2",
                        column: x => x.codePlant2,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlantsAuxiliaries",
                columns: table => new
                {
                    codePlant = table.Column<int>(nullable: false),
                    codeInsect = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantsAuxiliaries", x => new { x.codePlant, x.codeInsect });
                    table.ForeignKey(
                        name: "FK_PlantsAuxiliaries_Insects_codeInsect",
                        column: x => x.codeInsect,
                        principalTable: "Insects",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlantsAuxiliaries_Plants_codePlant",
                        column: x => x.codePlant,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlantsPests",
                columns: table => new
                {
                    codePlant = table.Column<int>(nullable: false),
                    codeInsect = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantsPests", x => new { x.codePlant, x.codeInsect });
                    table.ForeignKey(
                        name: "FK_PlantsPests_Insects_codeInsect",
                        column: x => x.codeInsect,
                        principalTable: "Insects",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlantsPests_Plants_codePlant",
                        column: x => x.codePlant,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlantsTypes",
                columns: table => new
                {
                    codePlant = table.Column<int>(nullable: false),
                    codeVegetationType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantsTypes", x => new { x.codePlant, x.codeVegetationType });
                    table.ForeignKey(
                        name: "FK_PlantsTypes_Plants_codePlant",
                        column: x => x.codePlant,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlantsTypes_VegetationTypes_codeVegetationType",
                        column: x => x.codeVegetationType,
                        principalTable: "VegetationTypes",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SophyData",
                columns: table => new
                {
                    codeMainPlant = table.Column<int>(nullable: false),
                    codeDiscriminantPlant = table.Column<int>(nullable: false),
                    fidelity = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SophyData", x => new { x.codeMainPlant, x.codeDiscriminantPlant });
                    table.ForeignKey(
                        name: "FK_SophyData_Plants_codeDiscriminantPlant",
                        column: x => x.codeDiscriminantPlant,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SophyData_Plants_codeMainPlant",
                        column: x => x.codeMainPlant,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Associations_Plantcode",
                table: "Associations",
                column: "Plantcode");

            migrationBuilder.CreateIndex(
                name: "IX_Associations_codePlant2",
                table: "Associations",
                column: "codePlant2");

            migrationBuilder.CreateIndex(
                name: "IX_Plants_codeFamily",
                table: "Plants",
                column: "codeFamily");

            migrationBuilder.CreateIndex(
                name: "IX_Plants_codeSex",
                table: "Plants",
                column: "codeSex");

            migrationBuilder.CreateIndex(
                name: "IX_Plants_codeStratum",
                table: "Plants",
                column: "codeStratum");

            migrationBuilder.CreateIndex(
                name: "IX_PlantsAuxiliaries_codeInsect",
                table: "PlantsAuxiliaries",
                column: "codeInsect");

            migrationBuilder.CreateIndex(
                name: "IX_PlantsPests_codeInsect",
                table: "PlantsPests",
                column: "codeInsect");

            migrationBuilder.CreateIndex(
                name: "IX_PlantsTypes_codeVegetationType",
                table: "PlantsTypes",
                column: "codeVegetationType");

            migrationBuilder.CreateIndex(
                name: "IX_SophyData_codeDiscriminantPlant",
                table: "SophyData",
                column: "codeDiscriminantPlant");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Associations");

            migrationBuilder.DropTable(
                name: "PlantsAuxiliaries");

            migrationBuilder.DropTable(
                name: "PlantsPests");

            migrationBuilder.DropTable(
                name: "PlantsTypes");

            migrationBuilder.DropTable(
                name: "SophyData");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Insects");

            migrationBuilder.DropTable(
                name: "VegetationTypes");

            migrationBuilder.DropTable(
                name: "Plants");

            migrationBuilder.DropTable(
                name: "PlantsFamilies");

            migrationBuilder.DropTable(
                name: "SexualCharacteristics");

            migrationBuilder.DropTable(
                name: "PlantsStratums");
        }
    }
}
