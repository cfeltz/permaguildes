﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class SophyPlantRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "codePlant",
                table: "Sophy",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "codeSophy",
                table: "Plants",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "codeSophyBackup",
                table: "Plants",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Plants_codeSophy",
                table: "Plants",
                column: "codeSophy",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Plants_Sophy_codeSophy",
                table: "Plants",
                column: "codeSophy",
                principalTable: "Sophy",
                principalColumn: "code",
                onDelete: ReferentialAction.Restrict);

            #region Fill codeSophy field
            string query =  "UPDATE Plants" + 
                            " SET codeSophy = CAST(REPLACE(REPLACE(SophyUrl, 'http://sophy.tela-botanica.org/PSHTM/PS', ''), '.htm', '') AS UNSIGNED)" + 
                            " WHERE NOT SophyUrl IS NULL AND SophyUrl != ''";
            migrationBuilder.Sql(query);
            #endregion
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plants_Sophy_codeSophy",
                table: "Plants");

            migrationBuilder.DropIndex(
                name: "IX_Plants_codeSophy",
                table: "Plants");

            migrationBuilder.DropColumn(
                name: "codePlant",
                table: "Sophy");

            migrationBuilder.DropColumn(
                name: "codeSophy",
                table: "Plants");

            migrationBuilder.DropColumn(
                name: "codeSophyBackup",
                table: "Plants");
        }
    }
}
