﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class FixData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            #region Fix auxiliariesAttractive values on Plants table
            string query =  "UPDATE Plants AS P" + 
                            " LEFT JOIN PlantsAuxiliaries AS A ON A.codePlant = P.code" + 
                            " SET P.auxiliariesAttractive = FALSE" + 
                            " WHERE P.auxiliariesAttractive = TRUE AND A.codePlant IS NULL";
            migrationBuilder.Sql(query);

            query = "UPDATE Plants AS P" + 
                    " LEFT JOIN PlantsAuxiliaries AS A ON A.codePlant = P.code" + 
                    " SET P.auxiliariesAttractive = TRUE" + 
                    " WHERE P.auxiliariesAttractive = FALSE AND NOT A.codePlant IS NULL";
            migrationBuilder.Sql(query);
            #endregion
            
            #region Fix pestRepellents values on Plants table
            query = "UPDATE Plants AS P" + 
                    " LEFT JOIN PlantsPests AS I ON I.codePlant = P.code" + 
                    " SET P.pestRepellents = FALSE" + 
                    " WHERE P.pestRepellents = TRUE AND I.codePlant IS NULL";
            migrationBuilder.Sql(query);

            query = "UPDATE Plants AS P" + 
                    " LEFT JOIN PlantsPests AS I ON I.codePlant = P.code" + 
                    " SET P.pestRepellents = TRUE" + 
                    " WHERE P.pestRepellents = FALSE AND NOT I.codePlant IS NULL";
            migrationBuilder.Sql(query);
            #endregion

            query = "DELETE FROM Plants " +
                    " WHERE latinNames IN ('Solidago', 'Brassica et sinapis', 'Cytisus scoparius bis', 'Hyacinthus orientalis', 'Tulipa')";
            migrationBuilder.Sql(query);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
