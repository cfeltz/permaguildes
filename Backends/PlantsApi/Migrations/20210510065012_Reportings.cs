﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class Reportings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Reportings",
                columns: table => new
                {
                    code = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    email = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    message = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    codePlant = table.Column<int>(type: "int", nullable: true),
                    dtCreation = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    dtUpdate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reportings", x => x.code);
                    table.ForeignKey(
                        name: "FK_Reportings_Plants_codePlant",
                        column: x => x.codePlant,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reportings_codePlant",
                table: "Reportings",
                column: "codePlant");

            #region Add new famillies
            string query =  "INSERT INTO PlantsFamilies (code, libelle)" + 
                            " VALUES (189, 'Balsaminaceae')";
            migrationBuilder.Sql(query);
            query =  "INSERT INTO PlantsFamilies (code, libelle)" + 
                            " VALUES (190, 'Empetraceae')";
            migrationBuilder.Sql(query);
            query =  "INSERT INTO PlantsFamilies (code, libelle)" + 
                            " VALUES (191, 'Menyanthaceae')";
            migrationBuilder.Sql(query);
            #endregion
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            #region Remove new famillies
            string query =  "DELETE FROM PlantsFamilies" + 
                            " WHERE code IN (189, 190, 191)";
            migrationBuilder.Sql(query);
            #endregion

            migrationBuilder.DropTable(
                name: "Reportings");
        }
    }
}
