﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class AddRootstocksAttributes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "maxHeight",
                table: "Rootstocks",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "maxSpan",
                table: "Rootstocks",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "minHeight",
                table: "Rootstocks",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "minSpan",
                table: "Rootstocks",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "maxHeight",
                table: "Rootstocks");

            migrationBuilder.DropColumn(
                name: "maxSpan",
                table: "Rootstocks");

            migrationBuilder.DropColumn(
                name: "minHeight",
                table: "Rootstocks");

            migrationBuilder.DropColumn(
                name: "minSpan",
                table: "Rootstocks");
        }
    }
}
