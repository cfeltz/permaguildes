﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class FixAttractiveValue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            #region Update auxiliariesAttractive values from pfaf
            string query =  "UPDATE Plants" + 
                            " SET auxiliariesAttractive = TRUE" + 
                            " WHERE code IN (8, 23, 37, 61, 77, 91, 92, 119, 183, 203, 237, 239, 241, 252, 295, 298, 304, 306, 311, 313, 332, 339, 369, 371, 385, 386, 387, 401, 411, 415, 417, 427, 431, 432, 440, 441, 443, 455, 470, 479, 489, 492, 494, 509, 531, 538)";
            migrationBuilder.Sql(query);
            #endregion
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
