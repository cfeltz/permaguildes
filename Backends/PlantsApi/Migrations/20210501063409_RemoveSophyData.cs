﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class RemoveSophyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SophyData");

            migrationBuilder.DropColumn(
                name: "sophyUrl",
                table: "Plants");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "sophyUrl",
                table: "Plants",
                type: "varchar(256) CHARACTER SET utf8mb4",
                maxLength: 256,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SophyData",
                columns: table => new
                {
                    codeMainPlant = table.Column<int>(type: "int", nullable: false),
                    codeDiscriminantPlant = table.Column<int>(type: "int", nullable: false),
                    fidelity = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SophyData", x => new { x.codeMainPlant, x.codeDiscriminantPlant });
                    table.ForeignKey(
                        name: "FK_SophyData_Plants_codeDiscriminantPlant",
                        column: x => x.codeDiscriminantPlant,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SophyData_Plants_codeMainPlant",
                        column: x => x.codeMainPlant,
                        principalTable: "Plants",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SophyData_codeDiscriminantPlant",
                table: "SophyData",
                column: "codeDiscriminantPlant");
        }
    }
}
