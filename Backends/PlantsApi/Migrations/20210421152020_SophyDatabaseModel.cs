﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class SophyDatabaseModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sophy",
                columns: table => new
                {
                    code = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    sophyName = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    obs = table.Column<int>(type: "int", nullable: false),
                    loc = table.Column<int>(type: "int", nullable: false),
                    dis = table.Column<int>(type: "int", nullable: false),
                    qdr = table.Column<int>(type: "int", nullable: false),
                    coc = table.Column<int>(type: "int", nullable: false),
                    dtSuppression = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    dtCreation = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    dtUpdate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sophy", x => x.code);
                });

            migrationBuilder.CreateTable(
                name: "SophyDiscriminatingPlants",
                columns: table => new
                {
                    codeMainPlant = table.Column<int>(type: "int", nullable: false),
                    sophyNameDiscriminantPlant = table.Column<string>(type: "varchar(255) CHARACTER SET utf8mb4", nullable: false),
                    codeDiscriminantPlant = table.Column<int>(type: "int", nullable: true),
                    fidelity = table.Column<int>(type: "int", nullable: false),
                    frequency = table.Column<int>(type: "int", nullable: false),
                    pdcum = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SophyDiscriminatingPlants", x => new { x.codeMainPlant, x.sophyNameDiscriminantPlant });
                    table.ForeignKey(
                        name: "FK_SophyDiscriminatingPlants_Sophy_codeDiscriminantPlant",
                        column: x => x.codeDiscriminantPlant,
                        principalTable: "Sophy",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SophyDiscriminatingPlants_Sophy_codeMainPlant",
                        column: x => x.codeMainPlant,
                        principalTable: "Sophy",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SophySimilarPlants",
                columns: table => new
                {
                    codeMainPlant = table.Column<int>(type: "int", nullable: false),
                    sophyNameSimilarPlant = table.Column<string>(type: "varchar(255) CHARACTER SET utf8mb4", nullable: false),
                    codeSimilarPlant = table.Column<int>(type: "int", nullable: true),
                    gap = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SophySimilarPlants", x => new { x.codeMainPlant, x.sophyNameSimilarPlant });
                    table.ForeignKey(
                        name: "FK_SophySimilarPlants_Sophy_codeMainPlant",
                        column: x => x.codeMainPlant,
                        principalTable: "Sophy",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SophySimilarPlants_Sophy_codeSimilarPlant",
                        column: x => x.codeSimilarPlant,
                        principalTable: "Sophy",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SophyDiscriminatingPlants_codeDiscriminantPlant",
                table: "SophyDiscriminatingPlants",
                column: "codeDiscriminantPlant");

            migrationBuilder.CreateIndex(
                name: "IX_SophySimilarPlants_codeSimilarPlant",
                table: "SophySimilarPlants",
                column: "codeSimilarPlant");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SophyDiscriminatingPlants");

            migrationBuilder.DropTable(
                name: "SophySimilarPlants");

            migrationBuilder.DropTable(
                name: "Sophy");
        }
    }
}
