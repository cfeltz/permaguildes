﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.PlantsApi.Migrations
{
    public partial class FixBugOnDiscriminantsValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "codePlant",
                table: "Sophy");

            migrationBuilder.AlterColumn<int>(
                name: "pdcum",
                table: "SophyDiscriminatingPlants",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "frequency",
                table: "SophyDiscriminatingPlants",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "fidelity",
                table: "SophyDiscriminatingPlants",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "pdcum",
                table: "SophyDiscriminatingPlants",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "frequency",
                table: "SophyDiscriminatingPlants",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "fidelity",
                table: "SophyDiscriminatingPlants",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "codePlant",
                table: "Sophy",
                type: "int",
                nullable: true);
        }
    }
}
