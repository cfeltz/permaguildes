using System;
using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace PermaGuildes.Backends.PlantsApi.Database
{
    public class PlantsApiContext  : IdentityDbContext<ApplicationUser>
    {
        private string _connectionStringForMigration = null;
        public PlantsApiContext() : base()
        {
            _connectionStringForMigration = "server=127.0.0.1;port=33306;userid=PermaguildesUser;password=Pass@word1;database=Plantsdev;";
        }

        public PlantsApiContext(DbContextOptions<PlantsApiContext> options)
            : base(options)
        {
        }

        #region Authenticate
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ApplicationRole> ApplicationRoles { get; set; }
        #endregion

        #region Common data tables
        public DbSet<VegetationType> VegetationTypes { get; set; }

        public DbSet<SexualCharacteristic> SexualCharacteristics { get; set; }

        public DbSet<Insect> Insects { get; set; }

        public DbSet<PlantStratum> PlantsStratums { get; set; }

        public DbSet<PlantFamily> PlantsFamilies { get; set; }
        public DbSet<PlantType> PlantsTypes { get; set; }
        #endregion

        #region Main tables
        public DbSet<Plant> Plants { get; set; }

        public DbSet<Association> Associations { get; set; }
        public DbSet<PlantAuxiliary> PlantsAuxiliaries { get; set; }
        public DbSet<PlantPest> PlantsPests { get; set; }
        public DbSet<Sophy> Sophy { get; set; }
        public DbSet<Reporting> Reportings { get; set; }
        public DbSet<SophyDiscriminatingPlants> SophyDiscriminatingPlants { get; set; }
        public DbSet<SophySimilarPlants> SophySimilarPlants { get; set; }
        #endregion
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Plant>()
                .HasOne(p => p.family)
                .WithMany(f => f.plants)
                .HasForeignKey(p => p.codeFamily);

            modelBuilder.Entity<Plant>()
                .HasOne(p => p.stratum)
                .WithMany(s => s.plants)
                .HasForeignKey(p => p.codeStratum);

            modelBuilder.Entity<Plant>()
                .HasOne(p => p.sex)
                .WithMany(s => s.plants)
                .HasForeignKey(p => p.codeSex);

            #region Varieties and rootstocks
            modelBuilder.Entity<Variety>()
                .HasOne(p => p.plant)
                .WithMany(p => p.varieties)
                .HasForeignKey(p => p.codePlant);

            modelBuilder.Entity<Rootstock>()
                .HasOne(p => p.plant)
                .WithMany(p => p.rootstocks)
                .HasForeignKey(p => p.codePlant);
            #endregion

            #region PlantType
            modelBuilder.Entity<PlantType>()
                .HasKey(r => new { r.codePlant, r.codeVegetationType });  
            modelBuilder.Entity<PlantType>()
                .HasOne(r => r.plant)
                .WithMany(p => p.vegetationTypes)
                .HasForeignKey(r => r.codePlant);  
            modelBuilder.Entity<PlantType>()
                .HasOne(r => r.vegetationType)
                .WithMany(t => t.vegetationTypes)
                .HasForeignKey(r => r.codeVegetationType);
            #endregion

            #region PlantAuxiliary
            modelBuilder.Entity<PlantAuxiliary>()
                .HasKey(r => new { r.codePlant, r.codeInsect });  
            modelBuilder.Entity<PlantAuxiliary>()
                .HasOne(r => r.plant)
                .WithMany(p => p.auxiliaries)
                .HasForeignKey(r => r.codePlant);  
            modelBuilder.Entity<PlantAuxiliary>()
                .HasOne(r => r.insect)
                .WithMany(i => i.plantsWithAuxiliary)
                .HasForeignKey(r => r.codeInsect);
            #endregion

            #region PlantPest
            modelBuilder.Entity<PlantPest>()
                .HasKey(r => new { r.codePlant, r.codeInsect });  
            modelBuilder.Entity<PlantPest>()
                .HasOne(r => r.plant)
                .WithMany(p => p.pests)
                .HasForeignKey(r => r.codePlant);  
            modelBuilder.Entity<PlantPest>()
                .HasOne(r => r.insect)
                .WithMany(i => i.plantsWithPest)
                .HasForeignKey(r => r.codeInsect);
            #endregion

            #region Association
            modelBuilder.Entity<Association>()
                .HasKey(r => new { r.codePlant1, r.codePlant2 });  
            modelBuilder.Entity<Association>()
                .HasOne(r => r.plant1)
                .WithMany(p => p.associationsDesc)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(r => r.codePlant1);  
            modelBuilder.Entity<Association>()
                .HasOne(r => r.plant2)
                .WithMany(p => p.associationsAsc)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(r => r.codePlant2);
            #endregion

            #region Sophy
            modelBuilder.Entity<Sophy>()
                .HasOne(s => s.plant)
                .WithOne(p => p.sophy)
                .HasForeignKey<Plant>(p => p.codeSophy);
            modelBuilder.Entity<Sophy>()
                .HasIndex(s => new { s.sophyName });
            #endregion

            #region SophyDiscriminatingPlants relation 
            modelBuilder.Entity<SophyDiscriminatingPlants>()
                .HasKey(r => new { r.codeMainPlant, r.sophyNameDiscriminantPlant });  
            modelBuilder.Entity<SophyDiscriminatingPlants>()
                .HasOne(r => r.mainPlant)
                .WithMany(p => p.discrimantsPlants)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(r => r.codeMainPlant);  
            modelBuilder.Entity<SophyDiscriminatingPlants>()
                .HasOne(r => r.discriminantPlant)
                .WithMany(p => p.mainDiscrimantsPlants)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(r => r.codeDiscriminantPlant);
            #endregion

            #region SophySimilarPlants relation 
            modelBuilder.Entity<SophySimilarPlants>()
                .HasKey(r => new { r.codeMainPlant, r.sophyNameSimilarPlant });  
            modelBuilder.Entity<SophySimilarPlants>()
                .HasOne(r => r.mainPlant)
                .WithMany(p => p.similarsPlants)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(r => r.codeMainPlant);  
            modelBuilder.Entity<SophySimilarPlants>()
                .HasOne(r => r.similarPlant)
                .WithMany(p => p.mainSimilarPlants)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(r => r.codeSimilarPlant);
            #endregion

            #region Reportings
            modelBuilder.Entity<Reporting>()
                .HasOne(r => r.plant)
                .WithMany(p => p.reportings)
                .HasForeignKey(r => r.codePlant);
            #endregion
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!String.IsNullOrEmpty(_connectionStringForMigration))
            {
                var version = MySqlServerVersion.AutoDetect(_connectionStringForMigration);
                optionsBuilder.UseMySql(_connectionStringForMigration, version);
            }
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.ConfigureWarnings(warnings => warnings.Log(CoreEventId.NavigationBaseIncludeIgnored));
        }

        #region Perform entities and catch sql exceptions
       private void PerformEntities()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is UpdateableEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            DateTime now = DateTime.Now;
            foreach (var entityEntry in entries)
            {
                ((UpdateableEntity)entityEntry.Entity).dtUpdate = now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((UpdateableEntity)entityEntry.Entity).dtCreation = now;
                }
            }
        }

        private void CatchingChanges(Exception ex)
        {
            if (ex.InnerException != null && ex.InnerException.GetType() == typeof(SqlException))
            {
                var sqlEx = (SqlException)ex.InnerException;
                // See https://docs.microsoft.com/fr-fr/sql/relational-databases/errors-events/database-engine-events-and-errors?view=sql-server-ver15
                switch (sqlEx.Number)
                {
                    case 2627: 
                    {
                        throw new LocalException(ex, ExceptionsIdFromWebApiHelper.DuplicateData, 
                            "Unable to insert duplicate data");
                    }
                }
            }
            throw new LocalException(ex, ExceptionsIdFromWebApiHelper.UnknownExceptionInDatabase, 
                "Unknown error in database update process, see inner exception for details");
        }
        #endregion
        
        #region SaveChanges
        public override int SaveChanges()
        {
            PerformEntities();
            try 
            {
                return base.SaveChanges();
            }
            catch(Exception ex) 
            {
                CatchingChanges(ex);
                throw;
            }
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            PerformEntities();
            try 
            {
                return base.SaveChanges(acceptAllChangesOnSuccess);
            }
            catch(Exception ex) 
            {
                CatchingChanges(ex);
                throw;
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            PerformEntities();
            try 
            {
                return await base.SaveChangesAsync(cancellationToken);
            }
            catch(Exception ex) 
            {
                CatchingChanges(ex);
                throw;
            }
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            PerformEntities();
            try 
            {
                return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            }
            catch(Exception ex) 
            {
                CatchingChanges(ex);
                throw;
            }
        }
        #endregion
    }
}