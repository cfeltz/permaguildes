using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.ExtensionsBase;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using CFeltz.Shared.WebApiHelper.Tools;
using System.Data.Common;

namespace PermaGuildes.Backends.PlantsApi.Database
{
    public class DbInitializer : DbInitializerExtension<PlantsApiContext>
    {
        override public void Initialize(PlantsApiContext context)
        {
            Initialize(context, userManager: null, roleManager: null);
        } 
        public void Initialize(PlantsApiContext context, 
            UserManager<ApplicationUser> userManager, 
            RoleManager<ApplicationRole> roleManager)
        {
            base.Initialize(context);
            
            if (context.VegetationTypes.Any())
            {
                return;   // DB has been seeded
            }

            InitializeIdentity(context, userManager, roleManager);
            InitializeCommonData(context);
            // InitializeScriptedData(context);

            return;
        }

        private static void InitializeIdentity(PlantsApiContext context, 
            UserManager<ApplicationUser> userManager, 
            RoleManager<ApplicationRole> roleManager)
        {
            if (roleManager != null) 
            {
                if(!context.Roles.Any())
                {
                    var task = roleManager.CreateAsync(new ApplicationRole("Administrator"));
                    task.Wait();
                }
            }

            if (userManager != null) 
            {
                if(!context.Users.Any())
                {
                    ApplicationUser user = new ApplicationUser()
                    {
                        Email = "christophe.feltz@gmail.com",
                        SecurityStamp = Guid.NewGuid().ToString(),
                        UserName = "cfeltz"
                    };
                    var task = userManager.CreateAsync(user, "Christophe.feltz@gmail.com1");
                    task.Wait();
                    var result = task.Result;
                    if (result.Succeeded)
                    {
                        var task2 = userManager.FindByNameAsync(user.UserName);
                        task2.Wait();
                        user = task2.Result;
                        var task3 = userManager.AddToRoleAsync(user, "Administrator");
                        task3.Wait();
                    }
                    else 
                    {
                        string error = string.Join(",", result.Errors.ToList().Select(e => e.Description));
                        throw new LocalException(ExceptionsIdFromWebApiHelper.AuthenticateError, error);
                    }
                }
            }
        }

        private static void InitializeCommonData(PlantsApiContext context)
        {

            #region Initialize Vegetation types
            VegetationType[] vegetationTypes = new VegetationType[]
            {
                new VegetationType {code=1, libelle="Annuelle"},
                new VegetationType {code=2, libelle="Biannuelle"},
                new VegetationType {code=3, libelle="Caduc"},
                new VegetationType {code=4, libelle="Semi-persistant"},
                new VegetationType {code=5, libelle="Persistant"}
            };
            foreach (VegetationType t in vegetationTypes)
            {
                context.VegetationTypes.Add(t);
            }
            context.SaveChanges();
            #endregion

            #region Initialize Sexual characteristics
            SexualCharacteristic[] sexualCharacteristics = new SexualCharacteristic[]
            {
                new SexualCharacteristic {code=1, libelle="Monoïques"},
                new SexualCharacteristic {code=2, libelle="Hermaphrodites"},
                new SexualCharacteristic {code=3, libelle="Dioïques"}
            };
            foreach (SexualCharacteristic s in sexualCharacteristics)
            {
                context.SexualCharacteristics.Add(s);
            }
            context.SaveChanges();
            #endregion

            #region Initialize Insects
            Insect[] insects = new Insect[]
            {
                new Insect {code=1, libelle="Aleurodes", auxiliary=false},
                new Insect {code=2, libelle="Araignées rouges (acariens)", auxiliary=false},
                new Insect {code=3, libelle="Bupreste", auxiliary=false},
                new Insect {code=4, libelle="Chenilles", auxiliary=false},
                new Insect {code=5, libelle="Cochenilles", auxiliary=false},
                new Insect {code=6, libelle="Doryphores", auxiliary=false},
                new Insect {code=7, libelle="Limaces", auxiliary=false},
                new Insect {code=8, libelle="Mouches", auxiliary=false},
                new Insect {code=9, libelle="Moustiques", auxiliary=false},
                new Insect {code=10, libelle="Phytoptes (acariens)", auxiliary=false},
                new Insect {code=11, libelle="Pucerons", auxiliary=false},
                new Insect {code=12, libelle="Thrips", auxiliary=false},
                new Insect {code=13, libelle="Chrysope", auxiliary=true},
                new Insect {code=14, libelle="Coccinelle", auxiliary=true},
                new Insect {code=15, libelle="Syrphe", auxiliary=true},
                new Insect {code=16, libelle="Guêpe", auxiliary=true},
                new Insect {code=17, libelle="Apantele glomeratus", auxiliary=true},
                new Insect {code=18, libelle="Chalcidien", auxiliary=true},
                new Insect {code=19, libelle="Ichneumon", auxiliary=true},
                new Insect {code=20, libelle="Mouche tachinaire", auxiliary=true},
                new Insect {code=21, libelle="Hyménoptère parasitoïde", auxiliary=true},
                new Insect {code=22, libelle="Cantharide", auxiliary=true},
                new Insect {code=23, libelle="Hémérobe", auxiliary=true},
                new Insect {code=24, libelle="Punaise prédatrice", auxiliary=true}              
            };
            foreach (Insect i in insects)
            {
                context.Insects.Add(i);
            }
            context.SaveChanges();
            #endregion

            #region Initialize Plants stratums
            PlantStratum[] stratums = new PlantStratum[]
            {
                new PlantStratum {code=1, libelle="Canopée"},
                new PlantStratum {code=2, libelle="Petits arbres"},
                new PlantStratum {code=3, libelle="Arbustres"},
                new PlantStratum {code=4, libelle="Herbes"},
                new PlantStratum {code=5, libelle="Couvre-sol"},
                new PlantStratum {code=6, libelle="Mycélium"},
                new PlantStratum {code=7, libelle="Plantes grimpantes"}                
            };
            foreach (PlantStratum s in stratums)
            {
                context.PlantsStratums.Add(s);
            }
            context.SaveChanges();
            #endregion

            #region Initialize Plants families
            PlantFamily[] families = new PlantFamily[]
            {
                new PlantFamily {code=1, libelle="Acanthaceae"},
                new PlantFamily {code=2, libelle="Aceraceae"},
                new PlantFamily {code=3, libelle="Actinidiaceae"},
                new PlantFamily {code=4, libelle="Adoxaceae"},
                new PlantFamily {code=5, libelle="Agavaceae"},
                new PlantFamily {code=6, libelle="Aizoaceae"},
                new PlantFamily {code=7, libelle="Aloaceae"},
                new PlantFamily {code=8, libelle="Amaranthaceae"},
                new PlantFamily {code=9, libelle="Amaryllidaceae"},
                new PlantFamily {code=10, libelle="Anacardiaceae"},
                new PlantFamily {code=11, libelle="Annonaceae"},
                new PlantFamily {code=12, libelle="Apiaceae"},
                new PlantFamily {code=13, libelle="Apocynaceae"},
                new PlantFamily {code=14, libelle="Aquifoliaceae"},
                new PlantFamily {code=15, libelle="Araceae"},
                new PlantFamily {code=16, libelle="Araliaceae"},
                new PlantFamily {code=17, libelle="Araucariaceae"},
                new PlantFamily {code=18, libelle="Arecaceae"},
                new PlantFamily {code=19, libelle="Aristolochiaceae"},
                new PlantFamily {code=20, libelle="Asclepiadaceae"},
                new PlantFamily {code=21, libelle="Asparagaceae"},
                new PlantFamily {code=22, libelle="Asphodelaceae"},
                new PlantFamily {code=23, libelle="Aspleniaceae"},
                new PlantFamily {code=24, libelle="Asteraceae"},
                new PlantFamily {code=25, libelle="alsaminaceae"},
                new PlantFamily {code=26, libelle="Begoniaceae"},
                new PlantFamily {code=27, libelle="Berberidaceae"},
                new PlantFamily {code=28, libelle="Betulaceae"},
                new PlantFamily {code=29, libelle="Bignoniaceae"},
                new PlantFamily {code=30, libelle="Bombacaceae"},
                new PlantFamily {code=31, libelle="Boraginaceae"},
                new PlantFamily {code=32, libelle="Brassicaceae"},
                new PlantFamily {code=33, libelle="Bromeliaceae"},
                new PlantFamily {code=34, libelle="Buxaceae"},
                new PlantFamily {code=35, libelle="Cactaceae"},
                new PlantFamily {code=36, libelle="Caesalpiniaceae"},
                new PlantFamily {code=37, libelle="Calycanthaceae"},
                new PlantFamily {code=38, libelle="Campanulaceae"},
                new PlantFamily {code=39, libelle="Cannabaceae"},
                new PlantFamily {code=40, libelle="Cannaceae"},
                new PlantFamily {code=41, libelle="Capparaceae"},
                new PlantFamily {code=42, libelle="Caprifoliaceae"},
                new PlantFamily {code=43, libelle="Caricaceae"},
                new PlantFamily {code=44, libelle="Caryophyllaceae"},
                new PlantFamily {code=45, libelle="Celastraceae"},
                new PlantFamily {code=46, libelle="Cephalotaceae"},
                new PlantFamily {code=47, libelle="Cercidiphyllaceae"},
                new PlantFamily {code=48, libelle="Chenopodiaceae"},
                new PlantFamily {code=49, libelle="Chloranthaceae"},
                new PlantFamily {code=50, libelle="Cistaceae"},
                new PlantFamily {code=51, libelle="Clusiaceae"},
                new PlantFamily {code=52, libelle="Commelinaceae"},
                new PlantFamily {code=53, libelle="Convolvulaceae"},
                new PlantFamily {code=54, libelle="Cornaceae"},
                new PlantFamily {code=55, libelle="Crassulaceae"},
                new PlantFamily {code=56, libelle="Cucurbitaceae"},
                new PlantFamily {code=57, libelle="Cupressaceae"},
                new PlantFamily {code=58, libelle="Cycadaceae"},
                new PlantFamily {code=59, libelle="Cyperaceae"},
                new PlantFamily {code=60, libelle="Dicksoniaceae"},
                new PlantFamily {code=61, libelle="Dioscoreaceae"},
                new PlantFamily {code=62, libelle="Dipsacaceae"},
                new PlantFamily {code=63, libelle="Droseraceae"},
                new PlantFamily {code=64, libelle="Dryopteridaceae"},
                new PlantFamily {code=65, libelle="Ebenaceae"},
                new PlantFamily {code=66, libelle="Elaeagnaceae"},
                new PlantFamily {code=67, libelle="Elaeocarpaceae"},
                new PlantFamily {code=68, libelle="Equisetaceae"},
                new PlantFamily {code=69, libelle="Ericaceae"},
                new PlantFamily {code=70, libelle="Eriocaulaceae"},
                new PlantFamily {code=71, libelle="Escalloniaceae"},
                new PlantFamily {code=72, libelle="Euphorbiaceae"},
                new PlantFamily {code=73, libelle="Fabaceae"},
                new PlantFamily {code=74, libelle="Fagaceae"},
                new PlantFamily {code=75, libelle="Gentianaceae"},
                new PlantFamily {code=76, libelle="Geraniaceae"},
                new PlantFamily {code=77, libelle="Gesneriaceae"},
                new PlantFamily {code=78, libelle="Ginkgoaceae"},
                new PlantFamily {code=79, libelle="Globulariaceae"},
                new PlantFamily {code=80, libelle="Goodeniaceae"},
                new PlantFamily {code=81, libelle="Grossulariaceae"},
                new PlantFamily {code=82, libelle="Gunneraceae"},
                new PlantFamily {code=83, libelle="Haemodoraceae"},
                new PlantFamily {code=84, libelle="Haloragaceae"},
                new PlantFamily {code=85, libelle="Hamamelidaceae"},
                new PlantFamily {code=86, libelle="Heliconiaceae"},
                new PlantFamily {code=87, libelle="Hippocastanaceae"},
                new PlantFamily {code=88, libelle="Hippuridaceae"},
                new PlantFamily {code=89, libelle="Hyacinthaceae"},
                new PlantFamily {code=90, libelle="Hydrangeaceae"},
                new PlantFamily {code=91, libelle="Hydrocharitaceae"},
                new PlantFamily {code=92, libelle="Hypoxidaceae"},
                new PlantFamily {code=93, libelle="Iridaceae"},
                new PlantFamily {code=94, libelle="Juglandaceae"},
                new PlantFamily {code=95, libelle="Lamiaceae"},
                new PlantFamily {code=96, libelle="Lardizabalaceae"},
                new PlantFamily {code=97, libelle="Lauraceae"},
                new PlantFamily {code=98, libelle="Lemnaceae"},
                new PlantFamily {code=99, libelle="Liliaceae"},
                new PlantFamily {code=100, libelle="Linaceae"},
                new PlantFamily {code=101, libelle="Loganiaceae"},
                new PlantFamily {code=102, libelle="Lythraceae"},
                new PlantFamily {code=103, libelle="Magnoliaceae"},
                new PlantFamily {code=104, libelle="Malvaceae"},
                new PlantFamily {code=105, libelle="Marantaceae"},
                new PlantFamily {code=106, libelle="Melastomaceae"},
                new PlantFamily {code=107, libelle="Melastomataceae"},
                new PlantFamily {code=108, libelle="Meliaceae"},
                new PlantFamily {code=109, libelle="Moraceae"},
                new PlantFamily {code=110, libelle="Musaceae"},
                new PlantFamily {code=111, libelle="Myoporaceae"},
                new PlantFamily {code=112, libelle="Myricaceae"},
                new PlantFamily {code=113, libelle="Myrtaceae"},
                new PlantFamily {code=114, libelle="Nelumbonaceae"},
                new PlantFamily {code=115, libelle="Nepenthaceae"},
                new PlantFamily {code=116, libelle="Nephrolepidaceae"},
                new PlantFamily {code=117, libelle="Nyctaginaceae"},
                new PlantFamily {code=118, libelle="Nymphaeaceae"},
                new PlantFamily {code=119, libelle="Nyssaceae"},
                new PlantFamily {code=120, libelle="Oleaceae"},
                new PlantFamily {code=121, libelle="Onagraceae"},
                new PlantFamily {code=122, libelle="Orchidaceae"},
                new PlantFamily {code=123, libelle="Osmundaceae"},
                new PlantFamily {code=124, libelle="Oxalidaceae"},
                new PlantFamily {code=125, libelle="Paeoniaceae"},
                new PlantFamily {code=126, libelle="Papaveraceae"},
                new PlantFamily {code=127, libelle="Passifloraceae"},
                new PlantFamily {code=128, libelle="Phallaceae"},
                new PlantFamily {code=129, libelle="Philesiaceae"},
                new PlantFamily {code=130, libelle="Phytolaccaceae"},
                new PlantFamily {code=131, libelle="Pinaceae"},
                new PlantFamily {code=132, libelle="Piperaceae"},
                new PlantFamily {code=133, libelle="Pittosporaceae"},
                new PlantFamily {code=134, libelle="Plantaginaceae"},
                new PlantFamily {code=135, libelle="Platanaceae"},
                new PlantFamily {code=136, libelle="Plumbaginaceae"},
                new PlantFamily {code=137, libelle="Poaceae"},
                new PlantFamily {code=138, libelle="Polemoniaceae"},
                new PlantFamily {code=139, libelle="Polygalaceae"},
                new PlantFamily {code=140, libelle="Polygonaceae"},
                new PlantFamily {code=141, libelle="Polypodiaceae"},
                new PlantFamily {code=142, libelle="Pontederiaceae"},
                new PlantFamily {code=143, libelle="Portulacaceae"},
                new PlantFamily {code=144, libelle="Primulaceae"},
                new PlantFamily {code=145, libelle="Proteaceae"},
                new PlantFamily {code=146, libelle="Pteridaceae"},
                new PlantFamily {code=147, libelle="Punicaceae"},
                new PlantFamily {code=148, libelle="Pyronemataceae"},
                new PlantFamily {code=149, libelle="Ranunculaceae"},
                new PlantFamily {code=150, libelle="Restionaceae"},
                new PlantFamily {code=151, libelle="Rhamnaceae"},
                new PlantFamily {code=152, libelle="Rosaceae"},
                new PlantFamily {code=153, libelle="Rubiaceae"},
                new PlantFamily {code=154, libelle="Rutaceae"},
                new PlantFamily {code=155, libelle="Sabiaceae"},
                new PlantFamily {code=156, libelle="Salicaceae"},
                new PlantFamily {code=157, libelle="Sapindaceae"},
                new PlantFamily {code=158, libelle="Sarraceniaceae"},
                new PlantFamily {code=159, libelle="Saururaceae"},
                new PlantFamily {code=160, libelle="Saxifragaceae"},
                new PlantFamily {code=161, libelle="Schisandraceae"},
                new PlantFamily {code=162, libelle="Sciadopityaceae"},
                new PlantFamily {code=163, libelle="Scrophulariaceae"},
                new PlantFamily {code=164, libelle="Smilacaceae"},
                new PlantFamily {code=165, libelle="Solanaceae"},
                new PlantFamily {code=166, libelle="Strelitziaceae"},
                new PlantFamily {code=167, libelle="Styracaceae"},
                new PlantFamily {code=168, libelle="Taccaceae"},
                new PlantFamily {code=169, libelle="Tamaricaceae"},
                new PlantFamily {code=170, libelle="Taxaceae"},
                new PlantFamily {code=171, libelle="Taxodiaceae"},
                new PlantFamily {code=172, libelle="Theaceae"},
                new PlantFamily {code=173, libelle="Thymelaeaceae"},
                new PlantFamily {code=174, libelle="Tiliaceae"},
                new PlantFamily {code=175, libelle="Trapaceae"},
                new PlantFamily {code=176, libelle="Trilliaceae"},
                new PlantFamily {code=177, libelle="Tropaeolaceae"},
                new PlantFamily {code=178, libelle="Typhaceae"},
                new PlantFamily {code=179, libelle="Ulmaceae"},
                new PlantFamily {code=180, libelle="Urticaceae"},
                new PlantFamily {code=181, libelle="Valerianaceae"},
                new PlantFamily {code=182, libelle="Verbenaceae"},
                new PlantFamily {code=183, libelle="Violaceae"},
                new PlantFamily {code=184, libelle="Viscaceae"},
                new PlantFamily {code=185, libelle="Vitaceae"},
                new PlantFamily {code=186, libelle="Winteraceae"},
                new PlantFamily {code=187, libelle="Zamiaceae"},
                new PlantFamily {code=188, libelle="Zingiberaceae"}
            };
            foreach (PlantFamily f in families)
            {
                context.PlantsFamilies.Add(f);
            }
            context.SaveChanges();
            #endregion
        }
    
        private static void InitializeScriptedData(PlantsApiContext context)
        {
            string scriptsFolder = null;
            if (context.Database.IsMySql()) {
                scriptsFolder = @"Database/Scripts/MySQL";
            } else if (context.Database.IsNpgsql()) {
                scriptsFolder = @"Database/Scripts/PostGres";
            } else if (context.Database.IsSqlServer()) {
                scriptsFolder = @"Database/Scripts/SQLServer";
            } else if (context.Database.IsSqlite()) {
                scriptsFolder = @"Database/Scripts/SqLite";
            }

            context.InitializeScriptedData(scriptsFolder);
       }
    }
}