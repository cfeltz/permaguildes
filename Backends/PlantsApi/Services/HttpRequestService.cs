using System.Net.Http;
using System.Threading;
using CFeltz.Shared.WebApiHelper.Services;
using Microsoft.Extensions.Logging;

namespace RepositoryApi.Services
{
    public class HttpRequestService : HttpRequestServiceBase
    {
        #region Initialization        
        public HttpRequestService(ILogger<HttpRequestService> logger)
             : base(logger, new HttpClient())
        {
            InitClient();
        }
        #endregion
        
    }
}