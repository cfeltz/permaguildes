using System.Collections.Generic;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public interface IVarietiesService : IRelations1NServiceWithCrud<Variety>
    {
    }
}