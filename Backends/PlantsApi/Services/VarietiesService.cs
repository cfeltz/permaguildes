using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Services;
using System.Linq;
using PermaGuildes.Backends.PlantsApi.Models;
using System.Collections.Generic;
using PermaGuildes.Backends.PlantsApi.DatabaseRepositories;
using System.Linq.Expressions;
using System;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public class VarietiesService : Relations1NServiceWithCrud<Plant, Variety, Variety>, IVarietiesService
    {
        #region Initialization
        public VarietiesService(ILogger<VarietiesService> logger, 
            IVarietiesRepository varietiesRepository,
            IRealtimeEntitiesHubService realtimeEntitiesHubService)
             : base(logger, varietiesRepository, realtimeEntitiesHubService)
        {
        }
        #endregion

        #region Override abstracts methods
        protected override Expression<Func<Variety, bool>> GetRelationByKeyPredicate(string relationKey, string referenceKey = null)
        {
            int relationKeyAsInt = int.Parse(relationKey);
            int? referenceKeyAsInt = null;
            if (!string.IsNullOrEmpty(referenceKey)) 
            {
                referenceKeyAsInt = int.Parse(referenceKey);
            }
            if (referenceKey == null) {
                return ((Variety r) => r.code == relationKeyAsInt);
            } else {
                return ((Variety r) => r.code == relationKeyAsInt && r.codePlant == referenceKeyAsInt);
            }
        }
        protected override Expression<Func<Variety, bool>> GetRelationsByReferenceKeyPredicate(string referenceKey)
        {
            int referenceKeyAsInt = int.Parse(referenceKey);
            return ((Variety r) => r.codePlant == referenceKeyAsInt);
        }
        
        override protected Expression<Func<Variety, bool>> GetRelationsByKeysPredicate(List<string> referenceKeys, List<string> relation1NKeys) 
        {
            List<int> relation1NKeysAsInt = relation1NKeys.Select(k => int.Parse(k)).ToList();
            return ((Variety r) => relation1NKeysAsInt.Contains(r.code));
        }

        protected override void SetRelation(string referenceKey, Variety relation) 
        { 
            int referenceKeyAsInt = int.Parse(referenceKey);
            relation.codePlant = referenceKeyAsInt;
        }

        protected override IEnumerable<Variety> GetDefaultOrderBy(IEnumerable<Variety> source)
        {
            return source.OrderBy(r => r.name);
        }
        #endregion
    }
}