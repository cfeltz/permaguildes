using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.DatabaseRepositories;
using PermaGuildes.Backends.PlantsApi.Models;
using System.Linq.Expressions;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public class PlantsService : EntitiesServiceWithCrud<Plant, Plant>, IPlantsService
    {        
        private IPlantsRepository GetBaseRepositoryTyped() => (IPlantsRepository)GetBaseRepository();

        public PlantsService(ILogger<PlantsService> logger, 
            IPlantsRepository plantsRepository, 
            IRealtimeEntitiesHubService realtimeEntitiesHubService)
             : base(logger, plantsRepository, realtimeEntitiesHubService)
        {
        }

        #region Override abstracts and default virtuals methods
        override protected Expression<Func<Plant, bool>> GetEntityByKeyPredicate(string key)
        {
            int keyAsInt = int.Parse(key);
            return ((Plant e) => e.code == keyAsInt);
        }
        override protected Expression<Func<Plant, bool>> GetEntitiesByKeysPredicate(List<string> keys)
        {
            List<int> keysAsInt = keys.Select(k => int.Parse(k)).ToList();
            return ((Plant e) => keysAsInt.Contains(e.code));
        }

        override protected Expression<Func<Plant, object>>[] GetDefaultIncludes()
        {
            Expression<Func<Plant, object>> include1 = ((Plant p) => p.varieties);
            Expression<Func<Plant, object>> include2 = ((Plant p) => p.rootstocks);
            Expression<Func<Plant, object>> include3 = ((Plant p) => p.vegetationTypes);
            Expression<Func<Plant, object>> include4 = ((Plant p) => p.auxiliaries);
            Expression<Func<Plant, object>> include5 = ((Plant p) => p.pests);
            return new[] { include1, include2, include3, include4, include5};
        }
        #endregion


        public async Task<Plant> LoadPlant(int codePlant)
        {
            Plant plant = await GetBaseRepositoryTyped().LoadPlant(codePlant);

            if (plant != null && string.IsNullOrEmpty(plant.description))
            {
                plant.description = plant.pfafDescription;
            }

            return plant;
        }

        public async Task<List<CompanionPlant>> LoadPlantCompanions(int codePlant)
        {
            CompanionsPlantLists lists = await GetBaseRepositoryTyped().LoadPlantCompanionsLists(codePlant);
            return GenerateCompanions(lists);
        }

        public Plant GetPlantFromCodeSophy(int codeSophy)
        {
            Plant plant = GetBaseRepositoryTyped().Get(p => p.codeSophy == codeSophy);
            return plant;
        }

        public List<CompanionPlant> GenerateCompanions(CompanionsPlantLists lists)
        {
            List<CompanionPlant> companionsList = null;
            if (lists.sophy != null)
            {
                var discriminants = lists.sophy.discrimantsPlants
                    .Select(r => new CompanionPlant(r.discriminantPlant?.plant?.code, r))
                    .OrderBy(r => r.pdcum)
                    .ToList();
                var similars = lists.sophy.similarsPlants
                    .Select(r => new CompanionPlant(r.similarPlant?.plant?.code, r))
                    .OrderBy(r => r.gap)
                    .ToList();

                // Add discriminants plants to return list
                companionsList = discriminants;

                #region Add similars plants to return list
                similars.ForEach(c => {
                    CompanionPlant existingCompagnion = companionsList.Where(e => e.codeSophy == c.codeSophy).FirstOrDefault();
                    if (existingCompagnion != null) 
                    {
                        existingCompagnion.gap = c.gap;
                        existingCompagnion.comment = c.comment;
                    }
                    else 
                    {
                        companionsList.Add(c);
                    }
                });
                #endregion
            }

            #region Add associations plants to return list
            var associations = lists.associations
                .Select(a => new CompanionPlant(lists.codePlant == a.codePlant1 ? a.codePlant2 : a.codePlant1, a))
                .ToList();

            if (companionsList == null)
            {
                companionsList = associations;
            }
            else 
            {
                associations.ForEach(c => {
                    CompanionPlant existingCompagnion = null;
                    if (c.codePlant != null)
                    {
                        existingCompagnion = companionsList.Where(e => e.codePlant == c.codePlant).FirstOrDefault();
                    }
                    if (existingCompagnion != null) 
                    {
                        existingCompagnion.comment = c.comment;
                    }
                    else 
                    {
                        companionsList.Add(c);
                    }                        
                });
            }
            #endregion
            return companionsList;
        }
    }
}