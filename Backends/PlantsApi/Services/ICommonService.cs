using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public interface ICommonService : IServiceExtensionBase
    {
         IQueryable<Insect> GetInsects();
         IQueryable<PlantFamily> GetPlantFamilies();
         IQueryable<PlantStratum> GetPlantStratums();
         IQueryable<VegetationType> GetVegetationTypes();
         IQueryable<SexualCharacteristic> GetSexualCharacteristics();
    }
}