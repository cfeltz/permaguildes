using System.Threading.Tasks;
using System.Linq;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.Models;
using System.Collections.Generic;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public interface IPlantsService : IEntitiesServiceWithCrud<Plant>
    {
         Task<Plant> LoadPlant(int codePlant);
        Task<List<CompanionPlant>> LoadPlantCompanions(int codePlant);

         Plant GetPlantFromCodeSophy(int codeSophy);
    }
}