using System;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using PermaGuildes.Backends.PlantsApi.DatabaseRepositories;
using PermaGuildes.Backends.PlantsApi.Models;
using CFeltz.Shared.WebApiHelper.Services;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;
using PermaGuildes.Backends.PlantsApi.Models.Relations;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public class SophyService : ServiceExtensionBase, ISophyService
    {        
        private readonly string SOPHY_LIST_URL = "http://sophy.tela-botanica.org/PSHTM/Flrl6tis.htm";
        private readonly string SOPHY_BASE_URL = "http://sophy.tela-botanica.org/PSHTM/PS{0}.htm";

        private IPlantsRepository _plantsRepository;
        private ISophyRepository _sophyRepository;
        private IHttpRequestService _httpRequestService;

        public SophyService(ILogger<SophyService> logger, IHttpRequestService httpRequestService, 
            ISophyRepository sophyRepository,
             IPlantsRepository plantsRepository)
             : base(logger)
        {
            _sophyRepository = sophyRepository;
            _plantsRepository = plantsRepository;
            _httpRequestService = httpRequestService;
        }

        #region From Tela Botanica
        #region Get List from Tela Botanica
        public async Task<List<Sophy>> GetListFromTelaBotanica()
        {
            var result = await _httpRequestService.GetResult(SOPHY_LIST_URL);
            var xml = DeserializeResultAsXml(result, (string result) => {
                result = result.Replace("&eacute;", "é")
                            .Replace("&nbsp;", " ")
                            .Replace("&", "+");
                result = RemoveBalises(result, "font", "pre", "center", "br");
                return result;
            });

            var plantsNodes = xml.SelectNodes("//*/A[starts-with(@HREF, 'PS')]");
            _logger.LogInformation($"Found {plantsNodes.Count} plants on Tela Botanica");

            var sophyList = plantsNodes.Cast<XmlNode>().Select(
                n => { 
                    string href = n.Attributes["HREF"].InnerText;
                    string codeAsString = href.TrimEnd().Replace("PS", "").Replace(".htm", "");
                    int code = Int32.Parse(codeAsString);

                    string name = n.InnerText.Replace("+", "&");
                    return new Sophy(code, name);
                }
            );
            return sophyList.ToList();
        }
        #endregion

        #region Get data from Tela Botanica
        public async Task<Sophy> GetSophyDataFromTelaBotanica(int code)
        {
            return await GetSophyDataFromTelaBotanica(new Sophy(code, null));
        }

        public string getSophyUrlFromCode(int? code) 
        {
            if (code == null) 
            {
                return null;
            }
            else 
            {
                return string.Format(SOPHY_BASE_URL, code);
            }
        }

        public async Task<Sophy> GetSophyDataFromTelaBotanica(Sophy sophy)
        {
            using (_logger.BeginScope(string.Format("GetSophyDataFromTelaBotanica({0})", sophy.code))) {
                try 
                {
                    _logger.LogInformation($"Get data of plant {sophy.code} on Tela Botanica");                    
                    var result = await _httpRequestService.GetResult(getSophyUrlFromCode(sophy.code));
                    var xml = DeserializeResultAsXml(result, (string result) => {
                        result = result.Replace("&eacute;", "é")
                                        .Replace("&nbsp;", " ")
                                        .Replace("&", "+");
                        result = RemoveBalises(result, "font", "pre", "center", "img", "table", "br", "td");

                        return result;
                    });
                    sophy.sophyName = xml.SelectNodes("//*/B")[0].InnerText.Replace("+", "&");

                    var body = xml.SelectNodes("//*/BODY").Cast<XmlNode>().FirstOrDefault();
                    #region Get OBS, LOC, DIS, QDR and COC data
                    string dataline = body.InnerText.Substring(body.InnerText.IndexOf("OBS ="));
                    dataline = dataline.Substring(0, dataline.IndexOf("\r\n"));
                    int locIdx = dataline.IndexOf("LOC =");
                    int disIdx = dataline.IndexOf("DIS =");
                    int qdrIdx = dataline.IndexOf("QDR =");
                    int cocIdx = dataline.IndexOf("COC =");
                    string obsStr = dataline.Substring(0, locIdx);
                    obsStr = obsStr.Substring(obsStr.IndexOf("=") + 1).Trim();
                    string locStr = dataline.Substring(locIdx, disIdx-locIdx);
                    locStr = locStr.Substring(locStr.IndexOf("=") + 1).Trim();
                    string disStr = dataline.Substring(disIdx, qdrIdx-disIdx);
                    disStr = disStr.Substring(disStr.IndexOf("=") + 1).Trim();
                    string qdrStr = dataline.Substring(qdrIdx, cocIdx-qdrIdx);
                    qdrStr = qdrStr.Substring(qdrStr.IndexOf("=") + 1).Trim();
                    string cocStr = dataline.Substring(cocIdx);
                    cocStr = cocStr.Substring(cocStr.IndexOf("=") + 1).Trim();
                    sophy.obs = Int32.Parse(obsStr);
                    sophy.loc = Int32.Parse(locStr);
                    sophy.dis = Int32.Parse(disStr);
                    sophy.qdr = Int32.Parse(qdrStr);
                    sophy.coc = Int32.Parse(cocStr);
                    _logger.LogDebug($"obs: { sophy.obs }, loc: { sophy.loc }, dis: { sophy.dis }, qdr: { sophy.qdr }, coc: { sophy.coc }");
                    #endregion

                    int similarPlantsIdx = body.InnerText.IndexOf("Plantes écologiquement similaires");
                    int discrimantsPlantsIdx = body.InnerText.IndexOf("Plantes discriminantes");

                    #region Plantes écologiquement similaires
                    string similarPlantsStr = body.InnerText.Substring(similarPlantsIdx, discrimantsPlantsIdx-similarPlantsIdx);
                    similarPlantsStr = similarPlantsStr.Substring(similarPlantsStr.IndexOf(".") - 1);
                    var similarPlantsLines = GetPlantsLines(similarPlantsStr, 32);
                    _logger.LogDebug($"Found { similarPlantsLines.Count() } similars plants");
                    foreach (var similarPlantStr in similarPlantsLines)
                    {
                        string gapStr = similarPlantStr.Substring(0, 4);
                        float gapDec = float.Parse(gapStr, CultureInfo.InvariantCulture);
                        int gap = (int)(100 * gapDec);

                        string sophyName = similarPlantStr.Substring(4).Trim().Replace("+", "&");

                        var similarPlant = new SophySimilarPlants(sophy, sophyName, gap);
                        sophy.similarsPlants.Add(similarPlant);
                    }
                    #endregion

                    #region Plantes discriminantes
                    string discrimantsPlantsStr = body.InnerText.Substring(discrimantsPlantsIdx);
                    discrimantsPlantsStr = discrimantsPlantsStr.Substring(discrimantsPlantsStr.IndexOf(".") - 1);
                    discrimantsPlantsStr = discrimantsPlantsStr.Substring(0, discrimantsPlantsStr.IndexOf("PDCUM pouvoirs discriminants cumulés"));
                    var discrimantsPlantsLines = GetPlantsLines(discrimantsPlantsStr, 42);
                    _logger.LogDebug($"Found { discrimantsPlantsLines.Count() } discrimants plants");
                    foreach (var discrimantPlantStr in discrimantsPlantsLines)
                    {
                        int? pdCum = null;
                        int? fid = null;
                        int? frq = null;

                        string pdCumStr = discrimantPlantStr.Substring(0, 4);
                        if (!String.IsNullOrEmpty(pdCumStr.Trim()))
                        {
                            float pdCumDec = float.Parse(pdCumStr, CultureInfo.InvariantCulture);
                            pdCum = (int)(100 * pdCumDec);
                        }

                        string sophyName = discrimantPlantStr.Substring(5, 27).Trim().Replace("+", "&");

                        if (!discrimantPlantStr.EndsWith("S DISNANTE")) 
                        {
                            string fidStr = discrimantPlantStr.Substring(33, 4);
                            if (!String.IsNullOrEmpty(fidStr.Trim()))
                            {
                                float fidDec = float.Parse(fidStr, CultureInfo.InvariantCulture);
                                fid = (int)(100 * fidDec);
                            }

                            string frqStr = discrimantPlantStr.Substring(38);
                            if (!String.IsNullOrEmpty(frqStr.Trim()))
                            {
                                frq = Int32.Parse(frqStr);
                            }
                        }

                        var discriminatingPlant = new SophyDiscriminatingPlants(sophy, sophyName, pdCum, fid, frq);
                        sophy.discrimantsPlants.Add(discriminatingPlant);
                    }
                    #endregion
                    return sophy;
                } 
                catch (Exception ex)
                {
                    throw new PlantsApiException(ex, ExceptionsIdFromPlantsApi.FailedToImportData, 
                        "Failed to import dato from Tela Botanico on {0} {1}", sophy.code, sophy.sophyName);
                }
            }
        }
        #endregion

        #region Internal 
        public XmlDocument DeserializeResultAsXml(HttpResponseMessage response, Func<string, string> format = null) 
        {
            var result = response.Content.ReadAsStringAsync().Result;
            if (format != null) 
            {
                result = format(result);
            }
            var xml = new XmlDocument();
            xml.LoadXml(result);
            return xml;
        }

        private string RemoveBalises(string html, params string[] balises)
        {
            string list = string.Join("|", balises);
            string pattern = "</?(" + list + ")[^>]*>";
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            html = regex.Replace(html, "");
            return html;
        }

        private IEnumerable<string> Chunk(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }

        private IEnumerable<string> GetPlantsLines(string source, int size)
        {
            List<string> lines = new List<string>();
            using (StringReader reader = new StringReader(source))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    // Do something with the line
                    if (line.Length < size)
                    {
                        lines.Add(line.PadRight(size));
                    } 
                    else if (line.Length == size)
                    {
                        lines.Add(line);
                    }
                    else 
                    {
                        var innerLines = Chunk(line, size);
                        foreach (var innerLine in innerLines)
                        {
                            lines.Add(innerLine);
                        }
                    }
                }
            }
            return lines;
        }
        #endregion
        #endregion

        #region Local
        public Sophy Get(int code)
        {
            var data = _sophyRepository.Get(s => s.code == code);
            return data;
        }

        public async Task<int> RemoveAllSophyData()
        {
            _plantsRepository.BackupCodesSophy();

            _sophyRepository.Delete(s => true);
            int count = await _sophyRepository.CommitAsync();
            return count;
        }
        public int Count()
        {
            return _sophyRepository.Count();
        }

        public async Task<int> Add(IEnumerable<Sophy> sophyList)
        {
            foreach (var sophy in sophyList)
            {
                _sophyRepository.Add(sophy);
            }
            int count = await _sophyRepository.CommitAsync();
            
            _plantsRepository.RestoreCodesSophy();

            await SetSophyRelations();
            return count;
        }

        public async Task SetSophyRelations()
        {
            await _sophyRepository.SetCodesDiscriminantPlant();
            await _sophyRepository.SetCodesSimilarPlant();
        }
        #endregion
    }
}