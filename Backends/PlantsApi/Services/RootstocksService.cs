using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Services;
using System.Linq;
using PermaGuildes.Backends.PlantsApi.Models;
using System.Collections.Generic;
using PermaGuildes.Backends.PlantsApi.DatabaseRepositories;
using System.Linq.Expressions;
using System;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public class RootstocksService : Relations1NServiceWithCrud<Plant, Rootstock, Rootstock>, IRootstocksService
    {
        #region Initialization
        public RootstocksService(ILogger<RootstocksService> logger, 
            IRootstocksRepository RootstocksRepository,
            IRealtimeEntitiesHubService realtimeEntitiesHubService)
             : base(logger, RootstocksRepository, realtimeEntitiesHubService)
        {
        }
        #endregion

        #region Override abstracts methods
        protected override Expression<Func<Rootstock, bool>> GetRelationByKeyPredicate(string relationKey, string referenceKey = null)
        {
            int relationKeyAsInt = int.Parse(relationKey);
            int? referenceKeyAsInt = null;
            if (!string.IsNullOrEmpty(referenceKey)) 
            {
                referenceKeyAsInt = int.Parse(referenceKey);
            }
            if (referenceKey == null) {
                return ((Rootstock r) => r.code == relationKeyAsInt);
            } else {
                return ((Rootstock r) => r.code == relationKeyAsInt && r.codePlant == referenceKeyAsInt);
            }
        }
        protected override Expression<Func<Rootstock, bool>> GetRelationsByReferenceKeyPredicate(string referenceKey)
        {
            int referenceKeyAsInt = int.Parse(referenceKey);
            return ((Rootstock r) => r.codePlant == referenceKeyAsInt);
        }

        override protected Expression<Func<Rootstock, bool>> GetRelationsByKeysPredicate(List<string> referenceKeys, List<string> relation1NKeys) 
        {
            List<int> relation1NKeysAsInt = relation1NKeys.Select(k => int.Parse(k)).ToList();
            return ((Rootstock r) => relation1NKeysAsInt.Contains(r.code));
        }

        protected override void SetRelation(string referenceKey, Rootstock relation) 
        { 
            int referenceKeyAsInt = int.Parse(referenceKey);
            relation.codePlant = referenceKeyAsInt;
        }

        protected override IEnumerable<Rootstock> GetDefaultOrderBy(IEnumerable<Rootstock> source)
        {
            return source.OrderBy(r => r.name);
        }
        #endregion
    }
}