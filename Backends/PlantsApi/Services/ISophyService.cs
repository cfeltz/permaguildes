using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public interface ISophyService : IServiceExtensionBase
    {
        Task<List<Sophy>> GetListFromTelaBotanica();
        Task<Sophy> GetSophyDataFromTelaBotanica(int code);
        Task<Sophy> GetSophyDataFromTelaBotanica(Sophy sophy);

        Task<int> RemoveAllSophyData();
        int Count();
        Task<int> Add(IEnumerable<Sophy> sophyList);
        Task SetSophyRelations();

        Sophy Get(int code);
        string getSophyUrlFromCode(int? code);
    }
}