using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public interface IRootstocksService : IRelations1NServiceWithCrud<Rootstock>
    {
    }
}