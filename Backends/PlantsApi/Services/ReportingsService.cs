using System;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.DatabaseRepositories;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public class ReportingsService : ServiceExtensionBase, IReportingsService
    {        
        private IReportingsRepository _reportingsRepository;

        public ReportingsService(ILogger<ReportingsService> logger, 
             IReportingsRepository reportingsRepository)
             : base(logger)
        {
            _reportingsRepository = reportingsRepository;
        }

        public Reporting Add(Reporting reporting)
        {
            _reportingsRepository.Add(reporting);
            _reportingsRepository.Commit();
            return reporting;
        }

        public void Delete(int code)
        {
            var reporting = _reportingsRepository.Get(r => r.code == code);
            _reportingsRepository.Delete(reporting);
            _reportingsRepository.Commit();
        }

        public List<Reporting> GetAll()
        {
            return _reportingsRepository.GetAll().ToList();
        }

        public Reporting Get(int code)
        {
            return _reportingsRepository.Get(r => r.code == code);
        }
    }
}