using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.IO.Compression;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.DatabaseRepositories;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public class CommonService : ServiceExtensionBase, ICommonService
    {        
        private IInsectsRepository _insectsRepository;
        private IVegetationTypeRepository _vegetationTypeRepository;
        private IPlantFamiliesRepository _plantFamiliesRepository;
        private IPlantStratumsRepository _plantStratumsRepository;
        private ISexualCharacteristicsRepository _sexualCharacteristicsRepository;
        #region Initialization        
        public CommonService(ILogger<CommonService> logger, 
            IInsectsRepository insectsRepository, 
            IVegetationTypeRepository vegetationTypeRepository, 
            IPlantFamiliesRepository plantFamiliesRepository, 
            IPlantStratumsRepository plantStratumsRepository, 
            ISexualCharacteristicsRepository sexualCharacteristicsRepository)
             : base(logger)
        {
            _insectsRepository = insectsRepository;
            _vegetationTypeRepository = vegetationTypeRepository;
            _plantFamiliesRepository = plantFamiliesRepository;
            _plantStratumsRepository = plantStratumsRepository;
            _sexualCharacteristicsRepository = sexualCharacteristicsRepository;
        }
        #endregion

        #region Get referetiel datas
        public IQueryable<Insect> GetInsects()
        {
            var datas = _insectsRepository.GetAll();
            return datas;
        }
        public IQueryable<PlantFamily> GetPlantFamilies()
        {
            var datas = _plantFamiliesRepository.GetAll().OrderBy(f => f.libelle);
            return datas;
        }
        public IQueryable<PlantStratum> GetPlantStratums()
        {
            var datas = _plantStratumsRepository.GetAll();
            return datas;
        }
        public IQueryable<VegetationType> GetVegetationTypes()
        {
            var datas = _vegetationTypeRepository.GetAll();
            return datas;
        }
        public IQueryable<SexualCharacteristic> GetSexualCharacteristics()
        {
            var datas = _sexualCharacteristicsRepository.GetAll();
            return datas;
        }
        #endregion
    }
}