using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.Services
{
    public interface IReportingsService : IServiceExtensionBase
    {
        List<Reporting> GetAll();
        Reporting Get(int code);
        Reporting Add(Reporting reporting);
        void Delete(int code);
    }
}