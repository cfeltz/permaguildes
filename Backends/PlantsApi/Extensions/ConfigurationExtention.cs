using Microsoft.Extensions.Configuration;

namespace PermaGuildes.Backends.PlantsApi.Extensions
{
    public static class ConfigurationExtention
    {
        public static string GetJwtSecureKey(this IConfiguration config) {
            return config["JwtSecureKey"];
        }

        public static string GetAuthBaseUrl(this IConfiguration config) {
            return config["AuthBaseUrl"];
        }
    }
}