using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class InsectsRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, Insect>, IInsectsRepository
    {        
        public InsectsRepository(PlantsApiContext dbContext, ILogger<InsectsRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }