using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public interface IPlantsRepository : IDatabaseRepositoryExtensionBase<Plant>
    {
        Task<Plant> LoadPlant(int codePlant);
        Task<CompanionsPlantLists> LoadPlantCompanionsLists(int codePlant);
        int BackupCodesSophy();
        int RestoreCodesSophy();
    }
}