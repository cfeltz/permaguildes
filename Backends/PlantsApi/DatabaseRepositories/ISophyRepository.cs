using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public interface ISophyRepository : IDatabaseRepositoryExtensionBase<Sophy>
    {
        Task SetCodesDiscriminantPlant();
        Task SetCodesSimilarPlant();
    }
}