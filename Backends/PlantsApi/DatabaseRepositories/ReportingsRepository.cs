using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class ReportingsRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, Reporting>, IReportingsRepository
    {        
        public ReportingsRepository(PlantsApiContext dbContext, ILogger<ReportingsRepository> logger)
             : base(dbContext, logger)
        {
            
        }
    }
 }