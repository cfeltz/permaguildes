using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public interface IInsectsRepository : IDatabaseRepositoryExtensionBase<Insect>
    {
        
    }
}