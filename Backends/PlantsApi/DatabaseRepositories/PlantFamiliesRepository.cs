using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class PlantFamiliesRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, PlantFamily>, IPlantFamiliesRepository
    {        
        public PlantFamiliesRepository(PlantsApiContext dbContext, ILogger<PlantFamiliesRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }