using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class SophyRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, Sophy>, ISophyRepository
    {        
        public SophyRepository(PlantsApiContext dbContext, ILogger<SophyRepository> logger)
             : base(dbContext, logger)
        {
            
        }

        public async Task SetCodesDiscriminantPlant()
        {
            await _dbContext.Database.ExecuteSqlRawAsync("UPDATE SophyDiscriminatingPlants AS D" + 
                    " INNER JOIN Sophy AS S ON S.sophyName = D.sophyNameDiscriminantPlant" + 
                    " SET D.codeDiscriminantPlant = S.code");

        }

        public async Task SetCodesSimilarPlant()
        {
            await _dbContext.Database.ExecuteSqlRawAsync("UPDATE SophySimilarPlants AS D" + 
                    " INNER JOIN Sophy AS S ON S.sophyName = D.sophyNameSimilarPlant" + 
                    " SET D.codeSimilarPlant = S.code");

        }
    }
 }