using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class PlantStratumsRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, PlantStratum>, IPlantStratumsRepository
    {        
        public PlantStratumsRepository(PlantsApiContext dbContext, ILogger<PlantStratumsRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }