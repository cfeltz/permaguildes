using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class RootstocksRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, Rootstock>, IRootstocksRepository
    {        
        public RootstocksRepository(PlantsApiContext dbContext, ILogger<RootstocksRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }