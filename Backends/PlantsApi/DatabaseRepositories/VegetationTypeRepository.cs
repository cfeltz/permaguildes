using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class VegetationTypeRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, VegetationType>, IVegetationTypeRepository
    {        
        public VegetationTypeRepository(PlantsApiContext dbContext, ILogger<VegetationTypeRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }