using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class SexualCharacteristicsRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, SexualCharacteristic>, ISexualCharacteristicsRepository
    {        
        public SexualCharacteristicsRepository(PlantsApiContext dbContext, ILogger<SexualCharacteristicsRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }