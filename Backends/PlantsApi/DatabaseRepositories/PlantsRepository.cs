using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class PlantsRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, Plant>, IPlantsRepository
    {        
        public PlantsRepository(PlantsApiContext dbContext, ILogger<PlantsRepository> logger)
             : base(dbContext, logger)
        {
            
        }

        public int BackupCodesSophy()
        {
            int count = _dbContext.Plants.Where(p => p.codeSophy != null)
                .UpdateFromQuery(p => new Plant() { codeSophyBackup = p.codeSophy, codeSophy = null });
            _logger.LogInformation($"Backup { count } codeSophy");
            return count;
        }
        public int RestoreCodesSophy() 
        {
            var codesSophyList = _dbContext.Sophy.Select(s => s.code).ToList();
            int count = _dbContext.Plants
                .Where(p => p.codeSophyBackup != null && codesSophyList.Contains((int)p.codeSophyBackup))
                .UpdateFromQuery(p => new Plant() { codeSophy = p.codeSophyBackup });
            _logger.LogInformation($"Restore { count } codeSophy");
            return count;
        }

        public async Task<Plant> LoadPlant(int codePlant)
        {
            var req = _dbContext.Plants.Where(p => p.code == codePlant)
                .Include(p => p.vegetationTypes)
                .Include(p => p.auxiliaries)
                .Include(p => p.pests);

            Plant plant = await req.FirstOrDefaultAsync();
            return plant;
        }

        public async Task<CompanionsPlantLists> LoadPlantCompanionsLists(int codePlant)
        {
            var req = _dbContext.Plants.Where(p => p.code == codePlant)
                .Include(p => p.associationsDesc).ThenInclude(a => a.plant1)
                .Include(p => p.associationsDesc).ThenInclude(a => a.plant2)
                .Include(p => p.associationsAsc).ThenInclude(a => a.plant1)
                .Include(p => p.associationsAsc).ThenInclude(a => a.plant2)
                .Include(p => p.sophy).ThenInclude(s => s.discrimantsPlants)
                    .ThenInclude(s => s.discriminantPlant).ThenInclude(d => d.plant)
                .Include(p => p.sophy).ThenInclude(s => s.similarsPlants)
                    .ThenInclude(s => s.similarPlant).ThenInclude(d => d.plant)
                .Include(p => p.auxiliaries)
                .Include(p => p.pests)
                .Select(p => new CompanionsPlantLists(p.code, p.associationsAsc, p.associationsDesc, p.sophy));
            CompanionsPlantLists result = await req.FirstOrDefaultAsync();
            return result;
        }
    }
 }