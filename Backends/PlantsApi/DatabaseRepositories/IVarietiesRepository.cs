using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public interface IVarietiesRepository : IDatabaseRepositoryExtensionBase<Variety>
    {
        
    }
}