using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models;

namespace PermaGuildes.Backends.PlantsApi.DatabaseRepositories
{
    public class VarietiesRepository : DatabaseRepositoryExtensionBase<PlantsApiContext, Variety>, IVarietiesRepository
    {        
        public VarietiesRepository(PlantsApiContext dbContext, ILogger<VarietiesRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }