using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.PlantsApi.Services;
using Microsoft.AspNetCore.Authorization;
using System;

namespace PermaGuildes.Backends.PlantsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlantsController : ControllerExtensionWithGettersBase<Plant, Plant>
    {
        ICommonService _commonService;
        PlantsApiContext _context;
        private IPlantsService GetProjectsService() => (IPlantsService)GetBaseEntitiesService();
        public PlantsController(ILogger<PlantsController> logger, PlantsApiContext context, 
            ICommonService commonService, 
            IPlantsService plantsService)
         : base(logger, plantsService)
        {
            _commonService = commonService;
            _context = context;
        }

        // GET: api/Plants/GetDefaultIconSrc/{codeStratum}
        [HttpGet]
        [Route("GetDefaultIconSrc/{codeStratum}")]
        public ActionResult<ITextApiMessage> GetDefaultIconSrc([FromRoute] int codeStratum)
        {
            string iconSrc = Plant.getIconSrc(codeStratum, 1);
            var returnValue = new TextApiMessage(iconSrc);
            return Ok(returnValue);
        }



        // GET: api/Plants/GetPlants
        [HttpGet]
        [Route("GetPlants")]
        public async Task<ActionResult<ICollection<Plant>>> GetPlants(int? limit = null)
        {
            var request = _context.Plants.OrderBy(p => p.name == null || p.name == string.Empty).ThenBy(p => p.name).ThenBy(p => p.latinNames);
            if (limit == null) {
                return await request.Select(p => p.Lightening()).ToListAsync();
            } 
            else 
            {
                return await request.Take((int)limit).Select(p => p.Lightening()).ToListAsync();
            }
        }

        // GET: api/Plants/GetPlantsNames
        [HttpGet]
        [Route("GetPlantsNames")]
        public async Task<ActionResult<ICollection<KeyValuePair<int, string>>>> GetPlantsNames()
        {
            return await _context.Plants.Select(p => new KeyValuePair<int, string>(p.code, p.name)).ToListAsync();
        }

        // GET: api/Plants/LoadPlant
        [HttpGet]
        [Route("LoadPlant")]
        public async Task<ActionResult<Plant>> LoadPlant(int codePlant)
        {
            Plant plant = await GetProjectsService().LoadPlant(codePlant);
            return plant;
        }

        [HttpGet]
        [Route("LoadPlantCompanions")]
        public async Task<ActionResult<List<CompanionPlant>>> LoadPlantCompanions(int codePlant)
        {
            var result = await GetProjectsService().LoadPlantCompanions(codePlant);
            return result;
        }

        // GET: api/Plants/AssociatePlants
        [HttpPost]
        [Authorize]
        [Route("AssociatePlants")]
        public async Task<ActionResult> AssociatePlants([FromBody] System.Json.JsonObject data)
        {
            return await CatchDatabaseViolationAsync(async () => {
                int codePlant1 = (int)data["codePlant1"];
                int codePlant2 = (int)data["codePlant2"];
                var plants = await _context.Plants.Where(p => p.code == codePlant1 || p.code == codePlant2).ToListAsync();
                plants[0].AddAssociationWithPlant(plants[1]);
                SaveContext();
                return Ok();
            });
        }

        // GET: api/Plants/RemovePlantsAssociation
        [HttpPost]
        [Authorize]
        [Route("RemovePlantsAssociation")]
        public async Task<ActionResult> RemovePlantsAssociation([FromBody] System.Json.JsonObject data)
        {
            return await CatchDatabaseViolationAsync(async () => {
                int codePlant1 = (int)data["codePlant1"];
                int codePlant2 = (int)data["codePlant2"];
                var plants = await _context.Plants.Where(p => p.code == codePlant1 || p.code == codePlant2)                
                    .Include(p => p.associationsAsc)
                    .Include(p => p.associationsDesc)
                    .ToListAsync();
                plants[0].RemoveAssociationWithPlant(plants[1]);
                SaveContext();
                return Ok();
            });
        }

        [HttpPost]
        [Authorize]
        [Route("CreatePlant")]
        public async Task<ActionResult<Plant>> CreatePlant([FromBody] Newtonsoft.Json.Linq.JObject jsonPlant)
        {
            int code = jsonPlant.Value<int>("code");
            if (code != 0) 
            {
                throw new LocalException(ExceptionsIdFromWebApiHelper.DuplicateData, $"Unable to create plant with not null code");
            }

            Plant localPlant = new Plant();
            // Copy attributes
            localPlant.CopyAttributes(jsonPlant);
            // Copy relations
            CopyPlantInsects(jsonPlant, localPlant);
            CopyPlantVegetationTypes(jsonPlant, localPlant);
            // Check mandatory fields
            checkMandatoryFields(localPlant);
            // Check codeSophy value
            checkCodeSophy(localPlant);
            // Add plant
            await GetProjectsService().Create(localPlant);
            SaveContext();
            return CreatedAtAction(nameof(LoadPlant), new { codePlant = localPlant.code }, localPlant);
        }
    
        [HttpPost]
        [Authorize]
        [Route("UpdatePlant")]
        public async Task<ActionResult<Plant>> UpdatePlant([FromBody] Newtonsoft.Json.Linq.JObject jsonPlant)
        {
            int code = jsonPlant.Value<int>("code");
            Plant localPlant = await GetProjectsService().LoadPlant(code);
            if (localPlant == null) 
            {
                throw new LocalException(ExceptionsIdFromWebApiHelper.DataNotFound, $"Plant with code {code} not found");
            }

            // Copy attributes
            localPlant.CopyAttributes(jsonPlant);
            // Copy relations
            CopyPlantInsects(jsonPlant, localPlant);
            // Check mandatory fields
            checkMandatoryFields(localPlant);
            // Check codeSophy value
            checkCodeSophy(localPlant);
            SaveContext();
            return localPlant;
        }

        private void checkMandatoryFields(Plant localPlant) 
        {
            if (localPlant.codeFamily == 0) 
            {
                throw new LocalException(ExceptionsIdFromWebApiHelper.MissingParameter, $"Veuillez renseigner la famille");
            }
            if (localPlant.codeStratum == 0) 
            {
                throw new LocalException(ExceptionsIdFromWebApiHelper.MissingParameter, $"Veuillez renseigner la strate");
            }
        }

        private void checkCodeSophy(Plant localPlant) 
        {
            if (localPlant.codeSophy != null) 
            {
                Plant plant = GetProjectsService().GetPlantFromCodeSophy((int)localPlant.codeSophy);
                if (plant != null && plant.code != localPlant.code) 
                {
                    throw new LocalException(ExceptionsIdFromWebApiHelper.DuplicateData, $"Le plant { plant.name } ({ plant.latinNames }) référence déjà le code sophy { localPlant.codeSophy }");
                }
            }
        }

        private void CopyPlantInsects(Newtonsoft.Json.Linq.JObject jsonPlant, Plant localPlant)
        {
            if (jsonPlant.ContainsKey("codesAuxiliariesInsects")) {
                var auxiliaries = jsonPlant.GetValue("codesAuxiliariesInsects").ToObject<List<int>>();
                if (!auxiliaries.SequenceEqual(localPlant.auxiliaries.Select(i => i.codeInsect)))
                {
                    var insects = _commonService.GetInsects().Where(i => auxiliaries.Contains(i.code)).ToList();
                    localPlant.UpdateAuxiliaries(insects);
                }
            }
            if (jsonPlant.ContainsKey("codesRepellentsInsects")) {
                var pests = jsonPlant.GetValue("codesRepellentsInsects").ToObject<List<int>>();
                if (!pests.SequenceEqual(localPlant.pests.Select(i => i.codeInsect)))
                {
                    var insects = _commonService.GetInsects().Where(i => pests.Contains(i.code)).ToList();
                    localPlant.UpdatePests(insects);
                }
            }
        }

        private void CopyPlantVegetationTypes(Newtonsoft.Json.Linq.JObject jsonPlant, Plant localPlant)
        {
            if (jsonPlant.ContainsKey("codesVegetationTypes")) {
                var vegetationTypes = jsonPlant.GetValue("codesVegetationTypes").ToObject<List<int>>();
                if (!vegetationTypes.SequenceEqual(localPlant.vegetationTypes.Select(t => t.codeVegetationType)))
                {
                    var types = _commonService.GetVegetationTypes().Where(t => vegetationTypes.Contains(t.code)).ToList();
                    localPlant.UpdateVegetationTypes(types);
                }
            }
        }

        protected async Task<ActionResult> CatchDatabaseViolationAsync(Func<Task<OkResult>> p)
        {
            try 
            {
                return await p();
            }
            catch(LocalException ex) 
            {
                switch(ex.id)
                {
                    case (int)ExceptionsIdFromWebApiHelper.DataNotFound: 
                        _logger.LogWarning(ex, "Attempt to access to not found data");
                        break;
                    case (int)ExceptionsIdFromWebApiHelper.DuplicateData: 
                        _logger.LogWarning(ex, "Attempt to duplicate data");
                        break;
                    default:
                        throw;
                }
                return Ok();
            }
        }

        protected void SaveContext(bool authorizeDuplicateData = false)
        {
            try 
            {
                _context.SaveChanges();
            }
            catch(LocalException ex) 
            {
                switch(ex.id)
                {
                    case (int)ExceptionsIdFromWebApiHelper.DuplicateData: 
                        _logger.LogWarning("Attemp to duplicate data");
                        break;
                    default:
                        throw;
                }
            }
        }
    }
}
