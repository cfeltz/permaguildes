using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Services;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper;
using PermaGuildes.Backends.ProjectsApi.SwaggerExamples;
using Microsoft.AspNetCore.Authorization;

namespace CFeltz.Backends.PlantsApi.Controllers
{
    [Route("api/plants/{referenceKey}/[controller]")]
    [ApiController]
    public class VarietiesController : ControllerExtensionWithRelation1NCrudBase<Plant, Variety, Variety>
    {
        private IVarietiesService GetVarietiesService() => (IVarietiesService)GetBaseRelationsServiceWithCrud();
        
        public VarietiesController(ILogger<VarietiesController> logger, 
        IVarietiesService VarietiesService) : base(logger, VarietiesService)
        {
        }

        #region CRUD
        // POST: api/plants/{key}/varieties
        [HttpPost("")]
        [Authorize]
        override public async Task<ActionResult<Variety>> AddRelation([FromRoute] string referenceKey, [FromBody] Variety relation)
        {
            return await base.AddRelation(referenceKey, relation);
        }

        [HttpPost("{relation1NKey}")]
        [Authorize]
        override public async Task<ActionResult<Variety>> UpdateRelation([FromRoute] string referenceKey, [FromRoute] string relation1NKey, 
            [FromBody] Variety relation)
        {
            return await base.UpdateRelation(referenceKey, relation1NKey, relation);
        }

        [HttpDelete("{relation1NKey}")]
        [Authorize]
        override public async Task<ActionResult<Variety>> DeleteRelation([FromRoute] string referenceKey, [FromRoute] string relation1NKey)
        {
            return await base.DeleteRelation(referenceKey, relation1NKey);
        }
        #endregion
    }
}