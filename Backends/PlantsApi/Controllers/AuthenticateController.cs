using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PermaGuildes.Backends.PlantsApi.Models;
using CFeltz.Shared.WebApiHelper.Controllers;
using CFeltz.Shared.WebApiHelper.Services;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper.Models;
using PermaGuildes.Backends.PlantsApi.Services;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace PermaGuildes.Backends.PlantsApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AuthenticateController : ControllerExtensionBase
    {
        ICommonService _commonService;
        private readonly IConfiguration _config;
        private readonly IJwtService _jwtService;
        private readonly UserManager<ApplicationUser> _userManager;

        public AuthenticateController(ILogger<AuthenticateController> logger, 
            IConfiguration config, 
            UserManager<ApplicationUser> userManager, ICommonService commonService,
            IJwtService jwtService) : base(logger)
        {
            this._config = config;
            this._userManager = userManager;
            this._jwtService = jwtService;
            _commonService = commonService;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<IApiMessage>> Login([FromBody] System.Json.JsonObject data)
        {
            string login = (string)data["login"];
            string password = (string)data["password"];

            var user = await _userManager.FindByNameAsync(login);
            if(user != null && await _userManager.CheckPasswordAsync(user, password))
            {
                var token = _jwtService.CreateJwtSecurityToken(user.UserName);
                var refreshToken = _jwtService.GenerateRefreshToken(login);

                string resultToken = new JwtSecurityTokenHandler().WriteToken(token);
                var wToken = new JwtToken() {
                    token = resultToken,
                    refreshToken = refreshToken.refreshToken,
                    expiresAt = token.ValidTo,
                    login = user.UserName
                };
                return Ok(wToken);
            }
            else 
            {
                var returnValue = new ApiMessage();
                returnValue.errorMessage = "Login failed";
                return Ok(returnValue);
            }
        }

        [AllowAnonymous]
        [HttpPost("refresh")]
        public IActionResult RefreshToken([FromBody] System.Json.JsonObject data)
        {
            string refreshToken = (string)data["refreshToken"];
            var localRefreshToken = _jwtService.GetRefreshToken(refreshToken);
            if (localRefreshToken == null)
            {
                return NotFound("Refresh token not found");
            }
            var token = _jwtService.CreateJwtSecurityToken(localRefreshToken.login);
            _jwtService.RemoveRefreshToken(localRefreshToken.refreshToken);
            var newRefreshToken = _jwtService.GenerateRefreshToken(localRefreshToken.login);

            string resultToken = new JwtSecurityTokenHandler().WriteToken(token);
            var wToken = new JwtToken() {
                token = resultToken,
                refreshToken = newRefreshToken.refreshToken,
                expiresAt = token.ValidTo,
                login = localRefreshToken.login
            };
            return Ok(wToken);
        }

        [HttpPost]
        [Route("logout")]
        public IActionResult Logout()
        {
            return Ok();
        }
    
        
        [HttpGet]
        [Route("TestAuthorization")]
        public IActionResult TestAuthorization()
        {
            return Ok(true);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Test")]
        public async Task<ActionResult<ICollection<PlantFamily>>> Test()
        {
            return await _commonService.GetPlantFamilies().ToListAsync();
        }
    }
}