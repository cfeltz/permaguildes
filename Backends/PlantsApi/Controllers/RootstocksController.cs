using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Services;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper;
using Microsoft.AspNetCore.Authorization;

namespace CFeltz.Backends.PlantsApi.Controllers
{
    [Route("api/plants/{referenceKey}/[controller]")]
    [ApiController]
    public class RootstocksController : ControllerExtensionWithRelation1NCrudBase<Plant, Rootstock, Rootstock>
    {
        private IRootstocksService GetRootstocksService() => (IRootstocksService)GetBaseRelationsServiceWithCrud();
        
        public RootstocksController(ILogger<RootstocksController> logger, 
        IRootstocksService RootstocksService) : base(logger, RootstocksService)
        {
        }

        #region Add authentification to CRUD
        #region CRUD
        // POST: api/plants/{key}/varieties
        [HttpPost("")]
        [Authorize]
        override public async Task<ActionResult<Rootstock>> AddRelation([FromRoute] string referenceKey, [FromBody] Rootstock relation)
        {
            return await base.AddRelation(referenceKey, relation);
        }

        [HttpPost("{relation1NKey}")]
        [Authorize]
        override public async Task<ActionResult<Rootstock>> UpdateRelation([FromRoute] string referenceKey, [FromRoute] string relation1NKey, 
            [FromBody] Rootstock relation)
        {
            return await base.UpdateRelation(referenceKey, relation1NKey, relation);
        }

        [HttpDelete("{relation1NKey}")]
        [Authorize]
        override public async Task<ActionResult<Rootstock>> DeleteRelation([FromRoute] string referenceKey, [FromRoute] string relation1NKey)
        {
            return await base.DeleteRelation(referenceKey, relation1NKey);
        }
        #endregion
        #endregion
    }
}