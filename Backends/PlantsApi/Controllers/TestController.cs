using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PermaGuildes.Backends.PlantsApi.Database;
using CFeltz.Shared.WebApiHelper.Controllers;

namespace PermaGuildes.Backends.PlantsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerExtensionBase
    {
        public TestController(ILogger<TestController> logger)
         : base(logger)
        {
        }

        // GET: api/Test/GetException
        [HttpGet]
        [Route("GetException")]
        public ActionResult<string> GetException()
        {
            throw new NotImplementedException();
        }
    }
}
