using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.PlantsApi.Services;

namespace PermaGuildes.Backends.PlantsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerExtensionBase
    {
        ICommonService _commonService;

        public CommonController(ILogger<CommonController> logger, 
            ICommonService commonService)
         : base(logger)
        {
            _commonService = commonService;
        }

        // GET: api/Common/VegetationTypes
        [HttpGet]
        [Route("VegetationTypes")]
        public async Task<ActionResult<ICollection<VegetationType>>> GetVegetationTypes()
        {
            return await _commonService.GetVegetationTypes().ToListAsync();
        }

        // GET: api/Common/SexualCharacteristics
        [HttpGet]
        [Route("SexualCharacteristics")]
        public async Task<ActionResult<ICollection<SexualCharacteristic>>> GetSexualCharacteristics()
        {
            return await _commonService.GetSexualCharacteristics().ToListAsync();
        }

        // GET: api/Common/Insects
        [HttpGet]
        [Route("Insects")]
        public async Task<ActionResult<ICollection<Insect>>> GetInsects()
        {
            return await _commonService.GetInsects().ToListAsync();
        }

        // GET: api/Common/Stratums
        [HttpGet]
        [Route("Stratums")]
        public async Task<ActionResult<ICollection<PlantStratum>>> GetStratums()
        {
            return await _commonService.GetPlantStratums().ToListAsync();
        }

        // GET: api/Common/Families
        [HttpGet]
        [Route("Families")]
        public async Task<ActionResult<ICollection<PlantFamily>>> GetFamilies()
        {
            return await _commonService.GetPlantFamilies().ToListAsync();
        }
    }
}
