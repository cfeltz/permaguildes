using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper;
using PermaGuildes.Backends.PlantsApi.Database;
using PermaGuildes.Backends.PlantsApi.Models;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.PlantsApi.Services;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using Swashbuckle.AspNetCore.Filters;

namespace PermaGuildes.Backends.PlantsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportingsController : ControllerExtensionBase
    {
        IReportingsService _reportingsService;
        public ReportingsController(ILogger<PlantsController> logger, 
            IReportingsService reportingsService)
         : base(logger)
        {
            _reportingsService = reportingsService;
        }

        // GET: api/Reportings
        [HttpGet]
        [Route("")]
        public ActionResult<ICollection<Reporting>> GetAll()
        {
            return _reportingsService.GetAll();
        }

        // GET: api/Reportings/{code}
        [HttpGet]
        [Route("{code}")]
        public ActionResult<Reporting> Get([FromRoute]int code)
        {
            return _reportingsService.Get(code);
        }

        // GET: api/Reportings/{code}
        [HttpDelete]
        [Route("{code}")]
        public ActionResult<IApiMessage> Delete([FromRoute]int code)
        {
            _reportingsService.Delete(code);
            return new ApiMessage();
        }

        public class ReportingExample : IExamplesProvider<Newtonsoft.Json.Linq.JObject>
        {
            public Newtonsoft.Json.Linq.JObject GetExamples() 
            {
                var example = new Newtonsoft.Json.Linq.JObject();
                example.Add("name", "Swagger");
                example.Add("email", "Swagger@test.fr");
                example.Add("message", "Test message");
                example.Add("codePlant", 1);
                return example;
            }
        }        
        [HttpPost]
        [Route("")]
        [SwaggerRequestExample(typeof(Newtonsoft.Json.Linq.JObject), typeof(ReportingExample))]
        public ActionResult<Plant> NewReporting([FromBody] Newtonsoft.Json.Linq.JObject jsonReporting)
        {
            Reporting localReporting = new Reporting();
            // Copy attributes
            localReporting.CopyAttributes(jsonReporting);
            // Add plant
            _reportingsService.Add(localReporting);
            return CreatedAtAction(nameof(Get), new { code = localReporting.code }, localReporting);
        }

    }
}
