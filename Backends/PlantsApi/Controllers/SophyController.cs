using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PermaGuildes.Backends.PlantsApi.Models;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.PlantsApi.Services;
using Microsoft.AspNetCore.Authorization;
using CFeltz.Shared.WebApiHelper.Infrastructure;

namespace PermaGuildes.Backends.PlantsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SophyController : ControllerExtensionBase
    {
        ISophyService _sophyService;
        public SophyController(ILogger<SophyController> logger, 
            ISophyService sophyService)
         : base(logger)
        {
            this._sophyService = sophyService;
        }

        // GET: api/Sophy/GetFromTelaBotanica
        [HttpGet]
        [Route("GetFromTelaBotanica")]
        public async Task<ActionResult<ICollection<Sophy>>> GetFromTelaBotanica([FromQuery] bool withData = true)
        {
            var sophyList = await _sophyService.GetListFromTelaBotanica();
            if (withData)
            {
                foreach (var sophy in sophyList)
                {
                    await _sophyService.GetSophyDataFromTelaBotanica(sophy);
                }
            }
            return sophyList;
        }

        // GET: api/Sophy/GetSophyDataFromTelaBotanica
        [HttpGet]
        [Route("GetSophyDataFromTelaBotanica")]
        public async Task<ActionResult<Sophy>> GetSophyDataFromTelaBotanica(int code)
        {
            var sophy = await _sophyService.GetSophyDataFromTelaBotanica(code);
            return sophy;
        }

        // GET: api/Sophy/ImportFromTelaBotanica
        [HttpPost]
        [Authorize]
        [Route("ImportFromTelaBotanica")]
        public async Task<ActionResult<ITextApiMessage>> ImportFromTelaBotanica([FromQuery] bool clean = false, int? limit = null)
        {
            #region Prepare table Sophy
            if (clean) 
            {
                int removedData = await _sophyService.RemoveAllSophyData();
                _logger.LogInformation($"{ removedData } lines removed from table Sophy");
            }
            else if (_sophyService.Count() > 0)
            {
                throw new PlantsApiException(ExceptionsIdFromPlantsApi.NeedToForce, 
                    "Table Sophy is not empty, please force action with parameter clean = true");
            }
            #endregion

            #region Get data from Tela Botanica
            var sophyList = await _sophyService.GetListFromTelaBotanica();
            if (limit != null && limit > 0)
            {
                sophyList = sophyList.Take((int)limit).ToList();
            }
            foreach (var sophy in sophyList)
            {
                await _sophyService.GetSophyDataFromTelaBotanica(sophy);
            }
            #endregion

            #region Save in local repository
            int count = await _sophyService.Add(sophyList);
            #endregion
            return new TextApiMessage($"{ count } lines added");
        }

        // GET: api/Sophy/SetSophyRelations
        [HttpPost]
        [Authorize]
        [Route("SetSophyRelations")]
        public async Task<ActionResult<IApiMessage>> SetSophyRelations()
        {
            await _sophyService.SetSophyRelations();
            return new ApiMessage();
        }

        // GET: api/Sophy/{code}
        [HttpGet]
        [Route("{code}")]
        public ActionResult<Sophy> Get(int code)
        {
            Sophy sophy = _sophyService.Get(code);
            return sophy;
        }

    }
}
