using PermaGuildes.Backends.PlantsApi.Models;
using Swashbuckle.AspNetCore.Filters;

namespace PermaGuildes.Backends.ProjectsApi.SwaggerExamples
{
    public class VarietyExample : IExamplesProvider<Variety>
    {
        public Variety GetExamples() => new Variety(0, "Swagger example", "Variety create by swagger") {
        };
    }

    public class RootstockExample : IExamplesProvider<Rootstock>
    {
        public Rootstock GetExamples() => new Rootstock(0, "Swagger example", "Rootstock create by swagger") {
        };
    }

}