using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using PermaGuildes.Backends.PlantsApi.Database;
using CFeltz.Shared.WebApiHelper;
using Microsoft.Extensions.Logging;
using PermaGuildes.Backends.PlantsApi.Models;
using System;
using Microsoft.AspNetCore.Identity;
using PermaGuildes.Backends.PlantsApi.Extensions;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.PlantsApi.Services;
using PermaGuildes.Backends.PlantsApi.DatabaseRepositories;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Filters;
using System.Collections.Generic;
using CFeltz.Shared.WebApiHelper.SignalRHubs;

namespace PermaGuildes.Backends.PlantsApi
{
    public class Startup : CFeltz.Shared.WebApiHelper.StartUpWithDatabase<PlantsApiContext>
    {
        public Startup(ILogger<Startup> logger, IConfiguration configuration, IWebHostEnvironment hostingEnvironment) : 
            base(logger, configuration, hostingEnvironment)
        {
            AddSignalRHub<RealtimeEntitiesHub>("RealtimeEntitiesHub");
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public override void ConfigureServices(IServiceCollection services)
        {
            AddIdentityFromEntityFramework<ApplicationUser, ApplicationRole>(services);
            base.ConfigureServices(services);
            
            services.AddScoped<IInsectsRepository, InsectsRepository>();
            services.AddScoped<IPlantFamiliesRepository, PlantFamiliesRepository>();
            services.AddScoped<IPlantStratumsRepository, PlantStratumsRepository>();
            services.AddScoped<IVegetationTypeRepository, VegetationTypeRepository>();
            services.AddScoped<ISexualCharacteristicsRepository, SexualCharacteristicsRepository>();

            services.AddScoped<IPlantsRepository, PlantsRepository>();
            services.AddScoped<IVarietiesRepository, VarietiesRepository>();
            services.AddScoped<IRootstocksRepository, RootstocksRepository>();
            services.AddScoped<ISophyRepository, SophyRepository>();
            services.AddScoped<IReportingsRepository, ReportingsRepository>();

            services.AddScoped<ICommonService, CommonService>();
            services.AddScoped<IPlantsService, PlantsService>();
            services.AddScoped<IVarietiesService, VarietiesService>();
            services.AddScoped<IRootstocksService, RootstocksService>();
            services.AddScoped<ISophyService, SophyService>();
            services.AddScoped<IReportingsService, ReportingsService>();

            services.AddScoped<IHttpRequestService, HttpRequestService>();
            services.AddScoped<IRealtimeEntitiesHubService, RealtimeEntitiesHubService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.Configure(app, env);

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<PlantsApiContext>();
                var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>();
                var dbInitializer = new DbInitializer();
                dbInitializer.Initialize(context, userManager, roleManager);
            }
        }
    }
}
