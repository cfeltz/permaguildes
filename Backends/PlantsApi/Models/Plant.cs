using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using PermaGuildes.Backends.PlantsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.PlantsApi.Models
{
    [Table("Plants")]
    public class Plant : UpdateableEntity, RemovableEntity, EntityWithKey
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }
        [MaxLength(255)]
        public string name { get; set; } = String.Empty;

        [NotMapped]
        public bool lighten { get; set; } = false;

        #region Generics data
        [MaxLength(255)]
        public string latinNames { get; set; }
        public int water { get; set; }
        public int growth { get; set; }
        public int codeFamily { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public PlantFamily family { get; set; }
        public int codeStratum { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public PlantStratum stratum { get; set; }
        #endregion

        #nullable enable
        #region Nullable generics data
        public int? iconCode { get; set; }
        public int? edibilityRating { get; set; }
        public int? medicinalRating { get; set; }
        public int? codeSex { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public SexualCharacteristic sex { get; set; }
        #endregion

        #region Data with intervals
        public int? minUsdahardiness { get; set; }
        public int? maxUsdahardiness { get; set; }
        public int? minHeight { get; set; }
        public int? maxHeight { get; set; }
        public int? minSpan { get; set; }
        public int? maxSpan { get; set; }
        #endregion

        #region Periods
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public int? startFloweringPeriod1 { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public int? endFloweringPeriod1 { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public int? startFloweringPeriod2 { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public int? endFloweringPeriod2 { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public int? startHarvestPeriod1 { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public int? endHarvestPeriod1 { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public int? startHarvestPeriod2 { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public int? endHarvestPeriod2 { get; set; }
        #endregion 
        #nullable restore

        #region Exposure
        public bool exposureShadow { get; set; }
        public bool exposureMiddle { get; set; }
        public bool exposureSun { get; set; }
        #endregion 

        #region Characteristics
        public bool fertility { get; set; }
        public bool windbreaker { get; set; }
        public bool nitrate { get; set; }
        public bool othersMinerals { get; set; }        
        public int? othersMineralsRating { get; set; }
        public bool melliferous { get; set; }
        public int? melliferousRating { get; set; }
        public bool aromaticPlant { get; set; }
        #endregion 

        #region Description, Pfaf and Sophy data
        #nullable enable
        public int? codeSophy { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public int? codeSophyBackup { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Sophy sophy { get; set; }
        
        [MaxLength(2048)]
        public string? description { get; set; }

        [MaxLength(2048)]
        public string? pfafKnownHazards { get; set; } = String.Empty;
        [MaxLength(2048)]
        public string? pfafDescription { get; set; } = String.Empty;
        [MaxLength(256)]
        public string? pfafUrl { get; set; } = String.Empty;
        [MaxLength(256)]
        public string? externalLink { get; set; }  = String.Empty;
        [MaxLength(2048)]
        public string? remarks { get; set; } = String.Empty;
        [MaxLength(2048)]
        public string? infosToKnow { get; set; } = String.Empty;
        #nullable restore
        #endregion 

        #region Relations with insects
        public bool auxiliariesAttractive { get; private set; }
        public bool pestRepellents { get; private set; }

        [NotMapped]
        public ICollection<int> codesAuxiliariesInsects {
            get 
            {
                return auxiliaries.Select(v => v.codeInsect).ToList();
            }
        }

        [NotMapped]
        public ICollection<int> codesRepellentsInsects {
            get 
            {
                return pests.Select(v => v.codeInsect).ToList();
            }
        }

        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<PlantAuxiliary> auxiliaries { get; set; } = new List<PlantAuxiliary>();
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<PlantPest> pests { get; set; } = new List<PlantPest>();

        public void UpdateAuxiliaries(List<Insect> insects) 
        {
            var auxiliariesToRemove = this.auxiliaries.Select(a => a.insect).Except(insects).ToList();
            var insectsToAdd = insects.Except(this.auxiliaries.Select(a => a.insect)).ToList();

            foreach(var insect in auxiliariesToRemove)
            {
                RemoveAuxiliary(insect);
            }

            foreach(var insect in insectsToAdd)
            {
                AddAuxiliary(insect);
            }
        }

        public void AddAuxiliary(Insect insect) 
        {
            if (this.auxiliaries.Where(r => r.codeInsect == insect.code).Count() == 0)
            {
                var r = new PlantAuxiliary(this, insect);
                this.auxiliaries.Add(r);
                this.auxiliariesAttractive = true;
            }
        }

        public void RemoveAuxiliary(Insect insect) 
        {
            PlantAuxiliary r = this.auxiliaries.Where(i => i.codeInsect == insect.code).FirstOrDefault();
            this.auxiliaries.Remove(r);
        }

        public void UpdatePests(List<Insect> insects) 
        {
            var pestsToRemove = this.pests.Select(a => a.insect).Except(insects).ToList();
            var insectsToAdd = insects.Except(this.pests.Select(a => a.insect)).ToList();

            foreach(var insect in pestsToRemove)
            {
                RemovePest(insect);
            }

            foreach(var insect in insectsToAdd)
            {
                AddPest(insect);
            }
        }

        public void AddPest(Insect insect) 
        {
            if (this.pests.Where(r => r.codeInsect == insect.code).Count() == 0)
            {
                var r = new PlantPest(this, insect);
                this.pests.Add(r);
                this.pestRepellents = true;
            }
        }

        public void RemovePest(Insect insect) 
        {
            PlantPest r = this.pests.Where(i => i.codeInsect == insect.code).FirstOrDefault();
            this.pests.Remove(r);
        }
        #endregion

        #region Vegetation types
        [NotMapped]
        public ICollection<int> codesVegetationTypes {
            get 
            {
                return vegetationTypes.Select(v => v.codeVegetationType).ToList();
            }
        }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<PlantType> vegetationTypes { get; set; } = new List<PlantType>();

        public void UpdateVegetationTypes(List<VegetationType> types) 
        {
            var typesToRemove = this.vegetationTypes.Select(a => a.vegetationType).Except(types).ToList();
            var typesToAdd = types.Except(this.vegetationTypes.Select(a => a.vegetationType)).ToList();

            foreach(var type in typesToRemove)
            {
                RemoveVegetationType(type);
            }

            foreach(var type in typesToAdd)
            {
                AddVegetationType(type);
            }
        }

        public void AddVegetationType(VegetationType type) 
        {
            if (this.vegetationTypes.Where(r => r.codeVegetationType == type.code).Count() == 0)
            {
                var r = new PlantType(this, type);
                this.vegetationTypes.Add(r);
            }
        }

        public void RemoveVegetationType(VegetationType type) 
        {
            PlantType r = this.vegetationTypes.Where(t => t.codeVegetationType == type.code).FirstOrDefault();
            this.vegetationTypes.Remove(r);
        }
        #endregion

        #region Associations
        public ICollection<CompanionPlant> companions { 
            get; internal set;
        }

        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<Association> associations { 
            get 
            {
                return this.associationsAsc.Concat(this.associationsDesc).ToList();
            } 
        }

        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<Association> associationsAsc { get; set; } = new List<Association>();
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<Association> associationsDesc { get; set; } = new List<Association>();
        public void AddAssociationWithPlant(Plant plant) 
        {
            if (this.code == plant.code) 
            {
                throw new LocalException(ExceptionsIdFromWebApiHelper.DataRulesViolation, 
                    "Attempt to associate plant {0} to itself", this.name);
            }

            if (this.associations.Where(r => 
                Math.Min(r.codePlant1, r.codePlant2) == Math.Min(this.code, plant.code)
                && Math.Max(r.codePlant1, r.codePlant2) == Math.Max(this.code, plant.code)
                ).Count() == 0)
            {
                // Add association
                var r = new Association(this, plant);
                if (r.codePlant1 == this.code) 
                {
                    // this.code is smaller than plant.code
                    this.associationsDesc.Add(r);
                }
                else 
                {
                    this.associationsAsc.Add(r);
                }
            }
        }

        public void RemoveAssociationWithPlant(Plant plant) 
        {
            if (this.code == plant.code) 
            {
                throw new LocalException(ExceptionsIdFromWebApiHelper.DataRulesViolation, 
                    "Attempt to remove association of plant {0} to itself", this.name);
            }

            if (this.code < plant.code) 
            {
                var r = this.associationsDesc.Where(r => r.codePlant1 == this.code && r.codePlant2 == plant.code).FirstOrDefault();
                if (r == null)
                {
                    throw new LocalException(ExceptionsIdFromWebApiHelper.DataNotFound, 
                    "Association between {0} and {1} doesn't exist", this.name, plant.name);
                }
                this.associationsDesc.Remove(r);
            }
            else 
            {
                var r = this.associationsAsc.Where(r => r.codePlant2 == this.code && r.codePlant1 == plant.code).FirstOrDefault();
                if (r == null)
                {
                    throw new LocalException(ExceptionsIdFromWebApiHelper.DataNotFound, 
                    "Association between {0} and {1} doesn't exist", this.name, plant.name);
                }
                this.associationsAsc.Remove(r);
            }
        }
        #endregion

        #region Varieties and rootstocks
        public ICollection<Variety> varieties { get; set; }
        public ICollection<Rootstock> rootstocks { get; set; }
        #endregion

        #region Reportings
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<Reporting> reportings { get; set; }
        #endregion

        public DateTime? dtSuppression { get; set; }

        #region Computed fields
        static public string getIconSrc(int codeStratum, int? iconCode)
        {
            if (iconCode > 0)
            {
                if (codeStratum <= 3)
                {
                    return "T" + iconCode + ".png";
                }
                else if (codeStratum == 7)
                {
                    return "C" + iconCode + ".png";
                }
                else
                {
                    return "G" + iconCode + ".png";
                }
            }
            else
            {
                return null;
            }
        }

        [NotMapped]
        public string iconSrc { 
            get 
            {
                return getIconSrc(codeStratum, this.iconCode);
            }
        }

        [NotMapped]
        public List<int> floweringPeriods { 
            get 
            {
                List<int> result = new List<int>();
                if (startFloweringPeriod1 != null) result.Add((int)startFloweringPeriod1);
                if (endFloweringPeriod1 != null) result.Add((int)endFloweringPeriod1);
                if (startFloweringPeriod2 != null) result.Add((int)startFloweringPeriod2);
                if (endFloweringPeriod2 != null) result.Add((int)endFloweringPeriod2);
                return result;
            }
        }

        [NotMapped]
        public List<int> harvestPeriods { 
            get 
            {
                List<int> result = new List<int>();
                if (startHarvestPeriod1 != null) result.Add((int)startHarvestPeriod1);
                if (endHarvestPeriod1 != null) result.Add((int)endHarvestPeriod1);
                if (startHarvestPeriod2 != null) result.Add((int)startHarvestPeriod2);
                if (endHarvestPeriod2 != null) result.Add((int)endHarvestPeriod2);
                return result;
            }
        }
        #endregion

        internal Plant() { }

        public Plant(string name)
        {
            this.name = name;
        }

        public Plant(string name, string latinNames, int water, int growth)
        {
            this.name = name;
            this.latinNames = latinNames;
            this.water = water;
            this.growth = growth;
        }

        public Plant Lightening()
        {
            this.pfafKnownHazards = null;
            if (string.IsNullOrEmpty(this.description))
            {
                this.description = this.pfafDescription;
            }
            this.pfafDescription = null;
            this.pfafUrl = null;
            this.externalLink = null;
            this.remarks = null;
            this.infosToKnow = null;
            this.lighten = true;
            return this;
        }

        public void CopyAttributes(Newtonsoft.Json.Linq.JObject jsonPlant)
        {
            if (jsonPlant.ContainsKey("name")) {
                this.name = jsonPlant.Value<string>("name");
            }
            if (jsonPlant.ContainsKey("latinNames")) {
                this.latinNames = jsonPlant.Value<string>("latinNames");
            }
            if (jsonPlant.ContainsKey("water")) {
                this.water = jsonPlant.Value<int>("water");
            }
            if (jsonPlant.ContainsKey("growth")) {
                this.growth = jsonPlant.Value<int>("growth");
            }
            if (jsonPlant.ContainsKey("codeFamily")) {
                this.codeFamily = jsonPlant.Value<int>("codeFamily");
            }
            if (jsonPlant.ContainsKey("codeStratum")) {
                this.codeStratum = jsonPlant.Value<int>("codeStratum");
            }
            if (jsonPlant.ContainsKey("iconCode")) {
                this.iconCode = jsonPlant.Value<int?>("iconCode");
            }
            if (jsonPlant.ContainsKey("edibilityRating")) {
                this.edibilityRating = jsonPlant.Value<int?>("edibilityRating");
            }
            if (jsonPlant.ContainsKey("medicinalRating")) {
                this.medicinalRating = jsonPlant.Value<int?>("medicinalRating");
            }
            if (jsonPlant.ContainsKey("codeSex")) {
                this.codeSex = jsonPlant.Value<int?>("codeSex");
            }
            if (jsonPlant.ContainsKey("codeSophy")) {
                this.codeSophy = jsonPlant.Value<int?>("codeSophy");
            }
            if (jsonPlant.ContainsKey("minUsdahardiness")) {
                this.minUsdahardiness = jsonPlant.Value<int?>("minUsdahardiness");
            }
            if (jsonPlant.ContainsKey("maxUsdahardiness")) {
                this.maxUsdahardiness = jsonPlant.Value<int?>("maxUsdahardiness");
            }
            if (jsonPlant.ContainsKey("minHeight")) {
                this.minHeight = jsonPlant.Value<int?>("minHeight");
            }
            if (jsonPlant.ContainsKey("maxHeight")) {
                this.maxHeight = jsonPlant.Value<int?>("maxHeight");
            }
            if (jsonPlant.ContainsKey("minSpan")) {
                this.minSpan = jsonPlant.Value<int?>("minSpan");
            }
            if (jsonPlant.ContainsKey("maxSpan")) {
                this.maxSpan = jsonPlant.Value<int?>("maxSpan");
            }
            if (jsonPlant.ContainsKey("auxiliariesAttractive")) {
                this.auxiliariesAttractive = jsonPlant.Value<bool>("auxiliariesAttractive");
            }
            if (jsonPlant.ContainsKey("pestRepellents")) {
                this.pestRepellents = jsonPlant.Value<bool>("pestRepellents");
            }
            if (jsonPlant.ContainsKey("exposureShadow")) {
                this.exposureShadow = jsonPlant.Value<bool>("exposureShadow");
            }
            if (jsonPlant.ContainsKey("exposureMiddle")) {
                this.exposureMiddle = jsonPlant.Value<bool>("exposureMiddle");
            }
            if (jsonPlant.ContainsKey("exposureSun")) {
                this.exposureSun = jsonPlant.Value<bool>("exposureSun");
            }
            if (jsonPlant.ContainsKey("fertility")) {
                this.fertility = jsonPlant.Value<bool>("fertility");
            }
            if (jsonPlant.ContainsKey("windbreaker")) {
                this.windbreaker = jsonPlant.Value<bool>("windbreaker");
            }
            if (jsonPlant.ContainsKey("nitrate")) {
                this.nitrate = jsonPlant.Value<bool>("nitrate");
            }
            if (jsonPlant.ContainsKey("othersMinerals")) {
                this.othersMinerals = jsonPlant.Value<bool>("othersMinerals");
            }
            if (jsonPlant.ContainsKey("othersMineralsRating")) {
                this.othersMineralsRating = jsonPlant.Value<int?>("othersMineralsRating");
            }
            if (jsonPlant.ContainsKey("melliferous")) {
                this.melliferous = jsonPlant.Value<bool>("melliferous");
            }
            if (jsonPlant.ContainsKey("melliferousRating")) {
                this.melliferousRating = jsonPlant.Value<int?>("melliferousRating");
            }
            if (jsonPlant.ContainsKey("aromaticPlant")) {
                this.aromaticPlant = jsonPlant.Value<bool>("aromaticPlant");
            }
            if (jsonPlant.ContainsKey("description")) {
                if (jsonPlant.Value<string>("description") != this.pfafDescription) {
                    this.description = jsonPlant.Value<string>("description");
                }
            }
            if (jsonPlant.ContainsKey("pfafKnownHazards")) {
                this.pfafKnownHazards = jsonPlant.Value<string>("pfafKnownHazards");
            }
            if (jsonPlant.ContainsKey("pfafDescription")) {
                this.pfafDescription = jsonPlant.Value<string>("pfafDescription");
            }
            if (jsonPlant.ContainsKey("pfafUrl")) {
                this.pfafUrl = jsonPlant.Value<string>("pfafUrl");
            }
            if (jsonPlant.ContainsKey("externalLink")) {
                this.externalLink = jsonPlant.Value<string>("externalLink");
            }
            if (jsonPlant.ContainsKey("remarks")) {
                this.remarks = jsonPlant.Value<string>("remarks");
            }
            if (jsonPlant.ContainsKey("infosToKnow")) {
                this.infosToKnow = jsonPlant.Value<string>("infosToKnow");
            }
            if (jsonPlant.ContainsKey("floweringPeriods")) {
                var floweringPeriods = jsonPlant.SelectToken("floweringPeriods").ToObject<int[]>();
                if (floweringPeriods.Count() == 0) {
                    this.startFloweringPeriod1 = null;
                    this.endFloweringPeriod1 = null;
                    this.startFloweringPeriod2 = null;
                    this.endFloweringPeriod2 = null;
                } else {
                    this.startFloweringPeriod1 = floweringPeriods[0];
                    this.endFloweringPeriod1 = floweringPeriods[1];
                    if (floweringPeriods.Count() == 4) {
                        this.startFloweringPeriod2 = floweringPeriods[2];
                        this.endFloweringPeriod2 = floweringPeriods[3];
                    } else {
                        this.startFloweringPeriod2 = null;
                        this.endFloweringPeriod2 = null;
                    }
                }
            }
            if (jsonPlant.ContainsKey("harvestPeriods")) {
                var harvestPeriods = jsonPlant.SelectToken("harvestPeriods").ToObject<int[]>();
                if (harvestPeriods.Count() == 0) {
                    this.startHarvestPeriod1 = null;
                    this.endHarvestPeriod1 = null;
                    this.startHarvestPeriod2 = null;
                    this.endHarvestPeriod2 = null;
                } else {
                    this.startHarvestPeriod1 = harvestPeriods[0];
                    this.endHarvestPeriod1 = harvestPeriods[1];
                    if (harvestPeriods.Count() == 4) {
                        this.startHarvestPeriod2 = harvestPeriods[2];
                        this.endHarvestPeriod2 = harvestPeriods[3];
                    } else {
                        this.startHarvestPeriod2 = null;
                        this.endHarvestPeriod2 = null;
                    }
                }
            }
        }
    }
}