using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using PermaGuildes.Backends.PlantsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.PlantsApi.Models
{
    [Table("Reportings")]
    public class Reporting : UpdateableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }
        [MaxLength(255)]
        public string name { get; set; } = String.Empty;
        [MaxLength(255)]
        public string email { get; set; }

        [MaxLength(2048)]
        public string message { get; set; }
        public int? codePlant { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Plant plant { get; set; }

        internal Reporting() { }

        public Reporting(string name, string email, string message, int? codePlant)
        {
            this.name = name;
            this.email = email;
            this.message = message;
            this.codePlant = codePlant;
        }

        public void CopyAttributes(Newtonsoft.Json.Linq.JObject jsonReporting)
        {
            if (jsonReporting.ContainsKey("name")) {
                this.name = jsonReporting.Value<string>("name");
            }
            if (jsonReporting.ContainsKey("email")) {
                this.email = jsonReporting.Value<string>("email");
            }
            if (jsonReporting.ContainsKey("message")) {
                this.message = jsonReporting.Value<string>("message");
            }
            if (jsonReporting.ContainsKey("codePlant")) {
                this.codePlant = jsonReporting.Value<int>("codePlant");
            }
        }
   }
}