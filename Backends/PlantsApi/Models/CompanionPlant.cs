using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using PermaGuildes.Backends.PlantsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;

namespace PermaGuildes.Backends.PlantsApi.Models
{
    [NotMapped]
    public class CompanionPlant : Entity
    {
        public int? codePlant { get; set; }
        public int? fidelity { get; set; } 
        public int? frequency { get; set; }
        public int? pdcum { get; set; }
        public int? gap { get; set; }
        public string name { get; set; }
        public string latinNames { get; set; }
        public int? codeSophy { get; set; }
        public string sophyName { get; set; }
        public string comment { get; set; }


        internal CompanionPlant()
        {
            this.codePlant = codePlant;
        }

        public CompanionPlant(int? codePlant)
        {
            this.codePlant = codePlant;
        }

        public CompanionPlant(int? codePlant, SophyDiscriminatingPlants discriminatingPlant)
        {
            this.codePlant = codePlant;
            this.name = discriminatingPlant.discriminantPlant?.plant?.name;
            this.latinNames = discriminatingPlant.discriminantPlant?.plant?.latinNames;
            this.codeSophy = discriminatingPlant.codeDiscriminantPlant;
            this.sophyName = discriminatingPlant.sophyNameDiscriminantPlant;
            this.fidelity = discriminatingPlant.fidelity;
            this.frequency = discriminatingPlant.frequency;
            this.pdcum = discriminatingPlant.pdcum;
        }

        public CompanionPlant(int? codePlant, SophySimilarPlants similarPlant)
        {
            this.codePlant = codePlant;
            this.name = similarPlant.similarPlant?.plant?.name;
            this.latinNames = similarPlant.similarPlant?.plant?.latinNames;
            this.codeSophy = similarPlant.codeSimilarPlant;
            this.sophyName = similarPlant.sophyNameSimilarPlant;
            this.gap = similarPlant.gap;
        }

        public CompanionPlant(int? codePlant, Association association)
        {
            this.codePlant = codePlant;
            if (codePlant == association.plant1.code)
            {
                this.name = association.plant1.name;
                this.latinNames = association.plant1.latinNames;            
            }
            else 
            {
                this.name = association.plant2.name;
                this.latinNames = association.plant2.latinNames;            
            }
        }
    }
}