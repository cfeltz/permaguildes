using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.PlantsApi.Models.Common
{
    [Table("SexualCharacteristics")]
    public class SexualCharacteristic : Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int code { get; set; }
        [Required]
        [MaxLength(255)]
        public string libelle { get; set; }
        
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<Plant> plants { get; set; }

        internal SexualCharacteristic() { }

        public SexualCharacteristic(int code, string libelle)
        {
            this.code = code;
            this.libelle = libelle;
        }
    }
}