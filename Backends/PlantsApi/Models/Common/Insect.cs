using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.PlantsApi.Models.Common
{
    [Table("Insects")]
    public class Insect : Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int code { get; set; }
        [Required]
        [MaxLength(255)]
        public string libelle { get; set; }
        [Required]
        public bool auxiliary { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<PlantAuxiliary> plantsWithAuxiliary { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<PlantPest> plantsWithPest { get; set; }

        internal Insect() { }

        public Insect(int code, string libelle, bool auxiliary)
        {
            this.code = code;
            this.libelle = libelle;
        }
    }
}