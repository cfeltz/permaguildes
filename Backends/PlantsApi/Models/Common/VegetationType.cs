using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Collections.Generic;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.PlantsApi.Models.Common
{
    [Table("VegetationTypes")]
    public class VegetationType : Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int code { get; set; }
        [Required]
        [MaxLength(255)]
        public string libelle { get; set; }
        
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<PlantType> vegetationTypes { get; set; } = new List<PlantType>();

        internal VegetationType() { }

        public VegetationType(int code, string libelle)
        {
            this.code = code;
            this.libelle = libelle;
        }
    }
}