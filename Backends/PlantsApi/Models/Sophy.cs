using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using PermaGuildes.Backends.PlantsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.PlantsApi.Models
{
    [Table("Sophy")]
    public class Sophy : UpdateableEntity, RemovableEntity
    {
        [Key]
        public int code { get; set; }
        
        [MinLength(27)]
        [MaxLength(27)]
        public string sophyName { get; set; } = String.Empty;
        public int obs { get; set; }
        public int loc { get; set; }
        public int dis { get; set; }
        public int qdr { get; set; }
        public int coc { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Plant plant { get; set; }

        #region Sophy discriminating plants
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<SophyDiscriminatingPlants> mainDiscrimantsPlants { get; set; } = new List<SophyDiscriminatingPlants>();
        public ICollection<SophyDiscriminatingPlants> discrimantsPlants { get; set; } = new List<SophyDiscriminatingPlants>();
        #endregion

        #region Sophy similar plants
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<SophySimilarPlants> mainSimilarPlants { get; set; } = new List<SophySimilarPlants>();
        public ICollection<SophySimilarPlants> similarsPlants { get; set; } = new List<SophySimilarPlants>();
        #endregion

        public DateTime? dtSuppression { get; set; }

        internal Sophy() { }

        public Sophy(int code, string sophyName, int obs = 0, int loc = 0, int dis = 0, int qdr = 0, int coc = 0)
        {
            this.code = code;
            this.sophyName = sophyName;
            this.obs = obs;
            this.loc = loc;
            this.dis = dis;
            this.qdr = qdr;
            this.coc = coc;
        }

    }
}