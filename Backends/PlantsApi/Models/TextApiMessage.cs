
using CFeltz.Shared.WebApiHelper.Infrastructure;

namespace PermaGuildes.Backends.PlantsApi.Models
{
    public class TextApiMessage : ApiMessage, ITextApiMessage
    {
        public string message { get; set; }
        public TextApiMessage(string message) 
        {
            this.message = message;
        }
    }
}