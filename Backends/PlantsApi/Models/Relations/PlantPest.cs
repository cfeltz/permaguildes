using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CFeltz.Shared.WebApiHelper;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.Models.Relations
{
    [Table("PlantsPests")]
    public class PlantPest
    {
        public int codePlant { get; set; }
        public Plant plant { get; set; }
        public int codeInsect { get; set; }
        public Insect insect { get; set; }

        internal PlantPest()
        {}

        public PlantPest(int codePlant, int codeInsect)
        {
            this.codePlant = codePlant;
            this.codeInsect = codeInsect;
        }

        public PlantPest(Plant plant, Insect insect)
        {
            if (insect.auxiliary == true) 
            {
                throw new LocalException(ExceptionsIdFromWebApiHelper.DataRulesViolation, 
                    "Attempt to add a auxiliary ({0}) to the pests collection of plant {1}", 
                    insect.libelle, plant.name);
            }
            this.codePlant = plant.code;
            this.plant = plant;
            this.codeInsect = insect.code;
            this.insect = insect;
        }
    }
}