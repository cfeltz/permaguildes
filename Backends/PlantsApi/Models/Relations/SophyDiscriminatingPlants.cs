using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.Models.Relations
{
    [Table("SophyDiscriminatingPlants")]
    public class SophyDiscriminatingPlants
    {
        [Key]
        public int codeMainPlant { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Sophy mainPlant { get; set; }
        public int? codeDiscriminantPlant { get; set; }
        [Key]
        [MinLength(27)]
        [MaxLength(27)]
        public string sophyNameDiscriminantPlant { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Sophy discriminantPlant { get; set; }
        
        public int? fidelity { get; set; } 
        public int? frequency { get; set; }
        public int? pdcum { get; set; }
        
        internal SophyDiscriminatingPlants()
        {}

        public SophyDiscriminatingPlants(Sophy mainPlant, string sophyNameDiscriminantPlant, int? pdcum, int? fidelity, int? frequency)
        {
            this.codeMainPlant = mainPlant.code;
            this.mainPlant = mainPlant;
            this.sophyNameDiscriminantPlant = sophyNameDiscriminantPlant;
            this.fidelity = fidelity;
            this.frequency = frequency;
            this.pdcum = pdcum;
        }

        public SophyDiscriminatingPlants(Sophy mainPlant, Sophy discriminantPlant, int? fidelity, int? frequency, int? pdcum)
        {
            this.codeMainPlant = mainPlant.code;
            this.mainPlant = mainPlant;
            this.codeDiscriminantPlant = discriminantPlant.code;
            this.sophyNameDiscriminantPlant = discriminantPlant.sophyName;
            this.discriminantPlant = discriminantPlant;
            this.fidelity = fidelity;
            this.frequency = frequency;
            this.pdcum = pdcum;
        }
    }
}