using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.Models.Relations
{
    [Table("PlantsTypes")]
    public class PlantType
    {
        public int codePlant { get; set; }
        public Plant plant { get; set; }
        public int codeVegetationType { get; set; }
        public VegetationType vegetationType { get; set; }

        internal PlantType()
        {}

        public PlantType(int codePlant, int codeVegetationType)
        {
            this.codePlant = codePlant;
            this.codeVegetationType = codeVegetationType;
        }

        public PlantType(Plant plant, VegetationType type)
        {
            this.codePlant = plant.code;
            this.plant = plant;
            this.codeVegetationType = type.code;
            this.vegetationType = type;
        }
    }
}