using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using CFeltz.Shared.WebApiHelper;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.Models.Relations
{
    [Table("PlantsAuxiliaries")]
    public class PlantAuxiliary
    {
        public int codePlant { get; set; }
        public Plant plant { get; set; }
        public int codeInsect { get; set; }
        public Insect insect { get; set; }

        internal PlantAuxiliary()
        {}

        public PlantAuxiliary(int codePlant, int codeInsect)
        {
            this.codePlant = codePlant;
            this.codeInsect = codeInsect;
        }

        public PlantAuxiliary(Plant plant, Insect insect)
        {
            if (insect.auxiliary == false) 
            {
                throw new LocalException(ExceptionsIdFromWebApiHelper.DataRulesViolation, 
                    "Attempt to add a pest ({0}) to the auxiliaries collection of plant {1}", 
                    insect.libelle, plant.name);
            }
            this.codePlant = plant.code;
            this.plant = plant;
            this.codeInsect = insect.code;
            this.insect = insect;
        }
    }
}