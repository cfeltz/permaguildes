using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.Models.Relations
{
    [Table("SophySimilarPlants")]
    public class SophySimilarPlants
    {
        [Key]
        public int codeMainPlant { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Sophy mainPlant { get; set; }
        public int? codeSimilarPlant { get; set; }
        [Key]
        public string sophyNameSimilarPlant { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Sophy similarPlant { get; set; }
        
        public int gap { get; set; } 

        internal SophySimilarPlants()
        {}

        public SophySimilarPlants(Sophy mainPlant, string sophyNameSimilarPlant, int gap)
        {
            this.codeMainPlant = mainPlant.code;
            this.mainPlant = mainPlant;
            this.sophyNameSimilarPlant = sophyNameSimilarPlant;
            this.gap = gap;
        }

        public SophySimilarPlants(Sophy mainPlant, Sophy similarPlant, int gap)
        {
            this.codeMainPlant = mainPlant.code;
            this.mainPlant = mainPlant;
            this.codeSimilarPlant = similarPlant.code;
            this.sophyNameSimilarPlant = similarPlant.sophyName;
            this.similarPlant = similarPlant;
            this.gap = gap;
        }
    }
}