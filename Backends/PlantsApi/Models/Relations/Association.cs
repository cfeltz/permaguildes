using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PermaGuildes.Backends.PlantsApi.Models;
using PermaGuildes.Backends.PlantsApi.Models.Common;

namespace PermaGuildes.Backends.PlantsApi.Models.Relations
{
    [Table("Associations")]
    public class Association
    {
        public int codePlant1 { get; set; }
        public Plant plant1 { get; set; }
        public int codePlant2 { get; set; }
        public Plant plant2 { get; set; }

        internal Association()
        {}

        public Association(int codePlant1, int codePlant2)
        {
            this.codePlant1 = codePlant1;
            this.codePlant2 = codePlant2;
        }

        public Association(Plant plant1, Plant plant2)
        {
            if (plant1.code < plant2.code) 
            {
                this.codePlant1 = plant1.code;
                this.plant1 = plant1;
                this.codePlant2 = plant2.code;
                this.plant2 = plant2;
            }
            else
            {
                this.codePlant1 = plant2.code;
                this.plant1 = plant2;
                this.codePlant2 = plant1.code;
                this.plant2 = plant1;
            }
        }
    }
}