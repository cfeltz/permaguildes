﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PermaGuildes.Backends.PlantsApi.Models
{
    [NotMapped]
    public class RefreshToken
    {
        public int id { get; set; }
        public string login { get; set; }
        public string refreshToken { get; set; }
        public bool revoked { get; set; }
    }
}
