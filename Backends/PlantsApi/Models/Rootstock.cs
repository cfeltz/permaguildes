using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.PlantsApi.Models
{
    [Table("Rootstocks")]
    public class Rootstock : UpdateableEntity, EntityWithKey, RelationWithOneReference
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);

        public int GetReferenceKey(Type type = null) => codePlant;
        public string GetReferenceKeyAsString(Type type = null) => GetReferenceKey(type).ToString();


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }
        [Required]
        public int codePlant { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Plant plant { get; set; }

        [Required]
        [MaxLength(255)]
        public string name { get; set; }

        public int? minHeight { get; set; }
        public int? maxHeight { get; set; }
        public int? minSpan { get; set; }
        public int? maxSpan { get; set; }

        #nullable enable
        [MaxLength(2048)]
        public string? description { get; set; }
        #nullable restore


        internal Rootstock() { }

        public Rootstock(int codePlant, string name, string description)
        {
            this.codePlant = codePlant;
            this.name = name;
            this.description = description;
        }
    }
}