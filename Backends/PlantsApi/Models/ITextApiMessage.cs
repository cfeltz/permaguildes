using System;
using CFeltz.Shared.WebApiHelper.Infrastructure;

namespace PermaGuildes.Backends.PlantsApi.Models
{
    public interface ITextApiMessage : IApiMessage
    {       
        string message { get; set; }
    }
}
