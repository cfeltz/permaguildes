using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.PlantsApi.Models.Common;
using PermaGuildes.Backends.PlantsApi.Models.Relations;
using PermaGuildes.Backends.PlantsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.PlantsApi.Models
{
    [NotMapped]
    public class CompanionsPlantLists
    {
        public int codePlant { get; set; }
        #region Associations
        public ICollection<Association> associations { 
            get 
            {
                return this.associationsAsc.Concat(this.associationsDesc).ToList();
            } 
        }

        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<Association> associationsAsc { get; set; } = new List<Association>();
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<Association> associationsDesc { get; set; } = new List<Association>();
        #endregion

        public Sophy sophy { get; set; }


        internal CompanionsPlantLists() { }

        public CompanionsPlantLists(int codePlant, ICollection<Association> associationsAsc, ICollection<Association> associationsDesc, Sophy sophy)
        {
            this.codePlant = codePlant;
            this.associationsAsc = associationsAsc;
            this.associationsDesc = associationsDesc;
            this.sophy = sophy;
        }
    }
}