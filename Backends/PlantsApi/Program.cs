using CFeltz.Shared.WebApiHelper;

namespace PermaGuildes.Backends.PlantsApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webApi = new WebApi<Startup>();
            webApi.Main(args, "PlantsApi");
        }
    }
}
