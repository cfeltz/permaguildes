using PermaGuildes.Backends.NurseriesApi.Models;
using Swashbuckle.AspNetCore.Filters;

namespace PermaGuildes.Backends.NurseriesApi.SwaggerExamples
{
    public class NurseryExample : IExamplesProvider<Nursery>
    {
        public Nursery GetExamples() => new Nursery("Swagger example") {
            description = "Nursery used by swagger"
        };
    }
}