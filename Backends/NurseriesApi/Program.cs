using CFeltz.Shared.WebApiHelper;

namespace PermaGuildes.Backends.NurseriesApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webApi = new WebApi<Startup>();
            webApi.Main(args, "NurseriesApi");
        }
    }
}
