using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.NurseriesApi.Database;
using PermaGuildes.Backends.NurseriesApi.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace PermaGuildes.Backends.NurseriesApi.DatabaseRepositories
{
    public class NurseriesRepository : DatabaseRepositoryExtensionBase<NurseriesApiContext, Nursery>, INurseriesRepository
    {        
        public NurseriesRepository(NurseriesApiContext dbContext, ILogger<NurseriesRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }