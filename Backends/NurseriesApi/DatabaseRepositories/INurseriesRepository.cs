using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.NurseriesApi.Models;

namespace PermaGuildes.Backends.NurseriesApi.DatabaseRepositories
{
    public interface INurseriesRepository : IDatabaseRepositoryExtensionBase<Nursery>
    {
    }
}