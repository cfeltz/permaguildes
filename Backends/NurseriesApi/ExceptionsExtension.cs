﻿using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Attributes;
using System;

namespace PermaGuildes.Backends.NurseriesApi
{
    public enum ExceptionsIdFromNurseriesApi {
        [DefaultMessage("Bad correlation reference")]
        BadReference = 30001,

    }

    public class NurseriesApiException : LocalException
    {
        public NurseriesApiException(ExceptionsIdFromNurseriesApi id)
         : base((int)id, AttributesHelpers.GetStringAttributeOfType<DefaultMessage>(
             typeof(ExceptionsIdFromNurseriesApi), id.ToString()))
        {
        }

        public NurseriesApiException(ExceptionsIdFromNurseriesApi id, string message, params object[] args)
         : base((int)id, message, args)
        {
        }

        public NurseriesApiException(Exception innerException, ExceptionsIdFromNurseriesApi id, string message, params object[] args)
         : base((int)id, message, innerException, args)
        {
        }
    }
}
