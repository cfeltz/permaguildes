using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.NurseriesApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;

namespace PermaGuildes.Backends.NurseriesApi.Models
{
    [Table("Nurseries")]
    public class Nursery : UpdateableEntity, EntityWithKey
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }

        [Required]
        [MaxLength(255)]
        public string name { get; set; }

        [MaxLength(255)]
        public string address { get; set; }
        
        [MaxLength(5)]
        public string zipCode { get; set; }
        
        [MaxLength(255)]
        public string city { get; set; }
        
        [MaxLength(255)]
        public string email { get; set; }

        #nullable enable
        [MaxLength(256)]
        public string? url { get; set; } = String.Empty;

        [MaxLength(25)]
        public string? tel1 { get; set; } = String.Empty;
        
        [MaxLength(25)]
        public string? tel2 { get; set; } = String.Empty;

        [MaxLength(2048)]
        public string? description { get; set; }
        #nullable restore

        internal Nursery() { }

        public Nursery(string name)
        {
            this.name = name;
        }
    }
}