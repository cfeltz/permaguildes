using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PermaGuildes.Backends.NurseriesApi.Database;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.NurseriesApi.Services;
using PermaGuildes.Backends.NurseriesApi.DatabaseRepositories;
using CFeltz.Shared.WebApiHelper.SignalRHubs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using CFeltz.Shared.WebApiHelper.Filters;

namespace PermaGuildes.Backends.NurseriesApi
{
    public class Startup : CFeltz.Shared.WebApiHelper.StartUpWithDatabase<NurseriesApiContext>
    {
        public Startup(ILogger<Startup> logger, IConfiguration configuration, IWebHostEnvironment hostingEnvironment) : 
            base(logger, configuration, hostingEnvironment)
        {
            AddSignalRHub<RealtimeEntitiesHub>("RealtimeEntitiesHub");
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            
            services.AddScoped<INurseriesRepository, NurseriesRepository>();

            services.AddScoped<INurseriesService, NurseriesService>();

            services.AddScoped<IRealtimeEntitiesHubService, RealtimeEntitiesHubService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.Configure(app, env);

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<NurseriesApiContext>();
                var dbInitializer = new DbInitializer();
                dbInitializer.Initialize(context);
            }
        }
    }
}
