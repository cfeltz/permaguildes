To use EF Core migration : 
    Install dotnet-ef --> dotnet tool install --global dotnet-ef
    Restart Windows if needed (if "dotnet ef" command is on error)

To add new migration :
    cd .\Backends\NurseriesApi\
    dotnet ef migrations add <migration-name> --startup-project NurseriesApi.csproj

To revert migration : 
    dotnet ef database update <last-good-migration> --startup-project NurseriesApi.csproj



 