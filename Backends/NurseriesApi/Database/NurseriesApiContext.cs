using System;
using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using PermaGuildes.Backends.NurseriesApi.Models;
using System.Threading.Tasks;
using System.Threading;

namespace PermaGuildes.Backends.NurseriesApi.Database
{
    public class NurseriesApiContext : DbContext
    {
        private string _connectionStringForMigration = null;
        public NurseriesApiContext() : base()
        {
            _connectionStringForMigration = "server=127.0.0.1;port=33306;userid=PermaguildesUser;password=Pass@word1;database=Nurseriesdev;";
        }

        public NurseriesApiContext(DbContextOptions<NurseriesApiContext> options)
            : base(options)
        {
        }

        #region Main tables
        public DbSet<Nursery> Nurseries { get; set; }
        #endregion
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!String.IsNullOrEmpty(_connectionStringForMigration))
            {
                var version = MySqlServerVersion.AutoDetect(_connectionStringForMigration);
                optionsBuilder.UseMySql(_connectionStringForMigration, version);
            }
            optionsBuilder.EnableSensitiveDataLogging();
        }

        #region Perform entities and catch sql exceptions
       private void PerformEntities()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is UpdateableEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            DateTime now = DateTime.Now;
            foreach (var entityEntry in entries)
            {
                ((UpdateableEntity)entityEntry.Entity).dtUpdate = now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((UpdateableEntity)entityEntry.Entity).dtCreation = now;
                }
            }
        }

        private void CatchingChanges(Exception ex)
        {
            if (ex.InnerException != null && ex.InnerException.GetType() == typeof(SqlException))
            {
                var sqlEx = (SqlException)ex.InnerException;
                // See https://docs.microsoft.com/fr-fr/sql/relational-databases/errors-events/database-engine-events-and-errors?view=sql-server-ver15
                switch (sqlEx.Number)
                {
                    case 2627: 
                    {
                        throw new LocalException(ex, ExceptionsIdFromWebApiHelper.DuplicateData, 
                            "Unable to insert duplicate data");
                    }
                }
            }
            throw new LocalException(ex, ExceptionsIdFromWebApiHelper.UnknownExceptionInDatabase, 
                "Unknown error in database update process, see inner exception for details");
        }
        #endregion
        
        #region SaveChanges
        public override int SaveChanges()
        {
            PerformEntities();
            try 
            {
                return base.SaveChanges();
            }
            catch(Exception ex) 
            {
                CatchingChanges(ex);
                throw;
            }
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            PerformEntities();
            try 
            {
                return base.SaveChanges(acceptAllChangesOnSuccess);
            }
            catch(Exception ex) 
            {
                CatchingChanges(ex);
                throw;
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            PerformEntities();
            try 
            {
                return await base.SaveChangesAsync(cancellationToken);
            }
            catch(Exception ex) 
            {
                CatchingChanges(ex);
                throw;
            }
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            PerformEntities();
            try 
            {
                return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            }
            catch(Exception ex) 
            {
                CatchingChanges(ex);
                throw;
            }
        }
        #endregion
    }
}