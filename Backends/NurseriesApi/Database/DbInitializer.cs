using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.NurseriesApi.Database;
using PermaGuildes.Backends.NurseriesApi.Models;
using CFeltz.Shared.WebApiHelper.Tools;
using System.Data.Common;

namespace PermaGuildes.Backends.NurseriesApi.Database
{
    public class DbInitializer : DbInitializerExtension<NurseriesApiContext>
    {
        override public void Initialize(NurseriesApiContext context)
        {
            base.Initialize(context);
            return;
        }
    }
}