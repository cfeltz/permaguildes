using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.NurseriesApi.Models;

namespace PermaGuildes.Backends.NurseriesApi.Services
{
    public interface INurseriesService : IEntitiesServiceWithCrud<Nursery>
    {
    }
}