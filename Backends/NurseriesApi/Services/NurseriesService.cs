using Microsoft.Extensions.Logging;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Services;
using System.Linq.Expressions;
using System;
using PermaGuildes.Backends.NurseriesApi.DatabaseRepositories;
using PermaGuildes.Backends.NurseriesApi.Models;

namespace PermaGuildes.Backends.NurseriesApi.Services
{
    public class NurseriesService : EntitiesServiceWithCrud<Nursery, Nursery>, INurseriesService
    {        
        private INurseriesRepository GetBaseRepositoryTyped() => (INurseriesRepository)GetBaseRepository();

        #region Initialization        
        public NurseriesService(ILogger<NurseriesService> logger, 
            INurseriesRepository nurseriesRepository, 
            IRealtimeEntitiesHubService realtimeEntitiesHubService)
             : base(logger, nurseriesRepository, realtimeEntitiesHubService)
        {
        }
        #endregion

        #region Override abstracts and default virtuals methods
        override protected Expression<Func<Nursery, bool>> GetEntityByKeyPredicate(string key)
        {
            int keyAsInt = int.Parse(key);
            return ((Nursery e) => e.code == keyAsInt);
        }
        override protected Expression<Func<Nursery, bool>> GetEntitiesByKeysPredicate(List<string> keys)
        {
            List<int> keysAsInt = keys.Select(k => int.Parse(k)).ToList();
            return ((Nursery e) => keysAsInt.Contains(e.code));
        }

        override protected IEnumerable<Nursery> GetDefaultOrderBy(IEnumerable<Nursery> source)
        {
            return source.OrderBy(n => n.name);
        }
        #endregion
    }
}