using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PermaGuildes.Backends.NurseriesApi.Models;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.NurseriesApi.Services;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper;
using Microsoft.AspNetCore.Authorization;

namespace PermaGuildes.Backends.NurseriesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class NurseriesController : ControllerExtensionWithCrudBase<Nursery, Nursery>
    {
        private INurseriesService GetNurseriesService() => (INurseriesService)GetBaseEntitiesServiceWithCrud();

        public NurseriesController(ILogger<NurseriesController> logger, 
            INurseriesService nurseriesService) : base(logger, nurseriesService)
        {
        }

        #region Add authentification to CRUD
        #region CRUD
        // PUT/POST: api/[controller]
        [Authorize]
        [Consumes("application/json")]
        [HttpPut(), HttpPost()]
        override public async Task<ActionResult<Nursery>> Add([FromBody] Nursery dto)
        {
            return await base.Add(dto);
        }

        // PUT/POST: api/[controller]/CreateMultiple
        [Authorize]
        [Consumes("application/json")]
        [HttpPut("CreateMultiple"), HttpPost("CreateMultiple")]
        override public async Task<ActionResult<IEnumerable<Nursery>>> AddMultiple([FromBody] List<Nursery> dtos)
        {
            return await base.AddMultiple(dtos);
        }

        // POST: api/[controller]/{key}
        [Authorize]
        [Consumes("application/json")]
        [HttpPost("{key}")]
        override public async Task<ActionResult<Nursery>> Update([FromRoute] string key, [FromBody] Nursery dto)
        {
            return await base.Update(key, dto);
        }

        // PUT/POST: api/[controller]/UpdateMultiple
        [Authorize]
        [Consumes("application/json")]
        [HttpPut("UpdateMultiple"), HttpPost("UpdateMultiple")]
        override public async Task<ActionResult<List<Nursery>>> UpdateMultiple([FromBody] List<Nursery> dtos)
        {
            return await base.UpdateMultiple(dtos);
        }
        #endregion
        
        #region Delete
        [Authorize]
        [HttpDelete("{key}")]
        override public async Task<ActionResult<Nursery>> Delete([FromRoute] string key)
        {
            return await base.Delete(key);
        }

        // DELETE: api/[controller]?keys=1&keys=2...
        [Authorize]
        [HttpDelete()]
        override public async Task<ActionResult<List<Nursery>>> DeleteMultiple([FromQuery] List<string> keys)
        {
            return await base.DeleteMultiple(keys);
        }
        #endregion
        #endregion

    }
}