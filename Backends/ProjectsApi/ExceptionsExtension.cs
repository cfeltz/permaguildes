﻿using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Attributes;
using System;

namespace PermaGuildes.Backends.ProjectsApi
{
    public enum ExceptionsIdFromProjectsApi {
        [DefaultMessage("Bad correlation reference")]
        BadReference = 30001,

    }

    public class ProjectsApiException : LocalException
    {
        public ProjectsApiException(ExceptionsIdFromProjectsApi id)
         : base((int)id, AttributesHelpers.GetStringAttributeOfType<DefaultMessage>(
             typeof(ExceptionsIdFromProjectsApi), id.ToString()))
        {
        }

        public ProjectsApiException(ExceptionsIdFromProjectsApi id, string message, params object[] args)
         : base((int)id, message, args)
        {
        }

        public ProjectsApiException(Exception innerException, ExceptionsIdFromProjectsApi id, string message, params object[] args)
         : base((int)id, message, innerException, args)
        {
        }
    }
}
