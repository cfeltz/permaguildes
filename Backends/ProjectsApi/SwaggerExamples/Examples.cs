using System;
using PermaGuildes.Backends.ProjectsApi.Models;
using Swashbuckle.AspNetCore.Filters;

namespace PermaGuildes.Backends.ProjectsApi.SwaggerExamples
{
    public class ProjectExample : IExamplesProvider<Project>
    {
        public Project GetExamples() => new Project("Swagger example", 43.565859f, 3.877085f, 18) {
            description = "Project used by swagger"
        };
    }

    public class PlantationExample : IExamplesProvider<Plantation>
    {
        public Plantation GetExamples() => new Plantation(556, "Plantation from Swagger", 43.565859f, 3.877085f, 7, 3) {
            comment = "Plantation used by swagger"
        };
    }

    public class PlantationHistoryExample : IExamplesProvider<PlantationHistory>
    {
        public PlantationHistory GetExamples() => new PlantationHistory(1, DateTime.Now, PlantationHistory.eHealth.VeryGood) {
            comment = "Plantation history by swagger"
        };
    }

    public class GroupPlantationExample : IExamplesProvider<GroupPlantation>
    {
        public GroupPlantation GetExamples() => new GroupPlantation("Group from Swagger", "Group created by swagger");
    }

    public class ShapeExample : IExamplesProvider<Shape>
    {
        public Shape GetExamples() {
            Shape shape = new Shape();
            shape.geometryType = "Polygon";
            shape.coordinates = "[[[3.854895,43.552043],[3.854895,43.552086],[3.854983,43.552086],[3.854983,43.552043],[3.854895,43.552043]]]";
            return shape;
        }
    }
}