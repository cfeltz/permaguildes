using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PermaGuildes.Backends.ProjectsApi.Database;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.ProjectsApi.Services;
using PermaGuildes.Backends.ProjectsApi.DatabaseRepositories;
using CFeltz.Shared.WebApiHelper.SignalRHubs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using CFeltz.WebApiHelper.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using PermaGuildes.Backends.ProjectsApi.Mappings;
using Microsoft.AspNetCore.Mvc;

namespace PermaGuildes.Backends.ProjectsApi
{
    public class Startup : CFeltz.Shared.WebApiHelper.StartUpWithDatabase<ProjectsApiContext>
    {
        public Startup(ILogger<Startup> logger, IConfiguration configuration, IWebHostEnvironment hostingEnvironment) : 
            base(logger, configuration, hostingEnvironment)
        {
            AddSignalRHub<RealtimeEntitiesHub>("RealtimeEntitiesHub");
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            services.AddAutoMapper(typeof(Startup));
            services.AddSingleton(provider => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            }).CreateMapper());
            
            services.AddScoped<IProjectsRepository, ProjectsRepository>();
            services.AddScoped<IGroupsPlantationsRepository, GroupsPlantationsRepository>();
            services.AddScoped<IPlantationsRepository, PlantationsRepository>();
            services.AddScoped<IPlantationHistoryRepository, PlantationHistoryRepository>();
            services.AddScoped<IPlantationHistoryPhotosRepository, PlantationHistoryPhotosRepository>();
            services.AddScoped<IShapesRepository, ShapesRepository>();

            services.AddScoped<IProjectsService, ProjectsService>();
            services.AddScoped<IPlantationsService, PlantationsService>();
            services.AddScoped<IPlantationHistoryService, PlantationHistoryService>();
            services.AddScoped<IGroupsPlantationsService, GroupsPlantationsService>();
            services.AddScoped<IShapesService, ShapesService>();

            services.AddScoped<IFilesService, FilesService>();
            services.AddScoped<IRealtimeEntitiesHubService, RealtimeEntitiesHubService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.Configure(app, env);

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<ProjectsApiContext>();
                var dbInitializer = new DbInitializer();
                dbInitializer.Initialize(context);
            }
        }
    }
}
