using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.ProjectsApi.Database;
using PermaGuildes.Backends.ProjectsApi.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace PermaGuildes.Backends.ProjectsApi.DatabaseRepositories
{
    public class ProjectsRepository : DatabaseRepositoryExtensionBase<ProjectsApiContext, Project>, IProjectsRepository
    {        
        public ProjectsRepository(ProjectsApiContext dbContext, ILogger<ProjectsRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }