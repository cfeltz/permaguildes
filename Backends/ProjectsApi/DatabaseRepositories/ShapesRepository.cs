using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.ProjectsApi.Database;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.DatabaseRepositories
{
    public class ShapesRepository : DatabaseRepositoryExtensionBase<ProjectsApiContext, Shape>, IShapesRepository
    {        
        public ShapesRepository(ProjectsApiContext dbContext, ILogger<ShapesRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }