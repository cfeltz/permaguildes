using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.ProjectsApi.Database;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.DatabaseRepositories
{
    public class PlantationsRepository : DatabaseRepositoryExtensionBase<ProjectsApiContext, Plantation>, IPlantationsRepository
    {        
        public PlantationsRepository(ProjectsApiContext dbContext, ILogger<PlantationsRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }