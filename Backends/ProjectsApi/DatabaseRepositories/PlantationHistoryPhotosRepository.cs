using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.ProjectsApi.Database;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.DatabaseRepositories
{
    public class PlantationHistoryPhotosRepository : DatabaseRepositoryExtensionBase<ProjectsApiContext, PlantationHistoryPhoto>, IPlantationHistoryPhotosRepository
    {        
        public PlantationHistoryPhotosRepository(ProjectsApiContext dbContext, ILogger<PlantationHistoryPhotosRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }