using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.DatabaseRepositories
{
    public interface IPlantationHistoryRepository : IDatabaseRepositoryExtensionBase<PlantationHistory>
    {
        
    }
}