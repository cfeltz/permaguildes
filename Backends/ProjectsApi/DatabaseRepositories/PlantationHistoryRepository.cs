using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.ProjectsApi.Database;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.DatabaseRepositories
{
    public class PlantationHistoryRepository : DatabaseRepositoryExtensionBase<ProjectsApiContext, PlantationHistory>, IPlantationHistoryRepository
    {        
        public PlantationHistoryRepository(ProjectsApiContext dbContext, ILogger<PlantationHistoryRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }