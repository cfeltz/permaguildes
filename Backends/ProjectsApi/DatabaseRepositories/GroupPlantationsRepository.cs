using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.ProjectsApi.Database;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.DatabaseRepositories
{
    public class GroupsPlantationsRepository : DatabaseRepositoryExtensionBase<ProjectsApiContext, GroupPlantation>, IGroupsPlantationsRepository
    {        
        public GroupsPlantationsRepository(ProjectsApiContext dbContext, ILogger<GroupsPlantationsRepository> logger)
             : base(dbContext, logger)
        {
        }
    }
 }