using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.ProjectsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.ProjectsApi.Models
{
    [Table("GroupsPlantations")]
    public class GroupPlantation : UpdateableEntity, EntityWithKey, RelationWithOneReference
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);

        public int GetReferenceKey(Type type = null) => codeProject;
        public string GetReferenceKeyAsString(Type type = null) => GetReferenceKey(type).ToString();



        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }
        [Required]
        public string name { get; set; }

        [Required]
        public int codeProject { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Project project { get; set; }

        #nullable enable
        [MaxLength(2048)]
        public string? description { get; set; }
        #nullable restore

        #region Relations
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<Plantation> plantations { get; set; }
        #endregion

        internal GroupPlantation() { }

        public GroupPlantation(string name, string description)
        {
            this.name = name;
            this.description = description;
        }

        public void CopyAttributes(GroupPlantation group)
        {
            this.name = group.name;
            this.description = group.description;
        }

        public void CopyAttributes(Newtonsoft.Json.Linq.JObject jsonProject)
        {
            if (jsonProject.ContainsKey("name")) {
                this.name = jsonProject.Value<string>("name");
            }
            if (jsonProject.ContainsKey("description")) {
                this.description = jsonProject.Value<string>("description");
            }
        }
    }
}