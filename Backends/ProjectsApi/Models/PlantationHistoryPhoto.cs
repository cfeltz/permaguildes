using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.ProjectsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace PermaGuildes.Backends.ProjectsApi.Models
{
    [Table("PlantationHistoryPhoto")]
    public class PlantationHistoryPhoto : Entity, EntityWithKey, RelationWithOneReference
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);

        public int GetReferenceKey(Type type = null) => codeHistory;
        public string GetReferenceKeyAsString(Type type = null) => GetReferenceKey(type).ToString();


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }
        [Required]
        public int codeHistory { get; set; }

        [MaxLength(256)]
        public string filename { get; set; }

        [NotMapped]
        public string url { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public PlantationHistory plantationHistory { get; set; }

        internal PlantationHistoryPhoto() { }

        public PlantationHistoryPhoto(int codeHistory, string filename)
        {
            this.codeHistory = codeHistory;
            this.filename = filename;
        }
    }
}