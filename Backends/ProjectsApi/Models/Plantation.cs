using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.ProjectsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.ProjectsApi.Models
{
    [Table("Plantations")]
    public class Plantation : UpdateableEntity, EntityWithKey, RelationWithOneReference
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);

        public int GetReferenceKey(Type type = null) => codeProject;
        public string GetReferenceKeyAsString(Type type = null) => GetReferenceKey(type).ToString();


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }
        [Required]
        public int codePlant { get; set; }
        public int? codeVariety { get; set; }
        public int? codeRootstock { get; set; }
        public int? codeNursery { get; set; }
        public int? codeGroup { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public GroupPlantation group { get; set; }

        [Required]
        public string name { get; set; }
        [MaxLength(50)]
        public string shortName { get; set; }
        
        [Required]
        public int codeStratum { get; set; }

        [Required]
        public int codeProject { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Project project { get; set; }

        [Required]
        public bool visible { get; set; }

        [Required]
        public double latitude { get; set; }
        [Required]
        public double longitude { get; set; }
        
        [Required]
        public int height { get; set; }
        [Required]
        public int span { get; set; }

        public string iconSrc { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime? plantingYear { get; set; }
        
        #nullable enable
        [MaxLength(2048)]
        public string? locationDescription { get; set; }

        [MaxLength(2048)]
        public string? comment { get; set; }
        #nullable restore

        #region Relations
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public ICollection<PlantationHistory> history { get; set; }
        #endregion



        internal Plantation() { }

        public Plantation(int codePlant, string name, 
                            float latitude, float longitude, int height, int span)
        {
            this.name = name;
            this.codePlant = codePlant;
            this.latitude = latitude;
            this.longitude = longitude;
            this.height = height;
            this.span = span;
        }
    }
}