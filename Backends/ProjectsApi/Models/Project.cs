using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.ProjectsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;

namespace PermaGuildes.Backends.ProjectsApi.Models
{
    [Table("Projects")]
    public class Project : UpdateableEntity, EntityWithKey
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }
        [Required]
        public string name { get; set; }


        [Required]
        public double latitude { get; set; }
        [Required]
        public double longitude { get; set; }
        [Required]
        public int zoom { get; set; }

        [Required]
        public bool positionLocked { get; set; } = false;

        [Required]
        public bool freezePlantations { get; set; } = false;

        [Required]
        public bool autoDrawPlantations { get; set; } = true;
        [Required]
        public bool drawPlantations { get; set; } = true;
        [Required]
        public bool showPlantationNames { get; set; } = true;
        [Required]
        public bool showPlantationVarieties { get; set; } = true;

        #nullable enable
        [MaxLength(2048)]
        public string? description { get; set; }
        #nullable restore

        #region Relations
        public ICollection<GroupPlantation> groups { get; set; }
        public ICollection<Plantation> plantations { get; set; }
        public ICollection<Shape> shapes { get; set; }
        #endregion

        internal Project() { }

        public Project(string name, float latitude, float longitude, int zoom)
        {
            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.zoom = zoom;
        }
    }
}