using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.ProjectsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Reflection;

namespace PermaGuildes.Backends.ProjectsApi.Models
{
    [Table("PlantationHistory")]
    public class PlantationHistory : UpdateableEntity, EntityWithKey, RelationWithOneReference
    {
        public enum eHealth {
            [Description("Mort")]
            Dead = 0,
            [Description("Très mal")]
            VeryBad = 1,
            [Description("Mal")]
            Bad = 2,
            [Description("Correct")]
            Correct = 3,
            [Description("Bon")]
            Good = 4,
            [Description("Très bon")]
            VeryGood = 5,
        }
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);

        public int GetReferenceKey(Type type = null) => codePlantation;
        public string GetReferenceKeyAsString(Type type = null) => GetReferenceKey(type).ToString();


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }
        [Required]
        public int codePlantation { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime dtHistory { get; set; }

        [Required]
        public eHealth health { get; set; }

        public int? height { get; set; }
        public int? span { get; set; }
        public int? trunkCircumference { get; set; }

        #nullable enable
        [MaxLength(2048)]
        public string? comment { get; set; }
        #nullable restore


        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Plantation plantation { get; set; }

        #region Relations
        public ICollection<PlantationHistoryPhoto> photos { get; set; }
        #endregion

        override public bool IsAttributeToCopy(PropertyInfo property) 
        {
            return base.IsAttributeToCopy(property) || property.Name == "health";
        }


        internal PlantationHistory() { }

        public PlantationHistory(int codePlantation, DateTime dtHistory, eHealth health)
        {
            this.codePlantation = codePlantation;
            this.dtHistory = dtHistory;
            this.health = health;
        }
    }
}