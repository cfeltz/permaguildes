using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.ProjectsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;

namespace PermaGuildes.Backends.ProjectsApi.Models
{
    [Table("Shapes")]
    public class Shape : UpdateableEntity, EntityWithKey, RelationWithOneReference
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);

        public int GetReferenceKey(Type type = null) => codeProject;
        public string GetReferenceKeyAsString(Type type = null) => GetReferenceKey(type).ToString();


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int code { get; set; }

        [Required]
        public int codeProject { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        [MessagePack.IgnoreMember]
        public Project project { get; set; }

        [Required]
        public string color { get; set; }
        public string fillColor { get; set; }

        // lineCap
        // lineJoin

        [Required]
        public bool fill { get; set; }

        [Required]
        public float opacity { get; set; }
        public float? fillOpacity { get; set; }
        
        [Required]
        public bool stroke { get; set; }
        [Required]
        public int weight { get; set; }
        [Required]
        public bool weightIsInFeet { get; set; }
        public double? radius { get; set; }

        [Required]
        public string geometryType { get; set; }
        [Required]
        public string coordinates { get; set; }

        public Shape()
        {
        }
    }
}