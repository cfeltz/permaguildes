using System;
using CFeltz.Shared.WebApiHelper.Infrastructure;

namespace PermaGuildes.Backends.ProjectsApi.Models
{
    public interface ITextApiMessage : IApiMessage
    {       
        string message { get; set; }
    }
}
