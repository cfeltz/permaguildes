
using CFeltz.Shared.WebApiHelper.Infrastructure;

namespace PermaGuildes.Backends.ProjectsApi.Models
{
    public class TextApiMessage : ApiMessage, ITextApiMessage
    {
        public string message { get; set; }
        public TextApiMessage(string message) 
        {
            this.message = message;
        }
    }
}