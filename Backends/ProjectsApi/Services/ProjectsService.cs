using Microsoft.Extensions.Logging;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Services;
using System.Linq.Expressions;
using System;
using PermaGuildes.Backends.ProjectsApi.DatabaseRepositories;
using PermaGuildes.Backends.ProjectsApi.Models;
using System.IO;
using Microsoft.Extensions.Configuration;
using CFeltz.Shared.WebApiHelper.Tools;

namespace PermaGuildes.Backends.ProjectsApi.Services
{
    public class ProjectsService : EntitiesServiceWithCrud<Project, Project>, IProjectsService
    {        
        private readonly IConfiguration config;
        private IProjectsRepository GetBaseRepositoryTyped() => (IProjectsRepository)GetBaseRepository();

        #region Initialization        
        public ProjectsService(ILogger<ProjectsService> logger, 
            IConfiguration config, 
            IProjectsRepository projectsRepository, 
            IRealtimeEntitiesHubService realtimeEntitiesHubService)
             : base(logger, projectsRepository, realtimeEntitiesHubService)
        {
            this.config = config;
        }
        #endregion

        #region Override abstracts and default virtuals methods
        override protected Expression<Func<Project, bool>> GetEntityByKeyPredicate(string key)
        {
            int keyAsInt = int.Parse(key);
            return ((Project e) => e.code == keyAsInt);
        }
        override protected Expression<Func<Project, bool>> GetEntitiesByKeysPredicate(List<string> keys)
        {
            List<int> keysAsInt = keys.Select(k => int.Parse(k)).ToList();
            return ((Project e) => keysAsInt.Contains(e.code));
        }

        override protected Expression<Func<Project, object>>[] GetSingleDefaultIncludes()
        {
            Expression<Func<Project, object>> include1 = ((Project i) => i.plantations.OrderByDescending(p => p.latitude));
            Expression<Func<Project, object>> include2 = ((Project i) => i.groups.OrderBy(g => g.name));
            return new[] { include1, include2 };
        }
        #endregion

        #region Before and after crud event
        override protected void OnDeleted(List<Project> projects) { 
            projects.ForEach(project => {
                Directory.Delete(GetProjectDirectory(project.code), true);
            });
        }
        #endregion

        public string GetProjectDirectory(int codeProject) {
            string photoPath = config.GetPathByKey("UploadFiles");
            photoPath = Path.Combine(photoPath, "Projects", codeProject.ToString());
            Directory.CreateDirectory(photoPath);
            return photoPath;
        }
    }
}