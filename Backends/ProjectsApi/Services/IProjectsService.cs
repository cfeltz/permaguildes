using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.Services
{
    public interface IProjectsService : IEntitiesServiceWithCrud<Project>
    {
    }
}