using System.Collections.Generic;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.ProjectsApi.Models;
using Microsoft.AspNetCore.Http;
using PermaGuildes.Backends.ProjectsApi.ModelsDTO;

namespace PermaGuildes.Backends.ProjectsApi.Services
{
    public interface IPlantationHistoryService : IRelations1NServiceWithCrud<PlantationHistoryDTO>
    {
        List<PlantationHistoryPhotoDTO> AddPhotos(Plantation plantation, int codeHistory, IFormFileCollection files);
        List<PlantationHistoryPhotoDTO> AddPhotos(int codeProject, int codePlantation, int codeHistory, IFormFileCollection files);
        string GetPhotoPath(Plantation plantation, string filename);
        string GetPhotoPath(int codeProject, int codePlantation, string filename);

        PlantationHistoryPhotoDTO DeletePhoto(Plantation plantation, int codeHistory, int codePhoto);
        PlantationHistoryPhotoDTO DeletePhoto(int codeProject, int codePlantation, int codeHistory, int codePhoto);
    }
}