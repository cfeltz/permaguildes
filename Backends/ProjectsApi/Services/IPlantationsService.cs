using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.Services
{
    public interface IPlantationsService : IRelations1NServiceWithCrud<Plantation>
    {
        string GetPhotoDirectory(Plantation plantation);
        string GetPhotoDirectory(int codeProject, int codePlantation);
    }
}