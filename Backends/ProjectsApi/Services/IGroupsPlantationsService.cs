using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.Services
{
    public interface IGroupsPlantationsService : IRelations1NServiceWithCrud<GroupPlantation>
    {
    }
}