using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Services;
using System.Linq;
using PermaGuildes.Backends.ProjectsApi.Models;
using PermaGuildes.Backends.ProjectsApi.DatabaseRepositories;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using Microsoft.AspNetCore.Http;
using CFeltz.WebApiHelper.Services;
using System.IO;
using CFeltz.Shared.WebApiHelper.Models;
using PermaGuildes.Backends.ProjectsApi.ModelsDTO;
using AutoMapper;

namespace PermaGuildes.Backends.ProjectsApi.Services
{
    public class PlantationHistoryService : Relations1NServiceWithCrud<Plantation, PlantationHistory, PlantationHistoryDTO>, IPlantationHistoryService
    {
        private readonly IFilesService filesService;
        private readonly IPlantationsService plantationsService;
        private readonly IPlantationHistoryPhotosRepository plantationHistoryPhotosRepository;

        #region Initialization
        public PlantationHistoryService(ILogger<PlantationHistoryService> logger, 
            IMapper mapper,
            IPlantationsService plantationsService, 
            IPlantationHistoryRepository plantationHistoryRepository,
            IPlantationHistoryPhotosRepository plantationHistoryPhotosRepository, 
            IFilesService filesService,
            IRealtimeEntitiesHubService realtimeEntitiesHubService)
             : base(logger, plantationHistoryRepository, realtimeEntitiesHubService, mapper)
        {
            this.filesService = filesService;
            this.plantationsService = plantationsService;
            this.plantationHistoryPhotosRepository = plantationHistoryPhotosRepository;

        }
        #endregion

        #region Override abstracts methods
        protected override Expression<Func<PlantationHistory, bool>> GetRelationByKeyPredicate(string relationKey, string referenceKey = null)
        {
            int relationKeyAsInt = int.Parse(relationKey);
            int? referenceKeyAsInt = null;
            if (!string.IsNullOrEmpty(referenceKey)) 
            {
                referenceKeyAsInt = int.Parse(referenceKey);
            }
            if (referenceKey == null) {
                return ((PlantationHistory r) => r.code == relationKeyAsInt);
            } else {
                return ((PlantationHistory r) => r.code == relationKeyAsInt && r.codePlantation == referenceKeyAsInt);
            }
        }
        protected override Expression<Func<PlantationHistory, bool>> GetRelationsByReferenceKeyPredicate(string referenceKey)
        {
            int referenceKeyAsInt = int.Parse(referenceKey);
            return ((PlantationHistory r) => r.codePlantation == referenceKeyAsInt);
        }
        
        override protected Expression<Func<PlantationHistory, bool>> GetRelationsByKeysPredicate(List<string> referenceKeys, List<string> relation1NKeys) 
        {
            List<int> relation1NKeysAsInt = relation1NKeys.Select(k => int.Parse(k)).ToList();
            return ((PlantationHistory r) => relation1NKeysAsInt.Contains(r.code));
        }

        protected override void SetRelation(string referenceKey, PlantationHistory relation) 
        { 
            int referenceKeyAsInt = int.Parse(referenceKey);
            relation.codePlantation = referenceKeyAsInt;
        }

        protected override Expression<Func<PlantationHistory, object>>[] GetDefaultIncludes()
        {
            Expression<Func<PlantationHistory, object>> include = ((PlantationHistory i) => i.photos);
            return new[] { include };
        }

        protected override IEnumerable<PlantationHistory> GetDefaultOrderBy(IEnumerable<PlantationHistory> source)
        {
            return source.OrderBy(r => r.dtHistory);
        }
        #endregion

        #region Before and after crud event
        override protected void OnDeleting(List<PlantationHistoryDTO> relationDtos) { 
            Plantation plantation = null;
            relationDtos.ForEach(history => {
                // Remove all photos in database
                this.plantationHistoryPhotosRepository.Delete(p => p.codeHistory == history.code);
                // Delete files
                history.photos.ToList().ForEach(photo => {
                    if (plantation == null || plantation.code != history.codePlantation) 
                    {
                        plantation = this.plantationsService.GetRelation(history.codePlantation);
                    }
                    string path = GetPhotoPath(plantation, photo.filename);
                    filesService.DeleteFile(path);
                });
            });  
        }
        #endregion

        #region Photos
        public List<PlantationHistoryPhotoDTO> AddPhotos(Plantation plantation, int codeHistory, IFormFileCollection files)
        {
            return AddPhotos(plantation.codeProject, plantation.code, codeHistory, files);
        }

        public List<PlantationHistoryPhotoDTO> AddPhotos(int codeProject, int codePlantation, int codeHistory, IFormFileCollection files)
        {
            var returnValue = new List<PlantationHistoryPhoto>();
            string pathToSave = plantationsService.GetPhotoDirectory(codeProject, codePlantation);
            List<string> filenames = filesService.SaveFiles(pathToSave, files, false, true);

            foreach(string filename in filenames)
            {
                var data = new PlantationHistoryPhoto(codeHistory, filename);
                returnValue.Add(data);
                plantationHistoryPhotosRepository.Add(data);
            }
            plantationHistoryPhotosRepository.Commit();

            // Send event to SignalR
            if (realtimeEntitiesHubService != null)
            {
                var history = GetRelation(codeHistory);
                realtimeEntitiesHubService.SendRealtimeEvent(RealtimeEventType.Updated, history);
            }

            return _mapper.Map<List<PlantationHistoryPhoto>, List<PlantationHistoryPhotoDTO>>(returnValue);
        }


        public string GetPhotoPath(Plantation plantation, string filename)
        {
            return GetPhotoPath(plantation.codeProject, plantation.code, filename);
        }
        public string GetPhotoPath(int codeProject, int codePlantation, string filename)
        {
            string photoPath = plantationsService.GetPhotoDirectory(codeProject, codePlantation);
            photoPath = Path.Combine(photoPath, filename);
            return photoPath;
        }


        public PlantationHistoryPhotoDTO DeletePhoto(Plantation plantation, int codeHistory, int codePhoto)
        {
            return DeletePhoto(plantation.codeProject, plantation.code, codeHistory, codePhoto);
        }

        public PlantationHistoryPhotoDTO DeletePhoto(int codeProject, int codePlantation, int codeHistory, int codePhoto)
        {
            var history = this.GetRelation(codeHistory);
            var photo = history.photos.Where(p => p.code == codePhoto).FirstOrDefault();
            ThrowDataNotFoundIfNull(photo, "Photo not found");
        
            string path = GetPhotoPath(codeProject, codePlantation, photo.filename);
            filesService.DeleteFile(path);
            history.photos.Remove(photo);
            plantationHistoryPhotosRepository.Delete(p => p.code == codePhoto);
            plantationHistoryPhotosRepository.Commit();

            // Send event to SignalR
            if (realtimeEntitiesHubService != null)
            {
                realtimeEntitiesHubService.SendRealtimeEvent(RealtimeEventType.Updated, history);
            }

            return photo;
        }
        #endregion
    }
}