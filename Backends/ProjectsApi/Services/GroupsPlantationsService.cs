using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Linq;
using CFeltz.Shared.WebApiHelper.Services;
using PermaGuildes.Backends.ProjectsApi.DatabaseRepositories;
using PermaGuildes.Backends.ProjectsApi.Models;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace PermaGuildes.Backends.ProjectsApi.Services
{
    public class GroupsPlantationsService : Relations1NServiceWithCrud<Project, GroupPlantation, GroupPlantation>, IGroupsPlantationsService
    {        
        public GroupsPlantationsService(ILogger<GroupsPlantationsService> logger, 
            IGroupsPlantationsRepository groupsPlantationsRepository,
            IRealtimeEntitiesHubService realtimeEntitiesHubService)
             : base(logger, groupsPlantationsRepository, realtimeEntitiesHubService)
        {
        }

        #region Override abstracts methods
        protected override Expression<Func<GroupPlantation, bool>> GetRelationByKeyPredicate(string relationKey, string referenceKey = null)
        {
            int relationKeyAsInt = int.Parse(relationKey);
            int? referenceKeyAsInt = null;
            if (!string.IsNullOrEmpty(referenceKey)) 
            {
                referenceKeyAsInt = int.Parse(referenceKey);
            }
            if (referenceKey == null) {
                return ((GroupPlantation r) => r.code == relationKeyAsInt);
            } else {
                return ((GroupPlantation r) => r.code == relationKeyAsInt && r.codeProject == referenceKeyAsInt);
            }
        }
        protected override Expression<Func<GroupPlantation, bool>> GetRelationsByReferenceKeyPredicate(string referenceKey)
        {
            int referenceKeyAsInt = int.Parse(referenceKey);
            return ((GroupPlantation r) => r.codeProject == referenceKeyAsInt);
        }
        
        override protected Expression<Func<GroupPlantation, bool>> GetRelationsByKeysPredicate(List<string> referenceKeys, List<string> relation1NKeys) 
        {
            List<int> relation1NKeysAsInt = relation1NKeys.Select(k => int.Parse(k)).ToList();
            return ((GroupPlantation r) => relation1NKeysAsInt.Contains(r.code));
        }

        protected override void SetRelation(string referenceKey, GroupPlantation entity) 
        { 
            int referenceKeyAsInt = int.Parse(referenceKey);
            entity.codeProject = referenceKeyAsInt;
        }
        #endregion
    }
}