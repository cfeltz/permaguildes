using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Services;
using System.Linq;
using PermaGuildes.Backends.ProjectsApi.Models;
using PermaGuildes.Backends.ProjectsApi.DatabaseRepositories;
using System.Collections.Generic;
using System.Linq.Expressions;
using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using CFeltz.Shared.WebApiHelper.Tools;

namespace PermaGuildes.Backends.ProjectsApi.Services
{
    public class PlantationsService : Relations1NServiceWithCrud<Project, Plantation, Plantation>, IPlantationsService
    {
        private readonly IConfiguration config;

        #region Initialization
        public PlantationsService(ILogger<PlantationsService> logger, 
            IConfiguration config, 
            IPlantationsRepository plantationsRepository,
            IRealtimeEntitiesHubService realtimeEntitiesHubService)
             : base(logger, plantationsRepository, realtimeEntitiesHubService)
        {
            this.config = config;
        }
        #endregion

        #region Override abstracts methods
        protected override Expression<Func<Plantation, bool>> GetRelationByKeyPredicate(string relationKey, string referenceKey = null)
        {
            int relationKeyAsInt = int.Parse(relationKey);
            int? referenceKeyAsInt = null;
            if (!string.IsNullOrEmpty(referenceKey)) 
            {
                referenceKeyAsInt = int.Parse(referenceKey);
            }
            if (referenceKey == null) {
                return ((Plantation r) => r.code == relationKeyAsInt);
            } else {
                return ((Plantation r) => r.code == relationKeyAsInt && r.codeProject == referenceKeyAsInt);
            }
        }
        protected override Expression<Func<Plantation, bool>> GetRelationsByReferenceKeyPredicate(string referenceKey)
        {
            int referenceKeyAsInt = int.Parse(referenceKey);
            return ((Plantation r) => r.codeProject == referenceKeyAsInt);
        }
        
        override protected Expression<Func<Plantation, bool>> GetRelationsByKeysPredicate(List<string> referenceKeys, List<string> relation1NKeys) 
        {
            List<int> relation1NKeysAsInt = relation1NKeys.Select(k => int.Parse(k)).ToList();
            return ((Plantation r) => relation1NKeysAsInt.Contains(r.code));
        }
        protected override void SetRelation(string referenceKey, Plantation relation) 
        { 
            int referenceKeyAsInt = int.Parse(referenceKey);
            relation.codeProject = referenceKeyAsInt;
        }

        protected override IEnumerable<Plantation> GetDefaultOrderBy(IEnumerable<Plantation> source)
        {
            return source.OrderByDescending(r => r.latitude);
        }
        #endregion
    

        #region Before and after crud event        
        override protected void OnUpdated(List<Plantation> plantation) { 
            plantation.ForEach(p => p.group = null);
        }

        override protected void OnDeleted(List<Plantation> plantations) { 
            plantations.ForEach(plantation => {
                Directory.Delete(GetPhotoDirectory(plantation), true);
            });
        }
        #endregion

        public string GetPhotoDirectory(Plantation plantation) {
            return GetPhotoDirectory(plantation.codeProject, plantation.code);
        }

        public string GetPhotoDirectory(int codeProject, int codePlantation) {
            string photoPath = config.GetPathByKey("UploadFiles");
            photoPath = Path.Combine(photoPath, "Projects", codeProject.ToString(), "Plantations", codePlantation.ToString());
            Directory.CreateDirectory(photoPath);
            return photoPath;
        }
    }
}