using Microsoft.Extensions.Logging;
using CFeltz.Shared.WebApiHelper.Services;
using System.Linq;
using PermaGuildes.Backends.ProjectsApi.Models;
using PermaGuildes.Backends.ProjectsApi.DatabaseRepositories;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;

namespace PermaGuildes.Backends.ProjectsApi.Services
{
    public class ShapesService : Relations1NServiceWithCrud<Project, Shape, Shape>, IShapesService
    {
        #region Initialization
        public ShapesService(ILogger<ShapesService> logger, 
            IShapesRepository shapesRepository,
            IRealtimeEntitiesHubService realtimeEntitiesHubService)
             : base(logger, shapesRepository, realtimeEntitiesHubService)
        {
        }
        #endregion

        #region Override abstracts methods
        protected override Expression<Func<Shape, bool>> GetRelationByKeyPredicate(string relationKey, string referenceKey = null)
        {
            int relationKeyAsInt = int.Parse(relationKey);
            int? referenceKeyAsInt = null;
            if (!string.IsNullOrEmpty(referenceKey)) 
            {
                referenceKeyAsInt = int.Parse(referenceKey);
            }
            if (referenceKey == null) {
                return ((Shape r) => r.code == relationKeyAsInt);
            } else {
                return ((Shape r) => r.code == relationKeyAsInt && r.codeProject == referenceKeyAsInt);
            }
        }
        protected override Expression<Func<Shape, bool>> GetRelationsByReferenceKeyPredicate(string referenceKey)
        {
            int referenceKeyAsInt = int.Parse(referenceKey);
            return ((Shape r) => r.codeProject == referenceKeyAsInt);
        }
        
        override protected Expression<Func<Shape, bool>> GetRelationsByKeysPredicate(List<string> referenceKeys, List<string> relation1NKeys) 
        {
            List<int> relation1NKeysAsInt = relation1NKeys.Select(k => int.Parse(k)).ToList();
            return ((Shape r) => relation1NKeysAsInt.Contains(r.code));
        }
        protected override void SetRelation(string referenceKey, Shape relation) 
        { 
            int referenceKeyAsInt = int.Parse(referenceKey);
            relation.codeProject = referenceKeyAsInt;
        }
        #endregion
    }
}