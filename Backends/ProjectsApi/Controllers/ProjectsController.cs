using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PermaGuildes.Backends.ProjectsApi.Models;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.ProjectsApi.Services;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper;
using Microsoft.AspNetCore.Authorization;

namespace PermaGuildes.Backends.ProjectsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class ProjectsController : ControllerExtensionWithAuthorizedCrudBase<Project, Project>
    {
        private IProjectsService GetProjectsService() => (IProjectsService)GetBaseEntitiesServiceWithCrud();

        public ProjectsController(ILogger<ProjectsController> logger, 
            IProjectsService projectsService) : base(logger, projectsService)
        {
        }

    }
}