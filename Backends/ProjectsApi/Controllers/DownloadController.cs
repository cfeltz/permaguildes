using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.ProjectsApi.Models;
using PermaGuildes.Backends.ProjectsApi.Services;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using System.IO;
using CFeltz.Shared.WebApiHelper.Tools;
using CFeltz.WebApiHelper.Services;
using PermaGuildes.Backends.ProjectsApi.ModelsDTO;

namespace CFeltz.Backends.PlantsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DownloadController : ControllerExtensionBase
    {
        IPlantationsService plantationsService;
        IPlantationHistoryService plantationHistoryService;

        public DownloadController(
            ILogger<DownloadController> logger,
            IPlantationsService plantationsService,
            IPlantationHistoryService plantationHistoryService) : base(logger)
        {
            this.plantationsService = plantationsService;
            this.plantationHistoryService = plantationHistoryService;
        }

        [HttpGet("plantations/history/{codeHistory}/{filename}")]
        public PhysicalFileResult GetPhoto([FromRoute] int codeHistory, [FromRoute] string filename)
        {
            var history = this.plantationHistoryService.GetRelation(codeHistory);
            ThrowDataNotFoundIfNull(history, "History not found");
            var plantation = this.plantationsService.GetRelation(history.codePlantation);

            string photoPath = this.plantationHistoryService.GetPhotoPath(plantation, filename);
            string extension = Path.GetExtension(filename).Substring(1);

            return new PhysicalFileResult(photoPath, "image/" + extension);
        }

    }
}