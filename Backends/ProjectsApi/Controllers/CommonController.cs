using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.ProjectsApi.Models;
using CFeltz.Shared.WebApiHelper.Infrastructure;

namespace CFeltz.Backends.PlantsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerExtensionBase
    {
        public CommonController(ILogger<CommonController> logger) : base(logger)
        {
        }

        [HttpGet("plantation-history-health")]
        public ActionResult<List<EntityWithKeyFromEnum<PlantationHistory.eHealth>>> GetHealthValues()
        {
            return EntityWithKeyFromEnum<PlantationHistory.eHealth>.GetListFromEnum();
        }
    }
}