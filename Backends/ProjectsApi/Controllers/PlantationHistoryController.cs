using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.ProjectsApi.Models;
using PermaGuildes.Backends.ProjectsApi.Services;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using System.IO;
using CFeltz.Shared.WebApiHelper.Tools;
using CFeltz.WebApiHelper.Services;
using PermaGuildes.Backends.ProjectsApi.ModelsDTO;

namespace CFeltz.Backends.PlantsApi.Controllers
{
    [Route("api/plantations/{referenceKey}/history")]
    [ApiController]
    public class PlantationHistoryController : ControllerExtensionWithRelation1NAuthorizedCrudBase<Plantation, PlantationHistory, PlantationHistoryDTO>
    {
        private readonly IPlantationsService plantationsService;
        
        private IPlantationHistoryService GetPlantationHistoryService() => (IPlantationHistoryService)GetBaseRelationsServiceWithCrud();
        
        public PlantationHistoryController(
            ILogger<PlantationHistoryController> logger, 
            IPlantationsService plantationsService, 
            IPlantationHistoryService plantationHistoryService) : base(logger, plantationHistoryService)
        {
            this.plantationsService = plantationsService;
        }

        [HttpPost("{relation1NKey}/AddPhotos")]
        [Consumes("multipart/form-data")]
        [RequestFormLimits(ValueLengthLimit = int.MaxValue, MultipartBodyLengthLimit = int.MaxValue)]
        [DisableRequestSizeLimit]
        public ActionResult<List<PlantationHistoryPhotoDTO>> AddPhotos([FromRoute] string referenceKey, [FromRoute] string relation1NKey)
        {
            int codeHistory = int.Parse(relation1NKey);
            int codePlantation = int.Parse(referenceKey);
            var plantation = this.plantationsService.GetRelation(codePlantation);
            ThrowDataNotFoundIfNull(plantation, "Plantation not found");

            var files = GetFilesFromRequest(Request);
            List<PlantationHistoryPhotoDTO> photos = GetPlantationHistoryService().AddPhotos(plantation, codeHistory, files);

            return CreatedAtAction(nameof(DownloadController.GetPhoto), new { 
                referenceKey = referenceKey, 
                filename = photos[0].filename
            }, photos);
        }

        [HttpDelete("{relation1NKey}/Photos/{codePhoto}")]
        public ActionResult<PlantationHistoryPhotoDTO> DeletePhoto([FromRoute] string referenceKey, [FromRoute] string relation1NKey, [FromRoute] int codePhoto)
        {
            int codeHistory = int.Parse(relation1NKey);
            int codePlantation = int.Parse(referenceKey);
            var plantation = this.plantationsService.GetRelation(codePlantation);
            ThrowDataNotFoundIfNull(plantation, "Plantation not found");

            return GetPlantationHistoryService().DeletePhoto(plantation, codeHistory, codePhoto);
        }
    }
}