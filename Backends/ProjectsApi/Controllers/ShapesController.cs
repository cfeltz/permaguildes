using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.ProjectsApi.Models;
using PermaGuildes.Backends.ProjectsApi.Services;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper;
using Microsoft.AspNetCore.Authorization;

namespace CFeltz.Backends.PlantsApi.Controllers
{
    [Route("api/projects/{referenceKey}/[controller]")]
    [ApiController]
    public class ShapesController : ControllerExtensionWithRelation1NAuthorizedCrudBase<Project, Shape, Shape>
    {
        private IShapesService GetShapesService() => (IShapesService)GetBaseRelationsServiceWithCrud();
        
        public ShapesController(ILogger<ShapesController> logger, 
        IShapesService shapesService) : base(logger, shapesService)
        {
        }    }
}