using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.ProjectsApi.Models;
using PermaGuildes.Backends.ProjectsApi.Services;
using Microsoft.AspNetCore.Authorization;

namespace CFeltz.Backends.PlantsApi.Controllers
{
    [Route("api/projects/{referenceKey}/[controller]")]
    [ApiController]
    public class GroupsController : ControllerExtensionWithRelation1NAuthorizedCrudBase<Project, GroupPlantation, GroupPlantation>
    {
        private IGroupsPlantationsService GetGroupsPlantationsService() => (IGroupsPlantationsService)GetBaseRelationsServiceWithCrud();
        
        public GroupsController(ILogger<GroupsController> logger, 
        IGroupsPlantationsService groupsPlantationsService) : base(logger, groupsPlantationsService)
        {
        }

    }
}