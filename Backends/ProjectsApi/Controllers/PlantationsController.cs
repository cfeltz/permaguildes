using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using CFeltz.Shared.WebApiHelper.Controllers;
using PermaGuildes.Backends.ProjectsApi.Models;
using PermaGuildes.Backends.ProjectsApi.Services;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using CFeltz.Shared.WebApiHelper;
using Microsoft.AspNetCore.Authorization;

namespace CFeltz.Backends.PlantsApi.Controllers
{
    [Route("api/projects/{referenceKey}/[controller]")]
    [ApiController]
    public class PlantationsController : ControllerExtensionWithRelation1NAuthorizedCrudBase<Project, Plantation, Plantation>
    {
        private IPlantationsService GetPlantationsService() => (IPlantationsService)GetBaseRelationsServiceWithCrud();
        private IGroupsPlantationsService _groupsPlantationsService;
        
        public PlantationsController(ILogger<PlantationsController> logger, 
        IPlantationsService plantationsService, 
        IGroupsPlantationsService groupsPlantationsService) : base(logger, plantationsService)
        {
            _groupsPlantationsService = groupsPlantationsService;
        }

        // POST: api/Projects/{codeProject}/plantations/{codePlantation}/updatePosition
        [Authorize]
        [HttpPost("{codePlantation}/updatePosition")]
        public async Task<ActionResult<IApiMessage>> UpdatePositionOfPlantation(
            [FromRoute] int referenceKey,
            [FromRoute] int codePlantation, 
            [FromBody] Newtonsoft.Json.Linq.JObject jsonPosition)
        {
            int codeProject = referenceKey;

            Plantation localPlantation = GetPlantationsService().GetRelation(codePlantation, codeProject);
            ThrowDataNotFoundIfNull(localPlantation, $"Plantation with code {codePlantation} not found in project {codeProject}");

            localPlantation.latitude = GetDoubleFromRequest(jsonPosition, "latitude");
            localPlantation.longitude = GetDoubleFromRequest(jsonPosition, "longitude");
            
            await GetPlantationsService().UpdateRelation(localPlantation);

            return localPlantation;
        }

        // POST: api/Projects/{codeProject}/plantations/{codePlantation}/updateGroup/{codeGroup}
        [Authorize]
        [HttpPost("{codePlantation}/updateGroup/{codeGroup}")]
        public async Task<ActionResult<IApiMessage>> UpdateGroupOfPlantation(
            [FromRoute] int referenceKey,
            [FromRoute] int codePlantation,
            [FromRoute] int codeGroup)
        {
            int codeProject = referenceKey;
            Plantation localPlantation = GetPlantationsService().GetRelation(codePlantation, codeProject);
            ThrowDataNotFoundIfNull(localPlantation, $"Plantation with code {codePlantation} not found in project {codeProject}");

            if (codeGroup != 0) 
            {
                GroupPlantation localGroup = _groupsPlantationsService.GetRelation(codeGroup, codeProject);
                ThrowDataNotFoundIfNull(localGroup, $"Group with code {codeGroup} not found in project {codeProject}");

                localPlantation.codeGroup = codeGroup;
            }
            else 
            {
                localPlantation.codeGroup = null;
            }

            await GetPlantationsService().UpdateRelation(localPlantation);

            return localPlantation;
        }
    }
}