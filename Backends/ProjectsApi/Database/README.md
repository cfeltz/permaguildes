To use EF Core migration : 
    Install dotnet-ef --> dotnet tool install --global dotnet-ef
    Restart Windows if needed (if "dotnet ef" command is on error)

To add new migration :
    cd .\Backends\ProjectsApi\
    dotnet ef migrations add <migration-name> --startup-project ProjectsApi.csproj

To revert migration : 
    dotnet ef database update <last-good-migration> --startup-project ProjectsApi.csproj

