using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Database;
using PermaGuildes.Backends.ProjectsApi.Database;
using PermaGuildes.Backends.ProjectsApi.Models;
using CFeltz.Shared.WebApiHelper.Tools;
using System.Data.Common;

namespace PermaGuildes.Backends.ProjectsApi.Database
{
    public class DbInitializer : DbInitializerExtension<ProjectsApiContext>
    {
        override public void Initialize(ProjectsApiContext context)
        {
            base.Initialize(context);
            
            // if (context.VegetationTypes.Any())
            // {
            //     return;   // DB has been seeded
            // }

            // InitializeCommonData(context);
            return;
        }

        // private static void InitializeCommonData(ProjectsApiContext context)
        // {
        // }
    }
}