﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PermaGuildes.Backends.ProjectsApi.Migrations
{
    public partial class AddPlantationsAttributes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "codeRootstock",
                table: "Plantations",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "codeVariety",
                table: "Plantations",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "plantingYear",
                table: "Plantations",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "shortName",
                table: "Plantations",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<bool>(
                name: "visible",
                table: "Plantations",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "codeRootstock",
                table: "Plantations");

            migrationBuilder.DropColumn(
                name: "codeVariety",
                table: "Plantations");

            migrationBuilder.DropColumn(
                name: "plantingYear",
                table: "Plantations");

            migrationBuilder.DropColumn(
                name: "shortName",
                table: "Plantations");

            migrationBuilder.DropColumn(
                name: "visible",
                table: "Plantations");
        }
    }
}
