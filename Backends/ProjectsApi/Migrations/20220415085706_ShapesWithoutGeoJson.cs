﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PermaGuildes.Backends.ProjectsApi.Migrations
{
    public partial class ShapesWithoutGeoJson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Shapes");
            migrationBuilder.RenameColumn(
                name: "geoJSON",
                table: "Shapes",
                newName: "geometryType");

            migrationBuilder.AddColumn<string>(
                name: "color",
                table: "Shapes",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "coordinates",
                table: "Shapes",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<bool>(
                name: "fill",
                table: "Shapes",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "fillColor",
                table: "Shapes",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<float>(
                name: "fillOpacity",
                table: "Shapes",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "opacity",
                table: "Shapes",
                type: "float",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<double>(
                name: "radius",
                table: "Shapes",
                type: "double",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "stroke",
                table: "Shapes",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "weight",
                table: "Shapes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "weightIsInFeet",
                table: "Shapes",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<bool>(
                name: "showPlantationVarieties",
                table: "Projects",
                type: "tinyint(1)",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "tinyint(1)",
                oldDefaultValue: true);

            migrationBuilder.AlterColumn<bool>(
                name: "showPlantationNames",
                table: "Projects",
                type: "tinyint(1)",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "tinyint(1)",
                oldDefaultValue: true);

            migrationBuilder.AlterColumn<bool>(
                name: "drawPlantations",
                table: "Projects",
                type: "tinyint(1)",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "tinyint(1)",
                oldDefaultValue: true);

            migrationBuilder.AlterColumn<bool>(
                name: "visible",
                table: "Plantations",
                type: "tinyint(1)",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "tinyint(1)",
                oldDefaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "color",
                table: "Shapes");

            migrationBuilder.DropColumn(
                name: "coordinates",
                table: "Shapes");

            migrationBuilder.DropColumn(
                name: "fill",
                table: "Shapes");

            migrationBuilder.DropColumn(
                name: "fillColor",
                table: "Shapes");

            migrationBuilder.DropColumn(
                name: "fillOpacity",
                table: "Shapes");

            migrationBuilder.DropColumn(
                name: "opacity",
                table: "Shapes");

            migrationBuilder.DropColumn(
                name: "radius",
                table: "Shapes");

            migrationBuilder.DropColumn(
                name: "stroke",
                table: "Shapes");

            migrationBuilder.DropColumn(
                name: "weight",
                table: "Shapes");

            migrationBuilder.DropColumn(
                name: "weightIsInFeet",
                table: "Shapes");

            migrationBuilder.RenameColumn(
                name: "geometryType",
                table: "Shapes",
                newName: "geoJSON");

            migrationBuilder.AlterColumn<bool>(
                name: "showPlantationVarieties",
                table: "Projects",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(bool),
                oldType: "tinyint(1)");

            migrationBuilder.AlterColumn<bool>(
                name: "showPlantationNames",
                table: "Projects",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(bool),
                oldType: "tinyint(1)");

            migrationBuilder.AlterColumn<bool>(
                name: "drawPlantations",
                table: "Projects",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(bool),
                oldType: "tinyint(1)");

            migrationBuilder.AlterColumn<bool>(
                name: "visible",
                table: "Plantations",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(bool),
                oldType: "tinyint(1)");
        }
    }
}
