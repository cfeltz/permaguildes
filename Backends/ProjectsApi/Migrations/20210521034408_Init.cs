﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PermaGuildes.Backends.ProjectsApi.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    code = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false),
                    latitude = table.Column<float>(type: "float", nullable: false),
                    longitude = table.Column<float>(type: "float", nullable: false),
                    zoom = table.Column<int>(type: "int", nullable: false),
                    description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", maxLength: 2048, nullable: true),
                    dtCreation = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    dtUpdate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.code);
                });

            migrationBuilder.CreateTable(
                name: "GroupsPlantations",
                columns: table => new
                {
                    code = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false),
                    codeProject = table.Column<int>(type: "int", nullable: false),
                    description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", maxLength: 2048, nullable: true),
                    dtCreation = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    dtUpdate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupsPlantations", x => x.code);
                    table.ForeignKey(
                        name: "FK_GroupsPlantations_Projects_codeProject",
                        column: x => x.codeProject,
                        principalTable: "Projects",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plantations",
                columns: table => new
                {
                    code = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    codeVegetable = table.Column<int>(type: "int", nullable: false),
                    codeGroup = table.Column<int>(type: "int", nullable: true),
                    name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false),
                    codeStratum = table.Column<int>(type: "int", nullable: false),
                    codeProject = table.Column<int>(type: "int", nullable: false),
                    latitude = table.Column<float>(type: "float", nullable: false),
                    longitude = table.Column<float>(type: "float", nullable: false),
                    height = table.Column<int>(type: "int", nullable: false),
                    span = table.Column<int>(type: "int", nullable: false),
                    iconSrc = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    comment = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", maxLength: 2048, nullable: true),
                    dtCreation = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    dtUpdate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plantations", x => x.code);
                    table.ForeignKey(
                        name: "FK_Plantations_GroupsPlantations_codeGroup",
                        column: x => x.codeGroup,
                        principalTable: "GroupsPlantations",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Plantations_Projects_codeProject",
                        column: x => x.codeProject,
                        principalTable: "Projects",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupsPlantations_codeProject",
                table: "GroupsPlantations",
                column: "codeProject");

            migrationBuilder.CreateIndex(
                name: "IX_Plantations_codeGroup",
                table: "Plantations",
                column: "codeGroup");

            migrationBuilder.CreateIndex(
                name: "IX_Plantations_codeProject",
                table: "Plantations",
                column: "codeProject");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Plantations");

            migrationBuilder.DropTable(
                name: "GroupsPlantations");

            migrationBuilder.DropTable(
                name: "Projects");
        }
    }
}
