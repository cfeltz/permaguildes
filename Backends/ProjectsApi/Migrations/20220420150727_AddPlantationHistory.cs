﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PermaGuildes.Backends.ProjectsApi.Migrations
{
    public partial class AddPlantationHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PlantationHistory",
                columns: table => new
                {
                    code = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    codePlantation = table.Column<int>(type: "int", nullable: false),
                    dtHistory = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    health = table.Column<int>(type: "int", nullable: false),
                    height = table.Column<int>(type: "int", nullable: true),
                    span = table.Column<int>(type: "int", nullable: true),
                    trunkCircumference = table.Column<int>(type: "int", nullable: true),
                    comment = table.Column<string>(type: "varchar(2048)", maxLength: 2048, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    dtCreation = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    dtUpdate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlantationHistory", x => x.code);
                    table.ForeignKey(
                        name: "FK_PlantationHistory_Plantations_codePlantation",
                        column: x => x.codePlantation,
                        principalTable: "Plantations",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_PlantationHistory_codePlantation",
                table: "PlantationHistory",
                column: "codePlantation");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlantationHistory");
        }
    }
}
