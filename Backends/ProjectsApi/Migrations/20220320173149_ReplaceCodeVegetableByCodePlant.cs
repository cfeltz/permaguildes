﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PermaGuildes.Backends.ProjectsApi.Migrations
{
    public partial class ReplaceCodeVegetableByCodePlant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "codeVegetable",
                table: "Plantations",
                newName: "codePlant");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "codePlant",
                table: "Plantations",
                newName: "codeVegetable");
        }
    }
}
