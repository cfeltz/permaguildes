﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PermaGuildes.Backends.ProjectsApi.Migrations
{
    public partial class AddLocationDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "locationDescription",
                table: "Plantations",
                type: "varchar(2048)",
                maxLength: 2048,
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "locationDescription",
                table: "Plantations");
        }
    }
}
