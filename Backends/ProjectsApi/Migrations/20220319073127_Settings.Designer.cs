﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PermaGuildes.Backends.ProjectsApi.Database;

#nullable disable

namespace PermaGuildes.Backends.ProjectsApi.Migrations
{
    [DbContext(typeof(ProjectsApiContext))]
    [Migration("20220319073127_Settings")]
    partial class Settings
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("PermaGuildes.Backends.ProjectsApi.Models.GroupPlantation", b =>
                {
                    b.Property<int>("code")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("codeProject")
                        .HasColumnType("int");

                    b.Property<string>("description")
                        .HasMaxLength(2048)
                        .HasColumnType("varchar(2048)");

                    b.Property<DateTime>("dtCreation")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime>("dtUpdate")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.HasKey("code");

                    b.HasIndex("codeProject");

                    b.ToTable("GroupsPlantations");
                });

            modelBuilder.Entity("PermaGuildes.Backends.ProjectsApi.Models.Plantation", b =>
                {
                    b.Property<int>("code")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int?>("codeGroup")
                        .HasColumnType("int");

                    b.Property<int>("codeProject")
                        .HasColumnType("int");

                    b.Property<int>("codeStratum")
                        .HasColumnType("int");

                    b.Property<int>("codeVegetable")
                        .HasColumnType("int");

                    b.Property<string>("comment")
                        .HasMaxLength(2048)
                        .HasColumnType("varchar(2048)");

                    b.Property<DateTime>("dtCreation")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime>("dtUpdate")
                        .HasColumnType("datetime(6)");

                    b.Property<int>("height")
                        .HasColumnType("int");

                    b.Property<string>("iconSrc")
                        .HasColumnType("longtext");

                    b.Property<float>("latitude")
                        .HasColumnType("float");

                    b.Property<float>("longitude")
                        .HasColumnType("float");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<int>("span")
                        .HasColumnType("int");

                    b.HasKey("code");

                    b.HasIndex("codeGroup");

                    b.HasIndex("codeProject");

                    b.ToTable("Plantations");
                });

            modelBuilder.Entity("PermaGuildes.Backends.ProjectsApi.Models.Project", b =>
                {
                    b.Property<int>("code")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("description")
                        .HasMaxLength(2048)
                        .HasColumnType("varchar(2048)");

                    b.Property<bool>("drawPlantations")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("tinyint(1)")
                        .HasDefaultValue(true);

                    b.Property<DateTime>("dtCreation")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime>("dtUpdate")
                        .HasColumnType("datetime(6)");

                    b.Property<float>("latitude")
                        .HasColumnType("float");

                    b.Property<float>("longitude")
                        .HasColumnType("float");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasColumnType("longtext");

                    b.Property<bool>("showPlantationNames")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("tinyint(1)")
                        .HasDefaultValue(true);

                    b.Property<bool>("showPlantationVarieties")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("tinyint(1)")
                        .HasDefaultValue(true);

                    b.Property<int>("zoom")
                        .HasColumnType("int");

                    b.HasKey("code");

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("PermaGuildes.Backends.ProjectsApi.Models.GroupPlantation", b =>
                {
                    b.HasOne("PermaGuildes.Backends.ProjectsApi.Models.Project", "project")
                        .WithMany("groups")
                        .HasForeignKey("codeProject")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("project");
                });

            modelBuilder.Entity("PermaGuildes.Backends.ProjectsApi.Models.Plantation", b =>
                {
                    b.HasOne("PermaGuildes.Backends.ProjectsApi.Models.GroupPlantation", "group")
                        .WithMany("plantations")
                        .HasForeignKey("codeGroup");

                    b.HasOne("PermaGuildes.Backends.ProjectsApi.Models.Project", "project")
                        .WithMany("plantations")
                        .HasForeignKey("codeProject")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("group");

                    b.Navigation("project");
                });

            modelBuilder.Entity("PermaGuildes.Backends.ProjectsApi.Models.GroupPlantation", b =>
                {
                    b.Navigation("plantations");
                });

            modelBuilder.Entity("PermaGuildes.Backends.ProjectsApi.Models.Project", b =>
                {
                    b.Navigation("groups");

                    b.Navigation("plantations");
                });
#pragma warning restore 612, 618
        }
    }
}
