using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.ProjectsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace PermaGuildes.Backends.ProjectsApi.ModelsDTO
{
    public class PlantationHistoryPhotoDTO : Entity, EntityWithKey, RelationWithOneReference
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);

        public int GetReferenceKey(Type type = null) => codeHistory;
        public string GetReferenceKeyAsString(Type type = null) => GetReferenceKey(type).ToString();


        public int code { get; set; }
        public int codeHistory { get; set; }
        public string filename { get; set; }
        public string url { get; set; }

        internal PlantationHistoryPhotoDTO() { }

        public PlantationHistoryPhotoDTO(int codeHistory)
        {
            this.codeHistory = codeHistory;
        }
    }
}