using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using PermaGuildes.Backends.ProjectsApi.Database;
using CFeltz.Shared.WebApiHelper;
using CFeltz.Shared.WebApiHelper.Infrastructure;
using System.Runtime.Serialization;
using System.ComponentModel;
using PermaGuildes.Backends.ProjectsApi.Models;

namespace PermaGuildes.Backends.ProjectsApi.ModelsDTO
{
    public class PlantationHistoryDTO : UpdateableEntity, EntityWithKey, RelationWithOneReference
    {
        public int GetKey() => code;
        public string GetKeyAsString() => GetKey().ToString();
        public bool KeyIsDefined() => (GetKey() != 0);

        public int GetReferenceKey(Type type = null) => codePlantation;
        public string GetReferenceKeyAsString(Type type = null) => GetReferenceKey(type).ToString();


        public int code { get; set; }
        public int codePlantation { get; set; }
        public DateTime dtHistory { get; set; }
        public PlantationHistory.eHealth health { get; set; }

        public int? height { get; set; }
        public int? span { get; set; }
        public int? trunkCircumference { get; set; }

        #nullable enable
        public string? comment { get; set; }
        #nullable restore

        #region Relations
        public ICollection<PlantationHistoryPhotoDTO> photos { get; set; }
        #endregion


        internal PlantationHistoryDTO() { }

        public PlantationHistoryDTO(int codePlantation, DateTime dtHistory, PlantationHistory.eHealth health)
        {
            this.codePlantation = codePlantation;
            this.dtHistory = dtHistory;
            this.health = health;
        }
    }
}