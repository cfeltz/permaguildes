﻿using System;
using AutoMapper;
using CFeltz.Backends.PlantsApi.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PermaGuildes.Backends.ProjectsApi.Models;
using PermaGuildes.Backends.ProjectsApi.ModelsDTO;

namespace PermaGuildes.Backends.ProjectsApi.Mappings
{
    public class MappingProfile : Profile
    {
        private string GetUrlFromFilename(PlantationHistoryPhoto photo) 
        {
            string url = null;
            if (photo != null)
            {
                url = "/api/download/plantations/history/{codeHistory}/{filename}";
                url = url.Replace("{codeHistory}", photo.codeHistory.ToString());
                url = url.Replace("{filename}", photo.filename);
            }
            return url;
        }

        public MappingProfile()
        {
            CreateMap<PlantationHistory, PlantationHistoryDTO>();
            CreateMap<PlantationHistoryDTO, PlantationHistory>();

            CreateMap<PlantationHistoryPhoto, PlantationHistoryPhotoDTO>()
                .ForMember(
                    dest => dest.url, 
                    opt => opt.MapFrom((src) => GetUrlFromFilename(src))
                );
            CreateMap<PlantationHistoryPhotoDTO, PlantationHistoryPhoto>();
        }
    }
}